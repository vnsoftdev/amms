# Changelog

### 3.3.14

  - Added WK2 ZOOpro title position
  - Updated WK2 ZOOpro default positions mapping
  - Fixed zlModel period date searching
  - Fixed WK2 ZOOpro ImagePro default source rendering
  - Fixed WK2 ZOOpro order fields editing issues
  - Removed WK2 ZOOpro link position default value

### 3.3.13

  - Fixed warning about ZOOtools during extension update
  - Fixed crashing file manager dialog if file has not correct name
  - Fixed issue with date format in DataTables

### 3.3.12

  - Added WK ZOOpro location field mapping
  - Merged ZOOtools elements
  - Improved WK ZOOpro compatibility with 3rd party elements
  - Fixed WK ZOOpro custom positions raw data rendering

### 3.3.11

  - Added PHP image resizing during upload
  - Added Widgetkit 2 ZOOpro mappings tags and media2 position
  - Added Widgetkit 2 Theme Widgets compatibility
  - Fixed PHP warning when checking Widgetkit 2 availability

### 3.3.10

  - Added default source dynamic path compatibility for FilesPro elements
  - Added Hour/Minute/Second FilesPro elements path variables
  - Improved item model performance
  - Fixed Widgetkit 2 ZOOpro mappings defaults
  - Fixed Widgetkit 2 integration issues
  - Fixed Plupload S3 SSL connection
  - Removed FilesPro double item:save event triggering
  - Removed dashboard temporary

### 3.3.9

  - Added Widgetkit ZOOpro new fields (author, date, categories)
  - Updated jQuery AlphaNumeric plugin

### 3.3.8

  - Added Widgetkit 2.3.0 compatibility
  - Fixed FilesPro elements Submission issues

### 3.3.7

  - Fixed extension versions comparison
  - Fixed 301 server response handle

### 3.3.6

  - Added max_input vars alert on ZOO App Manager
  - Fixed extensions manager issues
  - Improved Widgetkit 2 integration

### 3.3.5

  - Added Widgetkit 2.2 compatibility
  - Added dashboard view
  - Improved Extensions list workflow

### 3.3.4

  - Added UIkit version check
  - Fixed zlmodel search by range issue

### 3.3.3

  - Added Widgetkit plugin
  - Merged ZL Framework
  - Fixed some js deprecated functions

### 3.3.2

  - Updated ZLFW to 3.3.1

### 3.3.1

  - Improve error alerts when managing extensions
  - Fixed issue with server connection via CURL
  - Fixed displaying error
  - Fixed issue with inaccessible tmp folder

### 3.3.0

  - Update ZL Framework
  - Fixed Extensions Manager error reporting and offline mode

### 3.2.3

  - ZLFW 3.2.3 release

### 3.2.2

  - Fixed SSL issues when downloading
  - Server cofiguration diagnostic added
  - Manager Offline Mode added
  - ZLFW 3.2.2 release

### 3.2.1

  - Added tmp folder check
  - ZLFW 3.2.1 release

### 3.2.0

  - Fixed issue about credentials being cached
  - ZLFW 3.2 release

### 3.1.3

  - Style issues fixed
  - ZLFW 3.1.21 released

### 3.1.2

  - cURL SSL issues fixed
  - ZLFW 3.1.20 released

### 3.1.1

  - Fixed Download All extension feature
  - Joomla! 2.5 Admin Template style issues fixed

### 3.1.0

  - Initial Release
