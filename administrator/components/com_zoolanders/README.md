# ZOOlanders

## Build it up
The build task is managed by [Grunt](http://gruntjs.com/). There are several tasks you can run:

### Default
Will build and compress the package ready to be installed

```
grunt
```

### Dist
Will build and compress the package ready to be distributed in the build folder.

```
grunt dist
```
