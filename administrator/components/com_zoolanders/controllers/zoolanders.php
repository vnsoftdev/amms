<?php
/**
 * @package     ZOOlanders
 * @version     3.3.14
 * @author      ZOOlanders - http://zoolanders.com
 * @license     GNU General Public License v2 or later
 */

defined('_JEXEC') or die;

/**
 * The ZOOlanders Controller Class
 */
class ZoolandersController extends AppController
{
	public function __construct($default = array())
	{
		parent::__construct($default);

		// save the active comp ref
		$this->component = $this->app->component->active;

		$this->registerDefaultTask('extensions');
	}

	/**
	 * display task
	 *
	 * @return void
	 */
	function display($cachable = false, $urlparams = false)
	{
		// set default task if none
		$task = $this->app->request->getWord('task', 'dashboard');

		// display view
		$this->getView()->setLayout($task)->display();
	}

	/**
	 * Save form data to component params. Ajax request
	 *
	 * @return void
	 */
	public function saveOptions()
	{
		$success = false;
		$response = array('errors' => array(), 'notices' => array());

		// get form data
		$username = $this->app->request->get('username', 'string', '');
		$password = $this->app->request->get('password', 'string', '');

		if(empty($username) && empty($password)) {
			// encrypt password
			$password = $this->app->zlfw->crypt($password, 'encrypt');
			$this->app->zl->setConfig('zoolanders', compact('username', 'password'));
			$response['success'] = true;
			echo json_encode($response);
			return;
		}

		// check account credentials
		$url = $this->app->zl->dashboard->getZlServerURL() . '/index.php?option=com_zoo&controller=zooextensions&task=checkAuth&format=raw&zlv=' . $this->app->zl->dashboard->getZlVersion();
		$url .= '&username=' . urlencode($username) . '&password=' . urlencode($password);

		$zl_server_state = $this->app->zl->dashboard->getRemote($url);

		// set and return results
		if(!empty($zl_server_state)) {

			$result = json_decode($zl_server_state, true);
			if(empty($result['login_status'])) {
				$response['errors'][] = JText::_('PLG_ZLFRAMEWORK_ZL_LOGIN_FAILED');
			} else {
				// encrypt password
				$password = $this->app->zlfw->crypt($password, 'encrypt');
				// save data
				$this->app->zl->setConfig('zoolanders', compact('username', 'password'));
				$success = true;
			}

			// Refresh cache:
			$cache = $this->app->zl->dashboard->getCachedState();
			$cache->set('json', $zl_server_state)->save();

		} else {
			$response['errors'][] = JText::_('PLG_ZLFRAMEWORK_ZL_CONNECTION_ERROR');
		}

		// set and return results
		$response['success'] = $success;
		echo json_encode($response);
		return;
	}

	/**
	 * Dashboard display
	 *
	 * @return void
	 */
	public function dashboard()
	{
		// Check cache:
		$cache = $this->app->zl->dashboard->getCachedState();

		$json = (!empty($cache) && $cache->get('json')) ? @json_decode($cache->get('json')) : null;

		$this->data = empty($json) ? null : @$json->data;

		$offline = $this->app->session->get('zl_offline', false, 'zoolanders');

		// Refresh state right after display:
		$this->autoload = !$offline;

		$this->getView()->setLayout('dashboard')->display();
	}

	/**
	 * Extensions list display
	 *
	 * @return void
	 */
	public function extensions()
	{
		// Get cached list:
		$list = $this->app->zl->extensions->getCachedList();
		$this->autoload = false;
		$this->offline = $this->app->session->get('zl_offline', false, 'zoolanders');

		if(empty($list)) {
			// Anyway first load offline list:
			$list = $this->app->zl->extensions->getOfflineList();

			if(!$this->offline) {
				// Refresh list right after display:
				$this->autoload = true;
			}
		}

		$cache = $this->app->zl->dashboard->getCachedState();
		$json = $cache->get('json');

		if(!empty($cache)){
			$state = json_decode($json);
			$authorized = empty($state->login_status) ? false : $state->login_status;
		}else{
			$authorized = false;
		}

		$this->authorized = $authorized;
		$this->extensions = $this->app->zl->extensions->getExtensions(true, $list);

		$this->getView()->setLayout('extensions')->display();
	}

	/**
	 * Ping ZL server and get available notifications, informations, etc.
	 *
	 * @return void
	 */
	public function ping()
	{
		$success = false;
		$maintenance = false;

		$response = array('errors' => array(), 'notices' => array(), 'content' => '');

		$zl_server_state = $this->app->zl->dashboard->getRemoteState();

		// set and return results
		if(!empty($zl_server_state)) {

			$zl_server_state = json_decode($zl_server_state, true);

			$response['data'] = !empty($zl_server_state['data']) ? $zl_server_state['data'] : '';
			$response['notices'] = !empty($zl_server_state['notices']) ? $zl_server_state['notices'] : array();

			// Refresh cache:
			$cache = $this->app->zl->dashboard->getCachedState();
			$cache->set('json', json_encode($zl_server_state))->save();

			if($response['data']) {
				$maintenance = empty($response['data']['server_status']);
			}

			if(empty($zl_server_state['errors'])) {
				$success = true;
			} else {
				$success = false;
				$response['errors'] = $zl_server_state['errors'];
			}

		} else {
			$success = false;
			$response['errors'][] = JText::_('PLG_ZLFRAMEWORK_ZL_CONNECTION_ERROR');
		}

		if(!$success) {
			$response['notices'][] = JText::_('PLG_ZLFRAMEWORK_ZL_GOING_TO_OFFLINE_MODE');
		}

		$this->app->session->set('zl_offline', ((!$success) || $maintenance), 'zoolanders');

		// Summary
		$response['success'] = $success;

		echo json_encode($response);
		return;
	}

	/**
	 * Ping ZL server and get available notifications, informations, etc.
	 *
	 * @return void
	 */
	public function loadList()
	{
		$response = array('errors' => array(), 'notices' => array(), 'content' => '');
		$this->extensions = $this->app->zl->extensions->getExtensions(true);
		$errors = array_merge($this->app->zl->check->getMsg('error'), $this->app->zl->check->getMsg('hidden'));
		$notices = $this->app->zl->check->getMsg('warning');

		$success = !empty($this->extensions) && !count($errors);

		if(!$success) {

			if(!empty($errors)) {
				foreach ($errors as $error) {
					$response['errors'][] = $error;
				}
			}

			$response['errors'][] = JText::_('PLG_ZLFRAMEWORK_ZL_UNABLE_TO_LOAD_LIST');
			$response['notices'][] = JText::_('PLG_ZLFRAMEWORK_ZL_GOING_TO_OFFLINE_MODE');
		}

		if(!empty($notices)) {
			foreach ($notices as $notice) {
				$response['notices'][] = $notice;
			}
		}

		$this->app->session->set('zl_offline', !$success, 'zoolanders');

		$cache = $this->app->zl->dashboard->getCachedState();
		$json = $cache->get('json');

		if(!empty($cache)){
			$state = json_decode($json);
			$authorized = empty($state->login_status) ? false : $state->login_status;
		}else{
			$authorized = false;
		}

		// Summary
		$response['success'] = $success;
		$response['html'] = $this->getView()->partial('extensions_list', array('all_exts' => $this->extensions, 'authorized' => $authorized, 'offline' => !$success));

		echo json_encode($response);
		return;
	}

	/**
	 * Get languages list
	 */
	public function getLangList()
	{
		$languages = $this->app->zl->transifex->getLangList();

		// render layout
		$html = $this->app->zlfw->renderLayout($this->app->path->path('zl:views/zoolanders/tmpl/_languages.php'), array('languages' => $languages));
		$response['html'] = $html;

		$response['success'] = true;
		$response['languages'] = $languages;

		echo json_encode($response);
		return;
	}

	/**
	 * Get resource specific language list
	 *
	 * @return json
	 */
	public function getResourceLangList()
	{
		$title = $this->app->request->get('title', 'string', '');
		$resource = $this->app->zl->transifex->formatResName($title);
		$languages = $this->app->zl->transifex->getResourceLangList($resource);

		// render layout
		$html = $this->app->zlfw->renderLayout($this->app->path->path('zl:views/zoolanders/tmpl/_resource_lang.php'), array('languages' => $languages, 'title' => $title, 'resource' => $resource));
		$response['html'] = $html;

		$response['success'] = true;
		echo json_encode($response);
		return;
	}

	/**
	 * Download language pack installer
	 */
	public function downloadLangInstaller()
	{
		$resources = $this->app->request->get('resources', 'array', array());
		$language = $this->app->request->get('language', 'string', '');

		// if empty, get all resources
		if(empty($resources)) foreach ($this->app->zl->extensions->getExtensions() as $ext) {
			$resources[] = $this->app->zl->transifex->formatResName($ext->name);

			// make sure resources are well formated
		} else foreach ($resources as &$resource) {
			$resource = $this->app->zl->transifex->formatResName($resource);
		}

		// download
		$file = $this->app->zl->transifex->downloadLangInstaller($resources, $language);

		if(JFile::exists($file) && is_readable($file)) {
			$response['success'] = true;
			$response['filepath'] = $file;
		}

		echo json_encode($response);
		return;
	}

	/**
	 * Install language package
	 */
	public function installLangPack()
	{
		$resources = $this->app->request->get('resources', 'array', array());
		$language = $this->app->request->get('language', 'string', '');

		// if empty, get all resources
		if(empty($resources)) foreach ($this->app->zl->extensions->getExtensions() as $ext) {
			$resources[] = $this->app->zl->transifex->formatResName($ext->name);

			// make sure resources are well formated
		} else foreach ($resources as &$resource) {
			$resource = $this->app->zl->transifex->formatResName($resource);
		}

		// download
		$file = $this->app->zl->transifex->downloadLangInstaller($resources, $language);

		if(JFile::exists($file) && is_readable($file)) {
			$response = $this->app->zl->extensions->install($file);
		}

		echo json_encode($response);
		return;
	}

	/**
	 * Download and zip the extensions. Ajax request
	 *
	 * @return json
	 */
	public function downloadAndZip()
	{
		$extensions = $this->app->request->get('extensions', 'array', array());
		$response = $this->app->zl->extensions->downloadAndZip($extensions);

		echo json_encode($response);
		return;
	}

	/**
	 * Output a file to the browser. Ajax request
	 */
	public function outputFile()
	{
		$file = $this->app->request->get('file', 'string', array());

		// check file
		if(!is_readable($file) && !JFile::exists($file)) {
			$response['errors'][] = 'File does not exist or inaccessible';
			$response['success'] = false;

			echo json_encode($response);
			jexit();
		}

		$this->app->filesystem->output($file);
		JFile::delete($file);
	}

	/**
	 * Download and install extension. Ajax request
	 *
	 * @return json
	 */
	public function downloadAndInstall()
	{
		$extensions = $this->app->request->get('extensions', 'array', array());
		$response = $this->app->zl->extensions->downloadAndInstall($extensions);

		echo json_encode($response);
		return;
	}

	/**
	 * Uninstall extension. Ajax request
	 *
	 * @return json
	 */
	public function uninstall()
	{
		$name = $this->app->request->get('name', 'string', '');

		$response = $this->app->zl->extensions->uninstall($name);

		echo json_encode($response);
		return;
	}

	/**
	 * Toggle Extension state. Ajax request
	 *
	 * @return json
	 */
	public function toggleExtState()
	{
		// init vars
		$success = false;
		$response = array('errors' => array(), 'notices' => array());
		$name = $this->app->request->get('name', 'string', '');
		$db = JFactory::getDBO();

		// get extension
		$ext = $this->app->zl->extensions->getExtension($name);

		// if installed
		if($ext && $ext->installed) {
			$new_state = $ext->installed->enabled == 1 ? 0 : 1;
			$success = $db->setQuery("UPDATE `#__extensions` SET `enabled` = {$new_state} WHERE `extension_id` = '{$ext->installed->extension_id}'")->execute();
			$response['new_state'] = $new_state;
			$response['message'] = JText::sprintf($new_state ? 'PLG_ZLFRAMEWORK_EXT_ENABLED' : 'PLG_ZLFRAMEWORK_EXT_DISABLED', $ext->title);
		} else {
			$response['errors'][] = JText::_('PLG_ZLFRAMEWORK_EXT_NOT_INSTALLED');
		}

		// set and return results
		$response['success'] = $success;
		echo json_encode($response);
		return;
	}
}