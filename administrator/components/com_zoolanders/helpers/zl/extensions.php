<?php
/**
 * @package     ZOOlanders
 * @version     3.3.14
 * @author      ZOOlanders - http://zoolanders.com
 * @license     GNU General Public License v2 or later
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
require_once(dirname(__FILE__) . '/remote.php');

/**
 * Class zlHelperExtensions
 */
class zlHelperExtensions extends zlHelperRemote
{

	/**
	 * Extensions array
	 */
	protected $_extensions;

	/**
	 * Retrieve specific Extension
	 *
	 * @param string $name The extension name formated as "type_name"
	 * @param bool $force Avoid using the cached data
	 *
	 * @return object The extension
	 */
	public function getExtension($name, $force = false)
	{
		// populate the list
		$this->getExtensions($force);

		// clean name
		$name = strtolower(str_replace(array(' ', '-'), '', $name));

		// return the required extension
		if(isset($this->_extensions[$name])) {
			return $this->_extensions[$name];
		} else {
			return false;
		}
	}

	/**
	 * Retrieve all Extensions
	 *
	 * @param bool $force Avoid using the cached data
	 * @param mixed Use preloaded list for formatting ext list
	 *
	 * @return array The extensions
	 */
	public function getExtensions($force = false, $preloaded = null)
	{
		if(empty($this->_extensions) || $force || $preloaded) {

			// init vars
			$this->_extensions = array();
			$available = empty($preloaded) ? $this->getAvailable() : $preloaded;
			$installed = $this->getInstalled($preloaded);
			$deprecated = array('ZL Extensions Manager', 'Audio', 'ZOOtienda');
			$hidden = array('zlframework');
			$offline = $this->app->session->get('zl_offline', false, 'zoolanders');

			if(empty($available) || !empty($available->errors)) {
				$available = $this->getOfflineList();
				$this->app->zl->check->addMsg('PLG_ZLFRAMEWORK_EXTENSIONS_OFFLINE_MODE', 'hidden');
				$offline = true;
			}

			// iterate
			foreach ($available as $key => &$ext) {
				// title fix for check purpose
				if($ext->title == 'ZL Extensions Manager') {
					$title = 'zlmanager';
				} else {
					$title = $ext->title;
				}

				// get name
				$ext->name = strtolower(str_replace(' ', '', $title));

				// get install url
				$url = (array)$ext->url;
				$ext->url = array_shift($url);

				// check if installed
				if(array_key_exists($ext->name, $installed)) {
					$ext->installed = $installed[$ext->name];
				} else {
					$ext->installed = false;
				}

				// shortcut versions
				$ext->version = str_replace(' ', '', preg_replace('/[b|B][e|E][t|T][a|A]/', 'B', $ext->version));
				if($ext->installed) {
					$ext->installed->version = str_replace(' ', '', preg_replace('/[b|B][e|E][t|T][a|A]/', 'B', $ext->installed->version));
				}

				// hidden extensions
				if(in_array($ext->name, $hidden)) {
					$ext->hidden = true;
				}

				// deprecated extensions
				if(in_array($ext->title, $deprecated)) {
					$ext->deprecated = true;
				}

				// set state, if installed
				if($ext->installed) {
					// outdated
					if(version_compare($ext->version, $ext->installed->version, '>') && !$offline) {

						// any version lower than 3.0 is considered deprecated
						if(version_compare($ext->installed->version, '3.0') < 0) {
							$ext->state = 'deprecated';
						} else {
							$ext->state = 'outdated';
						}

						// updated, no action required
					} else {
						$ext->state = 'uptodate';
					}

					// if uninstalled
				} else {
					$ext->state = 'uninstalled';
				}

				// set link
				$ext->link = $this->getZlServerURL() . $ext->link;

				// add to list
				$this->_extensions[$ext->name] = $ext;
			}
		}

		return $this->_extensions;
	}

	/**
	 * Retrieve the available extensions from zl.com
	 *
	 * @return array List of extensions
	 */
	public function getAvailable()
	{
		$key = md5($this->_getCredentialsParams());
		$cache = $this->_getCache($key);

		$url = $this->getZlServerURL() . '/index.php?option=com_zoo&controller=zooextensions&task=getExtList&format=raw&zlv=' . $this->getZlVersion();
		$url .= $this->_getCredentialsParams();

		$file = $this->app->path->path('tmp:') . '/zlextensions.json';

		if(!$this->app->zlfw->path->checkPathAccessible($this->app->path->path('tmp:'))) {
			// try to use default path:
			$file = $this->app->path->path('root:tmp') . '/zlextensions.json';
		}

		$this->download($url, $file);

		$json = JFile::read($file);

		JFile::delete($file);

		if($cache && !empty($json)) {
			$cache->set('json', $json)->save();
		}

		// check if result returned
		if(empty($json) || json_decode($json) === null) {
			// Download attempt failed, let's work offline:
			return array();
		} else {
			$result = json_decode($json);
			if(!empty($result->errors)) {
				return array();
			}
		}

		// decode result
		$extensions = json_decode($json);

		// remove spaces
		foreach ($extensions as $key => &$ext) {
			foreach ($ext as &$data) {
				$data = trim($data);
			}

			// filter out ext that have not necessary data
			if(!strlen($ext->type)) {
				unset($extensions[$key]);
				continue;
			}

			// lowercase the type
			$ext->type = strtolower($ext->type);
		}

		return $extensions;
	}

	/**
	 * Get cached extensions list (if exists)
	 *
	 * @return mixed
	 */
	public function getCachedList()
	{
		// retrieve caching
		$extensions = null;
		$key = md5($this->_getCredentialsParams());
		$cache = $this->_getCache($key);

		if($cache && $json = $cache->get('json')) {
			$extensions = json_decode($json);
		}

		return $extensions;
	}

	/**
	 * Get Offline mode reserved list
	 *
	 * @param   bool If true - result will be returned as JSON string
	 *
	 * @return mixed
	 */
	public function getOfflineList($json = false)
	{

		$file = $this->app->path->path('zl:resources') . '/zlextensions.json';
		$content = JFile::read($file);

		return $json ? $content : json_decode($content);
	}

	/**
	 * Retrieve the installed extensions
	 *
	 * @return array Array of objects
	 */
	public function getInstalled($preloaded = array())
	{
		// init vars
		$installed = array();
		$db = JFactory::getDBO();
		$ext_list = array();

		// get available ext list
		$available = empty($preloaded) ? $this->getAvailable() : $preloaded;
		// check if available retrieved
		if(empty($available) || !empty($available->errors)) {
			$available = $this->getOfflineList();
		}

		foreach ($available as &$ext) {
			// quick naming fix
			if(trim($ext->title) == 'ZL Extensions Manager') {
				$ext->title = 'zlmanager';
			}

			$ext_list[] = $db->Quote(trim($ext->title));
		}

		/* PLUGINS/MODULES */
		$query = "SELECT * FROM `#__extensions`"
			. " WHERE `name` IN (" . implode(',', $ext_list) . ")"
			. " AND (type = 'plugin' OR type = 'module' OR type = 'component')";

		// update objects with manifest data
		foreach ($db->setQuery($query)->loadObjectList('name') as $name => $item) {
			if(strlen($item->manifest_cache)) {
				$data = json_decode($item->manifest_cache);
				if($data) foreach ($data as $key => $value) {

					// ignore the type field
					if($key == 'type') continue;

					$item->$key = trim($value);
				}
			}

			$name = strtolower(str_replace(' ', '', $name));

			// save item
			$installed[$name] = $item;
			$installed[$name]->type = 'extension';
		}

		/* ELEMENTS */
		foreach ($this->app->path->dirs('plugins:system/zoo_zlelements/zoo_zlelements/elements') as $type) {
			if(is_file($this->app->path->path("elements:$type/$type.php"))) {
				if($element = $this->app->element->create($type)) {
					if($element->getMetaData('hidden') != 'true') {
						$name = strtolower(str_replace(' ', '', $element->getMetaData('name')));
						$installed[$name] = $element->getMetaData();
						$installed[$name]->type = 'element';
					}
				}
			}
		}

		// Texts workaround
		if(isset($installed['textpro']) && isset($installed['textareapro'])) {
			$installed['texts'] = $installed['textareapro'];
			$installed['texts']->title = 'Texts';
		}
		unset($installed['textpro']);
		unset($installed['textareapro']);

		/* APPS */
		foreach ($this->app->zoo->getApplicationGroups() as $app) {
			$author = strtolower($app->getMetaData('author'));
			if($author == 'zoolanders' || $author == 'joolanders') {
				$name = strtolower(str_replace(' ', '', $app->getMetaData('name')));
				$installed[$name] = $app->getMetaData();
				$installed[$name]->type = 'app';
			}
		}

		return $installed;
	}

	/**
	 * Uninstall an extension
	 *
	 * @param string|array $name The extension/s name formated as "type_name"
	 *
	 * @return mixed Result string on success, false on failure
	 */
	public function uninstall($name)
	{
		// init vars
		$ext = $this->getExtension($name);
		$success = false;
		$response = array('errors' => array(), 'notices' => array());

		// extension check
		if($ext && $ext->installed) {
			if($ext->type == 'extension') {
				require_once JPATH_ADMINISTRATOR . '/components/com_installer/models/manage.php';
				$installer = JModelLegacy::getInstance('Manage', 'InstallerModel');

				if($installer->remove((array)$ext->installed->extension_id)) {
					$success = true;
				}

				// force true for ZL Manager due to a bug on its uninstall process
				if($ext->title == 'ZL Extensions Manager') {
					$success = true;
				}

			} else

				if($ext->type == 'element') {
					// init vars
					$paths = array();
					$element = strtolower(str_replace(' ', '', $ext->title));
					$els_path = $this->app->path->path('root:plugins/system/zoo_zlelements/zoo_zlelements/elements');

					// workaround for Texts elements
					if($ext->title == 'Texts') {
						$paths[] = $els_path . '/textareapro';
						$paths[] = $els_path . '/textpro';

					} else {
						$paths[] = $els_path . '/' . $element;
					}

					foreach ($paths as $path) {
						// delete the element specific folder
						if(JFolder::exists($path) && JFolder::exists($path) && JFolder::delete($path)) {
							$success = true;
						} else {
							$response['errors'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_INS_ERR_ELEMENT_FOLDER', $ext->title);
						}
					}
				}

			if($ext->type == 'app') {
				$app_group = strtolower($ext->title);

				// check if there are App instances created, if so, abort
				$db = JFactory::getDBO();
				$query = "SELECT * FROM `#__zoo_application`"
					. " WHERE `application_group` = " . $db->quote($app_group);
				$result = $db->setQuery($query)->loadObjectList();

				if(!empty($result)) {
					$response['notices'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_REMANING_APPS', $ext->title);
				} else {

					// create application object
					$application = $this->app->object->create('Application');
					$application->setGroup($app_group);

					try {
						// uninstall app
						$this->app->install->uninstallApplication($application);
						$success = true;

					} catch (InstallHelperException $e) {

						// raise notice on exception
						$response['errors'][] = JText::sprintf('Error uninstalling application group (%s).', $e);
					}
				}
			}

			// if succesfully uninstalled
			if($success) {
				$response['ext'] = array('title' => $ext->title, 'version' => (string)$ext->version);

				// if deprecated extension, send on response
				if(isset($ext->deprecated)) $response['ext']['deprecated'] = true;

				// set message
				$response['message'] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_UNINSTALLED', $ext->title);

			} else {

				// get error messages
				foreach (JFactory::getApplication()->getMessageQueue() as $msg) {
					$response['errors'][] = $msg['message'];
				}
			}

		} else {
			$response['errors'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_NOT_RECOGNIZED', $name);
		}

		$response['success'] = $success;
		return $response;
	}

	/**
	 * Download extension to desktop
	 *
	 * @return void
	 */
	public function downloadAndZip($extensions)
	{
		// init vars
		$success = false;
		$response = array('errors' => array(), 'notices' => array());
		$files = array();

		// check tmp
		if($warning = $this->app->zlfw->path->checkSystemPaths()) {
			$response['success'] = false;
			$response['errors'][] = $warning;
			return $response;
		}

		// make sure is array
		settype($extensions, 'array');

		// install
		foreach ($extensions as $key => &$ext) {
			$ext = $this->getExtension($ext['name']);

			// check if ext recognized
			if(!$ext) {
				$response['errors'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_NOT_RECOGNIZED', $name);
				$response['success'] = $success;
				return $response;
			}

			// download
			if(strlen($ext->url)) {
				// set url
				$url = $this->getZlServerURL() . $ext->url . $this->_getCredentialsParams();

				// override url params to get download trough zooextension plugin
				$url .= '&task=download&controller=zooextensions&zlv=' . $this->getZlVersion();

				// set path
				$file = JPath::clean($this->app->path->path('tmp:') . '/' . str_replace(' ', '_', $ext->title) . '_v' . str_replace(' ', '_', $ext->version) . '.zip');

				// download
				$success = $this->download($url, $file);

				// check the downloaded file
				if(!$success || filesize($file) < 0) {
					$response['errors'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_DOWNLOAD_FAILED', $ext->title);
					$response['success'] = $success;
					return $response;
				}

				$files[] = $file;
			}
		}

		// save file path reference
		if(count($files) > 1) {

			// zip the downloaded files
			$filepath = $this->app->path->path('tmp:') . '/ZOOlanders_' . date('m.d.Y') . '.zip';
			$zip = $this->app->archive->open($filepath, 'zip');
			$zip->create($files, PCLZIP_OPT_REMOVE_ALL_PATH);

			if(is_readable($filepath) && JFile::exists($filepath)) {

				// save file path reference
				$response['filepath'] = $filepath;

				// delete temporal files
				foreach ($files as $file) {
					if(JFile::exists($file)) {
						// JFile::delete($file);
					}
				}
			}

		} else {
			$response['filepath'] = array_shift($files);
		}

		$response['success'] = $success;
		return $response;
	}

	/**
	 * Download and install an Add-on
	 *
	 * @return void
	 */
	public function downloadAndInstall($extensions)
	{
		// init vars
		$response = array('errors' => array(), 'notices' => array());
		$failed_response = array();

		// check tmp
		if($warning = $this->app->zlfw->path->checkSystemPaths()) {
			$response['success'] = false;
			$response['errors'][] = $warning;
			return $response;
		}

		// make sure is array
		settype($extensions, 'array');

		// install
		foreach ($extensions as $key => &$ext) {
			$ext['response'] = $this->_downloadAndInstall($ext['name']);

			if(!$ext['response']['success']) {
				$failed_response = array_merge_recursive($failed_response, $ext['response']);
			}
		}

		// if multiple
		if(count($extensions) > 1) {

			if(!count($failed_response)) {
				$response = array('success' => true, 'message' => JText::_('PLG_ZLFRAMEWORK_EXT_ALL_INSTALLED'));

			} else {
				$response = $failed_response;
				$response['success'] = false;
			}

			// if singular
		} else {

			$ext = array_shift($extensions);
			if($ext['response']['success']) {
				$installed = $ext['response']['ext'];
				$ext['response']['message'] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_INSTALLED', $installed['title'], $installed['version']);
			}

			// set response
			$response = $ext['response'];
		}

		return $response;
	}

	protected function _downloadAndInstall($extension)
	{
		// init vars
		$ext = is_string($extension) ? $this->getExtension($extension) : $extension;
		$success = false;
		$response = array('errors' => array(), 'notices' => array());

		// check if ext recognized
		if(!$ext) {
			$response['errors'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_NOT_RECOGNIZED', $name);
			$response['success'] = $success;
			return $response;
		}

		// download
		if(strlen($ext->url)) {
			// set url
			$url = $this->getZlServerURL() . $ext->url . $this->_getCredentialsParams();

			// override url params to get download trough zooextension plugin
			$url .= '&task=download&controller=zooextensions&zlv=' . $this->getZlVersion();

			// set path
			$file = JPath::clean($this->app->path->path('tmp:') . '/zl_' . $ext->name . '.zip');

			// download
			$success = $this->download($url, $file);
		}

		// check the downloaded file
		if(!$success || filesize($file) < 0) {
			$response['errors'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_DOWNLOAD_FAILED', $ext->title);
			$response['success'] = $success;
			return $response;
		}

		// install
		jimport('joomla.installer.installer');

		// load lang files
		$lang = JFactory::getLanguage();
		$lang->load(strtolower('com_installer'), JPATH_ADMINISTRATOR, null, false, false) || $lang->load(strtolower('com_installer'), JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);

		jimport('joomla.installer.helper');
		jimport('joomla.filesystem.archive');

		// install
		$install = $this->install($file);
		$success = $success && $install['success'];
		$response = array_merge_recursive($response, $install);

		// check installation
		if($success) {

			// force ext retrievement to get it's new status
			$ext = $this->getExtension($ext->name, true);

			if($ext && $ext->installed) {
				// set ext info
				$response['ext'] = array(
					'title' => $ext->title,
					'version' => (string)$ext->installed->version,
					'enabled' => isset($ext->installed->enabled) ? $ext->installed->enabled : true,
					'type' => $ext->installed->type
				);

				if(!$response['ext']['enabled']) {
					$response['notices'][] = JText::sprintf('PLG_ZLFRAMEWORK_EXT_INS_WRN_DISABLED', $ext->title);
				}

			} else {
				$success = false;
				$response['errors'][] = JText::_('PLG_ZLFRAMEWORK_EXT_INSTALL_FAILED');
			}
		}


		// set and return results
		$response['success'] = $success;
		return $response;
	}

	/**
	 * Downloads from a URL and saves the result as a local file
	 *
	 * @param string $url
	 * @param path $target
	 *
	 * @return bool True on success
	 */
	public function download($url, $target)
	{
		// Import Joomla! libraries
		jimport('joomla.filesystem.file');

		/** @var bool Did we try to force permissions? */
		$hackPermissions = false;

		// Make sure the target does not exist
		if(JFile::exists($target)) {
			if(!@unlink($target)) {
				JFile::delete($target);
			}
		}

		// Try to open the output file for writing
		$fp = @fopen($target, 'wb');
		if($fp === false) {
			// The file can not be opened for writing. Let's try a hack.
			$empty = '';
			if(JFile::write($target, $empty)) {
				if($this->_chmod($target, 511)) {
					$fp = @fopen($target, 'wb');
					$hackPermissions = true;
				}
			}
		}

		$result = false;
		if($fp !== false) {
			// First try to download directly to file if $fp !== false
			$adapters = $this->_getAdapters();
			$result = false;
			while (!empty($adapters) && (empty($result) || ($result === false))) {
				// Run the current download method
				$method = '_get' . strtoupper(array_shift($adapters));
				$result = $this->$method($url, $fp);

				// Check if we have a download
				if(!empty($result)) {
					// The download is complete, close the file pointer
					@fclose($fp);
					// If the filesize is not at least 1 byte, we consider it failed.
					clearstatcache();
					$filesize = @filesize($target);
					if($filesize <= 0) {
						$result = false;
						$fp = @fopen($target, 'wb');
					}
				}
			}

			// If we have no download, close the file pointer
			if(empty($result)) {
				@fclose($fp);
			}
		}

		if(empty($result)) {
			// Delete the target file if it exists
			if(file_exists($target)) {
				if(!@unlink($target)) {
					JFile::delete($target);
				}
			}
			// Download and write using JFile::write();
			$result = $this->downloadAndReturn($url);

			if(!empty($result))
			{
				$download = empty($result['data']) ? (empty($result->body) ? null : $result->body): $result['data'];
				$result = JFile::write($target, $download);
			}
		}

		return $result;
	}

	/**
	 * Downloads from a URL and returns the result as a string
	 *
	 * @param string $url
	 *
	 * @return mixed Result string on success, false on failure
	 */
	public function downloadAndReturn($url)
	{
		$adapters = $this->_getAdapters();
		$result = false;

		while (!empty($adapters) && ($result === false)) {
			// Run the current download method
			$method = '_get' . strtoupper(array_shift($adapters));
			$result = $this->$method($url, null);
		}

		return $result;
	}

	/**
	 * Installs the downloaded package
	 *
	 * @param string $file Path to the download package
	 *
	 * @return mixed Result string on success, false on failure
	 */
	public function install($file)
	{
		// init vars
		$success = false;
		$response = array('errors' => array(), 'notices' => array());

		//check tmp
		if($warning = $this->app->zlfw->path->checkSystemPaths()) {
			$response['success'] = false;
			$response['errors'][] = $warning;
			return $response;
		}

		// Unpack the package
		$dir = $this->app->path->path('tmp:') . '/' . uniqid('zoolanders_');
		$success = JArchive::extract($file, $dir);

		if(!$success) {
			$response['success'] = false;
			$response['errors'][] = 'File extraction failed';
			return $response;
		}

		// Get an installer instance
		$installer = JInstaller::getInstance();

		// get manifest
		$xmls = JFolder::files($dir, '.xml$', 1, true);
		if(count($xmls)) {
			// It's an app maybe?
			$app_xml = $dir . '/application.xml';
			if(JFile::exists($app_xml)) {
				// Get group for folder name
				$xml = simplexml_load_file($app_xml);
				if(!$xml || $xml->getName() != 'application') {
					$success = false;
				} else {
					$group = (string)$xml->group;
					$dest = JPATH_SITE . '/media/zoo/applications/' . $group . '/';
					$dir = rtrim($dir, "\\/") . '/';

					// If this is an update - do not overwrite config/positions/metadata
					if(JFolder::exists($dest)) {

						// Get the ZOO App instance
						$zoo = App::getInstance('zoo');

						$files = $zoo->zoo->app->filesystem->readDirectoryFiles($dir . 'types/', '', '/\.config$/', false);
						foreach ($files as $file) {
							if(JFile::exists($dest . 'types/' . $file)) {
								JFile::delete($dir . 'types/' . $file);
							}
						}

						$files = $zoo->zoo->app->filesystem->readDirectoryFiles($dir, '', '/(positions\.(config|xml)|metadata\.xml)$/', true);
						foreach ($files as $file) {
							if(JFile::exists($dest . $file)) {
								JFile::delete($dir . $file);
							}
						}
					}

					JFolder::copy($dir, $dest, '', true);
					$success = true;
				}
			} else {
				// Install the package
				if(!$installer->install($dir)) {
					foreach (JFactory::getApplication()->getMessageQueue() as $msg) {
						$response['errors'][] = $msg['message'];
					}

					$success = false;
				} else {
					$success = true;
				}
			}

			// No xml, probably pack of packages
		} else {

			$zips = JFolder::files($dir, '.zip', 1, true);
			if(count($zips)) {
				foreach ($zips as $zip) {
					$install = $this->install($zip);
					$success = $install['success'];

					// if some install fail, abort and report
					if(!$success) {
						foreach (JFactory::getApplication()->getMessageQueue() as $msg) {
							$response['errors'][] = $msg['message'];
						}
						break;
					}
				}
			}
		}

		// cleanup temporal files
		JInstallerHelper::cleanupInstall($file, $dir);

		// set and return results
		$response['success'] = $success;
		return $response;
	}

	/**
	 * Change the permissions of a file, optionally using FTP
	 *
	 * @param string $file Absolute path to file
	 * @param int $mode Permissions, e.g. 0755
	 */
	private function _chmod($path, $mode)
	{
		if(is_string($mode)) {
			$mode = octdec($mode);
			if(($mode < 0600) || ($mode > 0777)) $mode = 0755;
		}

		// Initialize variables
		jimport('joomla.client.helper');
		$ftpOptions = JClientHelper::getCredentials('ftp');

		// Check to make sure the path valid and clean
		$path = JPath::clean($path);

		if($ftpOptions['enabled'] == 1) {
			// Connect the FTP client
			JLoader::import('joomla.client.ftp');
			if(version_compare(JVERSION, '3.0', 'ge')) {
				$ftp = JClientFTP::getInstance(
					$ftpOptions['host'], $ftpOptions['port'], array(), $ftpOptions['user'], $ftpOptions['pass']
				);
			} else {
				if(version_compare(JVERSION, '3.0', 'ge')) {
					$ftp = JClientFTP::getInstance(
						$ftpOptions['host'], $ftpOptions['port'], array(), $ftpOptions['user'], $ftpOptions['pass']
					);
				} else {
					$ftp = JFTP::getInstance(
						$ftpOptions['host'], $ftpOptions['port'], array(), $ftpOptions['user'], $ftpOptions['pass']
					);
				}
			}
		}

		if(@chmod($path, $mode)) {
			$ret = true;
		} elseif($ftpOptions['enabled'] == 1) {
			// Translate path and delete
			JLoader::import('joomla.client.ftp');
			$path = JPath::clean(str_replace(JPATH_ROOT, $ftpOptions['root'], $path), '/');
			// FTP connector throws an error
			$ret = $ftp->chmod($path, $mode);
		} else {
			return false;
		}
	}

}