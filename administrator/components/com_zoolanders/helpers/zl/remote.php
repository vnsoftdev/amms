<?php
/**
 * @package     ZOOlanders
 * @version     3.3.14
 * @author      ZOOlanders - http://zoolanders.com
 * @license     GNU General Public License v2 or later
 */

defined('_JEXEC') or die;
jimport('joomla.filesystem.file');

/**
 * Class zlHelperExtensions
 */
class zlHelperRemote extends AppHelper
{

	/**
	 * Cache object
	 */
	protected $_cache;

	/**
	 * ZL Server URL
	 */
	const ZL_SERVER = 'https://www.zoolanders.com';

	/**
	 * ZL version
	 */
	const ZL_VERSION = '3.3.5';

	/**
	 * Detect and return available download methods
	 *
	 * @return  array
	 */
	protected function _getAdapters()
	{
		// Detect available adapters
		$adapters = array();
		if($this->_hasFOPEN())
			$adapters[] = 'fopen';
		if($this->_hasCURL())
			$adapters[] = 'curl';

		return $adapters;
	}

	/**
	 * Does the server support PHP's cURL extension?
	 *
	 * @return bool True if it is supported
	 */
	private function _hasCURL()
	{
		static $result = null;

		if(is_null($result)) {
			$result = $this->app->zl->check->checkCurl();
		}

		return $result;
	}

	/**
	 * Get Ready for response error
	 *
	 * @param $response
	 *
	 * @return string
	 */
	private function _setCurlError($response)
	{
		$result = array();

		$curl_error = empty($response->error) ? $response->code : $response->error;
		$this->app->zl->check->addMsg($curl_error);
		$result['errors'][] = JText::_('PLG_ZLFRAMEWORK_ZL_CONNECTION_ERROR') . ': ' . $curl_error;
		$result = json_encode($result);

		return $result;
	}

	/**
	 * Downloads the contents of a URL and writes them to disk (if $fp is not null)
	 * or returns them as a string (if $fp is null)
	 *
	 * @param string $url The URL to download from
	 * @param resource $fp The file pointer to download to. Omit to return the contents.
	 *
	 * @return bool|string False on failure, true on success ($fp not null) or the URL contents (if $fp is null)
	 */
	protected function &_getCURL($url, $fp = null, $nofollow = true)
	{
		$result = false;
		$curl_settings = new JRegistry;
		$uri = new JUri($url);

		$curl_options = array(
			CURLOPT_FOLLOWLOCATION => 1,
			CURLOPT_AUTOREFERER => true,
			CURLOPT_FAILONERROR => true,
			CURLOPT_HEADER => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => 1,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_SSLVERSION => 'ALL',
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_TIMEOUT => 20
		);

		if($nofollow) {

			$curl_options[CURLOPT_MAXREDIRS] = 20;
			$curl_options[CURLOPT_HEADER] = false;
			$curl_options[CURLOPT_USERAGENT] = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)';

			if(is_resource($fp)) {
				$curl_options[CURLOPT_FILE] = $fp;
			}
		} else {

			// Init
			$newURL = $url;

			$curl_settings->loadArray($curl_options);
			$transport = new JHttpTransportCurl($curl_settings);
			$data = $transport->request('GET', $uri);

			if(empty($data)) {
				return $this->_setCurlError($data);
			}

			// Search the new location in the headers
			if ($data->headers && ($data->code > 299) && ($data->code < 400) && isset($data->headers['Location'])) {
				$newURL = $data->headers['Location'];
			}

			// Download from the new URL
			if($url != $newURL) {
				return $this->_getCURL($newURL, $fp);
			} else {
				return $this->_getCURL($newURL, $fp, true);
			}
		}

		$curl_settings->loadArray($curl_options);
		$transport = new JHttpTransportCurl($curl_settings);
		$curl_response = $transport->request('GET', $uri);

		if(!empty($curl_response) && $curl_response->code == 200) {
			if(!empty($curl_response->body)) {
				$result = $curl_response->body;
			} else {
				$result = $this->_setCurlError($curl_response);
			}
		} else {

            if(($curl_response->code > 299) && ($curl_response->code < 400)){
                return $this->_getCURL($url, $fp, false);
            }

			$curl_error = empty($curl_response->error) ? $curl_response->code : $curl_response->error;
			$this->app->zl->check->addMsg($curl_error);
			$result['errors'][] = JText::_('PLG_ZLFRAMEWORK_ZL_CONNECTION_ERROR') . ': ' . $curl_error;
			$result = $this->_setCurlError($curl_response);
		}

		return $result;
	}

	/**
	 * Send CURL request to remote server
	 *
	 * @param $url
	 *
	 * @return mixed
	 */
	public function getRemote($url)
	{
		return $this->_getCURL($url, null, true);
	}

	/**
	 * Does the server support URL fopen() wrappers?
	 *
	 * @return  boolean
	 */
	private function _hasFOPEN()
	{
		static $result = null;

		if(is_null($result)) {
			$result = $this->app->zl->check->checkWrappers();
		}
		return $result;
	}

	/**
	 * Downloads the contents of a URL and writes them to disk (if $fp is not null)
	 * or returns them as a string (if $fp is null) using fopen() URL wrappers
	 *
	 * @param   string $url The URL to download from
	 * @param   resource $fp The file pointer to download to. Omit to return the contents.
	 *
	 * @return  boolean|string  False on failure, true on success ($fp not null) or the URL contents (if $fp is null)
	 */
	protected function &_getFOPEN($url, $fp = null)
	{
		$result = false;

		// Track errors
		if(function_exists('ini_set')) {
			$track_errors = ini_set('track_errors', true);
		}

		// Open the URL for reading
		if(function_exists('stream_context_create')) {
			// PHP 5+ way (best)
			$httpopts = array(
				'user_agent' => 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)',
				'timeout' => 10.0,
			);
			$context = stream_context_create(array('http' => $httpopts));
			$ih = @fopen($url, 'r', false, $context);
		} else {
			// PHP 4 way (actually, it's just a fallback as we can't run this code in PHP4)
			if(function_exists('ini_set')) {
				ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)');
			}
			$ih = @fopen($url, 'r');
		}

		// If fopen() fails, abort
		if(!is_resource($ih)) {
			return $result;
		}

		// Try to download
		$bytes = 0;
		$result = true;
		$return = '';
		while (!feof($ih) && $result) {
			$contents = fread($ih, 4096);
			if($contents === false) {
				@fclose($ih);
				$result = false;
				return $result;
			} else {
				$bytes += strlen($contents);
				if(is_resource($fp)) {
					$result = @fwrite($fp, $contents);
				} else {
					$return .= $contents;
					unset($contents);
				}
			}
		}

		@fclose($ih);

		if(is_resource($fp)) {
			return $result;
		} elseif($result === true) {
			return $return;
		} else {
			return $result;
		}

		return $result;
	}

	/**
	 * Get ZL server url
	 *
	 * @return string
	 */
	public function getZlServerURL()
	{

		return self::ZL_SERVER;
	}

	/**
	 * Get ZL version
	 *
	 * @return string
	 */
	public function getZlVersion()
	{

		return self::ZL_VERSION;
	}

	/**
	 * Get the cache object
	 *
	 * @param string $key
	 * @return object Cache object
	 */
	protected function _getCache($key, $cache_time = 3600)
	{
		if(empty($this->_cache)) {
			$this->_cache = $this->app->cache->create($this->app->path->path('cache:') . '/com_zoolanders_' . $key, true, $cache_time, 'apc');
			if(!$this->_cache || !$this->_cache->check()) {
				$this->_cache = null;
			}
		}

		return $this->_cache;
	}

	/**
	 * Get credentials params
	 *
	 * @return string The url encoded credentials
	 */
	protected function _getCredentialsParams()
	{
		// get credentials
		$data = $this->app->zl->getConfig('zoolanders');
		$user = $data->get('username', '');
		$pass = $data->get('password', false);
		$pass = $pass ? $this->app->zlfw->crypt($pass, 'decrypt') : '';

		// return encoded
		return '&username=' . urlencode($user) . '&password=' . urlencode($pass);
	}
}