<?php
/**
 * @package     ZOOlanders
 * @version     3.3.14
 * @author      ZOOlanders - http://zoolanders.com
 * @license     GNU General Public License v2 or later
 */

defined('_JEXEC') or die;

// load assets
$this->app->document->addScript('media:com_zoolanders/js/admin-overlay.js');
$this->app->document->addScript('media:com_zoolanders/js/admin-extensions.js');

// add page title
JToolbarHelper::title('ZOOlanders::' . JText::_('Extensions'), '48-zoolanders');

// set toolbar buttons
JToolBar::getInstance('toolbar')->appendButton('Custom', '<div class="zx"><button type="button" class="uk-button uk-button-mini"><i class="uk-icon-gear"></i> '.JText::_('PLG_ZLFRAMEWORK_OPTIONS').'</button></div>', 'options');
JToolBar::getInstance('toolbar')->appendButton('Custom', '<div class="zx"><button type="button" class="uk-button uk-button-mini"><i class="uk-icon-download"></i> '.JText::_('PLG_ZLFRAMEWORK_EXT_DOWNLOAD_ALL').'</button></div>', 'download');
JToolBar::getInstance('toolbar')->appendButton('Custom', '<div class="zx"><button type="button" class="uk-button uk-button-mini"><i class="uk-icon-flag"></i> '.JText::_('PLG_ZLFRAMEWORK_EXT_LANGUAGE_PACKS').'</button></div>', 'langpacks');
JToolBar::getInstance('toolbar')->appendButton('Custom', '<div class="zx"><button type="button" class="uk-button uk-button-mini"><i class="uk-icon-refresh"></i> '.JText::_('PLG_ZLFRAMEWORK_RELOAD').'</button></div>', 'reload');

$warning = $this->app->zlfw->path->checkSystemPaths();
$errors = $this->app->zl->check->getMsg(NULL, true);

if(!empty($warning)){
	$errors = array_merge(array('warning' => $warning), $errors);
}
?>

<div id="zl-extensions">

	<!-- menu -->
	<?php echo $this->partial('zlmenu'); ?>

	<!-- Messages, warnings, notices: -->
	<?php if($errors):?>
		<?php echo $this->partial('errors', array('errors' => $errors)); ?>
	<?php endif; ?>

	<script type="text/javascript">
		jQuery(document).ready(function($)
		{
			// set lang strings
			zlux.lang.push( {
				'ZL_TIP_STATUS_ENABLED'      : '<?php echo $this->app->zlfw->html->tooltipText('PLG_ZLFRAMEWORK_ENABLED', 'PLG_ZLFRAMEWORK_TOGGLE_STATE'); ?>',
				'ZL_TIP_STATUS_DISABLED'     : '<?php echo $this->app->zlfw->html->tooltipText('PLG_ZLFRAMEWORK_DISABLED', 'PLG_ZLFRAMEWORK_TOGGLE_STATE'); ?>',
				'ZL_TIP_SUBSCRIBE'           : '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_SUBSCRIBE'); ?>',
				'ZL_TIP_INSTALL'             : '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_INSTALL'); ?>',
				'ZL_TIP_REINSTALL'           : '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_REINSTALL'); ?>',
				'ZL_LABEL_INSTALL'           : '<?php echo JText::_('PLG_ZLFRAMEWORK_INSTALL'); ?>',
				'ZL_LABEL_REINSTALL'         : '<?php echo JText::_('PLG_ZLFRAMEWORK_REINSTALL'); ?>',
				'ZL_MSG_CONFIRM_REMOVAL'     : '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_CONFIRM_REMOVAL'); ?>',
				'ZL_MSG_DOWNLOAD_ALL_START'  : '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_DOWNLOAD_ALL_START'); ?>',
				'ZL_MSG_DOWNLOAD_ALL_SUCCESS': '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_DOWNLOAD_ALL_SUCCESS'); ?>',
				'ZL_MSG_LANG_PACK_INSTALLED' : '<?php echo JText::_('PLG_ZLFRAMEWORK_EXT_LNG_INSTALLED'); ?>',
				'ZL_MSG_CONNECTING'          : '<?php echo JText::_('PLG_ZLFRAMEWORK_ZL_CONNECTING'); ?>'
			} );
		});
	</script>

	<div id="zl_list_container">
	<?php echo $this->partial('extensions_list', array('all_exts' => $this->extensions, 'autoload' => $this->autoload, 'authorized' => $this->authorized, 'offline' => $this->offline)); ?>
	</div>

	<!-- Options Modal Content -->
	<div id="zl-modal-options">
		<?php echo $this->partial('authenticate'); ?>
	</div>

</div>