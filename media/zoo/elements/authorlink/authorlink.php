<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       authorlink.php
* @version    3.1.2 September 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

/*
	Class: ElementAuthorLink
		The item author element class
*/
class ElementAuthorLink extends Element {
	
	/**
	 * Variable: $author
	 *    Active author.
	 *
	 * @var JUser
	 */
	protected $_author;

	/*
	   Function: Constructor
	*/
	public function __construct() {
		parent::__construct();		
		// load language
		$this->loadLanguage();
		// set callbacks
		//$this->registerCallback('changeauthor');
	}

	/*
		Function: hasValue
			Checks if the element's value is set.

	    Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/
	public function hasValue($params = array()) {
		
		return $this->getItem();
	}

	/*
	   Function: edit
	       Renders the edit form field.

	   Returns:
	       String - html
	*/
	public function edit() {
		return null;
	}

	/*
		Function: render
			Renders the element.

	   Parameters:
            $params - render parameter

		Returns:
			String - html
	*/
	public function render($params = array()) {
		// set Author
		$this->activeAuthor();
		// init vars
		$params = $this->app->data->create($params);

		// include assets css
		$this->app->document->addStylesheet('elements:authorlink/assets/css/style.css');

		// render layout
		if($layout = $this->getLayout()) {
			return $this->renderLayout($layout,
										array('params' => $params,
												'myLink' => $this->getLink(),
												'myName' => $this->getName($params->get('username', 1)),
												'myAvatar'=> $this->getAvatar())
			);
		}
		return null;
	}

	/*
		Function: loadLanguage
			Load elements language file.

		Returns:
			Void
	*/
	public function loadLanguage() {
		$element = strtolower($this->getElementType());
		//Lets load first english, then joomla default standard, then user language.
		$jlang = $this->app->system->language;
		$jlang->load('com_zoo.element.'.$element, $this->app->path->path('elements:'.$element), 'en-GB', true);
		$jlang->load('com_zoo.element.'.$element, $this->app->path->path('elements:'.$element), $jlang->getDefault(), true);
		$jlang->load('com_zoo.element.'.$element, $this->app->path->path('elements:'.$element), null, true);
	}

	/*
		Function: activeAuthor
			Retrieve currently active author object.
			
		Parameters:
			$params - render parameter

		Returns:
			JUser - the active author object
	*/
	
	public function activeAuthor($id = null) {

		if(!is_null($id) && (!isset($this->_author) || (isset($this->_author) && $this->_author->id != $id))) {			
			$this->_author = $this->app->user->get((int) $id);
		} elseif(!isset($this->_author)) {
			$this->_author = $this->app->user->get($this->getItem()->created_by);
		}

		return $this;
	}

	/*
		Function: getName
			Return User Name.

		Returns:
			String - text
	*/
	public function getName($name = true) {
		if ($this->_author->id == $this->getItem()->created_by && ($alias = $this->getItem()->created_by_alias) && !empty($alias)) {
			$authorName = $alias;
		} elseif ($name) {
			$authorName = $this->_author->name;
		} else {
			$authorName = $this->_author->username;
		}
		return $authorName;
	}

	/*
		Function: getAvatar
			Return User Avatar.

		Returns:
			String - url
	*/
	public function getAvatar() {		
		// init vars
		$defaultAvatar = JURI::root().'media/zoo/assets/images/avatar.png';
		// select integration
		if($this->_author->id) {
			switch ($this->config->get('linkto', 'com_users')) {
				case 'com_users':
					$authorAvatar = 'http://www.gravatar.com/avatar/'.md5($this->app->string->strtolower($this->_author->email)).'?d='.urlencode($defaultAvatar);
					break;
				case 'com_alphauserpoints':
					$authorAvatar = JURI::root().'components/com_alphauserpoints/assets/images/avatars/'.$this->app->database->queryResult('SELECT avatar FROM #__alpha_userpoints WHERE userid = '.$this->app->database->escape($this->_author->id).' LIMIT 1');
					break;
				case 'com_comprofiler':
					$cbAvatar = $this->app->database->queryResult('SELECT avatar FROM #__comprofiler WHERE user_id = '.$this->app->database->escape($this->_author->id).' AND avatarapproved = 1 LIMIT 1');
					$authorAvatar = JURI::root().'images/comprofiler/' .(strstr($cbAvatar,'gallery') ? '' : 'tn').rawurlencode($cbAvatar);
					break;
				case 'com_community':
					//$authorAvatar = JURI::root().$this->app->database->queryResult('SELECT thumb FROM #__community_users WHERE userid = '.$this->app->database->escape($this->_author->id).' LIMIT 1');
					// include the syndicate functions only once
					require_once JPATH_BASE.'/components/com_community/libraries/core.php';
					$authorAvatar = CFactory::getUser($this->_author->id)->getThumbAvatar();//getAvatar();				
					break;
				case 'com_kunena':
					$authorAvatar = JURI::root().'media/kunena/avatars/'.$this->app->database->queryResult('SELECT avatar FROM #__kunena_users WHERE userid = '.$this->app->database->escape($this->_author->id).' LIMIT 1');
					break;
				case 'com_zoo':
					if(!$this->app->zooauthor->getParams()) {
						$this->app->system->application->enqueueMessage('ELM_AUTHORLINK_ENABLE_ZOOAUTHOR_PLUGINS', 'error');
					} else {
						$profile = $this->app->zooauthor->getProfile($this->_author->id);
						$html    = $this->app->zooauthor->getProfileHTML($profile, 'element');
						// create HTML DOM object:
						$domdoc = new DOMDocument();
						$domdoc->strictErrorChecking = false;
						// load html for parsing
						$errorSetting = libxml_use_internal_errors(true);
						$domdoc->loadHTML($html);
						libxml_clear_errors();
						libxml_use_internal_errors($errorSetting);
						// get nodes
						$nodes = array();
						foreach ($domdoc->getElementsByTagName('img') as $img) {
							$nodes[] = $img;
						}
						$authorAvatar = isset($nodes[0]) ? $nodes[0]->getAttribute('src') : '';
					}
					break;
			}
		}
		// check
		//$this->app->system->application->enqueueMessage($authorAvatar, 'notice');
		$authorAvatar = @getimagesize($authorAvatar) ? $authorAvatar : $defaultAvatar;
		// and return
		return $authorAvatar;
	}

	/*
		Function: getLink
			Return link to User profile.

		Returns:
			String - url
	*/
	public function getLink() {
		// init vars
		$authorLink = 'javascript:void(0);';		
		// select integration
		if($this->_author->id) {
			switch ($this->config->get('linkto', 'com_users')) {
				case 'com_users':
					$authorLink = JRoute::_('index.php?option=com_users&view=profile&user_id='.$this->_author->id);
					break;
				case 'com_alphauserpoints':
					$authorLink = JRoute::_('index.php?option=com_alphauserpoints&view=account&userid='.$this->_author->id);
					break;
				case 'com_comprofiler':
					if ($menu_items	= $this->app->system->application->getMenu('site')->getItems('component_id', JComponentHelper::getComponent('com_comprofiler')->id)) {
						//$this->app->system->application->enqueueMessage(json_encode($menu_items));
						foreach($menu_items as $menu_item) {
							if (!isset($menu_item->query['task'])) {
								$Itemid = $menu_item->id;
							}
						}
						if (!$Itemid) {
							foreach($menu_items as $menu_item) {
								if (@$menu_item->query['task'] == 'usersList') {
									$Itemid = $menu_item->id;
								}
							}
						} else if (!$Itemid) {
							foreach($menu_items as $menu_item) {
								if (@$menu_item->query['task'] == 'userprofile') {
									$Itemid = $menu_item->id;
								}
							}
						}
						if (isset($Itemid)) {
							$Itemid = '&Itemid='.$Itemid;
						} else {
							$Itemid = '&Itemid='.$menu_items[0]->id;
						}
					} else {
						$Itemid = '&Itemid='.$this->app->system->application->getMenu('site')->getActive()->id;// or getDefault()?
					}
					$authorLink = JRoute::_('index.php?option=com_comprofiler&task=userProfile&user='.$this->_author->id.$Itemid);	
					break;
				case 'com_community':
					// include the syndicate functions only once
					require_once JPATH_BASE.'/components/com_community/libraries/core.php';
					$authorLink = CRoute::_('index.php?option=com_community&view=profile&userid='.$this->_author->id);
					break;
				case 'com_kunena':
					$authorLink = JRoute::_('index.php?option=com_kunena&func=profile&userid='.$this->_author->id);
					break;
				case 'com_zoo':
					if(!$this->app->zooauthor->getParams()) {
						$this->app->system->application->enqueueMessage('ELM_AUTHORLINK_ENABLE_ZOOAUTHOR_PLUGINS', 'error');
					} else {
						$profile = $this->app->zooauthor->getProfile($this->_author->id);
						$authorLink  = $this->app->route->item($profile);
					}
					break;
			}
		}
		return $authorLink;
	}
}
