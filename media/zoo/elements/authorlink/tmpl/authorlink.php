<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       authorlink.php
* @version    3.1.0 August 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div class="authorlink <?php echo 'align-'.$params->get('aligment', 'none');?>" >
	<a href="<?php echo $myLink; ?>" title="<?php echo $myName; ?>">
		<?php if ($params->get('showauthor', 1)) : ?>
			<?php echo $myName; if ($params->get('divstyle', 'vertical')==='vertical') echo '<br/>'; ?>
		<?php endif; ?>
		<?php if ($params->get('showavatar', 1)) : ?>
			<img class="<?php echo $params->get('divstyle', 'vertical');?>" src="<?php echo $myAvatar; ?>" alt="<?php  echo $myName; ?>" title="<?php  echo $myName; ?>"/>
		<?php endif; ?>
	</a>
</div>
