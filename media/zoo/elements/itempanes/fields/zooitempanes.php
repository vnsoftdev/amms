<?php
/**
* @package   ZOO Component
* @author    Attavus M.D. (R.A.S.Lab[.org]) http://www.raslab.org
* @copyright Copyright (C) 2011-2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// create panes options
$options = array($this->app->html->_('select.option', '-1', JText::_('JNONE')));
foreach ($parent->element->config->get('option') as $i => $pane) {
	$options[] = $this->app->html->_('select.option', $i, $pane['name']);
}

// output list
echo $this->app->html->_('select.genericlist', $options, $control_name.'['.$name.']', 'class="inputbox"', 'value', 'text', $value, $control_name.$name);
