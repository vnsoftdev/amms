<?php
/**
* @package   ZOO Component
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) 2011-2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// import library dependencies
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

// set template path and add global element renderer path
$paths   = array();
$paths[] = $this->app->path->path('component.site:renderer/element');
if ($relative_path = urldecode($this->app->request->getVar('path'))) {
	$paths[] = JPATH_ROOT . '/' . $relative_path.'/renderer/element';
}	

// set tmpl array 
$tmpls   = array();
foreach ($paths as $path) {
	if (JFolder::exists($path)) {
		$files = JFolder::files($path, '.php', false);
		foreach ($files as $file) {			
			$file = JFile::stripExt($file);			
			if(!in_array($file, $tmpls)) {
				$tmpls[] = $file;
			}
		}
	}
	
}

// set tmpl options
$options = array();						
foreach ($tmpls as $tmpl) {
	$options[] = $this->app->html->_('select.option', $tmpl, $tmpl);
}

// create panes options
?>
<table class="adminlist table table-striped" width="100%">
	<thead>
		<tr>
			<th class="title"><?php echo JText::_('ELM_ITEMPANES_PANE_LABEL'); ?></th>
			<th class="title"><?php echo JText::_('ELM_ITEMPANES_STYLEOFELMS_LABEL'); ?></th>
		</tr>
	</thead>	
	<tbody>
		<?php foreach ($parent->element->config->get('option') as $i => $pane) : ?>
			<tr class="row<?php $k = $i++; echo $i % 2; ?>">
				<td class="key"><?php echo $pane['name']; ?></td>
				<td>
					<?php echo $this->app->html->_('select.genericlist',  $options, "{$control_name}[{$name}][]", 'class="element"', 'value', 'text', (empty($value[$k]) ? 'default' : $value[$k]), $control_name.$name.$pane['value']); ?>							
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
