<?php
/**
* @package   ZOO Component
* @author    Attavus M.D. (R.A.S.Lab[.org]) http://www.raslab.org
* @copyright Copyright (C) 2011-2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$path = $this->app->request->getVar('path');

// render hidden input
echo $this->app->html->input('hidden', "{$control_name}[{$name}]", $path).urldecode($path);
