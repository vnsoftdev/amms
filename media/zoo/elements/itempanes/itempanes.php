<?php
/*************************
* @package   ZOO Component
* @file      itempanes.php
* @version   3.0.8 March 2013
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) 2012-2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

// register ElementOption class
App::getInstance('zoo')->loader->register('ElementOption', 'elements:option/option.php');

/*
  Class: ElementItemPanes
     the itempanes element class
*/
class ElementItemPanes extends ElementOption {

	/*
		Function: Constructor
	*/
	public function __construct() {
		parent::__construct();
		// load language
		$this->loadLanguage();
		// register events
		$this->app->event->dispatcher->connect('type:assignelements', array($this, 'assignElements'));
		// set callbacks
		$this->registerCallback('renderPane');
	}
	
	/*
		Function: getConfigForm
			Get parameter form object to render input form.

		Returns:
			Parameter Object
	*/
	public function getConfigForm() {
		$form = parent::getConfigForm();
		$form->addElementPath(dirname(__FILE__).'/fields');
		return $form;
	}
	
	/*
		Function: hasValue
			Checks if the element's value is set.

	    Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/
	public function hasValue($params = array()) {
		return true;
	}	
			
	/*
	   Function: edit
	       Renders the edit form field.

	   Returns:
	       String - html
	*/
	public function edit() {
		// nothing to render, hiding layout		
		return false;
	}

				
	/*
		Function: render
			Renders the element.

	    Parameters:
            $params - render parameter

		Returns:
			String - html
	*/
	public function render($params = array()) {
		// init display params
		$params = $this->app->data->create($params);
		// include assets js and css		
		// ui must be loaded first
		$this->app->document->addStylesheet('libraries:jquery/jquery-ui.custom.css');
		$this->app->document->addScript('libraries:jquery/jquery-ui.custom.min.js');
				
		// then elements assets
		$this->app->document->addStylesheet('elements:itempanes/assets/css/itempanes.css');		
		// and others
		// create renderPane link
		$query = array('task' => 'callelement', 'format' => 'raw', 'item_id' => $this->getItem()->id, 'element' => $this->identifier, 'method' => 'renderPane');
		// set pane
		$query['args[0]'] = '';		
		$renderPaneLink = $this->app->link($query);		
		// render layout
		if ($layout = $this->getLayout()) {
			return $this->renderLayout($layout, array(										
										'params' => $params,
										'renderPaneLink' => $renderPaneLink
										)
									);
		}

		return null;
	}
	
	/*
	    Function: assignElements
	       Adds to the AdminForm Panned Positions.
	   
	   Parameters:
            $event  - AppEvent parameter with array $arguments
			
		Returns:
			Void
			
	 */
	public function assignElements($event) {
		// init vars
		$type = $event->getSubject();
		$replaced_chars = array('%3C' => '<',
								'%3E' => '>',
								'%3D' => '=',
								'+'   => ' ',
								'%22' => '"',
								'%27' => '\'',
								'%21' => '!',
								'%2F' => '/',
								'%3A' => ':'
							);
		$tidyConfig = array('doctype'         => 'omit',
							'show-body-only'  => true,
							'drop-proprietary-attributes' => false,
							'char-encoding'   => 'utf8',
							'input-encoding'  => 'utf8',
							'output-encoding' => 'utf8',
							'force-output'    => true,
							'output-xhtml'    => true,
							"clean"           => true);		
		// create HTML DOM object:
		$domdoc = new DOMDocument();		
		$domdoc->preserveWhiteSpace  = false;
		$domdoc->strictErrorChecking = false;
		// load html for parsing with fixing for non-latin symbol
		$errorSetting = libxml_use_internal_errors(true);		
		$domdoc->loadHTML(strtr(urlencode($event['html']), $replaced_chars));        
        libxml_clear_errors();
		libxml_use_internal_errors($errorSetting);
		// get application group
		$group = $this->app->request->getString('group');
		// get application path and template layout
		$relative_path = urldecode($this->app->request->getVar('path'));
		$path		   = $relative_path ? JPATH_ROOT . '/' . $relative_path : '';		
		$layout		   = $this->app->request->getString('layout');
		// create renderer
        $renderer = $this->app->renderer->create('item')->addPath($path);
        // get positions config
        $positionsconfig = $renderer->getConfig('item')->get($group.'.'.$type->id.'.'.$layout);
		// render layout		
		if ($assignelements = $this->getLayout('assignelements.php')) {
			foreach($type->getElements() as $element){
				if(strtolower($element->getElementType()) == 'itempanes' && $element->hasValue()){
					$itempanes = $element;
					// check assigned panned position for itempanes
					$pane = $domdoc->getElementById('itempanes-positions-'.$itempanes->identifier);
					// if empty pane, then create new panned positions
					if(!is_object($pane)){
						// create fragment
						$fragment = $domdoc->createDocumentFragment(); 
						// render pane						
						$newpane = $this->renderLayout($assignelements, 
														compact ('type', 
														'itempanes', 
														'positionsconfig', 
														'path'));
						// validate and repair xhtml with fixing for non-latin symbol
						$tidy = new Tidy();						 
						$tidy->parseString(strtr(urlencode((string)$newpane), $replaced_chars), $tidyConfig, 'utf8');
						$tidy->cleanRepair();
						//if ($tidy->errorBuffer) {							
							//$errors = explode("\n", $tidy->errorBuffer);						 
							//foreach ($errors as $error) {							
							//	$this->app->system->application->enqueueMessage($error, 'error');
							//}						
						//}						
						// insert arbitary xhtml into the fragment
						$fragment->appendXML((string)$tidy->value);
						unset($tidy);
						// get adminForm
						$adminForm = $domdoc->getElementById('adminForm');
						if(is_object($adminForm)){
							// create nodelist as array
							$divs = $adminForm->getElementsByTagName('div');   
							$nodeList = array();
							foreach ($divs as $div) {
								$nodeList[] = $div;
							}
							// append fragment to form
							foreach ($nodeList as $node) {
								if ($node->getAttribute('class') == 'col col-left width-50'){
									$node->appendChild($fragment);									
								}			
							}							
						}
						unset($fragment);		
					}
				}
			}
		}
		$domdoc->formatOutput = true;		
		// set $event['html'] after modifying the html					
		$event['html'] = urldecode($domdoc->saveHTML());
		unset($domdoc);		
	}
	
	/*
		Function: renderPane
			render an one pane.
		
		 Parameters:
            $args - json encoded $arguments

		Returns:
			String - html
	*/
	public function renderPane($args) {	
		// get item object
		$item = $this->getItem();
		if(empty($item)){	
			// get item id from request
			$itemid = $this->app->request->getInt('item_id', 0);
			// get item
			$item = $this->app->table->item->get($itemid);				
		}
		//return $args;
		// get arguments
		$args   = (array) json_decode($args);
		$pane   = $args['pane'];
		$path   = $args['path'];
		$layout = $args['layout'];
		$style  = $args['style'];			
		// get renderer
		$renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $item->getApplication()->getTemplate()->getPath()));
		// get positions config
		$positionsconfig = $renderer->getConfig('item')->get($item->getApplication()->getGroup().'.'.$item->getType()->id.'.'.$layout);	
		// other vars
		$elements = array();
		$output   = array();			
		// render elements				
		if (!isset($positionsconfig[$pane])) return; //"<div class=\"ui-state-highlight\">".JText::_('ELM_ITEMPANES_NO_PANE_DEFINED')."\n</div>";
		foreach ($positionsconfig[$pane]  as $data) {
			if ($element = $item->getElement($data['element'])) {
				// check accsess
				if (!$element->canAccess($this->app->user->get())) continue;
				// check value
				if ($element->hasValue($this->app->data->create($data))) {
					// trigger elements beforedisplay event
					$render = true;
					$this->app->event->dispatcher->notify($this->app->event->create($item, 'element:beforedisplay', array('render' => &$render, 'element' => $element, 'params' => $data)));
						if ($render) {
							$elements[] = array('element' => $element, 'params' => $data);
						}
					}
				}
			}
			foreach ($elements as $key => $value) {		
				$params  = array_merge(array('first' => ($key == 0), 'last' => ($key == count($elements)-1)), $value['params']);
				// render element				 		
				$output[$key] = $this->renderElement($path, $style, 
													array(
													'element' => $value['element'], 
													'params' => $params
													));
				// trigger elements afterdisplay event
				$this->app->event->dispatcher->notify($this->app->event->create($item, 'element:afterdisplay', array('html' => &$output[$key], 'element' => $value['element'], 'params' => $params)));
			}
		return implode("\n", $output);			
	}
	
		/*
		Function: renderElement
			render an one element.
		
		 Parameters:
            $args   - array $arguments
            $style  - an element renderer style

		Returns:
			String - html
	*/
	public function renderElement($path, $style, $args) {
		// clean the file name
		$file = preg_replace('/[^A-Z0-9_\.-]/i', '', $style);		
		// set template path and add global element renderer path
		$paths   = array();
		$paths[] = $this->app->path->path('component.site:renderer/element');
		if ($relative_path = urldecode($path)) {
			$paths[] = JPATH_ROOT . '/' . $relative_path.'/renderer/element';
		}	
		// load the element renderer template
		$__file = strtolower($file).'.php';
		$__renderer_tmpl = JPath::find($paths, $__file);
		// render the element renderer template
		if ($__renderer_tmpl != false) {
			// import vars and get content
			extract($args);
			ob_start();
			include($__renderer_tmpl);
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}
		return $this->app->error->raiseError(500, 'Element renderer template "'.$__file.'" not found. ('.$this->app->utility->debugInfo(debug_backtrace()).')');
	}
	
	/*
		Function: loadLanguage
			Load elements language file.

		Returns:
			Void
	*/
	public function loadLanguage() {
		// init vars						
		$element = strtolower($this->getElementType());
		$path    = $this->app->path->path('elements:'.$element);
		$jlang   = $this->app->system->language;		
		// lets load first english, then joomla default standard, then user language
		$jlang->load('com_zoo.element.'.$element, $path, 'en-GB', true);
		$jlang->load('com_zoo.element.'.$element, $path, $jlang->getDefault(), true);
		$jlang->load('com_zoo.element.'.$element, $path, null, true);
	}
}
