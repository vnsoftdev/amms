<?php
/**
* @package   ZOO Component
* @file      assignelements.php
* @version   3.0.0 November 2012
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) 2012 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$identifier = $itempanes->identifier;
$name       = $itempanes->config->get('name');
$panes      = $itempanes->config->get('option');
?>

<div id="itempanes-positions-<?php echo $identifier; ?>">
	<fieldset>
		<legend><?php echo JText::_('ELM_ITEMPANES_PANNED_POS').' '.$name; ?></legend>
		<?php
			$elements  = array_merge($type->getElements(), $type->getCoreElements());
			if (!empty($panes)) {
				foreach ($panes as $pane) {
					echo '<div class="position">'.$pane['name'].'</div>';
					echo '<ul class="element-list" data-position="'.$pane['value'].'-'.$identifier.'">';
					if (!empty($positionsconfig) && isset($positionsconfig[$pane['value'].'-'.$identifier])) {
						$i = 0;
						foreach ($positionsconfig[$pane['value'].'-'.$identifier] as $data) {
							if (isset($data['element']) && isset($elements[$data['element']])) {
								$element = $elements[$data['element']];
								// get view
								$view = new AppView(array_merge(array('name' => 'manager', 'template_path' =>JPATH_COMPONENT. '/views/manager/tmpl')));
								// set path
								$view->assign('path', $path);
								// render partial
								echo $view->partial('assignelement', array('element' => $element, 'data' => $data, 'position' => $pane['value'].'-'.$identifier, 'index' => $i++, 'core' => ($element->getGroup() == 'Core')));									
							}
						}
					}
					echo '</ul>';
				}
			} else {
				echo '<i>'.JText::_('ELM_ITEMPANES_PANNED_POS_NO').'</i>';
			}
		?>
	</fieldset>
</div>
