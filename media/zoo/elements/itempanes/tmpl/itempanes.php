<?php
/**
* @package   ZOO Component
* @file      itempanes.php
* @version   3.0.7 February 2013
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) 2012-2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$panes   = $this->config->get('option');
$content = array();
$path    = $params->get('tmplpath');
$layout  = $params->get('itemlayout', 'full');
$style   = $params->get('itempanesstyle');
?>
	
<div id="<?php echo $this->identifier; ?>">
	<?php if ($params->get('panestype', 'tabs') == 'tabs') : ?>
	<ul>
		<?php foreach ($panes as $i => $pane) : ?>
			<?php $content[$pane['value']] = $this->renderPane(
						json_encode(array('pane' => $pane['value'].'-'.$this->identifier,
										'path' => $path,
										'layout' => $layout,
										'style' => isset($style[$i]) ? $style[$i] : 'default'))); ?>
			<?php if ($params->get('hideEmpty', 0) && empty($content[$pane['value']])) { continue; ?>			
			<?php } else { ?>
			<li class="pos-<?php echo $pane['value']; ?>">
				<a href="<?php echo JURI::getInstance()->toString();?>#<?php echo $pane['value'].'-'.$this->identifier; ?>"><?php echo $pane['name']; ?></a>
			</li>
			<?php } ?>
		<?php endforeach; ?>		
	</ul>
	<?php foreach ($panes as $pane) : ?>
		<?php if ($params->get('hideEmpty', 0) && empty($content[$pane['value']])) { continue; ?>			
		<?php } else { ?>
		<div id="<?php echo $pane['value'].'-'.$this->identifier; ?>" class="pos-<?php echo $pane['value']; ?>">	
			<?php echo $content[$pane['value']]?>					
		</div>
		<?php } ?>
	<?php endforeach; ?>
	<?php else : ?>
		<?php foreach ($panes as $i => $pane) : ?>
			<?php $content[$pane['value']] = $this->renderPane(
						json_encode(array('pane' => $pane['value'].'-'.$this->identifier,
										'path' => $path,
										'layout' => $layout,
										'style' => isset($style[$i]) ? $style[$i] : 'default'))); ?>
			<?php if ($params->get('hideEmpty', 0) && empty($content[$pane['value']])) { continue; ?>			
			<?php } else { ?>
			<h2><a href="#"><?php echo $pane['name']; ?></a></h2>
			<div class="pos-<?php echo $pane['value']; ?>">
				<?php echo $content[$pane['value']]; ?>	
			</div>
			<?php } ?>
		<?php endforeach; ?>
	<?php endif; ?>	
</div>
<script type="text/javascript">
	jQuery(function($){
	  $("#<?php echo $this->identifier; ?>").<?php echo $params->get('panesType', 'tabs'); ?>({
		collapsible: <?php echo $params->get('collapsible', 'false'); ?>,
		selected: <?php echo (!$params->get('collapsible') && $params->get('selected') == '-1') ? '0' : $params->get('selected', '0'); ?>,
		event: "<?php echo $params->get('event', 'click'); ?>",		
		<?php // only tabs options
			if ($params->get('panestype', 'tabs') == 'tabs') : ?>
		fx: {// e.g. { height: 'toggle', opacity: 'toggle', duration: 200 }
			opacity: "toggle",
			duration: "fast"//,
			//rotate: 3500
			},
		heightStyle: "<?php echo $params->get('heightStyle', 'content'); ?>",
		spinner: "<?php echo JText::_('ELM_ITEMPANES_LOADING'); ?>",
		cache: <?php echo $params->get('cache', 'true'); ?>
		<?php if ($params->get('cookie')) {
				$this->app->document->addScript('libraries:jquery/plugins/cookie/jquery.cookie.js');
				echo ",\n cookie: { expires: 30}"; // e.g. { expires: 7, path: '/', domain: 'jquery.com', secure: true }				
			}
		 ?>
		
		<?php // only accordion options
			else : ?>
		animated: <?php echo $params->get('animated', 'false'); ?>,
		autoHeight: <?php echo $params->get('autoHeight', 'true'); ?>,
		fillSpace: <?php echo $params->get('fillSpace', 'false'); ?>,
		clearStyle: <?php echo $params->get('clearStyle', 'false'); ?>
		<?php endif; ?>
	  });
	});
</script>
