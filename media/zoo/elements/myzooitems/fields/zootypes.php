<?php
/**
* @package   ZOO Component
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) 2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// add chosen in Joomla 2.5
if ($this->app->joomla->isVersion('2.5')) {
	$this->app->document->addScript('libraries:jquery/plugins/chosen/chosen.jquery.min.js');
	$this->app->document->addStylesheet('libraries:jquery/plugins/chosen/chosen.css');
} else {
	$this->app->html->_('formbehavior.chosen', '#elements'.$parent->element->identifier.$name);
}

// init vars
$params  = $this->app->parameterform->convertParams($parent);
$ctrl    = $control_name . '[' . $name . ']';
$options = array();
$attribs = '';
 
if ($v = $node->attributes()->class) {
    $attribs .= ' class="'.$v.'"';
} else {
    $attribs .= ' class="inputbox"';
}

if ($node->attributes()->multiple) {
    $attribs .= ' multiple="multiple"';
    $attribs .= ' data-no_results_text="'.JText::_('NO_RESULTS').'"';
    $attribs .= ' data-placeholder="'.JText::_('СHOOSE').'"';
    $ctrl .= '[]';
}

if ($v = $node->attributes()->width) {
      $attribs .= ' style="width: '.$v.';"';
}
// create application/types select
$options[] = $this->app->html->_('select.option', '', '');


foreach ($this->app->application->getApplications() as $application) {
	// get types
	$types = $application->getTypes();
	if (count($types)) {
		$options[] = $this->app->html->_('select.option', '<OPTGROUP>', JText::_('Application').' '.$application->name);			
		// create types select
		foreach ($types as $type) {
			$options[] = $this->app->html->_('select.option', $type->id, $type->name);
		}
		$options[] = $this->app->html->_('select.option', '</OPTGROUP>');					
	}
}

echo $this->app->html->_('select.genericlist', $options, $ctrl, trim($attribs), 'value', 'text', $value, $control_name.$name); ?>

<script type="text/javascript">
	jQuery(function($){
		<?php if ($parent->app->joomla->isVersion('2.5')) : ?>
		$("#elements<?php echo $parent->element->identifier.$name; ?>").chosen({ disable_search_threshold : 10, allow_single_deselect : true });
		<?php endif; ?>
		// add here since on 3.0 the options are hardcoded in the constructor of the PHP method
		$("#elements<?php echo $parent->element->identifier.$name; ?>").data("chosen").search_contains = true;
	});
</script>
