<?php
/*************************
* @package   ZOO Component
* @file      myzooitems.php
* @version   3.1.2 September 2013
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) R.A.S.Lab[.org]
* @license   http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
*****************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
   Class: ElementMyZooItems
       The myzooitems element class
*/
class ElementMyZooItems extends Element {

	/*
	   Function: Constructor
	*/
	public function __construct() {

		parent::__construct();

		// set callbacks
		$this->registerCallback('getByPage');
	}

	/*
		Function: getConfigForm
			Get parameter form object to render input form.

		Returns:
			Parameter Object
	*/
	public function getConfigForm() {
		$form = parent::getConfigForm();
		$form->addElementPath($this->app->path->path('elements:myzooitems/fields'));
		return $form;
	}

	/*
		Function: hasValue
			Checks if the element's value is set.

	    Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/
	public function hasValue($params = null) {
		$count = $this->_getItemsCount();
		return !empty($count);
	}

	/*
	   Function: edit
	       Renders the edit form field.

	   Returns:
	       String - html
	*/
	public function edit() {
		return null;
	}
		
    /*
		Function: render
			Renders the element.

	   Parameters:
            $params - render parameter

		Returns:
			String - html
	*/
	public function render($params = array()) {
		// init vars
		$count = $this->config->get('count', 10);		
		$pages = (int) ceil($this->_getItemsCount() / $count);
		$html  = $this->_getItemsHTML(0, $count);		
		// include assets js and css
		$this->app->document->addScript('elements:myzooitems/assets/js/jquery.bootpag.min.js');

        // render layout
		if ($layout = $this->getLayout()) {
			return $this->renderLayout($layout, array('pages' => $pages, 'html' => $html));
		}

		return null;
	}

    /*
		Function: getByPage
			get items html by page.

		Returns:
			Text - HTML
	*/
	public function getByPage($page = 1) {
		// init vars
		$count      = $this->config->get('count', 10);		
		$limitstart = max(($page - 1) * $count, 0);
		// output
		return $this->_getItemsHTML($limitstart, $count);		
	}

	private function _getItemsHTML($limitstart, $limit) {
		// init vars
		$items = $this->_getItems($limitstart, $limit);
		$html = "";
		// create output
		ob_start();
		?>
		<?php foreach ($items as $item) : ?>
			<div class="item">
				<?php $item_html = null;
					if($this->config->get('item_view', 'layout') == 'layout') {
						$renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $item->getApplication()->getTemplate()->getPath()));
						$path   = 'item';
						$prefix = 'item.';
						$type   = $item->getType()->id;
						if ($renderer->pathExists($path.DIRECTORY_SEPARATOR.$type)) {
							$path   .= DIRECTORY_SEPARATOR.$type;
							$prefix .= $type.'.';
						}
						if (in_array('teaser', $renderer->getLayouts($path))) {
							$item_html = $renderer->render($prefix.'teaser', array('view' => $item->getApplication(), 'item' => $item));
						} elseif (in_array('related', $renderer->getLayouts($path))) {
							$item_html = $renderer->render($prefix.'related', array('view' => $item->getApplication(), 'item' => $item));
						}
					} elseif (empty($item_html) || $this->config->get('item_view', 'layout') == 'name'){
						if ($this->config->get('link_to_item', false) && $item->getState()) {
							$item_html = '<a href="'.$this->app->route->item($item).'" title="'.$item->name.'">'.$item->name.'</a>';
						} else {
							$item_html = $item->name;
						}
					}
					echo $item_html; ?>				
			</div>
		<?php endforeach; ?>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	private function _getItems($offset = 0, $limit = 0) {
		// get item ordering
		list($join, $order) = $this->_getItemOrder($this->config->get('order', '_itemname'));
		//$this->app->system->application->enqueueMessage(__LINE__.': $order: '.json_encode(print_r($this->_getItemOrder($this->config->get('order', '_itemname')))));
		// get query
		$options = array('select' => 'a.*',
						'from' => ZOO_TABLE_ITEM.' AS a'
						.' LEFT JOIN '.ZOO_TABLE_TAG.' AS b ON a.id = b.item_id'
						.' LEFT JOIN '.ZOO_TABLE_CATEGORY_ITEM.' AS c ON a.id = c.item_id'
						.($join ? $join : ''),
						'conditions' => $this->_getConditions(),
						'order' => 'a.priority DESC'.($order ? ', '.$order : ''));
		if ($limit != 0) {
			$options['offset'] = (int) $offset;
			$options['limit'] = (int) $limit;
		}		
		// return items		
		return $this->app->table->item->all($options);
	}

	private function _getItemsCount() {
		// get database
		$db = $this->app->database;		
		// get query
		$query = $db->getQuery(true);
		$query->select('a.id')->from(ZOO_TABLE_ITEM.' AS a')
				->leftJoin(ZOO_TABLE_TAG.' AS b ON a.id = b.item_id')
				->leftJoin(ZOO_TABLE_CATEGORY_ITEM.' AS c ON a.id = c.item_id')
				->where($this->_getConditions())
				->group('a.id');
		$db->query($query);
		$count = $db->getNumRows();
		//$this->app->system->application->enqueueMessage(__LINE__.': $count: '.json_encode($count));
		return $count;
	}

	private function _getConditions() {
		// get database
		$db = $this->app->database;		
		// set base query options		
		$conditions = "a.".$this->app->user->getDBAccessString($this->app->user->get());
		// ... by applications
		if(($applications = $this->config->get('applications')) && !empty($applications)) {
			$applicationIds = array();
			foreach($applications as $id) {
				if(!empty($id)) {
					$applicationIds[] = $db->Quote((int) $id);
				}
			}
			if(!empty($applicationIds)) {
				$conditions .= " AND a.application_id".($this->config->get('applications_selection', 0) ? "" : " NOT")." IN (".implode(', ', $applicationIds).")";
			}
		}
		// ... by types
		if(($types = $this->config->get('types')) && !empty($types)) {			
			$typeIds = array();
			foreach($types as $id) {
				if(!empty($id)) {
					$typeIds[] = $db->Quote($db->escape((string) $id));
				}
			}
			if(!empty($typeIds)) {
				$conditions .= " AND a.type".($this->config->get('types_selection', 0) ? "" : " NOT")." IN (".implode(', ', $typeIds).")";
			}
		}
		// ... by categories
		if(($categories = $this->config->get('categories')) && !empty($categories)) {
			$categoryIds = array();
			foreach($categories as $id) {
				if(!empty($id)) {
					$categoryIds[] = $db->Quote((int) $id);
				}
			}
			if(!empty($categoryIds)) {
				$conditions .= " AND c.category_id".($this->config->get('categories_selection', 0) ? "" : " NOT")." IN (".implode(', ', $categoryIds).")";
			}
		}
		// ... by tags
		if($this->config->get('limit_by_tags', 0) && ($tags  = $this->getItem()->getTags())) {
			$tagNames = array();
			foreach($tags as $name) {
				if(!empty($name)) {
					$tagNames[] = $db->Quote($db->escape((string) $name));
				}
			}
			if(!empty($tagNames)) {
				$conditions .= " AND b.name".($this->config->get('tags_selection', 0) ? "" : " NOT")." IN (".implode(', ', $tagNames).")";
			}
		}
		// ... by author
		if($this->config->get('limit_by_author', 1)) {
			$conditions .= " AND a.created_by = ".$db->Quote($db->escape((int) $this->getItem()->created_by));
		}
		// ... expired
		if(!$this->config->get('expired', 0)) {
			// get dates and prepare
			$date = $this->app->date->create();
			$now  = $db->Quote($date->toSQL());
			$null = $db->Quote($db->getNullDate());
			$conditions .= " AND (a.publish_up = ".$null." OR a.publish_up <= ".$now.")" 
						." AND (a.publish_down = ".$null." OR a.publish_down >= ".$now.")";
		}
		// ... published
		if($this->config->get('published', 1)) {			
			$conditions .= " AND a.state = 1";
		}
		//$this->app->system->application->enqueueMessage(__LINE__.': $conditions: '.json_encode($conditions));
		return $conditions;
	}

	protected function _getItemOrder($order) {

		// if string, try to convert ordering
		if (is_string($order)) {
			$order = $this->app->itemorder->convert($order);
		}

		$result = array(null, null);
		$order = (array) $order;

		// remove empty and duplicate values
		$order = array_unique(array_filter($order));

		// if random return immediately
		if (in_array('_random', $order)) {
			$result[1] = 'RAND()';
			return $result;
		}

		// get order dir
		if (($index = array_search('_reversed', $order)) !== false) {
			$reversed = 'DESC';
			unset($order[$index]);
		} else {
			$reversed = 'ASC';
		}

		// get ordering type
		$alphanumeric = false;
		if (($index = array_search('_alphanumeric', $order)) !== false) {
			$alphanumeric = true;
			unset($order[$index]);
		}

		// set default ordering attribute
		if (empty($order)) {
			$order[] = '_itemname';
		}

		// if there is a none core element present, ordering will only take place for those elements
		if (count($order) > 1) {
			$order = array_filter($order, create_function('$a', 'return strpos($a, "_item") === false;'));
		}

		// order by core attribute
		foreach ($order as $element) {
			if (strpos($element, '_item') === 0) {
				$var = str_replace('_item', '', $element);
				if ($alphanumeric) {
					$result[1] = $reversed == 'ASC' ? "a.$var+0<>0 DESC, a.$var+0, a.$var" : "a.$var+0<>0, a.$var+0 DESC, a.$var DESC";
				} else {
					$result[1] = $reversed == 'ASC' ? "a.$var" : "a.$var DESC";
				}

			}
		}

		// else order by elements
		if (!isset($result[1])) {
			$result[0] = " LEFT JOIN ".ZOO_TABLE_SEARCH." AS s ON a.id = s.item_id AND s.element_id IN ('".implode("', '", $order)."')";
			if ($alphanumeric) {
				$result[1] = $reversed == 'ASC' ? "ISNULL(s.value), s.value+0<>0 DESC, s.value+0, s.value" : "s.value+0<>0, s.value+0 DESC, s.value DESC";
			} else {
				$result[1] = $reversed == 'ASC' ? "s.value" : "s.value DESC";
			}
		}

		// trigger init event
		$this->app->event->dispatcher->notify($this->app->event->create($order, 'item:orderquery', array('result' => &$result)));

		return $result;
	}
}
