<?php
/*************************
* @package   ZOO Component
* @file      myzooitems.php
* @version   3.0.0 July 2013
* @author    Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) 2013 R.A.S.Lab[.org]
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="<?php echo $this->identifier.'-'.$this->getItem()->id; ?>">
	<div class="items">
		<?php echo $html; ?>
	</div>
	<?php if ($this->config->get('paging', 1)) : ?>
	<div class="pagination"></div>
	<script type="text/javascript">
	<!--
		jQuery(function($) {
			$("#<?php echo $this->identifier.'-'.$this->getItem()->id; ?> .pagination").bootpag({
				total: <?php echo $pages; ?>, // total pages
				page: 1,                      // default page
				maxVisible: 10,               // visible pagination
				leaps: true                   // next/prev leaps through maxVisible
			}).on("page", function(event, page){
				// ajax loading...
				$("#<?php echo $this->identifier.'-'.$this->getItem()->id; ?> .items").load("<?php echo JURI::base().'index.php?option=com_zoo&task=callelement&item_id='.$this->getItem()->id.'&element='.$this->identifier.'&method=getByPage&tmpl=component&args[0]='; ?>"+page);
			}); 
		});
	-->
	</script>
	<?php endif; ?>
</div>
