<?php
/**
* @package   com_zoo
* @author    Mustaq Sheikh http://herdboy.com * Form Developed by Jason Lau http://jasonlau.biz/
* @copyright Copyright (C) 2011 HerdBoy Web Design cc
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/
/**
 * @version 1.0.0
 */

// joomla constant to disable direct access to this file
defined('_JEXEC') or die('Restricted access');

// Authorization cookie contents. Don't change this unless you know what you're doing.
define("AUTHORIZATION_CODE", md5($_SERVER["HTTP_HOST"] . $_SERVER["REMOTE_ADDR"] . $_SERVER["HTTP_USER_AGENT"] . date("M d, Y") . base64_encode(EMAIL_TO)));

// Add a random aspect to the auth code
$rand = rand(100,1000);
$auth_code = AUTHORIZATION_CODE.$rand;

// Assemble cookie content, encode variables.
$cookie_content = $auth_code. "," . base64_encode(EMAIL_TO) . "," . base64_encode(EMAIL_SUBJECT) . "," . base64_encode(SUCCESS_MESSAGE) . "," . base64_encode(PAGE_PATH) . "," . base64_encode(INCLUDE_FOOTER);

// Set the form submission authorization cookie. This ensures the user of the form and the user of send.php are the same.
setcookie("scauth", $cookie_content, time()+60*60, '/', $_SERVER["HTTP_HOST"]);
?>
<script type="text/javascript">
	<!--
    (function($){
	// increase the default animation speed to exaggerate the effect
	$.fx.speeds._default = 1000;
	$(function() {
	   // initiate the overlay
		$("#dialog<?php echo $rand; ?>").dialog({
			autoOpen: false,
			show: "<?php echo DIALOG_ANIMATION_IN; ?>",
			hide: "<?php echo DIALOG_ANIMATION_OUT; ?>",
      modal: true,
      width: "<?php echo DIALOG_WIDTH; ?>"
		});

        // set the action for the overlay opener
        $(".super_contact<?php echo $rand; ?>").each(function(){
            $(this).bind('click',function(){
                $("#dialog<?php echo $rand; ?>").dialog("open");
            });
        });

        // set the action for the reset button
        $("#reset<?php echo $rand; ?>").click(function(){
            // clear all values and error classes
            $("#name_first<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            $("#name_last<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            $("#email<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            $("#email2<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            $("#tele<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            $("#message<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            $("#cc<?php echo $rand; ?>").val('').removeClass('ui-state-error');
            // clear html and class for paragraphs whose id attribute contains the text "helper"
            $('p[id*="helper<?php echo $rand; ?>"]').each(function(){
                $(this).html('').removeClass('ui-state-highlight');
            });
        });

        // set the action for the submit button. require mouse-up action.
        $("#sub-button<?php echo $rand; ?>").mouseup(function(){
            // set the variables, trim the whitespace from the values, and validate the form fields
            var pass = true,
            first_name = (!$("#name_first<?php echo $rand; ?>").val()) ? '' : trim($("#name_first<?php echo $rand; ?>").val()),
            last_name = (!$("#name_last<?php echo $rand; ?>").val()) ? '' : trim($("#name_last<?php echo $rand; ?>").val()),
            email = (!$("#email<?php echo $rand; ?>").val()) ? '' : trim($("#email<?php echo $rand; ?>").val()),
            email2 = (!$("#email2<?php echo $rand; ?>").val()) ? '' : trim($("#email2<?php echo $rand; ?>").val()),
            tele = (!$("#tele<?php echo $rand; ?>").val()) ? '' : trim($("#tele<?php echo $rand; ?>").val()),
            message = (!$("#message<?php echo $rand; ?>").val()) ? '' : trim($("#message<?php echo $rand; ?>").val());

            // valid characters for first and last name
            var validNameChars = /^( |[^\s])+$/; /* Regular expression changed by Constantin Rack 2011-10-09, was /^[a-zA-Z\'\- ]+$/   */
            if(first_name == '' || !first_name.match(validNameChars)){
                // first name is empty or contains illegal characters
                pass = false;
                $("#name_first-helper<?php echo $rand; ?>").html('<?php echo ERROR_INVALID_NAME; ?>').addClass('ui-state-highlight');
                $("#name_first<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // first name passed
               $("#name_first-helper<?php echo $rand; ?>").html('').removeClass('ui-state-highlight');
               $("#name_first<?php echo $rand; ?>").removeClass('ui-state-error');
            }

            if(last_name == '' || !last_name.match(validNameChars)){
                // last name is empty or contains illegal characters
                pass = false;
                $("#name_last-helper<?php echo $rand; ?>").html('<?php echo ERROR_INVALID_NAME; ?>').addClass('ui-state-highlight');
                $("#name_last<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // last name passed
               $("#name_last-helper<?php echo $rand; ?>").html('').removeClass('ui-state-highlight');
               $("#name_last<?php echo $rand; ?>").removeClass('ui-state-error');
            }

            // validate email address
            if(email == '' || !checkEmail($("#email<?php echo $rand; ?>"),/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i)){
                // email is empty or invalid
               pass = false;
            $("#email-helper<?php echo $rand; ?>").html('<?php echo ERROR_INVALID_EMAIL; ?>').addClass('ui-state-highlight');
            $("#email<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // email passed
               $("#email-helper<?php echo $rand; ?>").html('').removeClass('ui-state-highlight');
               $("#email<?php echo $rand; ?>").removeClass('ui-state-error');
            }

            if(email2 == ''){
                // confirm email is empty or invalid
              pass = false;
            $("#email2-helper<?php echo $rand; ?>").html('<?php echo ERROR_INVALID_CONFIRM_EMAIL; ?>').addClass('ui-state-highlight');
            $("#email2<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // confirm email passed check if email and confirm email match
              if(email != email2){
               // confirm email and email did not match
               pass = false;
            $("#email2-helper<?php echo $rand; ?>").html('<?php echo ERROR_EMAIL_FIELDS_DONT_MATCH; ?>').addClass('ui-state-highlight');
            $("#email2<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // confirm email and email match - continue submission
               $("#email2-helper<?php echo $rand; ?>").html('').removeClass('ui-state-highlight');
               $("#email2<?php echo $rand; ?>").removeClass('ui-state-error');
            }
            }

            if(tele == ''){
                // message is empty - fail
               pass = true;
            $("#message-helper<?php echo $rand; ?>").html('<?php echo $error_message_required; ?>').addClass('ui-state-highlight');
            $("#message<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // message passed
               $("#message-helper<?php echo $rand; ?>").html('').removeClass('ui-state-highlight');
               $("#message<?php echo $rand; ?>").removeClass('ui-state-error');
            }

            if(message == ''){
                // message is empty - fail
               pass = false;
            $("#message-helper<?php echo $rand; ?>").html('<?php echo $error_message_required; ?>').addClass('ui-state-highlight');
            $("#message<?php echo $rand; ?>").addClass('ui-state-error');
            } else {
                // message passed
               $("#message-helper<?php echo $rand; ?>").html('').removeClass('ui-state-highlight');
               $("#message<?php echo $rand; ?>").removeClass('ui-state-error');
            }

            // All fields passed - ajax the data to send.php
            if(pass){
                $.post('<?php echo PATH_TO_PROCESSING_SCRIPT; ?>?r=<?php echo $rand; ?>&nocache=' + Math.floor(Math.random()*1000), {name:first_name+' '+last_name,email:email,email2:email2,tele:tele,message:message,cc:$("#cc<?php echo $rand; ?>").val(),scauth:'<?php echo $auth_code; ?>'},
            function(data){
                // place the returned data in the results div
                $("#results<?php echo $rand; ?>").html(data);
                // div#response is returned by send.php. if the submission was successful, div#response will have the class ui-state-highlight. otherwise, the submission failed. Hide the form on success.
                if($("#response").hasClass('ui-state-highlight')){
                  $('#c-form<?php echo $rand; ?>').hide('slow');
                }
            });
            }
        });
	});

    // function to validate the email
    function checkEmail(o,regexp) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass('ui-state-error');
            return false;
        } else {
            o.removeClass('ui-state-error');
            return true;
        }
    }

    // function to remove whitespace from both ends of a string
    function trim(str, chars) {
        var result = false;
        try{
            result = ltrim(rtrim(str, chars), chars);
        } catch(e){}
        if(result){
            return result;
        }
    }

    // function to remove whitespace from the beginning of a string
    function ltrim(str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
    }

    // function to remove whitespace from the end of a string
    function rtrim(str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    }

})(jQuery);
     -->
</script>
<div id="contact-form<?php echo $rand; ?>">
<div id="dialog<?php echo $rand; ?>" title="<?php echo $dialog_title; ?>" style="display: none;" class="popup-contact">
<div id="results<?php echo $rand; ?>"></div>
<fieldset id="c-form<?php echo $rand; ?>">
<label for="name_first"><?php echo $first_name_label; ?></label>
<input type="name" id="name_first<?php echo $rand; ?>" placeholder="<?php echo $first_name_placeholder; ?>" title="<?php echo $first_name_title; ?>" class="required" />
<p class="helper" id="name_first-helper<?php echo $rand; ?>"></p>
<label for="name_last"><?php echo $last_name_label; ?></label>
<input type="name" id="name_last<?php echo $rand; ?>" placeholder="<?php echo $last_name_placeholder; ?>" title="<?php echo $last_name_title; ?>" class="required" />
<p class="helper" id="name_last-helper<?php echo $rand; ?>"></p>
<label for="email"><?php echo $email_label; ?></label>
<input type="email" id="email<?php echo $rand; ?>" placeholder="<?php echo $email_placeholder; ?>" title="<?php echo $email_title; ?>" class="required email" />
<p class="helper" id="email-helper<?php echo $rand; ?>"></p>
<label for="email2"><?php echo $confirm_email_label; ?></label>
<input type="email" id="email2<?php echo $rand; ?>" placeholder="<?php echo $confirm_email_placeholder; ?>" title="<?php echo $confirm_email_title; ?>" class="required email" />
<p class="helper" id="email2-helper<?php echo $rand; ?>"></p>
<label for="tele"><?php echo $tele_label; ?></label>
<input type="tel" id="tele<?php echo $rand; ?>" placeholder="<?php echo $tele_placeholder; ?>" title="<?php echo $tele_title; ?>" />
<p class="helper" id="email2-helper<?php echo $rand; ?>"></p>
<label for="message"><?php echo $message_label; ?></label>
<textarea id="message<?php echo $rand; ?>"></textarea>
<p class="helper" id="message-helper<?php echo $rand; ?>"></p>
<label for="cc"><?php echo $carbon_copy_label; ?></label>
<select size="1" id="cc<?php echo $rand; ?>" title="<?php echo $carbon_copy_title; ?>">
	<option value="yes" selected="selected"><?php echo $carbon_copy_yes; ?></option>
	<option value="no"><?php echo $carbon_copy_no; ?></option>
</select>
<p class="helper"></p>
<button class="quote-send-btn btn input" id="sub-button<?php echo $rand; ?>"><?php echo $submit_button_label; ?></button>
<input id="reset<?php echo $rand; ?>" class="quote-reset-btn btn input" type="reset" value="<?php echo $reset_button_label; ?>" />
</fieldset>
</div>
</div>
<button class="tall <?php echo $button_color; ?> btn super_contact<?php echo $rand; ?>" id="opener<?php echo $rand; ?>"><?php echo $opener_label; ?></button>