<?php
/**
* @package   mod_supercontact
* @author    Mustaq Sheikh http://herdboy.com
* @copyright Copyright (C) 2011 HerdBoy Web Design cc
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/
/**
 * @version 2.5.0
 */

// get the random number from the request uri (url)
$r = $_GET["r"];

// validate the user that submitted the form. check the auth cookie value against the posted auth value and the posted value against the real value.
if(isset($_COOKIE['scauth'])){
    // cookie exists. extract data
    $cookie_data = explode(",", $_COOKIE['scauth']);
    define("AUTH_CODE", $cookie_data[0]);
    define("EMAIL_TO", base64_decode($cookie_data[1]));
    define("EMAIL_SUBJECT",  base64_decode($cookie_data[2]));
    define("SUCCESS_MESSAGE",  base64_decode($cookie_data[3]));
    define("PAGE_PATH",  base64_decode($cookie_data[4]));
    define("INCLUDE_FOOTER",  base64_decode($cookie_data[5]));
} else {
    // no cookie. reject the submission
    die("<div id=\"response\" class=\"ui-state-error\">Authorization required! Code 1</div>");
}

if($_SERVER["REQUEST_METHOD"] == 'POST' && $_POST['scauth'] == md5($_SERVER["HTTP_HOST"] . $_SERVER["REMOTE_ADDR"] . $_SERVER["HTTP_USER_AGENT"] . date("M d, Y") . $cookie_data[1]) . $r):

  // trim whitespace and validate the submitted data
    $error = '';
    if(trim($_POST['name']) == "" || filter_var(trim($_POST['name']), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(|[^\s])+$/')))) {
        // name is empty or has illegal charcters
        $error .= "Name is missing or invalid. ";
    }
    if(trim($_POST['email']) == "" || !preg_match("/^[a-z0-9][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
        // email is empty or has illegal charcters
        $error .= "Invalid email address. ";
    }
    if(trim($_POST['email2']) == "" || !preg_match("/^[a-z0-9][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email2']))) {
        // email is empty or has illegal charcters
        $error .= "Invalid email address. ";
    }
    if (trim($_POST['email2']) != trim($_POST['email'])) {
        // confirm email and email do not match
        $error .= "Email and confirm email do not match.";
    }
    if (trim($_POST['tele']) == "" || !preg_match("/^(([0-9]{1})*[- .(]*([0-9a-zA-Z]{3})*[- .)]*[0-9a-zA-Z]{3}[- .]*[0-9a-zA-Z]{4})+$/i", trim($_POST['tele']))) {
        // check telephone number format
        //$error .= "Not a valid telephone number.";
    }
    if (trim($_POST['message']) == "") {
        // message is empty
        $error .= "Message is required. ";
    }
    if($error != ''){
        // there was an error, print the error message
     echo "<div id=\"response\" class=\"ui-state-error\">".$error."</div>";
    } else {
        // all fields passed - trim whitespace from the email
        $from = trim($_POST['email']);
        // assemble the message to the administrator. htmlentities() is used on the message to thwart malicious code

		// Next 2 lines added by Constantin Rack 2011-10-09
		$tele = filter_var($_POST['tele'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		$msg = filter_var($_POST['message'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $message = "Name:\n".$_POST['name']."\n\nTelephone:\n$tele\n\nMessage:\n$msg\n\n";

        // check footer option and append stats
        if(INCLUDE_FOOTER){
          $message .= "Message Sent Via:\nhttp://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]."\nFrom IP:\n".$_SERVER["REMOTE_ADDR"]."\nFrom User Agent:\n".$_SERVER["HTTP_USER_AGENT"]."\nOriginating page:\n".PAGE_PATH."\n\n";
        }

        // dispatch the first email to the administrator
        mail(EMAIL_TO, EMAIL_SUBJECT, $message, "From: " . $from . "\nX-Mailer: PHP/" . phpversion());

        // check carbon copy setting
    if($_POST['cc'] == "yes") {
        // assemble a separate message for the user. this version excludes the url path, ip address, and user agent. htmlentities() is used on the message to prevent malicious code
        $to = trim($_POST['email']);
        $subject = "Carbon Copy: " . EMAIL_SUBJECT;
        // Line commented out by Constantin Rack 2011-10-09
        /* $message = htmlentities($_POST['message'],ENT_NOQUOTES); */
        // dispatch the second email to the user
        mail($to, $subject, $message, "From: DoNotReply@" . $_SERVER["HTTP_HOST"] . "\nX-Mailer: PHP/" . phpversion());
    }
      // process was successful
      echo "<div id=\"response\" class=\"ui-state-highlight\">".SUCCESS_MESSAGE."\n</div>";
    }
else:
// user validation failed. reject the submission
die("<div id=\"response\" class=\"ui-state-error\">Authorization required! Code 2</div>");
endif;

?>