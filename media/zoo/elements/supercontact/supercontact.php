<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// register ElementRepeatable class
App::getInstance('zoo')->loader->register('ElementRepeatable', 'elements:repeatable/repeatable.php');

/*
   Class: ElementLink
       The link element class
*/
class ElementSupercontact extends ElementRepeatable implements iRepeatSubmittable {

	/*
		Function: _hasValue
			Checks if the repeatables element's value is set.

	   Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/
		public function hasValue($params = array()) {
		//$value = $this->get('value', '');
		$params = $this->app->data->create($params);
		$recipient = $this->get('recipient') ? $this->get('recipient') : $params->get('recipient', '');
		return !empty($recipient);
		//return !empty($value) && !empty($recipient);
	}

	/*
		Function: getOpener Label
			Gets the link text for the Call to action button.

		Returns:
			String - opener_label
	*/
	public function getOpener_label() {
		$opener_label = $this->get('opener_label', '');
		return empty($opener_label) ? $this->get('value', '') : $opener_label;
	}
	/*
		Function: getSuccess_message
			Gets the text shown to the form submitter on sucessfull form submission.

		Returns:
			String - text
	*/

	public function getSuccess_message() {
		$success_message = $this->get('success_message', '');
		return empty($success_message) ? $this->get('value', '') : $success_message;
	}
	/*
		Function: getDialog_title
			Gets the text show as the title of the popup dialog.

		Returns:
			String - text
	*/

	public function getDialog_title() {
		$dialog_title = $this->get('dialog_title', '');
		return empty($dialog_title) ? $this->get('value', '') : $dialog_title;
	}
	/*
		Function: getButton_color
			Gets the used for the button.

		Returns:
			String - text
	*/

	public function getButton_color() {
		$button_color = $this->get('button_color', '');
		return empty($button_color) ? $this->get('value', '') : $button_color;
	}

	/*
		Function: render
			Renders the repeatable element.

	   Parameters:
            $params - render parameter

		Returns:
			String - html
	*/
	protected function _render($params = array()) {

		$params = $this->app->data->create($params);

		$recipient			= $this->get('recipient') ? $this->get('recipient') : $params->get('recipient', '');
		$opener_label		= $this->get('opener_label') ? $this->get('opener_label') : $params->get('opener_label', '');
		$success_message	= $this->get('success_message') ? $this->get('success_message') : $params->get('success_message', '');
		$dialog_title 		= $this->get('dialog_title') ? $this->get('dialog_title') : $params->get('dialog_title', '');
		$button_color 		= $this->get('button_color') ? $this->get('button_color') : $params->get('button_color', '');

		$include_footer	= $this->config->get('include_footer', '');
		$subject	= $this->_item->name;

        // Errors
		$error_invalid_name	= $this->config->get('error_invalid_name', '');
		$error_invalid_email	= $this->config->get('error_invalid_email', '');
		$error_invalid_confirm_email	= $this->config->get('error_invalid_confirm_email', '');
		$error_invalid_fields_dont_match	= $this->config->get('error_invalid_fields_dont_match', '');
		$error_message_required	= $this->config->get('error_message_required', '');
        // Form language
		$first_name_label	= $this->config->get('first_name_label','');
		$first_name_placeholder	= $this->config->get('first_name_placeholder', '');
		$first_name_title	= $this->config->get('first_name_title', '');
		$last_name_label	= $this->config->get('last_name_label', '');
		$last_name_placeholder	= $this->config->get('last_name_placeholder', '');
		$last_name_title	= $this->config->get('last_name_title', '');
		$email_label	= $this->config->get('email_label', '');
		$email_placeholder	= $this->config->get('email_placeholder', '');
		$email_title	= $this->config->get('email_title', '');
		$confirm_email_label	= $this->config->get('confirm_email_label', '');
		$confirm_email_placeholder	= $this->config->get('confirm_email_placeholder', '');
		$confirm_email_title	= $this->config->get('confirm_email_title', '');
		$tele_label	= $this->config->get('tele_label', '');
		$tele_placeholder	= $this->config->get('tele_placeholder', '');
		$tele_title	= $this->config->get('tele_title', '');
		$message_label	= $this->config->get('message_label', '');
		$carbon_copy_label	= $this->config->get('carbon_copy_label', '');
		$carbon_copy_title	= $this->config->get('carbon_copy_title', '');
		$carbon_copy_yes	= $this->config->get('carbon_copy_yes', '');
		$carbon_copy_no	= $this->config->get('carbon_copy_no', '');
		$submit_button_label	= $this->config->get('submit_button_label', '');
		$reset_button_label	= $this->config->get('reset_button_label', '');
		$dialog_width	= $this->config->get('dialog_width', '');
		$dialog_animation_in	= $this->config->get('dialog_animation_in', '');
		$dialog_animation_out	= $this->config->get('dialog_animation_out', '');
		$dialog_animation_speed	= $this->config->get('dialog_animation_speed', '');

        $this->app->document->addScript('libraries:jquery/jquery-ui.custom.min.js');
        $this->app->document->addStylesheet('elements:supercontact/assets/css/jquery.ui.all.css');

        // The settings for this script

        // Include the footer stats in the recipient email. Boolean value.
        define("INCLUDE_FOOTER", $include_footer);
        // The recipient email address, subject and success message. The form will send email to this address.
        define("EMAIL_TO", $recipient);
        // The url to send.php. You can rename the file send.php and define it's path here
        define("PATH_TO_PROCESSING_SCRIPT", JURI::base( true ).'/media/zoo/elements/supercontact/send.php');

        // The email subject.
        define("EMAIL_SUBJECT", $subject);
        // The message the user will see when the email is sent successfully.
        define("SUCCESS_MESSAGE", $success_message);
        // Errors
        define("ERROR_INVALID_NAME", $error_invalid_name);
        define("ERROR_INVALID_EMAIL", $error_invalid_email);
        define("ERROR_INVALID_CONFIRM_EMAIL", $error_invalid_confirm_email);
        define("ERROR_EMAIL_FIELDS_DONT_MATCH", $error_invalid_fields_dont_match);
        define("ERROR_MESSAGE_REQUIRED", $error_message_required);
        // Form language
        define("DIALOG_WIDTH", $dialog_width);
        define("DIALOG_ANIMATION_IN", $dialog_animation_in);
        define("DIALOG_ANIMATION_OUT", $dialog_animation_out);
        define("DIALOG_ANIMATION_SPEED", $dialog_animation_speed);
        // End settings
        define("PAGE_PATH", "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
        include("contact_form.php");

	    }

	/*
	   Function: _edit
	       Renders the repeatable edit form field.

	   Returns:
	       String - html
	*/
	protected function _edit(){
		return $this->_editForm();
	}

	/*
		Function: _renderSubmission
			Renders the element in submission.

	   Parameters:
            $params - AppData submission parameters

		Returns:
			String - html
	*/
	public function _renderSubmission($params = array()) {
        return $this->_editForm($params->get('trusted_mode'));
	}

	protected function _editForm($trusted_mode = true) {
        if ($layout = $this->getLayout('edit.php')) {
            return $this->renderLayout($layout,
                compact('trusted_mode')
            );
        }
	}

	/*
		Function: _validateSubmission
			Validates the submitted element

	   Parameters:
            $value  - AppData value
            $params - AppData submission parameters

		Returns:
			Array - cleaned value
	*/
	public function _validateSubmission($value, $params) {
        $values       = $value;

        $validator  = $this->app->validator->create('string', array('required' => false));
        $recipient  = $validator->clean($values->get('recipient'));
        $opener_label   = $validator->clean($values->get('opener_label'));
        $dialog_title   = $validator->clean($values->get('dialog_title'));
        $button_color   = $validator->clean($values->get('button_color'));
        $success_message   = $validator->clean($values->get('success_message'));

        $value        = $this->app->validator
				->create('url', array('required' => $params->get('required')), array('required' => 'Please enter an URL.'))
				->clean($values->get('value'));

		return compact('value', 'recipient', 'opener_label', 'dialog_title', 'button_color', 'success_message');
    }

}