<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<div>


    <?php echo $this->app->html->_('control.text', $this->getControlName('recipient'), $this->get('recipient'), 'size="60" title="'.JText::_('Recipients Email Address').'" placeholder="'.JText::_('Recipients Email Address').'"'); ?>
    <?php if ($trusted_mode) : ?>

	<div class="more-options">
		<div class="trigger">
			<div>
				<div class="advanced button hide"><?php echo JText::_('Hide Form Options'); ?></div>
				<div class="advanced button"><?php echo JText::_('Click to set Form Options'); ?></div>
			</div>
		</div>

		<div class="advanced options">

			<div class="row">
				<?php echo $this->app->html->_('control.text', $this->getControlName('opener_label'), $this->get('opener_label'), 'size="60" title="'.JText::_('Text for the Button').'" placeholder="'.JText::_('Text for the Button eg Contact Us').'"'); ?>
			</div>

			<div class="row">
				<?php echo $this->app->html->_('control.text', $this->getControlName('success_message'), $this->get('success_message'), 'size="60" title="'.JText::_('Success Message').'" placeholder="'.JText::_('Success Message').'"'); ?>
			</div>

			<div class="row">
				<?php echo $this->app->html->_('control.text', $this->getControlName('dialog_title'), $this->get('dialog_title'), 'size="60" title="'.JText::_('Form Title').'" placeholder="'.JText::_('Form Title - Short and Sweet').'"'); ?>
			</div>

			<div class="row">
				<?php echo $this->app->html->_('control.text', $this->getControlName('button_color'), $this->get('button_color'), 'size="60" title="'.JText::_('Button Color').'" placeholder="'.JText::_('Button Color eg green').'"'); ?>
			</div>

		</div>
	</div>

    <?php endif; ?>

</div>