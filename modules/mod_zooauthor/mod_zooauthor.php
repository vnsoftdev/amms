<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       mod_zooauthor.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

// get app
$zoo = App::getInstance('zoo');

// load zoo frontend language file
$zoo->system->language->load('com_zoo');
$zoo->zooauthor->loadLanguage();

// get User Profile
$profile = $zoo->zooauthor->getProfile();

include(JModuleHelper::getLayoutPath('mod_zooauthor', $params->get('theme', 'default')));
