<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       default.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include css
$zoo->document->addStylesheet('root:modules/mod_zooauthor/tmpl/default/style.css');
?>

<?php if ($zoo->user->get()->id == 0) : ?>
	<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >		
		<fieldset class="userdata">
		<p id="form-login-username">
			<label for="modlgn-username"><?php echo JText::_('MOD_ZOOAUTHOR_VALUE_USERNAME') ?></label>
			<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" />
		</p>
		<p id="form-login-password">
			<label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label>
			<input id="modlgn-passwd" type="password" name="password" class="inputbox" size="18"  />
		</p>
		<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
		<p id="form-login-remember">
			<label for="modlgn-remember"><?php echo JText::_('MOD_ZOOAUTHOR_REMEMBER_ME') ?></label>
			<input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
		</p>
		<?php endif; ?>
		<input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN') ?>" />
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.login" />
		<input type="hidden" name="return" value="<?php echo base64_encode(JFactory::getURI()); ?>" />
		<?php echo JHtml::_('form.token'); ?>
		</fieldset>
		<ul>
			<li>
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
				<?php echo JText::_('MOD_ZOOAUTHOR_FORGOT_YOUR_PASSWORD'); ?></a>
			</li>
			<li>
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
				<?php echo JText::_('MOD_ZOOAUTHOR_FORGOT_YOUR_USERNAME'); ?></a>
			</li>
			<?php
			$usersConfig = JComponentHelper::getParams('com_users');
			if ($usersConfig->get('allowUserRegistration')) : ?>
			<li>
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
					<?php echo JText::_('MOD_ZOOAUTHOR_REGISTER'); ?></a>
			</li>
			<?php endif; ?>
		</ul>		
	</form>
<?php else : ?>
	<div class="zooauthor">		
		<?php echo $zoo->zooauthor->getProfileHTML($profile, 'module', $params); ?>
	</div>
	<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">	
		<div class="logout-button">
			<input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGOUT'); ?>" />
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="user.logout" />
			<input type="hidden" name="return" value="<?php echo base64_encode(JFactory::getURI()); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
<?php endif;
