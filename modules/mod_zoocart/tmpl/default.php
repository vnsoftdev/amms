<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// init vars
$user = JFactory::getUser();
$app_id = $params->get('application', ($zoo->zoo->getApplication() ? $zoo->zoo->getApplication()->id : null));

?>

<div class="zoocart-smallcart">

	<!-- items -->
	<?php if ($items): ?>

		<!-- cart items -->
		<?php if ($params->get('show_items', true)) : ?>
			<div class="zoocart-smallcart-items">

				<?php foreach ($items as $item): ?>
				<div class="zoocart-smallcart-item">
					<a href="<?php echo $zoo->route->item($item->getItem()); ?>"><?php echo $item->getItem()->name; ?></a> x<?php echo $item->quantity; ?>
				</div>
				<?php endforeach; ?>

				<hr />

				<!-- totals -->
				<strong><?php echo JText::_('PLG_ZOOCART_SUBTOTAL'); ?>:</strong> <?php 
					echo $zoo->zoocart->currency->format($app_id, $zoo->zoocart->cart->getSubtotal());
				?><br />
				<strong><?php echo JText::_('PLG_ZOOCART_TAXES'); ?>:</strong> <?php 
					echo $zoo->zoocart->currency->format($app_id, $zoo->zoocart->cart->getTaxes());
				?><br />
				<strong><?php echo JText::_('PLG_ZOOCART_TOTAL'); ?>:</strong> <?php 
					echo $zoo->zoocart->currency->format($app_id, $zoo->zoocart->cart->getTotal($app_id));
				?>

			</div>
			<hr />
		<?php endif; ?>

		<!-- cart links -->
		<?php if($params->get('show_cart_link', true)): ?>
			<a href="<?php echo $zoo->link(array('controller' => 'cart', 'app_id' => $app_id, 'Itemid' => $params->get('cart_itemid'))); ?>"><?php echo JText::_('PLG_ZOOCART_VIEW_CART_CHECKOUT'); ?></a><br />
		<?php endif; ?>

	<!-- if cart empty -->
	<?php else: ?>
		<div class="zoocart-empty-cart">
		<?php echo JText::_('PLG_ZOOCART_EMPTY_CART'); ?>
		</div>
	<?php endif; ?>

	<!-- orders links -->
	<?php if($params->get('show_orders_link', true) && !$user->guest): ?>
		<a href="<?php echo $zoo->link(array('controller' => 'orders', 'app_id' => $app_id, 'Itemid' => $params->get('orders_itemid'))); ?>"><?php echo JText::_('PLG_ZOOCART_ORDER_MY_ORDERS'); ?></a><br />
	<?php endif; ?>

	<!-- addresses link -->
	<?php if($params->get('show_addresses_link', true) && !$user->guest): ?>
		<a href="<?php echo $zoo->link(array('controller' => 'addresses', 'app_id' => $app_id, 'Itemid' => $params->get('addresses_itemid'))); ?>"><?php echo JText::_('PLG_ZOOCART_ADDRESS_MY_ADDRESSES'); ?></a><br />
	<?php endif; ?>

</div>