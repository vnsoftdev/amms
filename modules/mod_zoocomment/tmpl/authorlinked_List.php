<?php
/**
* @package   ZOO Comment
* @author    YOOtheme http://www.yootheme.com, modified by Attavus M.D. http://www.raslab.org
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include css
$zoo->document->addStylesheet('mod_zoocomment:tmpl/list/style.css');

?>

<?php if (count($comments)) : ?>

<section class="zoo-comments-list">

	<?php $i = 0; foreach ($comments as $comment) : ?>

		<?php // set author data
			$author = $comment->getAuthor();
			$author->name = $author->name ? $author->name : JText::_('COM_ZOO_ANONYMOUS');
			if($author->user_id && ($authorlinks = $comment->getItem()->getElementsByType('authorlink'))) {
				foreach($authorlinks as $one) {
					$authorlink = $one;
					break;
				}
				$authorName   = $authorlink->activeAuthor($author->user_id)->getName(true);
				$authorAvatar = $authorlink->activeAuthor($author->user_id)->getAvatar();
				$authorLink   = $authorlink->activeAuthor($author->user_id)->getLink();
			}
		?>

		<article class="<?php if ($author->isJoomlaAdmin()) echo 'comment-byadmin'; ?>">

			<?php if ($params->get('show_avatar', 1)) : ?>
			<div class="avatar">
				<?php if ($author->url || (isset($authorLink))) : ?><a href="<?php echo isset($authorLink) ? $authorLink : JRoute::_($author->url); ?>" title="<?php echo isset($authorLink) ? $authorName : $author->url; ?>" rel="nofollow"><?php endif; ?>
				<?php echo isset($authorlink) ? '<img alt="'.$authorName.'" title="'.$authorName.'" src="'.$authorAvatar.'" height="'.$params->get('avatar_size', 50).'px" width="'.$params->get('avatar_size', 50).'px" />' : $author->getAvatar($params->get('avatar_size', 50)); ?>
				<?php if ($author->url || (isset($authorLink))) : ?></a><?php endif; ?>
			</div>
			<?php endif; ?>

			<?php if ($params->get('show_author', 1)) : ?>
			<h4 class="author">
				<?php if ($author->url || (isset($authorLink))) : ?><a href="<?php echo isset($authorLink) ? $authorLink : JRoute::_($author->url); ?>" title="<?php echo isset($authorLink) ? $authorName : $author->url; ?>" rel="nofollow"><?php endif; ?>
				<?php echo isset($authorLink) ? $authorName : $author->name; ?>
				<?php if ($author->url || (isset($authorLink))) : ?></a><?php endif; ?>
			</h4>
			<?php endif; ?>

			<?php if ($params->get('show_meta', 1)) : ?>
			<p class="meta">
				<?php echo $zoo->html->_('date', $comment->created, $zoo->date->format(JText::_('ZOO_COMMENT_MODULE_DATE_FORMAT')), $zoo->date->getOffset()); ?>
				| <a class="permalink" href="<?php echo JRoute::_($zoo->route->comment($comment)); ?>">#</a>
			</p>
			<?php endif; ?>

			<div class="content"><p><?php echo $zoo->comment->filterContentOutput($zoo->string->truncate($comment->content, $zoo->get('commentsmodule.max_characters'))); ?></p></div>

		</article>

	<?php $i++; endforeach; ?>

</section>

<?php else : ?>
	<?php echo JText::_('COM_ZOO_NO_COMMENTS_FOUND'); ?>
<?php endif;
