<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class modZOOcompareHelper 
{
	public static function getItems($type)
	{
		$zoo = App::getInstance('zoo');
		$items = $zoo->system->session->get('com_zoo.compare.'.$type.'.items', array());
		
		foreach($items as &$item)
		{
			$item = $zoo->table->item->get($item);
		}
		
		return array_filter($items);
	}
}