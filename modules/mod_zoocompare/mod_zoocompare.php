<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// load config & Helper
require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');
require_once(dirname(__FILE__).'/helper.php');

// get app
$zoo = App::getInstance('zoo');

// load zoo frontend language file
$zoo->system->language->load('com_zoo');

// set params data
$params = $zoo->data->create($params->toArray());

if ($application = $params->find('application._chosenapps')){

	// init vars
	$app_id 	 = $application;
	$type_id	 = $params->find('application._chosentypes');
	$items 		 = modZOOcompareHelper::getItems($type_id);
	
	// errors notice
	$errors = '';
	if (!JPluginHelper::isEnabled('system', 'zoocompare')) $errors .= JText::sprintf('PLG_ZLFRAMEWORK_PLUGIN_DISABLED', 'ZOOcompare').'<br />';
	
	// render layout
	if (empty($errors)){

		// set renderer
		$renderer = $zoo->renderer->create('item')->addPath(array($zoo->path->path('component.site:'), dirname(__FILE__)));
		$renderer_layout = basename($params->find('layout._renderer_layout', 'default'), '.php');
		
		include(JModuleHelper::getLayoutPath('mod_zoocompare', basename($params->find('layout._layout', 'default'), '.php')));
		
	} else {
		echo $errors;
	}
}