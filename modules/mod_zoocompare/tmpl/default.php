<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	// load assets
	$zoo->document->addScript('zoocompare:assets/js/compare.min.js');
	$zoo->document->addStylesheet('zoocompare:assets/css/compare.css');
	$zoo->document->addStylesheet('modules:mod_zoocompare/tmpl/default/style.css');

	// load frontend assets
	$zoo->zlfw->loadLibrary('frontendui');

	// init vars
	$url = $zoo->link(array('controller' => 'compare'), false);

	$app_params = $zoo->table->application->get($app_id)->params->get('global.zoocompare.layout');

	// call once time the script
	if (!defined('ZOOCOMPARE_ELEMENTS_SCRIPT_DECLARATION')) {
		define('ZOOCOMPARE_ELEMENTS_SCRIPT_DECLARATION', true);

		// save the app id in session for comparing later use
		$zoo->system->session->set('com_zoo.zoocompare.app_id', $app_id);

		// init js functions
		$javascript = 'jQuery(function($) { $("body").ZOOcompare({ app_id: '.$app_id.', url: "'.$url.'", app_params: '.json_encode($app_params).', total_items: '.count($items).', txtAddToCompare: "'.JText::_('PLG_ZOOCOMPARE_ADD_TO_COMPARE').'", txtCompare: "'.JText::_('PLG_ZOOCOMPARE_COMPARE').'", txtRemove: "'.JText::_('PLG_ZOOCOMPARE_REMOVE').'" }) });';
		
		$zoo->document->addScriptDeclaration($javascript);
	}
?>

<div class="zoocompare-module zoocompare-module-<?php echo $type_id; ?> zl-bootstrap" data-zc-renderer-layout="<?php echo $renderer_layout ?>" data-zc-style="<?php echo $params->find('layout._style', 'vertical') ?>" data-zc-type="<?php echo $type_id; ?>">

	<div class="container-fluid">
		<?php if (!empty($items)) : $i = 0; foreach ($items as $item) : ?>
		<?php 
			// item data
			$item_data  = array(
				'id' => $item->id,
				'type' => $type_id
			);
		?>
		<div class="row" data-item-id="<?php echo $item->id ?>" data-item-data='<?php echo json_encode($item_data) ?>'>
			<?php
				$item_row = $renderer->render('item.'.$renderer_layout, compact('item', 'params'));
				$positions = $renderer->getPositions('item.'.$renderer_layout);
				$positions = array_pop($positions);
				
				// check if the rendered layout has been populated
				$populated = $renderer->checkPositions('item.'.$renderer_layout);

				// if layout not populated render the raw name
				$row = $populated ? $item_row : $item->name;
			?>

			<div class="content"><?php echo $row ?></div>
			<button type="button" class="btn btn-mini zoocompare remove"><?php echo JText::_('PLG_ZOOCOMPARE_REMOVE') ?></button>
		</div>
		<?php $i++; endforeach; endif;?>
	</div>
	
	<div class="btns" style="display: <?php echo empty($items) ? 'none' : 'block' ?>;">
		<button type="button" class="btn btn-mini zoocompare compare btn-success"><?php echo JText::_('PLG_ZOOCOMPARE_COMPARE'); ?></button>
		<button type="button" class="btn btn-mini zoocompare removeall btn-danger"><?php echo JText::_('PLG_ZOOCOMPARE_REMOVE_ALL'); ?></button>
	</div>
	
</div>