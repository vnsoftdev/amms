<?php
/**
* @package		ZOOcompare
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"_renderer_layout":{
			"type":"layout",
			"label":"PLG_ZOOCOMPARE_MOD_ITEM_ROW_LAYOUT",
			"help":"PLG_ZOOCOMPARE_MOD_ITEM_ROW_LAYOUT_DESC",
			"specific": {
				"path":"modules:mod_zoocompare/renderer/item",
				"regex":'.json_encode('^([^_][_A-Za-z0-9]*)\.php$').'
			}
		},
		"_style":{
			"type":"select",
			"label":"PLG_ZLFRAMEWORK_STYLE",
			"help":"PLG_ZOOCOMPARE_MOD_LAYOUT_STYLE_DESC",
			"default":"vertical",
			"specific":{
				"options":{
					"PLG_ZLFRAMEWORK_VERTICAL":"vertical",
					"PLG_ZLFRAMEWORK_HORIZONTAL":"horizontal"
				}
			}
		}

	}}';