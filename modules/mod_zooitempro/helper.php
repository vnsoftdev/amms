<?php
/**
* @package		ZOOitem Pro
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * The ZOOitem Pro helper class.
 *
 * @since 3.0
 */
class mod_zooitemproHelper
{
	/**
	 * Get the filtered and ordered items objects
	 * 
	 * @param Params The module params
	 *
	 * @since 3.0
	 */
	public static function getItems($params)
	{
		// init vars
		$zoo = App::getInstance('zoo');
		$cache = $params->find('layout.cache_time', null);

		// set model
		$model = $zoo->zlmodel->getNew('item');

		// get cache, refreshes after one hour automatically
		$cache = $cache ? $zoo->cache->create($zoo->path->path('cache:') . '/mod_zooitempro', true, $cache, 'apc') : null;
		if ($cache && !$cache->check()) {
			$cache = null;
		}

		// get Items
		$result = null;
		$key = md5(serialize($params));

		// use cached IDs to make simpler query
		if ($cache && $ids = $cache->get($key)) 
		{
			// set items ids
			$model->id($ids);

			// set order
			$order = array_values((array)$params->find('items.itemorder', array('_itemname')));
			$model->setState('order_by', $order);

			// perform query
			$result = $model->getList();
		} 

		// or perform full query
		else 
		{
			self::setQuery($model, $params);
			$result = $model->getList();
			if ($cache) {
				// save Items IDs to cache
				$ids = array_keys($result);
				$cache->set($key, $ids);
				$cache->save();
			}
		}

		// pretty print of sql
		if (JDEBUG) {
			$find = Array("FROM", "WHERE", "AND", "ORDER BY", "LIMIT", "OR");
			$replace = Array("<br />FROM", "<br />WHERE", "<br />AND", "<br />ORDER BY", "<br />LIMIT", "<br />OR");
			$in = $model->getQuery();
			echo str_replace($find, $replace, $in);
			echo '<br /><br />Total Items: '.count($result);
		}
		
		// return Items objects
		return $result;
	}

	/**
	 * Retreieve Items Objects
	 * 
	 * @param Model The Items Model Object
	 * @param Params The module params
	 *
	 * @since 3.2
	 */
	public static function setQuery(&$model, $params)
	{
		// set query
		$model->setState('select', 'DISTINCT a.*');

		// set ids
		if ($ids = $params->find('items._ids')) {
			$model->id($ids);
		}

		// set apps
		if ($apps = $params->find('items._chosenapps')) foreach ($apps as $app) {
			$model->application(array('value'  => $app));
		}

		// set categories
		if ($categories = $params->find('items._chosencats')) {
			$model->categories(array('value'  => $categories));
		}

		// set types
		if ($types = $params->find('items._chosentypes')) foreach ($types as $type) {
			$model->type(array('value'  => $type));
		}

		// set item state
		$model->state($params->find('items.itemfilter._state', 1));

		// set limits
		$model->setState('limitstart', $params->find('items.itemfilter._offset', 0), true);
		$model->setState('limit', $params->find('items.itemfilter._limit', 0), true);

		// set created
		if ($params->find('items.itemfilter.dates.created._filter')) {
			$model->created($params->find('items.itemfilter.dates.created'));
		}

		// set modified
		if ($params->find('items.itemfilter.dates.modified._filter')) {
			$model->modified($params->find('items.itemfilter.dates.modified'));
		}

		// set published
		if ($params->find('items.itemfilter.dates.published._filter')) {
			$model->published($params->find('items.itemfilter.dates.published'));
		}

		// set elements
		foreach ($params->find('items.itemfilter.elements', array()) as $id => $options) {
			
			// init
			$options['id'] = $id;
			$value = @$options['value'];
			$filter = @$options['_filter'];

			// skip if filter not enabled
			if (!isset($filter) || empty($filter)) continue;

			// range workaround
			if (!is_array($value)) {
				$options['from'] = $value;
				$options['to'] = @$options['value_to'];
			}

			// // workaround for period date query, the value must be set
			if (@$options['is_date'] == 1 && $options['type'] == 'period') {
				$options['value'] = true;
			}

			// set filter
			$model->element($options);
		}

		// set order
		$order = array_values((array)$params->find('items.itemorder', array('_itemname')));
		$model->setState('order_by', $order);
	}

	/**
	 * Get the filtered and ordered items objects trough ajax
	 * 
	 * @param Params The module params
	 *
	 * @since 3.0
	 */
	public static function callback($params)
	{
		return self::getItems($params);
	}
}