<?php
/**
* @package		ZOOitem Pro
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include css
// $zoo->document->addStylesheet('mod_zooitempro:tmpl/default/style.css');

// get items
$items = mod_zooitemproHelper::getItems($params);

?>

<?php if (!empty($items)) : ?>

<ul class="zoo-itempro-default zoo-deafult">
	<?php $i = 0; foreach ($items as $item) : ?>
	<li><?php echo $renderer->render('item.'.$renderer_layout, compact('item', 'params')); ?></li>
	<?php $i++; endforeach; ?>
</ul>

<?php else : ?>
<?php echo JText::_('MOD_ZOOITEMPRO_NO_ITEMS_FOUND'); ?>
<?php endif; ?>