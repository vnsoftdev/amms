<?php
/**
* @package		ZOOitem Pro
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"cache_time": {
			"type":"text",
			"label":"MOD_ZOOITEMPRO_FIELD_CACHE_TIME_LABEL",
			"help":"MOD_ZOOITEMPRO_FIELD_CACHE_TIME_DESC",
			"default":"3600"
		},
		"itemsfilter":{
			"type":"subfield",
			"path":"modules:mod_zooitempro\/fields\/zlitemsfilter.php"
		}

	}}';