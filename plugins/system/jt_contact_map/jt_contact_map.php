<?php
/**
 * JT Contact Map 2.5.1
 * @package jt_contact_map
 * @copyright (C) 2012 http://www.joomtut.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomtut.com
 **/
defined('JPATH_BASE') or die;
class plgSystemJt_contact_map extends JPlugin {
	public function OnAfterRender() {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$plugin =& JPluginHelper::getPlugin('system', 'jt_contact_map');
		$params = new JRegistry($plugin->params);
		if($app->isSite() && ($option == "com_contact")) {
			$apiKey = $params->get('apiKey','');
			$mapWidth = $params->get('mapWidth','500px');
			$mapHeight = $params->get('mapHeight','400px');
			$latitude = $params->get('latitude','0');
			$longitude = $params->get('longitude','0');
			$zoom = $params->get('zoom','0');
			$mapType = $params->get('mapType','ROADMAP');
			$resetButton = $params->get('resetButton','1');
			$errorText = $params->get('errorText','');
			$resetText = $params->get('resetText','Reset');
			$mapCss = $params->get('mapCss','');
			$mapLanguage = $params->get('mapLanguage','');
			$marker = $params->get('marker','1');
			$infoTitle = $params->get('infoTitle','');
			$infoContent = $params->get('infoContent','');
			$infoContent = preg_replace('/[\r\n]+/', '<br>', $infoContent);
			$infoImg = $params->get('infoImg','');
			if ($infoImg != "") {
				$infoImg = '/'.$infoImg;
			}
			$infoCss = $params->get('infoCss','');
			if ($mapLanguage == "") {
				$mapLanguage = JFactory::getLanguage()->getTag();
			}
		
			if ($apiKey == "") {
				$html = '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&amp;language='.$mapLanguage.'"></script>';
			}
			else {
				$html = '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key='.$apiKey.'&sensor=false&amp;language='.$mapLanguage.'"></script>';
			}
			$html .= '<script type="text/javascript">'. "\n";
			$html .= 'var geocoder;'. "\n";
			$html .= 'var map;'. "\n";
			$html .= 'function initialize() {'. "\n";
			$html .= 'geocoder = new google.maps.Geocoder();'. "\n";
			$html .= 'var defaultMap = new google.maps.LatLng('.$latitude.','.$longitude.');'. "\n";
			$html .= 'var mapOptions = {'. "\n";
			$html .= 'zoom: '.$zoom.','. "\n";
			$html .= 'center: defaultMap,'. "\n";
			$html .= 'mapTypeId: google.maps.MapTypeId.'.$mapType;
			$html .= '}'. "\n";
			$html .= 'var image = \''.$infoImg.'\';'. "\n";
			$html .= 'var marker = new google.maps.Marker({'. "\n";
			$html .= 'position: defaultMap,'. "\n";
			$html .= 'icon: image,'. "\n";
			$html .= 'title:\''.$infoTitle.'\''. "\n";
			$html .= '});'. "\n";
			$html .= 'var mapMarker = \''.$marker.'\''. "\n";
			$html .= 'var infoContent = \''.$infoContent.'\''. "\n";
			$html .= 'var contentString = \'<div id="infowindow" style="'.$infoCss.'"><b>'.$infoTitle.'</b><br>'.$infoContent.'</div>\''. "\n";
			$html .= 'var infowindow = new google.maps.InfoWindow({'. "\n";
			$html .= 'content: contentString'. "\n";
			$html .= '});'. "\n";
			$html .= 'map = new google.maps.Map(document.getElementById(\'map_canvas\'), mapOptions);'. "\n";
			$html .= 'if (mapMarker == "1") {'. "\n";
			$html .= 'marker.setMap(map);'. "\n";
			$html .= '}'. "\n";
			$html .= 'if (infoContent != "") {'. "\n";
			$html .= 'infowindow.open(map,marker);'. "\n";
			$html .= '}'. "\n";
			$html .= '}'. "\n";
			$html .= 'function codeAddress() {'. "\n";
			$html .= 'var address = document.getElementById(\'address\').value;'. "\n";
			$html .= 'geocoder.geocode( { \'address\': address}, function(results, status) {'. "\n";
			$html .= 'if (status == google.maps.GeocoderStatus.OK) {'. "\n";
			$html .= 'map.setCenter(results[0].geometry.location);'. "\n";
			$html .= 'var marker = new google.maps.Marker({'. "\n";
			$html .= 'map: map,'. "\n";
			$html .= 'position: results[0].geometry.location'. "\n";
			$html .= '});'. "\n";
			$html .= '} else {'. "\n";
			$html .= 'alert(\''.$errorText.': \' + status);'. "\n";
			$html .= '}'. "\n";
			$html .= '});'. "\n";
			$html .= '}'. "\n";
			$html .= 'google.maps.event.addDomListener(window, \'load\', initialize);'. "\n";
			$html .= '</script>'. "\n";
			$html .= '<div id="map_canvas" style="width:'.$mapWidth.';height:'.$mapHeight.';'.$mapCss.'"></div>'. "\n";
			$html .= '<!-- Google Logo Requirements - Do not remove -->'. "\n";
			$html .= '<div style="width:'.$mapWidth.';height:16px;margin:0;padding:0;text-align:right;"><img src="'.JURI::base().'plugins/system/jt_contact_map/powered-by-google-on-white.png"/></div>'. "\n";
			if ($resetButton == "1") {
				$html .= '<div class="jt_contact_map_reset" style="width:'.$mapWidth.';text-align:center;margin:5px 0;">'. "\n";
				$html .= '<input type="button" value="&nbsp;'.$resetText.'&nbsp;" onclick="javascript:initialize()">'. "\n";
				$html .= '</div>'. "\n";
			}
			$regex = "{jt_contact_map}";
			$body = JResponse::getBody();
			$replacer = '<!-- JT Contact Map - http://www.joomtut.com -->'.$html;
			$body = str_replace( $regex, $replacer, $body );
			JResponse::setBody($body);
		}
	}
}