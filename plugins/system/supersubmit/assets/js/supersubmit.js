/**
 * Copyright (C) 2012 HerdBoy Web Design (admin@herdboy.com)
 */
/**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT
 * HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR
 * FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE
 * OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS,
 * COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.COPYRIGHT HOLDERS WILL NOT
 * BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL
 * DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://gnu.org/licenses/>.
 */
 /* Based on a plugin originally developed by jankoatwarpspeed.com */
(function($){$.fn.formToWizard=function(options){options=$.extend({submitButton:"",title:"Step",infoBox:"Lorem",nextLabel:"Next",prevLabel:"Back"},options);var element=this;var steps=$(element).find("fieldset");var count=steps.size();var submmitButtonName="#"+options.submitButton;$(submmitButtonName).hide();$(element).before("<div class='infobox'>"+options.infoBox+"</div><ul id='steps' class='steps stepup'></ul>");steps.each(function(i){$(this).wrap("<div id='step"+i+"'></div>");$(this).append("<p id='step"+i+"commands'></p>");var name=$(this).find("legend").html();$("#steps").append("<li id='stepDesc"+i+"'>"+options.title+" "+(i+1)+"<span>"+name+"</span></li>");if(i==0){createNextButton(i);selectStep(i)}else{if(i==count-1){$("#step"+i).hide();createPrevButton(i)}else{$("#step"+i).hide();createPrevButton(i);createNextButton(i)}}});function createPrevButton(i){var stepName="step"+i;$("#"+stepName+"commands").append("<a href='#' id='"+stepName+"Prev' class='prev button-green'>< "+options.prevLabel+"</a>");$("#"+stepName+"Prev").bind("click",function(e){$("#"+stepName).hide();$("#step"+(i-1)).show();$(submmitButtonName).hide();selectStep(i-1)})}function createNextButton(i){var stepName="step"+i;$("#"+stepName+"commands").append("<a href='#' id='"+stepName+"Next' class='next button-green'>"+options.nextLabel+" ></a>");$("#"+stepName+"Next").bind("click",function(e){$("#"+stepName).hide();$("#step"+(i+1)).show();if(i+2==count){$(submmitButtonName).show()}selectStep(i+1)})}function selectStep(i){$("#steps li").removeClass("current");$("#stepDesc"+i).addClass("current")}}})(jQuery);