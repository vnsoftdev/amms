<?php
/**
* @version		$version 2.5.20
* @copyright	Copyright (C) 2012 HerdBoy Web Design. All rights reserved.
* @license		GPLv2
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin');
jimport( 'joomla.environment.request' );

class plgSystemSupersubmit extends JPlugin
{

	function plgSupersubmit(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->_plugin = JPluginHelper::getPlugin( 'system', 'supersubmit' );
		$this->_params = new JParameter( $this->_plugin->params );
	}

	function onAfterInitialise()
	{

		$app = JFactory::getApplication();

		if($app->isAdmin())
		{
			return;
		}

        $doc = & JFactory :: getDocument();
    	$doc->addStylesheet(JURI::root(true).'/plugins/system/supersubmit/assets/css/style.css');

	}

	function onAfterRender()
	{

		$app = JFactory::getApplication();

		if($app->isAdmin())
		{
			return;
		}

        if (JRequest::getCmd( 'option' ) == 'com_zoo'){

		$lang =& JFactory::getLanguage();
		$this->loadLanguage('plg_system_supersubmit');

		$ltitle = JText:: _('PLG_SYS_SUPERSUBMIT_TITLE_TEXT');
		$linfo = JText :: _('PLG_SYS_SUPERSUBMIT_INFO_BOX');
		$lnext = JText :: _('PLG_SYS_SUPERSUBMIT_NEXT_LABEL');
		$lprev = JText :: _('PLG_SYS_SUPERSUBMIT_PREV_LABEL');

        $jmpath = ( JURI::root(true) .'/plugins/system/supersubmit/assets/js/supersubmit.js');
        $jmpath2 = '<script type="text/javascript">jQuery(document).ready(function(){jQuery("#item-submission").formToWizard({submitButton:"submit-button",title:"'.$ltitle.'",infoBox:"'.$linfo.'",nextLabel:"'.$lnext.'",prevLabel:"'.$lprev.'"})});</script>';

		$javascript = '<script src="'.$jmpath.'" type="text/javascript"></script>'.$jmpath2.'';

		$buffer = JResponse::getBody();

        $buffer = preg_replace ("/<\/body>/", "".$javascript."\n\n</body>", $buffer);

		JResponse::setBody($buffer);

		return true;

        }

    }

}