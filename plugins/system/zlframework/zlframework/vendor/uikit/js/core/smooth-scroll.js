/**
 * @package     ZOOlanders
 * @version     3.3.15
 * @author      ZOOlanders - http://zoolanders.com
 * @license     GNU General Public License v2 or later
 */

!function(t){"use strict";function o(o,i){i=t.$.extend({duration:1e3,transition:"easeOutExpo",offset:0,complete:function(){}},i);var n=o.offset().top-i.offset,s=t.$doc.height(),e=window.innerHeight;n+e>s&&(n=s-e),t.$("html,body").stop().animate({scrollTop:n},i.duration,i.transition).promise().done(i.complete)}t.component("smoothScroll",{boot:function(){t.$html.on("click.smooth-scroll.uikit","[data-uk-smooth-scroll]",function(o){var i=t.$(this);if(!i.data("smoothScroll")){t.smoothScroll(i,t.Utils.options(i.attr("data-uk-smooth-scroll")));i.trigger("click")}return!1})},init:function(){var i=this;this.on("click",function(n){n.preventDefault(),o(t.$(this.hash).length?t.$(this.hash):t.$("body"),i.options)})}}),t.Utils.scrollToElement=o,t.$.easing.easeOutExpo||(t.$.easing.easeOutExpo=function(t,o,i,n,s){return o==s?i+n:n*(-Math.pow(2,-10*o/s)+1)+i})}(UIkit);