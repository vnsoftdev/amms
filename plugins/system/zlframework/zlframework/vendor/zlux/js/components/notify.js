/**
 * @package     ZOOlanders
 * @version     3.3.15
 * @author      ZOOlanders - http://zoolanders.com
 * @license     GNU General Public License v2 or later
 */

!function(n,i,t){"use strict";var e=function(i,t){var e=n.UIkit.notify(i,t);return e.element.parent().addClass("zx"),e},c=function(t,e){return e=n.extend(e,{timeout:!1}),n.Deferred(function(n){var c=i.notify(t+'<div class="uk-text-center uk-margin-top">                    <a class="zx-x-confirm uk-margin-right"><i class="uk-icon-check uk-icon-small"></i></a>                    <a class="zx-x-cancel uk-margin-left"><i class="uk-icon-times uk-icon-small"></i></a>                </div>',e);c.element.on("click",".zx-x-confirm",function(i,t){n.resolve()}),c.element.on("click",function(i,t){n.reject()})}).promise()},o=function(i,t){return n.UIkit.notify.closeAll(i,t),this};i.notify=e,i.notify.confirm=c,i.notify.closeAll=o}(jQuery,zlux,UIkit);