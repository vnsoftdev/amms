<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class plgSystemZoo_zlelementsInstallerScript
{
	protected $_error;
	protected $_src;
	protected $_ext = 'zoo_zlelements';
	protected $_ext_name = 'ZL Elements';
	protected $_lng_prefix = 'PLG_ZLELEMENTS_SYS';

	/* List of obsolete files and folders */
	protected $_obsolete = array(
		'files'	=> array(
			'plugins/system/zoo_zlelements/zoo_zlelements/control.json'
		),
		'folders' => array(
			'plugins/system/zoo_zlelements/zoo_zlelements/languages'
		)
	);

	/**
	 * Called before any type of action
	 *
	 * @param   string  $type  Which action is happening (install|uninstall|discover_install)
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function preflight($type, $parent)
	{
		// init vars
		$db = JFactory::getDBO();

		// load ZLFW sys language file
		JFactory::getLanguage()->load('plg_system_zlframework.sys', JPATH_ADMINISTRATOR, 'en-GB', true);

		// save install temp path
		$this->_src = $parent->getParent()->getPath('source'); // tmp folder

		// init vars
		$inst_path = dirname(__FILE__);
		$html = '';

		if($type == 'install' || $type == 'update')
		{
			if($type == 'update')
			{
				$cur_manifest = simplexml_load_file(JPATH_ROOT.'/plugins/system/zoo_zlelements/zoo_zlelements.xml');
				$new_manifest = $parent->get("manifest");

				// if trying to update with older plugin, just update the elements
				if( version_compare((string)$cur_manifest->version, (string)$new_manifest->version, '>') ) {
					// copy installed plugin files into temp folder to be able to install without issues
					JFile::copy(
						JPATH_ROOT.'/plugins/system/zoo_zlelements/zoo_zlelements.xml',
						"{$this->_src}/zoo_zlelements.xml"
					);
					JFile::copy(
						JPATH_ROOT.'/plugins/system/zoo_zlelements/zoo_zlelements.php',
						"{$this->_src}/zoo_zlelements.php"
					);
					if (!JFolder::exists("{$this->_src}/languages")) {
						JFolder::create("{$this->_src}/languages");
					}
					JFile::copy(
						JPATH_ROOT.'/administrator/language/en-GB/en-GB.plg_system_zoo_zlelements.ini',
						"{$this->_src}/languages/en-GB.plg_system_zoo_zlelements.ini"
					);

					$html .= '<p>The current installed Plugin is more recent than the one being installed, the plugin update was skipped.</p>';
				} else {
					// Remove depricated folders
					jimport('joomla.filesystem.file');
					$f = JPATH_ROOT.'/plugins/system/zoo_zlelements/zoo_zlelements/fields'; // even if removed will be repopulated with the new content
					if(JFolder::exists($f))	JFolder::delete($f);
				}
			}

			// make sure elements folder exist
			if (!JFolder::exists("{$this->_src}/zoo_zlelements")) {
				JFolder::create("{$this->_src}/zoo_zlelements");
			}

			if (JFolder::exists("{$this->_src}/elements"))
			{
				// make sure element lang folder exist
				if (!JFolder::exists("{$this->_src}/zoo_zlelements/language/en-GB")) {
					JFolder::create("{$this->_src}/zoo_zlelements/language/en-GB");
				}

				// move all element lang files into one folder
				foreach(JFolder::Folders("{$this->_src}/elements") as $element){
					if(is_dir("{$this->_src}/elements/$element/language/en-GB")){
						JFile::move(
							"{$this->_src}/elements/$element/language/en-GB/en-GB.plg_system_zoo_zlelements_$element.ini",
							"{$this->_src}/zoo_zlelements/language/en-GB/en-GB.plg_system_zoo_zlelements_$element.ini"
						);
						JFolder::delete("{$this->_src}/elements/$element/language");
					}
				}

				// move elements to install folder
				if (JFolder::exists("{$this->_src}/elements")) JFolder::move(
					"{$this->_src}/elements",
					"{$this->_src}/zoo_zlelements/elements"
				);

				// execute individual elements install scripts
				foreach(JFolder::Folders("{$this->_src}/zoo_zlelements/elements") as $element) {
					$file = "{$this->_src}/zoo_zlelements/elements/$element/install.script.php";
					if(is_file($file)){
						ob_start();
						include($file);
						$html .= ob_get_contents();
						ob_end_clean();
					}
				}
			}
		}

		// render message
		echo $html;
	}

	/**
	 * Called on installation
	 *
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	function install($parent)
	{
		// init vars
		$db = JFactory::getDBO();       
    }

    /**
	 * Called on uninstallation
	 *
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	function uninstall($parent)
	{
		// execute individual elements uninstall scripts
		$type = 'uninstall';
		foreach(JFolder::Folders(JPATH_ROOT.'/plugins/system/zoo_zlelements/zoo_zlelements/elements') as $element) {
			$file = JPATH_ROOT."/plugins/system/zoo_zlelements/zoo_zlelements/elements/$element/install.script.php";
			if(is_file($file)){
				ob_start();
				include($file);
				$html .= ob_get_contents();
				ob_end_clean();
			}
		};
		echo $html;

		// enqueue Message
        JFactory::getApplication()->enqueueMessage(JText::_($this->langString('_UNINSTALL')));
    }

	/**
	 * Called after install
	 *
	 * @param   string  $type  Which action is happening (install|uninstall|discover_install)
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function postflight($type, $parent)
	{
		// init vars
		$db = JFactory::getDBO();
		$release = $parent->get( "manifest" )->version;

		if($type == 'install'){
			echo JText::sprintf('PLG_ZLFRAMEWORK_SYS_INSTALL', $this->_ext_name, $release);
		}

		if($type == 'update'){
			echo JText::sprintf('PLG_ZLFRAMEWORK_SYS_UPDATE', $this->_ext_name, $release);
		}

		// enable plugin
		$db->setQuery("UPDATE `#__extensions` SET `enabled` = 1 WHERE `type` = 'plugin' AND `element` = '{$this->_ext}' AND `folder` = 'system'")->query();

		// remove obsolete
		$this->removeObsolete();
	}

	/**
	 * Removes obsolete files and folders
	 * @version 1.1
	 */
	protected function removeObsolete()
	{
		// Remove files
		if(!empty($this->_obsolete['files'])) foreach($this->_obsolete['files'] as $file) {
			$f = JPATH_ROOT.'/'.$file;
			if(!JFile::exists($f)) continue;
			JFile::delete($f);
		}

		// Remove folders
		if(!empty($this->_obsolete['folders'])) foreach($this->_obsolete['folders'] as $folder) {
			$f = JPATH_ROOT.'/'.$folder;
			if(!JFolder::exists($f)) continue;
			JFolder::delete($f);
		}
	}

	/**
	 * creates the lang string
	 * @version 1.0
	 *
	 * @return  string
	 */
	protected function langString($string)
	{
		return $this->_lng_prefix.$string;
	}
}