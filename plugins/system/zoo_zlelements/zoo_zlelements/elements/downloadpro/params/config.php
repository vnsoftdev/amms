<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{
		"_download_mode":{
			"type": "select",
			"label": "PLG_ZLELEMENTS_DWP_MODE",
			"help": "PLG_ZLELEMENTS_DWP_MODE_DESC",
			"specific":{
				"options":{
					"PLG_ZLELEMENTS_DWP_DIRECT":"0",
					"PLG_ZLELEMENTS_DWP_ATTACHMENT":"1",
					"PLG_ZLELEMENTS_DWP_PROTECTED":"2"
				}
			}
		}
	}';