<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include assets css
$this->app->document->addStylesheet('elements:downloadpro/assets/css/downloadpro.css');

if ($file) {
	
	if ($limit_reached) {
		echo '<a class="yoo-zoo element-download-button" href="javascript:alert(\''.JText::_('PLG_ZLELEMENTS_DWP_DOWNLOAD_LIMIT_REACHED').'\');" title="'.JText::_('PLG_ZLELEMENTS_DWP_DOWNLOAD_LIMIT_REACHED').'"><span><span>'.JText::_('PLG_ZLELEMENTS_DWP_DOWNLOAD').'</span></span></a>';
	} else {
		$target = $params->find('layout._target') ? ' target="_blank"' : '';
		echo '<a class="yoo-zoo element-download-button" href="'.JRoute::_($download_link).'" title="'.$download_name.'"'.$target.'><span><span>'.$download_name.'</span></span></a>';
	}
	
} else {
	echo JText::_('PLG_ZLELEMENTS_DWP_NO_FILE_SELECTED');
}
