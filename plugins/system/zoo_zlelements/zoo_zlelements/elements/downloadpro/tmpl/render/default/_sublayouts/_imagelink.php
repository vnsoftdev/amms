<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// include assets css
$this->app->document->addStylesheet('elements:downloadpro/tmpl/render/default/_sublayouts/_imagelink/sets/'.$params->find('layout.sublayout._set', 'default').'/style.css');

if ($file) {
	
	if ($limit_reached) {
		echo '<div class="zl-zoo element-download-type element-download-type-'.$filetype.'" title="'.JText::_('PLG_ZLELEMENTS_DWP_DOWNLOAD_LIMIT_REACHED').'"></div>';
	} else {
		$target = $params->find('layout._target') ? ' target="_blank"' : '';
		echo '<a class="zl-zoo element-download-type element-download-type-'.$filetype.'" href="'.JRoute::_($download_link).'" title="'.$download_name.'"'.$target.'></a>';
	}
	
} else {
	echo JText::_('PLG_ZLELEMENTS_DWP_NO_FILE_SELECTED');
}
