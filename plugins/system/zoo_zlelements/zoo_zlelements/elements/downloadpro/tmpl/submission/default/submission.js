/* Copyright (C) YOOtheme GmbH, YOOtheme Proprietary Use License (http://www.yootheme.com/license) */
(function ($) {
    var a = function () {};
    a.prototype = $.extend(a.prototype, {
        name: "DownloadSubmissionPro",
		initialize: function (a, c) {
            this.options = $.extend({}, this.options, c);
            var b = this;
			
			// on each new instance apply
			var element = a.closest('.filespro');
            element.find('p.add a').bind('click', function () {
				b.apply(element.find('[type="file"]'));
			});
			
			// first init
			b.apply(element.find('[type="file"]'));
		},
		apply: function (inputs)
		{
			var b = this;
				
			inputs.each(function (c) {
				
				if (!$(this).data('initialized')) { 
				
					var input = $(this),
						element = input.closest('.download-element'),
						select_upload = element.find("input.upload");
           
					element.find('[type="file"]').bind('change', function()
					{
						var name = $(this).val().replace(/^.*[\/\\]/g, '');
						element.find('input.filename').val(name);
					});
					element.find("span.download-cancel").bind("click", function () 
					{
						select_upload.val("");
						element.find("div.download-limit input").val("");
						b.sanatize(element, select_upload)
					});
					b.sanatize(element, select_upload)
						
				$(this).data('initialized', !0);}
			})
        },
        sanatize: function (element, select_upload) {
            select_upload.val() ? (element.find("div.download-select").addClass("hidden"), element.find("div.download-preview").removeClass("hidden")) : (element.find("div.download-select").removeClass("hidden"), element.find("div.download-preview").addClass("hidden"))
        }
    });
    $.fn[a.prototype.name] = function () {
        var e = arguments,
            b = e[0] ? e[0] : null;
        return this.each(function () {
            var d = $(this);
            if (a.prototype[b] && d.data(a.prototype.name) && b != "initialize") d.data(a.prototype.name)[b].apply(d.data(a.prototype.name), Array.prototype.slice.call(e, 1));
            else if (!b || $.isPlainObject(b)) {
                var f = new a;
                a.prototype.initialize && f.initialize.apply(f, $.merge([d], e));
                d.data(a.prototype.name, f)
            } else $.error("Method " + b + " does not exist on jQuery." + a.name)
        })
    }
})(jQuery);