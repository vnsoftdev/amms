<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	if($type == 'install' || $type == 'update')
	{
		if($type == 'install') {
			echo '<p><strong>Image Pro</strong> Element installed succesfully.</p>';
		} else {

			// Remove depricated folders
			jimport('joomla.filesystem.file');
			$f = JPATH_ROOT.'/plugins/system/zoo_zlelements/zoo_zlelements/elements/imagepro/tmpl'; // even if removed will be repopulated with the new content
			if(JFolder::exists($f))	JFolder::delete($f);

			echo '<p><strong>Image Pro</strong> Element updated succesfully.</p>';
		}
	}
	else if($type == 'uninstall')
	{
		
	}