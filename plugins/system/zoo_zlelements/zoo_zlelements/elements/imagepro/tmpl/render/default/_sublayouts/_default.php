<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$target = $img['target'] ? 'target="_blank"' : '';
$rel	= $img['rel'] ? 'rel="'.$img['rel'].'"' : '';
$title  = $img['title'] ? ' title="'.$img['title'].'"' : '';

$link_enabled = !empty($img['link']);

$content = '<img src="'.$img['fileurl'].'"'.$title.' alt="'.$img['alt'].'" width="'.$img['width'].'" height="'.$img['height'].'" />';

if ($link_enabled) {
	echo '<a href="'.JRoute::_($img['link']).'" '.$rel.$title.$target.'>'.$content.'</a>';
} else {
	echo $content;
}