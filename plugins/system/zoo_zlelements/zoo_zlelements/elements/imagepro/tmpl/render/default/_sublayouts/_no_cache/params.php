<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"layout_separator":{
			"type":"separator",
			"text":"No Cache Layout",
			"big":1
		},

		"layout_wrapper":{
			"type":"subfield",
			"path":"elements:'.$element->getElementType().'\/params\/image.php",
			"arguments":{
				"params":{
					"link":"true"
				}
			},
			"adjust_ctrl":{
				"pattern":'.json_encode('/layout/').',
				"replacement":"specific"
			}
		}

	}}';