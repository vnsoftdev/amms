<?php
/**
* @package		ZL FrameWork
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"widget_separator":{
			"type": "separator",
			"text": "PLG_ZLFRAMEWORK_LAYOUT",
			"big":"1"
		},
		"widget_settings": {
			"type":"subfield",
			"path":"elements:pro\/tmpl\/render\/widgetkit\/gallery\/showcase_box\/settings.php"
		},
		"image_separator":{
			"type": "separator",
			"text": "PLG_ZLFRAMEWORK_IMAGE"
		},
		"image_options": {
			"type": "subfield",
			"path":"elements:'.$element->getElementType().'\/params\/image.php",
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[layout\]\[widgetkit\]\[settings\]/').',
				"replacement":"[specific]"
			}
		},
		"_link_to_item":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_IMGP_LINK_TO_ITEM",
			"help": "PLG_ZLELEMENTS_IMGP_LINK_TO_ITEM_DESC",
			"default": "0",
			"specific":{
				"label":"JYES"
			},
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[layout\]\[widgetkit\]\[settings\]/').',
				"replacement":"[specific]"
			},
			"dependents":"main_wrapper_lightbox !> 1"
		}
		
	},
	"control": "settings"}';