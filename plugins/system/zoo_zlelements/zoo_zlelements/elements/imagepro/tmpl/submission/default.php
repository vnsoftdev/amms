<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// load assets
	$this->app->document->addScript('elements:imagepro/tmpl/submission/default/submission.min.js');

	// init vars
	$image = $this->get('file');

	// is uploaded file
	$image = is_array($image) ? array_shift($image) : $image;
	
	if (!empty($image)) {
		$image = $this->app->zlfw->resizeImage($this->app->path->path("root:$image"), 0, 0);
		$image = $this->app->path->relative($image);
		$image = $this->app->path->url("root:$image");
	}

?>

<div class="image-element">

	<div class="image-select">

		<div class="upload">
		
			<input class="filename" type="text" id="filename<?php echo $this->identifier; ?>" readonly="readonly" />
			<div class="button-container">
				<button class="button-grey search" type="button"><?php echo JText::_('PLG_ZLFRAMEWORK_SEARCH'); ?></button>
				<input type="file" name="elements_<?php echo $this->identifier; ?>[]" />
			</div>
		</div>
		
		<input type="hidden" class="image" name="<?php echo $this->getControlName('image'); ?>" value="<?php echo ($image ? $this->index() : ''); ?>">

	</div>

	<div class="image-preview">
		<img src="<?php echo $image; ?>" alt="preview">
		<span class="image-cancel" title="<?php echo JText::_('PLG_ZLFRAMEWORK_REMOVE'); ?>"></span>
	</div>

	<script type="text/javascript">
	jQuery(function($) {
		$('#<?php echo $this->identifier; ?>').ImageSubmissionpro();
	});
	</script>

</div>