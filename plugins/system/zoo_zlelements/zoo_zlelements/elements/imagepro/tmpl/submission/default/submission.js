/* Copyright (C) ZOOlanders.com - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only */
/* ===================================================
 * ZLUX SaveElement v0.2
 * https://zoolanders.com/extensions/zl-framework
 * ===================================================
 * Copyright (C) JOOlanders SL 
 * http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * ========================================================== */
(function ($) {
	var Plugin = function(){};
	Plugin.prototype = $.extend(Plugin.prototype, {
		name: 'ImageSubmissionpro',
		options: {
		},
		initialize: function(element, options) {
			this.options = $.extend({}, this.options, options);
			var $this = this;

			// save element reference
			$this.element = element;
			
			// on each new image apply
			$('p.add a', element).on('click', function () {
				$this.apply(element.find('[type="file"]'));
			});
			
			// first init
			$this.apply(element.find('[type="file"]'));
		},
		apply: function (inputs)
		{
			var $this = this;
				
			inputs.each(function (c) {

				var input = $(this),
					instance = input.closest('.image-element'),
					select_image = instance.find("input.image");
				
				if (!$(this).data('initialized')) { 
				
					$('[type="file"]', instance).on('change', function()
					{
						var name = $(this).val().replace(/^.*[\/\\]/g, '');
						instance.find('input.filename').val(name);
					});
					$('span.image-cancel', instance).on('click', function () {
						select_image.val('');
						$this.sanatize(instance, select_image)
					});
					$this.sanatize(instance, select_image);
					
				$(this).data('initialized', !0);}
			})

			$this.element.data('initialized', true);
		},
		sanatize: function (instance, select_image) {
			var $this = this;

			// it not value and element not initialized
			if (select_image.val() && !$this.element.data('initialized')) {

				// hide select, show preview
				instance.find("div.image-select").addClass("hidden");
				instance.find("div.image-preview").removeClass("hidden");

			} else {
				// empty value
				select_image.val('');

				// show select, hide preview
				instance.find("div.image-select").removeClass("hidden");
				instance.find("div.image-preview").addClass("hidden");
			}
		}
	});
	// Don't touch
	$.fn[Plugin.prototype.name] = function() {
		var args   = arguments;
		var method = args[0] ? args[0] : null;
		return this.each(function() {
			var element = $(this);
			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();
				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}
				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}
		});
	};
})(jQuery);