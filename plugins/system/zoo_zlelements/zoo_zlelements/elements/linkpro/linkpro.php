<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
   Class: ElementLink
       The link element class
*/
class ElementLinkPro extends ElementRepeatablePro implements iRepeatSubmittable {

	/*
	   Function: Constructor
	*/
	public function __construct() {

		// call parent constructor
		parent::__construct();

		// register events
		$this->app->event->dispatcher->connect('element:afterdisplay', array($this, 'afterDisplay'));

		// load default and current language 
		$this->app->system->language->load("plg_system_zoo_zlelements_linkpro", $this->app->path->path('zlpath:'), 'en-GB', true);
		$this->app->system->language->load("plg_system_zoo_zlelements_linkpro", $this->app->path->path('zlpath:'), null, true);
	}

	/*
		Function: afterDisplay
			Change the element layout after it has been displayed

		Parameters:
			$event - object
	*/
	public static function afterDisplay($event)
	{
		$event['html'] = str_replace('class="element element-linkpro', 'class="element element-link element-linkpro', $event['html']);
	}

	/*
		Function: _hasValue
			Checks if the repeatables element's value is set

	   Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/
	protected function _hasValue($params = array()) {
		$value='';
		switch ($params->find('layout._if_no_url')) {
			case '1': // render text
				$value = $this->getText($params);
				break;

			default:
				$value = $this->getURL($params);
				break;
		}
		return !empty($value);
	}

	/*
		Function: getURL
			Gets the link url

		Returns:
			String - url
	*/
	public function getURL($params) {
		return $this->get('value') ? $this->get('value') : $params->find('layout._url', '');
	}

	/*
		Function: getText
			Gets the link text

		Returns:
			String - text
	*/
	public function getText($params) {
		$text = $this->get('text') ? $this->get('text') : $params->find('layout._text', '');
		if (empty($text) && ($params->find('layout._clean_link_text', 0)))
			// clean the link text if it's taken from url
			$text = preg_replace($params->find('layout._clean_link_text'), '', $this->getURL($params));
		else if (empty($text))
			$text = $this->getURL($params);
		return $text;
	}

	/*
		Function: getTitle
			Gets the link title

		Returns:
			String - title
	*/
	public function getTitle($params) {
		$title = $this->get('title') ? $this->get('title') : $params->find('layout._title', '');
		return empty($title) ? $this->getText($params) : $title;
	}
	
	/*
		Function: getTarget
			Gets the link target

		Returns:
			String - target
	*/
	public function getTarget($params) {
		$target = $this->get('target') == 'default' || $this->get('target') == null ? $params->find('layout._target', '') : $this->get('target');
		return empty($target) ? '' : '_blank';
	}

	/*
		Function: getRel
			Gets the rel data

		Returns:
			String - rel
	*/
	public function getRel($params) {
		return $this->get('rel') ? $this->get('rel') : $params->find('layout._rel', '');
	}

	/*
		Function: _render
			Renders the repeatable element

	   Parameters:
            $params - AppData render parameter

		Returns:
			String - html
	*/
	protected function _render($params = array()) {
		$link = array();
		$link['text']		= $this->getText($params);
		$link['title']		= $this->getTitle($params);
		$link['target']		= $this->getTarget($params);
		$link['rel']		= $this->getRel($params);
		$link['class']		= $params->find('layout._class', '');
		$link['url']		= $this->getURL($params);

		return $link;
	}

	/*
	   Function: _edit
	       Renders the repeatable edit form field.

	   Returns:
	       String - html
	*/
	protected function _edit(){
		return $this->_editForm();
	}

	/*
		Function: _renderSubmission
			Renders the element in submission

	   Parameters:
            $params - AppData submission parameters

		Returns:
			String - html
	*/
	public function _renderSubmission($params = array()) {
        return $this->_editForm($params->get('trusted_mode'));
	}

	protected function _editForm($trusted_mode = true) {
        if ($layout = $this->getLayout('edit/_edit.php')) {
            return $this->renderLayout($layout,
                compact('trusted_mode')
            );
        }
	}

	/*
		Function: _validateSubmission
			Validates the submitted element

	   Parameters:
            $value  - AppData value
            $params - AppData submission parameters

		Returns:
			Array - cleaned value
	*/
	public function _validateSubmission($value, $params)
	{
        $values       = $value;
        $trusted_mode = $params->get('trusted_mode');

        $validator    = $this->app->validator->create('string', array('required' => false));
        $text         = $validator->clean($values->get('text'));
        $target       = $validator->clean($values->get('target'));
        $title		  = $validator->clean($values->get('title'));
        $rel          = $validator->clean($values->get('rel'));

		$value = '';
        if ($trusted_mode) {
        	$value = $this->app->validator
				->create('string', array('required' => $params->get('required')), array('required' => JText::_('PLG_ZLELEMENTS_LP_URL_IS_MISSING')))
				->clean($values->get('value'));
        } else {
        	$value = $this->app->validator
				->create('url', array('required' => $params->get('required')), array('required' => JText::_('PLG_ZLELEMENTS_LP_URL_IS_MISSING')))
				->clean($values->get('value'));
        }

		return compact('value', 'text', 'target', 'title', 'rel');
    }
}