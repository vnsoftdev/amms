<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	$edit_path = array();
	$edit_path[] = 'zlpath:elements\/'.$element->getElementType().'\/tmpl\/edit';
	if (is_object($element->getType())) $edit_path[] = 'applications:'.$element->getType()->getApplication()->getGroup().'\/elements\/'.$element->getElementType().'\/tmpl\/edit'; // zoo template overide path

	return 
	'{
		"_edit_sublayout":{
		"type": "layout",
		"label": "PLG_ZLFRAMEWORK_EDIT_SUBLAYOUT",
		"help": "PLG_ZLFRAMEWORK_EDIT_SUBLAYOUT_DESC",
		"default": "_edit.php",
		"specific": {
			"path":"'.implode(',', $edit_path).'",
			"regex":'.json_encode('^([_][_A-Za-z0-9]*)\.php$').',
			"min_opts":"2"
		}
		},
		"_custom_text":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_LP_CUSTOM_TEXT",
			"help": "PLG_ZLELEMENTS_LP_CUSTOM_TEXT_DESC",
			"specific": {
				"label":"JYES"
			}
		},
		"_custom_options":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_LP_CUSTOM_OPTIONS",
			"help": "PLG_ZLELEMENTS_LP_CUSTOM_OPTIONS_DESC",
			"specific": {
				"label":"JYES"
			}
		},
		"_default_value":{
			"type": "text",
			"label": "PLG_ZLFRAMEWORK_DEFAULT_VALUE",
			"help": "PLG_ZLFRAMEWORK_DEFAULT_VALUE_DESC"
		}
	}';