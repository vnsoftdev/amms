<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return
	'{
		"_url":{
			"type": "text",
			"label": "PLG_ZLFRAMEWORK_LINK",
			"help": "PLG_ZLELEMENTS_LP_URL_DESC",
			"dependents":"_if_no_url !> 1"
		},
		"_text":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_LP_TEXT",
			"help": "PLG_ZLELEMENTS_LP_TEXT_DESC"
		},
		"_title":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_LP_TITLE",
			"help": "PLG_ZLELEMENTS_LP_TITLE_DESC"
		},
		'.(isset($params['target']) ?
		'"_target":{
			"type": "checkbox",
			"label": "PLG_ZLFRAMEWORK_NEW_WINDOW",
			"help": "PLG_ZLELEMENTS_LP_NEW_WINDOW_DESC",
			"specific": {
				"label":"JYES"
			}
		},' : '').'
		"_rel":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_LP_REL",
			"help": "PLG_ZLELEMENTS_LP_REL_DESC"
		},
		"_class":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_LP_CLASS",
			"help": "PLG_ZLELEMENTS_LP_CLASS_DESC"
		}
	}';