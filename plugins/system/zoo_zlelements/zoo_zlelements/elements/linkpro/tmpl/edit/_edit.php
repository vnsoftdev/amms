<?php
/**
* @package   ZOO
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// set default, if item is new
$default = $this->config->find('specific._default_value');
if ($default != '' && $this->_item != null && $this->_item->id == 0) {
	$this->set('value', $default);
}
?>

<div>
  <?php if ($trusted_mode && $this->config->find('specific._custom_text', 0)) : ?>
	<div class="row">
		<?php echo $this->app->html->_('control.text', $this->getControlName('text'), $this->get('text'), 'size="60" title="'.JText::_('PLG_ZLFRAMEWORK_TEXT').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_TEXT').'"'); ?>
	</div>
	<?php endif; ?>
	
	<div class="row">
    	<?php echo $this->app->html->_('control.text', $this->getControlName('value'), $this->get('value'), 'size="60" title="'.JText::_('PLG_ZLFRAMEWORK_LINK').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_LINK').'"'); ?>
  	</div>
	
	<?php if ($trusted_mode && $this->config->find('specific._custom_options', 0)) : ?>
	<div class="more-options">
		<div class="trigger">
			<div>
				<div class="advanced button hide"><?php echo JText::_('PLG_ZLFRAMEWORK_HIDE_OPTIONS'); ?></div>
				<div class="advanced button"><?php echo JText::_('PLG_ZLFRAMEWORK_SHOW_OPTIONS'); ?></div>
			</div>
		</div>

		<div class="advanced options">

			<?php if ($this->config->find('specific._custom_options', 0)) : ?>
			<div class="row">
				<strong><?php echo JText::_('PLG_ZLFRAMEWORK_NEW_WINDOW'); ?></strong>
				<?php echo $this->app->html->_('control.arraylist', array(
					'default' => JText::_('PLG_ZLFRAMEWORK_DEFAULT'),
					'0' => JText::_('JNO'),
					'1' => JText::_('JYES')
				), array(), $this->getControlName('target'), null, 'value', 'text', $this->get('target')); ?>
			</div>

			<div class="row short">
				<?php echo $this->app->html->_('control.text', $this->getControlName('title'), $this->get('title'), 'size="60" title="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'"'); ?>
			</div>

			<div class="row short">
				<?php echo $this->app->html->_('control.text', $this->getControlName('rel'), $this->get('rel'), 'size="60" title="'.JText::_('PLG_ZLFRAMEWORK_REL').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_REL').'"'); ?>
			</div>
			<?php endif; ?>

		</div>
	</div>
	<?php endif; ?>
</div>