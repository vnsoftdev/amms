<?php
/**
* @package		Link Pro
* @author    	JOOlanders
* @copyright 	Copyright (C) JOOlanders
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// render values
	$result = $this->getRenderedValues($params);

	// perform the _sublayout if is not WidgetKit
	foreach ($result['result'] as $key => $link)
	{
		$mainlayout = basename($params->find('layout._layout', 'default.php'), '.php');
		if($layout = $this->getLayout('render/'.$mainlayout.'/_sublayouts/'.$params->find('layout._sublayout', '_default.php'))){
			$result['result'][$key] = $this->renderLayout($layout, compact('link', 'params'));
		}
	}

	$separator = $params->find('separator._by_custom') != '' ? $params->find('separator._by_custom') : $params->find('separator._by');

	// render
	$result = $this->app->zlfw->applySeparators($separator, $result['result'], $params->find('separator._class'), $params->find('separator._fixhtml'));

	echo $this->app->zlfw->replaceShortCodes($result, array('item' => $this->_item));
?>