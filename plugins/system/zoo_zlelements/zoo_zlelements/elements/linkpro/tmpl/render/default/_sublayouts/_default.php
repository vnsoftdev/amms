<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	$title 		= $link['title'] ? 'title="'.$link['title'].'"' : '';
	$target 	= $link['target'] ? 'target="'.$link['target'].'"' : '';
	$rel	 	= $link['rel'] ? 'rel="'.$link['rel'].'"' : '';
	$class	 	= $link['class'] ? 'class="'.$link['class'].'"' : '';

	if (empty($link['url']) && $params->find('layout._if_no_url') == '1'){
		echo $link['text'];
	} else if ($link['url']){
		echo '<a href="'.JRoute::_($link['url']).'" '.$title.' '.$target.' '. $rel .' '.$class.'>'.$link['text'].'</a>';
	}
?>