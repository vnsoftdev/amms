<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"layout_wrapper":{
			"type": "fieldset",
			"fields": {

				"_clean_link_text":{
					"type": "text",
					"label": "PLG_ZLELEMENTS_LP_CLEAN_LINK_TEXT",
					"help": "PLG_ZLELEMENTS_LP_CLEAN_LINK_TEXT_DESC",
					"specific": {
						"placeholder":"/regex/"
					}
				},

				"_if_no_url":{
					"type": "select",
					"label": "PLG_ZLELEMENTS_LP_IF_NO_URL",
					"help": "PLG_ZLELEMENTS_LP_IF_NO_URL_DESC",
					"specific":{
						"options": {
							"PLG_ZLELEMENTS_LP_ABORT_RENDERING":"",
							"PLG_ZLELEMENTS_LP_RENDER_RAW_LINK_TEXT":"1"
						}
					}
				},

				"_defaults_sep":{
					"type": "separator",
					"text": "PLG_ZLELEMENTS_LP_DEFAULTS"
				},
				"link_options": {
					"type": "subfield",
					"path":"elements:'.$element->getElementType().'\/params\/link.php",
					"arguments":{
						"params":{
							"target":"true"
						}
					}
				}
				'.($element->config->find('specific._custom_options') ? ',
				"_info":{
					"type": "info",
					"specific":{
						"text": "PLG_ZLELEMENTS_LP_DEFAULTS_TIP"
					}
				}' : '').'

			}
		}

	}}';