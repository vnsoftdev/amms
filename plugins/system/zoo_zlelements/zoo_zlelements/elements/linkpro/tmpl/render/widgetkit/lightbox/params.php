<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {
		
		"wrapper_link_defaults": {
			"type":"wrapper",
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[widgetkit\]/').',
				"replacement":""
			},
			"fields": {

				"_clean_link_text":{
					"type": "text",
					"label": "PLG_ZLELEMENTS_LP_CLEAN_LINK_TEXT",
					"help": "PLG_ZLELEMENTS_LP_CLEAN_LINK_TEXT_DESC",
					"specific": {
						"placeholder":"/regex/"
					}
				},

				"_if_no_url":{
					"type": "select",
					"label": "PLG_ZLELEMENTS_LP_IF_NO_URL",
					"help": "PLG_ZLELEMENTS_LP_IF_NO_URL_DESC",
					"specific":{
						"options": {
							"PLG_ZLELEMENTS_LP_ABORT_RENDERING":"",
							"PLG_ZLELEMENTS_LP_RENDER_RAW_LINK_TEXT":"1"
						}
					}
				},

				"_defaults_sep":{
					"type": "separator",
					"text": "PLG_ZLELEMENTS_LP_DEFAULTS"
				},
				"link_options": {
					"type": "subfield",
					"path":"elements:'.$element->getElementType().'\/params\/link.php"
				}
				'.($element->config->find('specific._custom_options') ? ',
				"_info":{
					"type": "info",
					"specific":{
						"text": "PLG_ZLELEMENTS_LP_DEFAULTS_TIP"
					}
				}' : '').'

			}
		},

		"wrapper_lightbox_settings":{
			"type": "wrapper",
			"fields": {
				"_separatorid":{
					"type": "separator",
					"text": "Lightbox"
				},
				"width":{
					"type": "text",
					"label": "PLG_ZLFRAMEWORK_WIDTH",
					"specific":{
						"placeholder":"auto"
					}
				},
				"height":{
					"type": "text",
					"label": "PLG_ZLFRAMEWORK_HEIGHT",
					"specific":{
						"placeholder":"auto"
					}
				},
				"lightbox_caption":{
					"type": "select",
					"label": "PLG_ZLFRAMEWORK_TITLE",
					"default": "1",
					"specific":{
						"options":{
							"PLG_ZLFRAMEWORK_DISABLED":"0",
							"PLG_ZLFRAMEWORK_DEFAULT":"1",
							"PLG_ZLFRAMEWORK_CUSTOM":"2"
						}
					},
					"dependents":"_lightbox_custom_title > 2"
				},
				"_custom_title":{
					"label": "PLG_ZLFRAMEWORK_CUSTOM_TITLE",
					"type":"text",
					"control": "lightbox_settings"
				},
				"lightbox_defaults": {
					"type":"subfield",
					"path":"elements:pro\/tmpl\/render\/widgetkit\/lightbox\/lightbox_defaults.php"
				}
			},
			"control": "settings"
		}

	}}';