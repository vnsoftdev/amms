<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* extends
* @package   Widgetkit
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   YOOtheme Proprietary Use License (http://www.yootheme.com/license)
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	$widget_id  = $widget->id.'-'.uniqid();
	$settings  	= $widget->settings;
	
	// get elements values
	$links = $this->getRenderedValues($params, $widget->mode);

	// get lightbox settings and remove them from widget ones
	$lbsettings	= $settings['lightbox_settings'];
	unset($settings['lightbox_settings']);

	// create the settings object
	$settings = $this->app->data->create($settings);
	$lbsettings = $this->app->data->create($lbsettings);
	
	// set custom lightbox options
	$lightbox_options = '';
	if(isset($lbsettings['lightbox_overide']))
	{
		foreach($lbsettings as $name => $value){
			$lightbox_options .= "$name:$value;";
		}
	}

	$result = array();
?>

<?php if (count($links['result'])) : ?>

	<?php foreach ($links['result'] as $key => $link) : ?>
	
		<?php

			/* Prepare Caption */
			$caption = strlen($link['title']) ? strip_tags($link['title']) : strip_tags($image['text']);

			/* Prepare Lightbox */
			{
				// set dimensions
				$lightbox_options .= $settings->get('width') && $settings->get('width') != 'auto' ? 'width:'.$settings->get('width').';' : '';
				$lightbox_options .= $settings->get('height') && $settings->get('width') != 'auto' ? 'height:'.$settings->get('height').';' : '';

				$lightbox = 'data-lightbox="group:'.$widget_id.';'.$lightbox_options.'"';

				// override caption options
				$lbc = $caption;
				if($settings->get('lightbox_caption') == 0){
					$lbc = '';
				} elseif($settings->get('lightbox_caption') == 2 && $custom_title = $settings->get('_custom_title')){
					$lbc = $custom_title;
				}

				if (strlen($lbc)) $lightbox .= ' title="'.$lbc.'"';
			}

			$title 		= empty($title) ? 'title="'.$link['title'].'"' : '';
			$rel	 	= $link['rel'] ? 'rel="'.$link['rel'].'"' : '';
			$class	 	= $link['class'] ? 'class="'.$link['class'].'"' : '';

			// link
			if (empty($link['url']) && $params->find('layout._if_no_url') == '1'){
				$result[] = $link['text'];
			} else if ($link['url']){
				$result[] = '<a href="'.JRoute::_($link['url']).'" '.$title.' '. $rel .' '.$class.' '.$lightbox.'>'.$link['text'].'</a>';
			}

		?>
	
	<?php endforeach; ?>
	
	<?php echo $this->app->zlfw->applySeparators($params->find('separator._by'), $result, $params->find('separator._class')); ?>
	
<?php else : ?>
	<?php echo "No links found."; ?>
<?php endif; ?>