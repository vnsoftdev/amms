(function($){
	var Plugin = function(){};
	$.extend(Plugin.prototype, {
		name: 'ElementMediaPro',
		initialize: function(element, options) {
            this.options = $.extend({}, this.options, options);
            var $this = this;

			// apply on each new instances
			element.on('click', 'p.add a', function () {
				$this.apply(element.find('.repeatable-element'));
			});
			
			// first init
			$this.apply(element.find('.repeatable-element'));
		},
		apply: function (instances)
		{
			var $this = this;

			instances.each(function(index, instance)
			{
				var instance = $(instance);
				if (!instance.data('initialized')){
					
					var file_row	  	= instance.find('input:text.file').parent(),
						file_button  	= instance.find('.trigger .file'),
						url_row	  		= instance.find('input:text.url').parent(),
						url_button   	= instance.find('.trigger .url');

					url_row.css('margin-top', '0');
					file_button.bind('click', function() {
						file_row.prependTo(instance);
						url_row.detach();
						file_button.hide();
						url_button.show();
					});
					url_button.bind('click', function() {
						file_row.detach();
						url_row.prependTo(instance);
						file_button.show();
						url_button.hide();
					});

					instance.find('input:text.file').val() ? file_button.trigger('click') : url_button.trigger('click');

				instance.data('initialized', !0)}
			})
		}
	});
	// Don't touch
	$.fn[Plugin.prototype.name] = function() {
		var args   = arguments;
		var method = args[0] ? args[0] : null;
		return this.each(function() {
			var element = $(this);
			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();
				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}
				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}
		});
	};
})(jQuery);