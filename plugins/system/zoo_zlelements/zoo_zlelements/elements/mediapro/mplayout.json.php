<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$zoo = App::getInstance('zoo');
$player = $element->config->find('specific._player');

// set custom params
$params->set('layout', array(
	'path' => "elements:mediapro/tmpl/render/$player",
	'help' => $params->find('load.help'),
	'label' => $params->find('load.label')
));

// import the ZL Field JSON Layout
return include($zoo->path->path('zlfield:json/layout.json.php'));