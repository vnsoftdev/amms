<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{
		"_player":{
			"type":"layout",
			"label":"PLG_ZLELEMENTS_MP_PLAYER",
			"help":"PLG_ZLELEMENTS_MP_PLAYER_DESC",
			"specific": {
				"path":"elements:mediapro/tmpl/render",
				"mode":"folders"
			},
			"childs":{
				"loadfields": {
					"subfield": {
						"type":"subfield",
						"path":"elements:mediapro\/params\/{value}\/config.php"
					}
				}
			}
		}
	}';