<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');	

// JSON
return 
'{"fields": {

	"_info":{
		"type":"info",
		"specific":{
			"text":"PLG_ZLELEMENTS_MP_JPLAYER_INFO"
		}
	}

}}';