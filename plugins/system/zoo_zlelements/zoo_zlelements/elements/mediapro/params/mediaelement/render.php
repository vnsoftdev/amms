<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

// init vars
$plugin = 'jwplayer';
$el_type = $element->getElementType();

// JSON
return
'{
	"_width":{
		"type":"text",
		"label":"PLG_ZLFRAMEWORK_WIDTH",
		"default":"480"
	},
	"_height":{
		"type":"text",
		"label":"PLG_ZLFRAMEWORK_HEIGHT",
		"default":"270"
	},
	"_autoplay":{
		"type": "checkbox",
		"label": "PLG_ZLELEMENTS_MP_AUTOPLAY",
		"help": "PLG_ZLELEMENTS_MP_AUTOPLAY_DESC",
		"specific":{
			"label":"JYES"
		}
	}

}';