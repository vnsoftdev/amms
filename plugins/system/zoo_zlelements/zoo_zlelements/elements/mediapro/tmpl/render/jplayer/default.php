<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$result		= $supplied = array();
$media_list = $this->getRenderedValues($params);
$skin		= $params->find('layout._skin', 'pink.flag');

$autoplay 	= $params->find('layout._autoplay');
$width		= $params->find('layout._width');
$height		= $params->find('layout._height');

// load assets
$this->app->document->addScript('elements:mediapro/assets/players/jplayer/jquery.jplayer.min.js');
$this->app->document->addStylesheet('elements:mediapro/assets/players/jplayer/skins/'.$skin.'/style.css');
$this->app->document->addScript('elements:mediapro/assets/players/jplayer/add-on/jplayer.playlist.min.js');
$this->app->document->addScript('elements:mediapro/tmpl/render/jplayer/default/script.min.js');
$this->app->document->addStylesheet('elements:mediapro/tmpl/render/jplayer/default/style.css');

// render media
foreach ($media_list['result'] as $index => $media_obj)
{
	// set the element data to it's file index
	$this->seek($index);

	// init vars
	$id = uniqid();
	$media = array();

	switch ($this->getVideoFormat($media_obj->getURL())) {
		case 'vimeo':
		case 'youtube':
		case 'youtu.be':
			if (!$params->find('layout._playlist', 0)) {
				$result[] = $this->renderEmbedURL($width, $height, $media_obj->getURL());
			}
			break;

		case 'swf':
			if (!$params->find('layout._playlist', 0)) {
				$this->app->document->addScript('elements:media/assets/js/swfobject.js');
				$result[] = "<div id=\"$id\">
							<p><a href=\"http://www.adobe.com/go/getflashplayer\"><img src=\"http://www.adobe.com/images/shared/download_buttons/get_adobe_flash_player.png\" alt=\"Get Adobe Flash player\" /></a></p>
						</div>
						<script type=\"text/javascript\">
							swfobject.embedSWF(\"$source\", \"$id\", \"$width\", \"$height\", \"7.0.0\", \"\", {}, {allowFullScreen:\"true\", wmode: \"transparent\", play:\"$autoplay\" });
						</script>";
			}
			break;

		default: // case file

			$ext = $media_obj->getExtension();
			$ext = $ext == 'mp4' ? 'm4v' : $ext; // MP4 Media workaround
			$supplied[] = $ext;

			$media[$ext] 		= $media_obj->getURL();
			$media['title'] 	= $media_obj->getTitle($this->get('title'));
			$media['poster'] 	= $this->getPreviewImg($params)->get('url');

			// if playlist save the media for later processing
			if ($params->find('layout._playlist', 0)) {
				$playlist[] = $media;

			} else {
				$playlist = array($media);

				// process media
				if ($layout = $this->getLayout("render/jplayer/default/layout.php")) {
					$result[] = $this->renderLayout($layout, compact('params', 'id', 'playlist', 'supplied'));
				}
			}
	}
}

// processs playlist now
if ($params->find('layout._playlist', 0)) {
	if ($layout = $this->getLayout("render/jplayer/default/layout.php")) {
		$result[] = $this->renderLayout($layout, compact('params', 'id', 'playlist', 'supplied'));
	}
}

// set the separator	
$separator = $params->find('separator._by_custom') != '' ? $params->find('separator._by_custom') : $params->find('separator._by');

// apply separator
$result = $this->app->zlfw->applySeparators($separator, $result, $params->find('separator._class'), $params->find('separator._fixhtml'));

// replace short codes and display
echo $this->app->zlfw->replaceShortCodes($result, array('item' => $this->_item));