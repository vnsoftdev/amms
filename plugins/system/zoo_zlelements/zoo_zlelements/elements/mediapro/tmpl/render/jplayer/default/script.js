/* ===================================================
 * MediaProJPlayer v0.1
 * https://zoolanders.com/extensions/media-pro
 * ===================================================
 * Copyright (C) JOOlanders SL 
 * http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * ========================================================== */
(function ($) {
	var Plugin = function(){};
	Plugin.prototype = $.extend(Plugin.prototype, {
		name: 'MediaProJPlayer',
		options: {
			id: '',
			playlist: {},
			playlist_on: false,
			supplied: '',
			swfpath: '',
			width: '',
			height: '',
			autoplay: 0,
			lopp: 0,
			screenmode: 1
		},
		initialize: function(jp_container, options) {
			this.options = $.extend({}, this.options, options);
			var $this = this;

			// set container width
			jp_container.width($this.options.width);

			// actions if screen enabled
			if ($this.options.screenmode) {

				// set the play button height for centering purpose
				$('.jp-video-play', jp_container).height($this.options.height);

			// actions if not
			} else {

				// add the no screen class
				jp_container.addClass('jp-noscreen');

				// hide the screen - we can't hide the jp dom as will brake the plugin init
				// but we can set the height to 0
				$this.options.height = 0;
				$('.jp-video-play', jp_container).height(0);

				// hide unnecesary toggles
				$('.jp-full-screen, .jp-restore-screen', jp_container).parent().hide();
			}

			jp_container.addClass('jp-size-mini').data('size', 'mini');
			if (jp_container.width() > 500) {
				// remove the mini size class
				jp_container.removeClass('jp-size-mini').addClass('jp-size-middle').data('size', 'middle');
			}

			// // if width less than
			// $.onMediaQuery('(max-width: 500px)', {
			// 	valid: function() {
			// 		// add the mini size class
			// 		jp_container.addClass('jp-size-mini');
			// 		jp_container.removeClass('jp-size-middle');
			// 	}
			// 	// the 'novalid' trigger is not executed at start, that's why using an extra query
			// });

			// // if width more than
			// $.onMediaQuery('(min-width: 500px)', {
			// 	valid: function() {
			// 		// remove the mini size class
			// 		jp_container.removeClass('jp-size-mini');
			// 		jp_container.addClass('jp-size-middle');
			// 	}
			// });

			// set events actions
			$('#jquery_jplayer_'+$this.options.id)
			.on($.jPlayer.event.pause, function(event) {
				$('.jp-video-play', jp_container).show();
			})
			.on($.jPlayer.event.play, function(event) {
				$('.jp-video-play', jp_container).hide();
			})
			.on($.jPlayer.event.resize, function(event) {
				// when getting back from full mode, recover size class
				jp_container.removeClass('jp-size-mini');
				if (!jp_container.hasClass('jp-video-full')) {
					jp_container.addClass('jp-size-'+jp_container.data('size'));
				} else {
					// $('.jp-volume-controls', jp_container).prependTo($('.jp-col-controls .jp-col-wrapper', jp_container));
					
				}
			});

			// init player
			new jPlayerPlaylist(
			{
				jPlayer: '#jquery_jplayer_'+$this.options.id,
				cssSelectorAncestor: '#jp_container_'+$this.options.id
			}, 
			$this.options.playlist,
			{
				swfPath: $this.options.swfpath,
				supplied: $this.options.supplied,
				size: {
					width: $this.options.width,
					height: $this.options.height,
					cssClass: '' // set to none to avoid the default class
				},
				errorAlerts: true,
				warningAlerts: true,
				loop: $this.options.loop,
				ready: function () {
					if ($this.options.autoplay) $(this).jPlayer('play');
				}
			});
		}
	});
	// Don't touch
	$.fn[Plugin.prototype.name] = function() {
		var args   = arguments;
		var method = args[0] ? args[0] : null;
		return this.each(function() {
			var element = $(this);
			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();
				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}
				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}
		});
	};
})(jQuery);