<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$skin		= $params->find('layout._skin');
$skin   	= $this->app->path->url("elements:mediapro/assets/players/jwplayer/skins/$skin/$skin.xml");
$jwpath 	= $this->app->path->url('elements:mediapro/assets/players/jwplayer/jwplayer.flash.swf');
$cfront 	= $params->find('layout._cloudfront', '');
$width    	= $params->find('layout._width', 300);
$height   	= $params->find('layout._height', 200);
$autoplay 	= $params->find('layout._autoplay', 0);

?>

<div id="jwplayer_<?php echo $id ?>">
	<div style="text-align: center;"><?php echo JText::_('PLG_ZLELEMENTS_MP_NO_JAVASCRIPT') ?></div>
</div>

<script type="text/javascript">
jQuery(function($){

	jwplayer("jwplayer_<?php echo $id ?>").setup({

		// play list
		playlist: <?php echo json_encode($playlist) ?>,
		<?php if ($size = $params->find('layout._playlist')) : ?>
		listbar: {
			position: "<?php echo $params->find('layout._playlist_position', 'none') ?>"
			<?php if ($size = $params->find('layout._playlist_size')) : ?>,
			size: <?php echo $size ?>
			<?php endif; ?>
		},
		<?php endif; ?>

		// options
		autostart: "<?php echo $autoplay ?>",

		// layout
		width: <?php echo $width ?>,
		height: <?php echo $height ?>
		<?php echo ($skin ? ",\n".'skin: "'.$skin.'"' : '') ?>
	});
	
});
</script>