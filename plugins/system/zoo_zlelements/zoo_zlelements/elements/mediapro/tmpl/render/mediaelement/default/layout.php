<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init vars
$cfront 	= $params->find('layout._cloudfront', '');
$width    	= $params->find('layout._width', 300);
$height   	= $params->find('layout._height', 200);
$autoplay 	= $params->find('layout._autoplay', 0);

$width_attr  = $width ? ' width="'.$width.'"' : '';
$height_attr = $height && $playlist[0]['ext'] != 'mp3' ?  ' height="'.$height.'"' : '';
$autoplay 	 = $autoplay ? ' autoplay="autoplay"' : '';
$poster 	 = $playlist[0]['poster'] ? ' poster="'.$playlist[0]['poster'].'"' : '';

// set image preview
$image = $this->getPreviewImg($params);

echo '<'.$playlist[0]['tag'].' src="'.$playlist[0]['src'].'"'.$poster.$width_attr.$height_attr.$autoplay.$playlist[0]['type'].'></'.$playlist[0]['tag'].'>';
?>