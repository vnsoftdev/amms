<?php
/**
* @package		ZL Elements
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

// init vars
$player = $element->config->find('specific._player');
$el_type = $element->getElementType();

// JSON
return 
'{"fields": {

	"layout_wrapper":{
		"type": "fieldset",
		"min_count":"1",
		"fields": {

			"layout_separator":{
				"type":"separator",
				"text":"PLG_ZLFRAMEWORK_DEFAULT_LAYOUT",
				"big":1
			},

			"player_options_subfield":{
				"type":"subfield",
				"path":"elements:'.$el_type.'\/params\/'.$player.'\/render.php"
			},

			"cloudfront_subfield":{
				"type":"subfield",
				"path":"elements:'.$el_type.'\/params\/cloudfront.php"
			}

		}
	}

}}';