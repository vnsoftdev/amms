<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{
		"_multiselect":{
			"type":"checkbox",
			"label":"PLG_ZLELEMENTS_RCP_MULTI",
			"help":"PLG_ZLELEMENTS_RCP_MULTI_DESC",
			"default":"0",
			"specific":{
				"label":"JYES"
			},
			"dependents":"_fieldsize > 1"
		},

		"_relatetoitem":{
			"type":"checkbox",
			"label":"PLG_ZLELEMENTS_RCP_RELATE_TO_ITEM",
			"help":"PLG_ZLELEMENTS_RCP_RELATE_TO_ITEM_DESC",
			"specific":{
				"label":"JYES"
			}
		}

	}';