<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
   Class: ElementRelatedCategoriesPro
	   The category element class
*/
class ElementRelatedCategoriesPro extends ElementPro implements iSubmittable {

	/*
	   Function: Constructor
	*/
	public function __construct() {

		// call parent constructor
		parent::__construct();

		// register events
		$this->app->event->dispatcher->connect('element:afterdisplay', array($this, 'afterDisplay'));
		$this->app->event->dispatcher->connect('item:beforeSaveCategoryRelations', array($this, 'saveCategoryRelations'));
		$this->app->event->dispatcher->connect('submission:saved', array($this, 'triggerCategoryRelations'));

		// load default and current language 
		$this->app->system->language->load("plg_system_zoo_zlelements_relatedcategoriespro", $this->app->path->path('zlpath:'), 'en-GB', true);
		$this->app->system->language->load("plg_system_zoo_zlelements_relatedcategoriespro", $this->app->path->path('zlpath:'), null, true);
	}

	/*
		Function: afterDisplay
			Change the element layout after it has been displayed

		Parameters:
			$event - object
	*/
	public static function afterDisplay($event)
	{
		$event['html'] = str_replace('class="element element-relatedcategoriespro', 'class="element element-relatedcategories element-relatedcategoriespro', $event['html']);
	}

	/*
		Function: triggerCategoryRelations
			On Submission the item:beforeSaveCategoryRelations event will not be triggered by default,
			as a solution we just execute the saveCategoryItemRelations function wich will do the rest.

		Parameters:
			$event - object
	*/
	public function triggerCategoryRelations($event)
	{
		$item = $event['item'];
		$element = $item->getElement($this->identifier);
		
		if ($element->config->find('specific._relatetoitem'))
			$this->app->category->saveCategoryItemRelations($item, array());
	}

	/*
		Function: saveCategoryRelations

		Parameters:
			$event - object
	*/
	public function saveCategoryRelations($event)
	{
		// init vars
		$item = $event->getSubject();
		$el_id = $this->identifier;
		$element = $item->getElement($el_id);
		$categories = $event['categories'];

		// optional but only if relating categories to item from same app
		if($element->config->find('specific._relatetoitem')
			&& $item->getApplication()->id == $element->config->find('application._chosenapps')){

			// get element cats and add them
			$new_cat = $element->get('category', array());
			if(!empty($new_cat)){
				$event['categories'] = array_merge($categories, $new_cat);
			}
		}
	}
	
	/*
		Function: hasValue
			Checks if the element's value is set.

	   Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/	
	public function hasValue($params = array()) {
		$value = $this->getRenderedValues($this->app->data->create($params));
		return !empty($value);
	}
	
	/*
		Function: getSearchData
			Get elements search data.

		Returns:
			String - Search data
	*/
	public function getSearchData() {
		if($application = $this->getApplications()){
			$options = $this->get('category', array());
			
			$result = array();
			foreach ($application->getCategoryTree() as $category) {
				if (in_array($category->id, $options)) {
					$result[] = $category->id;
				}
			}
			
			return (empty($result) ? null : implode("\n", $result));
		}
	}
	
	/*
		Function: getRenderedValues
			renders the element content

		Returns:
			array
	*/
	public function getRenderedValues($params = array())
	{
		$result = array();
		switch ($params->find('specific._mode', 'categories')){
			case 'categories':
				$category_ids = $this->get('category', array());
				$categories = $this->app->table->category->getById($category_ids, true);
				
				// if no value it can be inherited from Item Primary Cat
				if (empty($categories) && $params->find('specific._inherit_primarycat')) {
					$category_ids = array(0 => $this->_item->getPrimaryCategoryId());
					$categories = $this->app->table->category->getById($category_ids, true);
				}

				foreach ($categories as $category) {
					$name = $params->find('specific._link_text', 0) ? $params->find('specific._link_text') : $category->name;
					if ($params->find('specific._link_to_item', false))
						$result['result'][] = '<a href="'.$this->app->route->category($category).'">'.$name.'</a>';
					else
						$result['result'][] = $name;
					}
				break;
				
			case 'items':
				// get items, filter and sort
				$items = $this->_getCategoryItems($params);
				
				if (!empty($items)){
					$items = $this->_filterItems($items, $params->get('filter'));
					$items = $this->_orderItems($items, $params->get('order'));

					// create output
					foreach($items as $item) {
						// as we support crossapp feature, renderer path changes each item
						$renderer = $this->app->renderer->create('item')->addPath( array($item->getApplication()->getTemplate()->getPath(), $this->app->path->path('component.site:')) );
						$itemlayout = $params->find('specific._rlayout');
					
						$path   = 'item';
						$prefix = 'item.';
						$type   = $item->getType()->id;
						if ($renderer->pathExists($path.DIRECTORY_SEPARATOR.$type)) {
							$path   .= DIRECTORY_SEPARATOR.$type;
							$prefix .= $type.'.';
						}
						
						if (in_array($itemlayout, $renderer->getLayouts($path))) {
							$result['result'][] = $renderer->render($prefix.$itemlayout, array('item' => $item));
						} elseif ($params->find('specific._link_to_item', false) && $item->getState()) {
							$name = $params->find('specific._link_text', 0) ? $params->find('specific._link_text') : $item->name;
							$result['result'][] = '<a href="'.$this->app->route->item($item).'" title="'.$name.'">'.$name.'</a>';
						} else {
							$result['result'][] = $item->name;
						}
					}
				}
				break;
		}
		
		return $result;

	}
	
	/*
	   Function: _orderItems
		   Order the items before render

	   Returns:
		   String - array
	*/	
	protected function _orderItems($items, $order) {

		$items = (array) $items;
		$order = (array) $order;
		$sorted = array();
		$reversed = false;

		// remove empty values
		$order = array_filter($order);

		// if random return immediately
		if (in_array('_random', $order)) {
			shuffle($items);
			return $items;
		}

		// get order dir
		if (($index = array_search('_reversed', $order)) !== false) {
			$reversed = true;
			unset($order[$index]);
		} else {
			$reversed = false;
		}

		// order by default
		if (empty($order)) {
			return $reversed ? array_reverse($items, true) : $items;
		}

		// if there is a none core element present, ordering will only take place for those elements
		if (count($order) > 1) {
			$order = array_filter($order, create_function('$a', 'return strpos($a, "_item") === false;'));
		}

		if (!empty($order)) {

			// get sorting values
			foreach ($items as $item) {
				foreach ($order as $identifier) {
					if ($element = $item->getElement($identifier)) {
						$sorted[$item->id] = strpos($identifier, '_item') === 0 ? $item->{str_replace('_item', '', $identifier)} : $element->getSearchData();
						break;
					}
				}
			}

			// do the actual sorting
			$reversed ? arsort($sorted) : asort($sorted);

			// fill the result array
			foreach (array_keys($sorted) as $id) {
				if (isset($items[$id])) {
					$sorted[$id] = $items[$id];
				}
			}

			// attach unsorted items
			$sorted += array_diff_key($items, $sorted);

		// no sort order provided
		} else {
			$sorted = $items;
		}

		return $sorted;
	}
	
	protected function _filterItems($items, $filter) {

		$items = (array) $items;
		$filter = (array) $filter;
		$filtered = array();

		// remove empty values
		$filter = array_filter($filter);
		
		if (!empty($filter)) {
		
			// filter by type
			if (!empty($filter['_types'])) foreach ($items as $item) {
				if (in_array($item->getType()->id,  $filter['_types'])) {
					$filtered[$item->id] = $item;
				}
			} else {$filtered = $items;}
			
			$offset = array_key_exists('_offset', $filter) ? $filter['_offset'] : 0;
			$limit  = array_key_exists('_limit', $filter) ? $filter['_limit'] : null;
			
			// set offset/limit
			$filtered = array_slice($filtered, $offset, $limit, true);
		
		// no sort order provided
		} else {
			$filtered = $items;
		}

		return $filtered;
	}
	
	public function _getCategoryItems($params) {
	
		$category_ids = $this->get('category', array());
	
		if ($categories = $this->app->table->category->getById($category_ids, true)){
		
			$application = $this->getApplications();
		
			$limit = $params->get('count', 0);
			if (!$params->find('specific._render_curr_item') && !empty($limit)){
				$limit = $limit+1;
				// apply extra limit if we are not rendering current item
			}
			
			$items = $this->app->table->item->getByCategory($application->id, $category_ids, true, null, $params->get('order', array('_itemname')), 0, $limit, $params->get('chosen_types', array()));
			if ( !$params->find('specific._render_curr_item', 0) && isset($items[$this->_item->id]) ){
				unset($items[$this->_item->id]); // unset this item from the listing
			} elseif(!$params->find('specific._render_curr_item') && !empty($limit)) {
				// if current item is not in array remove the extra item added by extra limit
				array_pop($items);
			}
			
			return $items;
		}
		return false;
	}
	
	/*
	   Function: getApplications
			Get the chosen app from config,
			in case value is 0, get the item app
	   Returns:
		   object
	*/	
	public function getApplications() {
	
		$table = $this->app->table->application;
		$apps = $this->config->find('application._chosenapps', 0) ? $this->config->find('application._chosenapps') : array($this->_item->application_id); // must be passed as array

		$applications = $this->app->zlfw->getApplications($apps);

		if(empty($applications)){
			return array_shift($this->app->zlfw->getApplications($this->_item->application_id));
		} else {
			// let's return just the first index for now
			return array_shift($applications);
		}
	}

	/*
	   Function: edit
		   Renders the edit form field.

	   Returns:
		   String - html
	*/
	public function edit() {
		if ($layout = $this->getLayout('edit/edit.php')) {
			return $this->renderLayout($layout,
				array()
			);
		}
	}
	
	/*
		Function: renderSubmission
			Renders the element in submission.

	   Parameters:
			$params - AppData submission parameters

		Returns:
			String - html
	*/
	public function renderSubmission($params = array())
	{
		// init vars
		$trusted_mode = $params->get('trusted_mode');
		$layout		  = $params->find('layout._layout', 'default.php');

		if ($layout = $this->getLayout("submission/$layout")) {
			return $this->renderLayout($layout, compact('params', 'trusted_mode'));
		}
	}

	/*
		Function: validateSubmission
			Validates the submitted element

	   Parameters:
			$value  - AppData value
			$params - AppData submission parameters

		Returns:
			Array - cleaned value
	*/
	public function validateSubmission($value, $params) {
		$options = array('required' => $params->get('required'));
		$messages = array('required' => JText::_('PLG_ZLELEMENTS_RCP_CHOOSE_SOME_OPTION'));
		$clean = $this->app->validator
				->create('foreach', $this->app->validator->create('string', $options, $messages), $options, $messages)
				->clean($value->get('category'));

		return array('category' => $clean);
	}

}