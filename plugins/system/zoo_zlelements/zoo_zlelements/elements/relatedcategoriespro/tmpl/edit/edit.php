<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// init vars
	$application = $this->getApplications();
	$options = array();
	$select_id = "#elements{$this->identifier}category";

	// get root cat, but keep old param compatibility
	$root_cat = $this->config->find('application._chosencats', $this->config->find('application._chosencat', 0));

	// multiselect
	$multiselect = $this->config->find('specific._multiselect', array());
    if (!$multiselect) {
        $options[] = $this->app->html->_('select.option', '', '-' . JText::_('Select Category') . '-');
    }

    // set attributes
	$attribs = ($multiselect) ? 'size="5" multiple="multiple"' : '';

	// add necesary CSS for Chosen
	$this->app->document->addStyleDeclaration('
		.creation-form .element-relatedcategoriespro,
		.creation-form .element-relatedcategoriespro > div {overflow: visible}');
?>

<div id="<?php echo $this->identifier; ?>">
	<?php echo $this->app->html->_('zoo.categorylist', $application, $options, $this->getControlName('category', true), $attribs, 'value', 'text', $this->get('category', array()), false, false, $root_cat, '-&nbsp;', '.&nbsp;&nbsp;&nbsp;', '&nbsp;&nbsp;'); ?>
</div>

<script type="text/javascript">
	jQuery(function($) {

		// chosen integration
		<?php if ($this->app->joomla->isVersion('2.5')) : ?>
			$('<?php echo $select_id ?>').chosen({ disable_search_threshold: 10, allow_single_deselect: true });
		<?php else : ?>
			<?php JHtml::_('formbehavior.chosen', $select_id); ?>
			// Add here since on 3.0 the options are hardcoded in the constructor of the PHP method
			$('<?php echo $select_id ?>').data('chosen').search_contains = true;
		<?php endif; ?>
	});
</script>