<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	$_main_icon = array('Red dot' => 'red-dot', 'Orange dot' => 'orange-dot', 'Yellow dot' => 'yellow-dot', 'Pink dot' => 'pink-dot', 'Blue dot' => 'blue-dot', 'Lightblue dot' => 'ltblue-dot', 'Green dot' => 'green-dot', 'Purple dot' => 'purple-dot', 'Red pushpin' => 'red-pushpin', 'Yellow pushpin' => 'ylw-pushpin', 'Pink pushpin' => 'pink-pushpin', 'Blue pushpin' => 'blue-pushpin', 'Lightblue pushpin' => 'ltblu-pushpin', 'Green pushpin' => 'grn-pushpin', 'Purple pushpin' => 'purple-pushpin');


	return 
	'{"fields": {

		"layout_wrapper":{
			"type": "fieldset",
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[layout\]/').',
				"replacement":"[specific]"
			},
			"fields": {
	
				"layout_sep":{
					"type": "separator",
					"text": "PLG_ZLFRAMEWORK_DEFAULT_LAYOUT",
					"big": "1"
				},
				"_mode":{
					"type": "select",
					"label": "PLG_ZLFRAMEWORK_MODE",
					"specific": {
						"options": {
							"PLG_ZLFRAMEWORK_CATEGORIES":"categories",
							"PLG_ZLFRAMEWORK_ITEMS":"items"
						}
					},
					"default": "categories",
					"dependents": "rcp-ritems > items | _inherit_primarycat > categories"
				},
				"rcp-ritems":{
					"type": "wrapper",
					"fields": {
						"_rlayout":{
							"type": "itemLayoutList",
							"label": "PLG_ZLFRAMEWORK_RELATED_LAYOUT",
							"help": "OVERRIDE_DEFAULT_OUTPUT",
							"specific":{
								"options":{
									"PLG_ZLELEMENTS_RCP_WRPR_ITEM_NAME":""
								}
							}
						},
						"_render_curr_item":{
							"type": "checkbox",
							"label": "PLG_ZLELEMENTS_RCP_WRPR_RENDER_CURRENT_ITEM",
							"specific":{
								"label": "JYES"
							}
						}
					}
				},
				"_inherit_primarycat":{
					"type": "checkbox",
					"label": "PLG_ZLELEMENTS_RCP_WRPR_INHERIT_PRIMARY_CATEGORY",
					"specific":{
						"label": "JYES"
					}
				},
				"rcp-linkoptions":{
					"type": "wrapper",
					"fields": {
						"_link_to_item":{
							"type": "checkbox",
							"label": "PLG_ZLFRAMEWORK_LINK_TO_ITEM",
							"help":"ZL_LINK_TO_ITEM",
							"specific":{
								"label": "JYES"
							}
						},
						"_link_text":{
							"type": "text",
							"label": "PLG_ZLELEMENTS_RCP_WRPR_ITEM_LINK_TEXT"
						}
					}
				}

			}
		}

	}}';