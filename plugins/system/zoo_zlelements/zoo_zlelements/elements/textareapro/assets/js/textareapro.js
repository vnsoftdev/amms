/* Copyright (C) JOOlanders SL http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only */

(function ($) {
    var Plugin = function(){};
    Plugin.prototype = $.extend(Plugin.prototype, {
		name: 'ElementTextareaPro',
		options: {
			msgLoadEditor: 'Load editor',
			tinymce_url: null,
			inputLimit: 0,
			msgRemText: '%n characters remaining.',
			OnLoadEditor: 0,
			msgLimitText: 'Field limited to a %n characters.'
		},
		initialize: function (element, options) {
			this.options = $.extend({}, this.options, options);
			var $this = this;
			
			// on each new image apply
			element.delegate('p.add a', 'click', function () {
				$this.apply(element.find('textarea'), true);
			});
			
			$this.apply(element.find('textarea'));
			
		},
		apply: function (textareas, newitem) {
			var $this = this;

			textareas.each(function (index) {
				
				if (!$(this).data('initialized')) { 
				
					var textarea = $(this);

					if ($this.options.OnLoadEditor) {
						// TinyMCE editor loaded by Default
						$this.loadEditor(textarea);
						textarea.closest('.repeatable-element').find('.sort').remove();
					} else {
						// TinyMCE editor
						$this.options.tinymce_url && $('<span title="' + $this.options.msgLoadEditor + '" class="load" />').insertAfter($(this)).bind("click", function () {
							$this.loadEditor(textarea);
							textarea.closest('.repeatable-element').find('.sort').remove();
							$(this).remove();
						});
					}
					// Input Limiter
					$this.options.inputLimit && textarea.inputlimiter({
						limit: parseFloat($this.options.inputLimit),
						remText: $this.options.msgRemText,
						limitText: $this.options.msgLimitText
					});
					
					// clean new textarea values
					if (newitem){ textarea.val('') };
					
				$(this).data('initialized', !0); }
				
			});
			
		},
		loadEditor: function (textarea) {

			var $this = this;
		
			textarea.tinymce({  
				// Location of TinyMCE script
				script_url: $this.options.tinymce_url,
				height: textarea.height(),

				theme : "advanced",
				mode : "textareas",
				plugins : "advlist,inlinepopups,paste,tabfocus,advimage",
				
				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,|,bullist,numlist,outdent,indent,|,link,|,image,code",
				theme_advanced_buttons2 : '',
				theme_advanced_buttons3 : '',
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				theme_advanced_resizing_min_height : textarea.height(), // it make the editor match the textarea current height
				theme_advanced_resizing_min_width  : 415,
				theme_advanced_resizing_max_width  : 620,

				// let's extend valid tags
				extended_valid_elements : 'pre[class]',
				
				// Plugins Options
				paste_auto_cleanup_on_paste : true, // Autoclins past code from Word
				dialog_type : "modal"
				
				/* setup : function(ed) {
					ed.onLoadContent.add(function(ed, cm) {
						//callback();
					});
				} */
				
			});
		}
	});
	/* Don't touch */
	$.fn[Plugin.prototype.name] = function() {
		var args   = arguments;
		var method = args[0] ? args[0] : null;
		return this.each(function() {
			var element = $(this);
			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();
				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}
				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}
		});
	};
})(jQuery);