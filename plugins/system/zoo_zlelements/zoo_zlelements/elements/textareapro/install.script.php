<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	/* List of obsolete files and folders */
	$obsolete = array(
		'files'	=> array(
			'plugins/system/zoo_zlelements/zoo_zlelements/elements/textareapro/textareapro.js'
		),
		'folders' => array(
		)
	);

	if($type == 'install' || $type == 'update')
	{
		if($type == 'install') {
			echo '<p><strong>Textarea Pro</strong> Element installed succesfully.</p>';
		} else {
			echo '<p><strong>Textarea Pro</strong> Element updated succesfully.</p>';
		}

		/* Removes obsolete files and folders */
		if(!empty($obsolete['files'])) foreach($obsolete['files'] as $file) {
			$f = JPATH_ROOT.'/'.$file;
			if(!JFile::exists($f)) continue;
			JFile::delete($f);
		}

		if(!empty($obsolete['folders'])) foreach($obsolete['folders'] as $folder) {
			$f = JPATH_ROOT.'/'.$folder;
			if(!JFolder::exists($f)) continue;
			JFolder::delete($f);
		}
	}
	else if($type == 'uninstall')
	{
		
	}