<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	$edit_path = array();
	$edit_path[] = 'zlpath:elements\/'.$element->getElementType().'\/tmpl\/edit';
	if (is_object($element->getType())) $edit_path[] = 'applications:'.$element->getType()->getApplication()->getGroup().'\/elements\/'.$element->getElementType().'\/tmpl\/edit'; // zoo template overide path

	return 
	'{
		"_edit_sublayout":{
			"type": "layout",
			"label": "PLG_ZLFRAMEWORK_EDIT_SUBLAYOUT",
			"help":"PLG_ZLFRAMEWORK_EDIT_SUBLAYOUT_DESC",
			"default": "_edit.php",
			"specific": {
				"path":"'.implode(',', $edit_path).'",
				"regex":'.json_encode('^([_][_A-Za-z0-9]*)\.php$').',
				"min_opts":"2",
				"hidden_opts":"_edit_yoo.php"
			}
		},
		"_jplugins":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_TEXTS_PLUGINS",
			"help": "PLG_ZLELEMENTS_TEXTS_PLUGINS_DESC",
			"specific":{
				"label":"PLG_ZLELEMENTS_TEXTS_PLUGINS_CHECK"
			}
		},
		"_extra_options":{
			"type": "select",
			"label": "PLG_ZLELEMENTS_TEXTS_EXTRA_OPTS",
			"help": "PLG_ZLELEMENTS_TEXTS_EXTRA_OPTS_DESC",
			"specific":{
				"options":{
					"PLG_ZLFRAMEWORK_SHOW":"1",
					"PLG_ZLFRAMEWORK_HIDE":""
				}
			}
		},
		"_editor":{
			"type": "select",
			"label": "PLG_ZLELEMENTS_TEXTS_EDITOR",
			"help": "PLG_ZLELEMENTS_TEXTS_EDITOR_DESC",
			"default": "customeditor",
			"specific":{
				"options":{
					"PLG_ZLELEMENTS_TEXTS_TINY_EDITOR":"customeditor",
					"PLG_ZLELEMENTS_TEXTS_JOOMLA_EDITOR":"joomlaeditor",
					"PLG_ZLELEMENTS_TEXTS_NO_EDITOR":"noeditor"
				}
			},
			"dependents": "_editor_enabled_by_defualt > customeditor | _editor_size > customeditor OR noeditor | _input_limit > noeditor "
		},
		"_editor_enabled_by_defualt":{
			"type": "checkbox",
			"label": "PLG_ZLELEMENTS_TEXTS_ENABLED_BY_DEFAULT",
			"help":"PLG_ZLELEMENTS_TEXTS_ENABLED_BY_DEFAULT_DESC",
			"default": "0",
			"specific":{
				"label":"JYES"
			}
		},
		"_editor_size":{
			"type": "select",
			"label": "PLG_ZLELEMENTS_TEXTS_INPUT_SIZE",
			"help":"PLG_ZLELEMENTS_TEXTS_INPUT_SIZE_DESC",
			"default": "small",
			"specific":{
				"options":{
					"PLG_ZLELEMENTS_TEXTS_SMALL":"small",
					"PLG_ZLFRAMEWORK_DEFAULT":"default",
					"PLG_ZLELEMENTS_TEXTS_BIG":"big"
				}
			}
		},
		"_input_limit":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_TEXTS_MAX_CHARACTERS",
			"help": "PLG_ZLELEMENTS_TEXTS_MAX_CH_EDIT_DESC"
		},
		"default":{
			"type": "textarea",
			"label": "PLG_ZLFRAMEWORK_DEFAULT_VALUE",
			"help": "PLG_ZLFRAMEWORK_DEFAULT_VALUE_DESC",
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[specific\]/').',
				"replacement":""
			}
		}
	}';