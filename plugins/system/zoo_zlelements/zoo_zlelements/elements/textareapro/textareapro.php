<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
   Class: ElementTextarea
   The textarea element class
*/
class ElementTextareaPro extends ElementRepeatablePro implements iSubmittable {

	const ROWS = 20;
	const COLS = 60;
	const MAX_HIDDEN_EDITORS = 5;

	/*
	   Function: Constructor
	*/
	public function __construct() {

		// call parent constructor
		parent::__construct();

		// set callbacks
		$this->registerCallback('returndatabyindex');

		// register events
		$this->app->event->dispatcher->connect('element:afterdisplay', array($this, 'afterDisplay'));

		// load default and current language 
		$this->app->system->language->load("plg_system_zoo_zlelements_textareapro", $this->app->path->path('zlpath:'), 'en-GB', true);
		$this->app->system->language->load("plg_system_zoo_zlelements_textareapro", $this->app->path->path('zlpath:'), null, true);
	}

	/*
		Function: afterDisplay
			Change the element layout after it has been displayed

		Parameters:
			$event - object
	*/
	public static function afterDisplay($event)
	{
		$event['html'] = str_replace('class="element element-textareapro', 'class="element element-textarea element-textareapro', $event['html']);
	}
	
	/*
		Function: bindData
			Set data through data array.

		Parameters:
			$data - array

		Returns:
			Void
	*/
	public function bindData($data = array()) {
		// set raw input for textarea
		$post = $this->app->request->get('post', JREQUEST_ALLOWRAW);
		foreach ($data as $index => $instance_data) {
			if (isset($post['elements'][$this->identifier][$index]['value'])) {
				$data[$index]['value'] = $post['elements'][$this->identifier][$index]['value'];
			}
		}

		parent::bindData($data);
	}

	/*
		Function: _getSearchData
			Get repeatable elements search data.

		Returns:
			String - Search data
	*/
	protected function _getSearchData() {
		// clean html tags
		$value  = $this->app->object->create('JFilterInput')->clean($this->get('value', ''));

		return (empty($value) ? null : $value);
	}
	
	/*
		Function: getRenderedValues
			renders the element content

		Returns:
			array
	*/
	public function getRenderedValues($params=array(), $mode=false, $opts=array()) 
	{
		// get results
		$result = parent::getRenderedValues($params, $mode, $opts);
	
		if (empty($result)) return null; // if no results abort
		
		// trigger content plugins
		if ($this->config->find('specific._jplugins')) {
			for ($i = 0; $i < count($result['result']); $i++) {
				switch ($mode)
				{
					// WK Slideset // WK Accordion
					case 'slideset.default': case 'accordion.default':
						$result['result'][$i]['content'] = $this->app->zoo->triggerContentPlugins($result['result'][$i]['content']);
						break;

					default:
						$result['result'][$i] = $this->app->zoo->triggerContentPlugins($result['result'][$i]);
						break;
				}
			}
		}
		
		// clean the results from HTML tags
		if ($params->find('specific._strip_tags')) {
			for ($i = 0; $i < count($result['result']); $i++) {
				switch ($mode)
				{
					// WK Slideset // WK Accordion
					case 'slideset.default': case 'accordion.default':
						$result['result'][$i]['content'] = strip_tags($result['result'][$i]['content']);
						break;

					default:
						$result['result'][$i] = strip_tags($result['result'][$i]);
						break;
				}
			}
		}
		
		// truncate
		if ($max_length = $params->find('specific._max_car', 0))
		{
			$tr_mode = $params->find('specific._max_car_mode', 1);
			$tr_suffix = $params->find('specific._max_car_suffix', '...');

			// group all text & truncate
			if (isset($max_length) && $max_length > 0 && count($result['result']) > 1 && $tr_mode == 1)
			{
				$result_len = 0;
				for ($i = 0; $i < count($result['result']); $i++) switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
				{
					// WK Slideset // WK Accordion
					case 'slideset': case 'accordion':
						$text = $result['result'][$i]['content'];
						$result_len += strlen(strip_tags($text));
						if ($result_len > $max_length){
							$truncate_len = $i > 1 ? $result_len - $max_length : $max_length;
							$result['result'][$i]['content'] = $this->app->zlstring->truncate($text, $truncate_len, $tr_suffix);
							$result['result'] = array_slice($result['result'], 0, $i+1); // remove the rest
							$result['report']['limited'] = true;
							break;
						} else { $result['result'][$i]['content'] = $text; }
						break;

					default:
						$text = $result['result'][$i];
						$result_len += strlen(strip_tags($text));
						if ($result_len > $max_length){
							$truncate_len = $i > 1 ? $result_len - $max_length : $max_length;
							$result['result'][$i] = $this->app->zlstring->truncate($text, $truncate_len, $tr_suffix);
							$result['result'] = array_slice($result['result'], 0, $i+1); // remove the rest
							$result['report']['limited'] = true;
							break;
						} else { $result['result'][$i] = $text; }
						break;
				}
			}

			// truncate individualy
			else if (isset($max_length) && $max_length > 0)
			{
				$limited_once = false;
				for ($i = 0; $i < count($result['result']); $i++) switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
				{
					// WK Slideset // WK Accordion
					case 'slideset': case 'accordion':
						if (strlen($result['result'][$i]['content']) > $max_length){
							$result['result'][$i]['content'] = $this->app->zlstring->truncate($result['result'][$i]['content'], $max_length, $tr_suffix);
							$limited_once == true;
						}
						break;
					default:
						if (strlen($result['result'][$i]) > $max_length){
							$result['result'][$i] = $this->app->zlstring->truncate($result['result'][$i], $max_length, $tr_suffix);
							$limited_once == true;
						}
						break;
				}
				$result['report']['limited'] = $limited_once ? $result['report']['limited'] : true;
			}
		}
		
		return $result;
	}

	/*
		Function: _render
			Renders the repeatable element.

	   Parameters:
            $params - AppData render parameter

		Returns:
			String - html
	*/
	protected function _render($params = array(), $mode=false, $opts=array())
	{
		// prepare the widgetkit mode
		$mode = "{$params->find('layout.widgetkit._widget')}.{$params->find('layout.widgetkit._style')}";

		// render layout or value
		$main_layout = basename($params->find('layout._layout', 'default.php'), '.php');
		if ($layout = $this->getLayout('render/'.$main_layout.'/_sublayouts/'.$params->find('layout._sublayout', '_default.php')))
		{
			return $this->renderLayout($layout, compact('params'));
		}
		else
		{
			switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
			{
				// WK Slideset // WK Accordion
				case 'slideset': case 'accordion':
					$wkitem = array();
					$wkitem['set'] 			= '';
					$wkitem['title'] 		= $this->get('title');
					$wkitem['content'] 		= $this->get('value');
					$wkitem['navigation'] 	= $this->get('title');

					return $wkitem;
					break;

				default:
					return $this->get('value');
					break;
			}
		}
	}
	
	/*
		Function: returnDataByIndex
			Renders the element data - use for ajax requests
	*/
	public function returnDataByIndex($index = 0) {
		// returns the element with specified index
		$this->rewind();
		$value = '';
		foreach ($this as $self) {
			if ($self->index() == (int)$index) {
				$value = $this->get('value', '');
			}
		}
		return $value;
	}

	/*
		Function: loadAssets
			Load elements css/js assets.

		Returns:
			Void
	*/
	public function loadAssets($params = array()) {
		parent::loadAssets();
		$params  = $this->app->data->create($params);
		$trusted = $params->get('trusted_mode', true);
		$editor  = $this->config->find('specific._editor');
		$doc	 = $this->app->document;

		if ($editor != 'joomlaeditor') {
			$doc->addScript('elements:textareapro/assets/js/textareapro.min.js');
			if ($editor == 'customeditor' && $trusted)
				$doc->addScript('zlfw:assets/js/jquery.plugins/jquery.tinymce.js');
			if ($this->config->find('specific._input_limit', 0) && $editor != 'customeditor')
				$doc->addScript('zlfw:assets/js/jquery.plugins/jquery.inputlimiter.min.js');
		}

		if ($editor == 'joomlaeditor' && $this->config->get('repeatable')) {
			$doc->addScript('elements:textarea/textarea.js');
		}

		$doc->addStylesheet('elements:textareapro/assets/css/textareapro.css');
		
		return $this;
	}

	/*
		Function: renderSubmission
			Renders the element in submission.

	   Parameters:
            $params - AppData submission parameters

		Returns:
			String - html
	*/
	public function renderSubmission($params = array()) {
        $this->loadAssets($params);
        return $this->_renderRepeatable('_renderSubmission', $params);
	}
	
	/*
		Function: _renderSubmission
			Renders the element in submission.

	   Parameters:
            $params - submission parameters

		Returns:
			String - html
	*/
	public function _renderSubmission($params = array()) {
	
/*		// get params
        $trusted_mode = $params->get('trusted_mode');
		
        if ($layout = $this->getLayout('submission.php')) {
			return $this->renderLayout($layout,
				compact('trusted_mode')
			);
		}*/

		return $this->_edit();
	}

	/*
		Function: _validateSubmission
			Validates the submitted element

	   Parameters:
			$value  - AppData value
			$params - AppData submission parameters

		Returns:
			Array - cleaned value
	*/
	public function _validateSubmission($values, $params)
	{
		// init vars
		$required = $params->get('required');

		// validate values
		$value  = $this->app->validator->create('string', array('required' => $required))->clean($values->get('value'));
		$title = $this->app->validator->create('string', array('required' => false))->clean($values->get('title'));

		return compact('value', 'title');
	}

	/* YOO Joomla Editor compatibility
	---------------------------------------------------------------------------------------------------------------------------*/

	/*
	   Function: edit
	       Renders the edit form field.

	   Returns:
	       String - html
	*/
	public function edit() {
		if ($this->config->find('specific._editor') == 'joomlaeditor')
			return $this->_renderRepeatable(null, $this->app->data->create()->set('trusted_mode', true));
		else return parent::edit();
	}

	/*
		Function: _renderRepeatable
			Renders the repeatable

		Returns:
			String - output
	*/
    protected function _renderRepeatable($function, $params = array()) {
    	if ($this->config->find('specific._editor') == 'joomlaeditor'){
    		$params = $this->app->data->create($params);
			$this->rewind();
			return $this->config->get('repeatable') ? $this->renderLayout($this->getLayout('edit/edit_yoo.php'), compact('params')) : $this->_addEditor(0, $this->get('value', $this->config->get('default')), $params->get('trusted_mode', false));
    	} else {
    		return parent::_renderRepeatable($function, $params);
    	}
    }

    protected function _addEditor($index, $value = '', $trusted_mode = true) {
    	// render layout
		if ($layout = $this->getLayout('edit/_edit_yoo.php'))
		{
			return '<div>'.$this->renderLayout($layout, compact('index', 'value', 'trusted_mode')).'</div>';
		}
	}

	public function getYooControlName($name, $index) {
		return "elements[{$this->identifier}][{$index}][$name]";
	}

}