<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init var
$default = $this->config->get('default');

// set default, if item is new
if ($default != '' && $this->_item != null && $this->_item->id == 0) {
	$this->set('value', $default);
}

?>

<div class="row">
	<?php
		if ($this->config->find('specific._editor') == 'joomlaeditor')
		{
			// using YOO layout instead
			// echo $this->app->system->editor->display($this->getControlName('value'), htmlspecialchars( $this->get('value'), ENT_QUOTES, 'UTF-8' ), null, null, 20, 60, array());
		}
		else if($this->config->find('specific._editor') == 'customeditor')
		{
			echo $this->app->html->_('control.textarea', $this->getControlName('value'), $this->get('value'), 'class="tinymce '.$this->config->find('specific._editor_size').'" cols=20 rows=10');
		}
		else
		{
			echo $this->app->html->_('control.textarea', $this->getControlName('value'), $this->get('value'), 'class="'.$this->config->find('specific._editor_size').'" cols=20 rows=10');
		}
	?>
</div>

<?php if($this->config->find('specific._extra_options')) : ?>
<div class="more-options">
	<div class="trigger">
		<div>
			<div class="title button"><?php echo JText::_('PLG_ZLFRAMEWORK_TITLE'); ?></div>
		</div>
	</div>

	<div class="title options">
		<div class="row">
			<?php echo $this->app->html->_('control.text', $this->getControlName('title'), $this->get('title'), 'maxlength="255" title="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'"'); ?>
		</div>
	</div>
</div>
<?php endif; ?>