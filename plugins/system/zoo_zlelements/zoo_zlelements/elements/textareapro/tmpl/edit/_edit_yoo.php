<?php
/**
* @package   ZOO Component
* @file      edit.php
* @version   2.1.0 BETA3 September 2010
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) 2007 - 2010 YOOtheme GmbH
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init var
$default = $this->config->get('default');

// set default, if item is new
if ($default != '' && $this->_item != null && $this->_item->id == 0) {
	$this->set('value', $default);
}

	$html[] = '<div class="repeatable-content">';
	if ($trusted_mode) {
		$html[] = $this->app->editor->display($this->getYooControlName('value', $index), htmlspecialchars($value, ENT_QUOTES, 'UTF-8'), null, null, $this->COLS, $this->ROWS, array('pagebreak', 'readmore', 'article'));
	} else {
		$html[] = $this->app->html->_('control.textarea', $this->getYooControlName('value', $index), $value, 'cols='.$this->COLS.' rows='.$this->ROWS);
	}
	$html[] = '</div>';
	$html[] = '<span class="delete" title="'.JText::_('PLG_ZLFRAMEWORK_DELETE_ELEMENT').'"></span>';
	echo implode("\n", $html);

?>

<?php if($this->config->find('specific._extra_options')) : ?>
<div class="more-options">
	<div class="trigger">
		<div>
			<div class="title button"><?php echo JText::_('PLG_ZLFRAMEWORK_TITLE'); ?></div>
		</div>
	</div>

	<div class="title options">
		<div class="row">
			<?php echo $this->app->html->_('control.text', $this->getControlName('title'), $this->get('title'), 'maxlength="255" title="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'"'); ?>
		</div>
	</div>
</div>
<?php endif; ?>