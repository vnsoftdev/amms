<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// submission params
$params = $this->app->data->create($params);
?>

<?php if ($this->config->get('repeatable')) : ?>

	<div id="<?php echo $this->identifier; ?>" class="repeat-elements zl">
		<ul class="repeatable-list">

			<?php $this->rewind(); ?>
			<?php foreach($this as $self) : ?>
				<li class="repeatable-element">
					<?php echo $this->$function($params); ?>
				</li>
			<?php endforeach; ?>

			<?php $this->rewind(); ?>

			<li class="repeatable-element hidden">
				<?php echo preg_replace('/(elements\[\S+])\[(\d+)\]/', '$1[-1]', $this->$function($params)); ?>
			</li>

		</ul>
		<p class="add">
			<a class="zl-btn" href="javascript:void(0);"><?php echo JText::_('PLG_ZLFRAMEWORK_ADD_INSTANCE'); ?></a>
		</p>
	</div>

	<script type="text/javascript">
		jQuery('#<?php echo $this->identifier; ?>').ElementRepeatablePro({ msgDeleteElement : '<?php echo JText::_('PLG_ZLFRAMEWORK_DELETE_ELEMENT'); ?>', msgSortElement : '<?php echo JText::_('PLG_ZLFRAMEWORK_SORT_ELEMENT'); ?>', msgLimitReached : '<?php echo JText::_('PLG_ZLFRAMEWORK_INSTANCE_LIMIT_REACHED'); ?>', instanceLimit: '<?php echo $this->config->get('instancelimit') ?>' });
		<?php if($this->config->find('specific._editor') != 'joomlaeditor') : ?>
		jQuery('#<?php echo $this->identifier; ?>').ElementTextareaPro({
			<?php if($params->get('trusted_mode', true) && $this->config->find('specific._editor') == 'customeditor') : ?>
				tinymce_url: '<?php echo $this->app->path->url('media:editors/tinymce/jscripts/tiny_mce/tiny_mce.js'); ?>', 
				msgLoadEditor : '<?php echo JText::_('PLG_ZLELEMENTS_TEXTS_LOAD_EDITOR'); ?>',
				OnLoadEditor: <?php echo $this->config->find('specific._editor_enabled_by_defualt', 0); ?>,
			<?php endif; ?>
			inputLimit: '<?php echo $this->config->find('specific._input_limit', ''); ?>',
			msgRemText: '<?php echo JText::_('PLG_ZLELEMENTS_TEXTS_IL_REMTEXT') ?>',
			msgLimitText: '<?php echo JText::_('PLG_ZLELEMENTS_TEXTS_IL_LIMITTEXT') ?>'
		});
		<?php endif; ?>
	</script>
	
<?php else : ?>

	<div id="<?php echo $this->identifier; ?>" class="repeatable-element zl">
		<?php echo $this->$function($params); ?>
	</div>

	<?php if($this->config->find('specific._editor') != 'joomlaeditor') : ?>
	<script type="text/javascript">
		jQuery('#<?php echo $this->identifier; ?>').ElementTextareaPro({
			<?php if($params->get('trusted_mode', true) && $this->config->find('specific._editor') == 'customeditor') : ?>
				tinymce_url: '<?php echo $this->app->path->url('media:editors/tinymce/jscripts/tiny_mce/tiny_mce.js'); ?>', 
				msgLoadEditor : '<?php echo JText::_('PLG_ZLELEMENTS_TEXTS_LOAD_EDITOR'); ?>',
				OnLoadEditor: <?php echo $this->config->find('specific._editor_enabled_by_defualt', 0); ?>,
			<?php endif; ?>
			inputLimit: '<?php echo $this->config->find('specific._input_limit', ''); ?>',
			msgRemText: '<?php echo JText::_('PLG_ZLELEMENTS_TEXTS_IL_REMTEXT') ?>',
			msgLimitText: '<?php echo JText::_('PLG_ZLELEMENTS_TEXTS_IL_LIMITTEXT') ?>'
		});
	</script>
	<?php endif; ?>

<?php endif; ?>