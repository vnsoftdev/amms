<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	$edit_path = array();
	$edit_path[] = 'zlpath:elements\/'.$element->getElementType().'\/tmpl\/edit';
	if (is_object($element->getType())) $edit_path[] = 'applications:'.$element->getType()->getApplication()->getGroup().'\/elements\/'.$element->getElementType().'\/tmpl\/edit'; // zoo template overide path

	return 
	'{
		"_edit_sublayout":{
			"type":"layout",
			"label":"PLG_ZLFRAMEWORK_EDIT_SUBLAYOUT",
			"help":"PLG_ZLFRAMEWORK_EDIT_SUBLAYOUT_DESC",
			"default":"_edit.php",
			"specific": {
				"path":"'.implode(',', $edit_path).'",
				"regex":'.json_encode('^([_][_A-Za-z0-9]*)\.php$').',
				"min_opts":"2"
			}
		},
		"_extra_options":{
			"type":"select",
			"label":"PLG_ZLELEMENTS_TEXTS_EXTRA_OPTS",
			"help":"PLG_ZLELEMENTS_TEXTS_EXTRA_OPTS_DESC",
			"specific":{
				"options":{
					"PLG_ZLFRAMEWORK_SHOW":"1",
					"PLG_ZLFRAMEWORK_HIDE":""
				}
			}
		},
		"_input_allowed":{
			"type":"select",
			"label":"PLG_ZLELEMENTS_TEXTS_ALLOWED_INPUT",
			"help":"PLG_ZLELEMENTS_TEXTS_ALLOWED_INPUT_DESC",
			"default": "all",
			"specific":{
				"options":{
					"PLG_ZLFRAMEWORK_ANY":"all",
					"PLG_ZLELEMENTS_TEXTS_ALPHA":"alpha",
					"PLG_ZLELEMENTS_TEXTS_NUMERIC":"numeric"
				}
			},
			"dependents":"_input_exceptions !> all"
		},
		"_input_exceptions":{
			"type":"text",
			"label":"PLG_ZLELEMENTS_TEXTS_INPUT_EXCEPTIONS",
			"help":"PLG_ZLELEMENTS_TEXTS_INPUT_EXCEPTIONS_DESC"
		},
			"default":{
			"type":"text",
			"label":"PLG_ZLFRAMEWORK_DEFAULT",
			"help":"PLG_ZLELEMENTS_TEXTS_DEFAULT_TEXT_DESC",
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[specific\]/').',
				"replacement":""
			}
		},
		"_input_limit":{
			"type":"text",
			"label":"PLG_ZLELEMENTS_TEXTS_MAX_CHARACTERS",
			"help":"PLG_ZLELEMENTS_TEXTS_MAX_CH_EDIT_DESC"
		}
	}';