<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return
	'{
		"_prefix":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_TEXTS_PREFIX",
			"help": "PLG_ZLELEMENTS_TEXTS_PREFIX_DESC"
		},
		"_suffix":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_TEXTS_SUFFIX",
			"help": "PLG_ZLELEMENTS_TEXTS_SUFFIX_DESC"
		},
		"_max_car":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_TEXTS_MAX_CHARACTERS",
			"help": "PLG_ZLELEMENTS_TEXTS_MAX_CH_RENDER_DESC",
			"dependents": "_max_car_suffix > 1 | _max_car_mode > 1"
		},
		"_max_car_mode":{
			"type": "select",
			"label": "PLG_ZLELEMENTS_TEXTS_MAX_CH_MODE",
			"help": "PLG_ZLELEMENTS_TEXTS_MAX_CH_MODE_DESC",
			"default": "1",
			"specific": {
				"options": {
					"PLG_ZLELEMENTS_TEXTS_MAX_CH_GROUP":"1",
					"PLG_ZLELEMENTS_TEXTS_MAX_CH_INDIVIDUAL":"2"
				}
			}
		},
		"_max_car_suffix":{
			"type": "text",
			"label": "PLG_ZLELEMENTS_TEXTS_MAX_CH_SUFFIX",
			"help": "PLG_ZLELEMENTS_TEXTS_MAX_CH_SUFFIX_DESC",
			"default": "..."
		}
	}';