/* Copyright (C) 2007 - 2011 ZOOlanders.com - Inspired by YOOtheme GmbH code */

(function ($) {
    var b = function () {};
    $.extend(b.prototype, {
        name: "EditElementTextPro",
        options: {
			allowed: '',
			exceptions: '',
			inputLimit: 0,
			msgRemText: '%n characters remaining.',
			msgLimitText: 'Field limited to a %n characters.'
        },
        initialize: function (b, c) {
            this.options = $.extend({}, this.options, c);
			var d = this,
				op = d.options;
			
            b.delegate("p.add a", "click", function () {
				d.apply(b.find("input"));
            });
			
			if (op.allowed || op.inputLimit){
				d.apply(b.find("input"));
			}
        },
		apply: function (inputs) {
			var d = this,
				op = d.options;
				
			inputs.each( function() {
				
				var input = $(this);
			
				( $(this).data("initialized-textpro") || (
					(op.allowed && d.alphanumeric(input)),
					(op.inputLimit && d.inputlimiter(input))
				, $(this).data("initialized-textpro", !0)) );
			});
		},
		alphanumeric: function (input){
			var op = this.options;
			
			if(op.allowed == 'numeric'){
				input.numeric({allow:op.exceptions});
			} else if(op.allowed == 'alpha'){
				input.alpha({allow:op.exceptions});
			}
		},
		inputlimiter: function (input){
			var op = this.options;
			
			input.inputlimiter({
				limit: parseFloat(op.inputLimit),
				remText: op.msgRemText,
				limitText: op.msgLimitText
			});
		}
    });
    $.fn[b.prototype.name] = function () {
        var e = arguments,
            c = e[0] ? e[0] : null;
        return this.each(function () {
            var d = $(this);
            if (b.prototype[c] && d.data(b.prototype.name) && c != "initialize") d.data(b.prototype.name)[c].apply(d.data(b.prototype.name), Array.prototype.slice.call(e, 1));
            else if (!c || $.isPlainObject(c)) {
                var f = new b;
                b.prototype.initialize && f.initialize.apply(f, $.merge([d], e));
                d.data(b.prototype.name, f)
            } else $.error("Method " + c + " does not exist on jQuery." + b.name)
        })
    }
})(jQuery);