<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
   Class: ElementText
       The text element class
*/
class ElementTextPro extends ElementRepeatablePro implements iRepeatSubmittable {

	/*
	   Function: Constructor
	*/
	public function __construct() {

		// call parent constructor
		parent::__construct();

		// register events
		$this->app->event->dispatcher->connect('element:afterdisplay', array($this, 'afterDisplay'));
	}

	/*
		Function: afterDisplay
			Change the element layout after it has been displayed

		Parameters:
			$event - object
	*/
	public static function afterDisplay($event)
	{
		$new_str = 'id="e' . $event['element']->identifier . '" class="element element-text element-textpro';
		$event['html'] = str_replace('class="element element-textpro', $new_str, $event['html']);
	}

	/*
		Function: _getSearchData
			Get repeatable elements search data.
					
		Returns:
			String - Search data
	*/	
	protected function _getSearchData() {
		return $this->get('value');
	}
	
	/*
		Function: loadAssets
			Load elements css/js assets.

		Returns:
			Void
	*/
	public function loadAssets() {
		parent::loadAssets();
		$this->config->find('specific._input_allowed', 0) && $this->app->document->addScript('zlfw:assets/js/jquery.plugins/jquery.alphanumeric.min.js');
		$this->config->find('specific._input_limit', 0) && $this->app->document->addScript('zlfw:assets/js/jquery.plugins/jquery.inputlimiter.min.js');
		$this->app->document->addScript('elements:textpro/textpro.js');
		return $this;
	}
	
	/*
		Function: getRenderedValues
			render repeatable values

		Returns:
			array
	*/
	public function getRenderedValues($params=array(), $mode=false, $opts=array()) 
	{
		// init vars
		$result = parent::getRenderedValues($params, $mode, $opts);
		
		if (empty($result)) return null; // if no results abort

		// truncate
		if ($max_length = $params->find('specific._max_car', 0))
		{
			$tr_mode = $params->find('specific._max_car_mode', 1);
			$tr_suffix = $params->find('specific._max_car_suffix', '...');

			// group all text & truncate
			if (isset($max_length) && $max_length > 0 && count($result['result']) > 1 && $tr_mode == 1)
			{
				$result_len = 0;
				for ($i = 0; $i < count($result['result']); $i++) switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
				{
					// WK Slideset // WK Accordion
					case 'slideset': case 'accordion':
						$text = $result['result'][$i]['content'];
						$result_len += strlen(strip_tags($text));
						if ($result_len > $max_length){
							$truncate_len = $i > 1 ? $result_len - $max_length : $max_length;
							$result['result'][$i]['content'] = $this->app->zlstring->truncate($text, $truncate_len, $tr_suffix);
							$result['result'] = array_slice($result['result'], 0, $i+1); // remove the rest
							$result['report']['limited'] = true;
							break;
						} else { $result['result'][$i]['content'] = $text; }
						break;

					default:
						$text = $result['result'][$i];
						$result_len += strlen(strip_tags($text));
						if ($result_len > $max_length){
							$truncate_len = $i > 1 ? $result_len - $max_length : $max_length;
							$result['result'][$i] = $this->app->zlstring->truncate($text, $truncate_len, $tr_suffix);
							$result['result'] = array_slice($result['result'], 0, $i+1); // remove the rest
							$result['report']['limited'] = true;
							break;
						} else { $result['result'][$i] = $text; }
						break;
				}
			}

			// truncate individualy
			else if (isset($max_length) && $max_length > 0 && $tr_mode == 2)
			{
				$limited_once = false;
				for ($i = 0; $i < count($result['result']); $i++) switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
				{
					// WK Slideset // WK Accordion
					case 'slideset': case 'accordion':
						if (strlen($result['result'][$i]['content']) > $max_length){
							$result['result'][$i]['content'] = $this->app->zlstring->truncate($result['result'][$i]['content'], $max_length, $tr_suffix);
							$limited_once == true;
						}
						break;
					default:
						if (strlen($result['result'][$i]) > $max_length){
							$result['result'][$i] = $this->app->zlstring->truncate($result['result'][$i], $max_length, $tr_suffix);
							$limited_once == true;
						}
						break;
				}
				$result['report']['limited'] = $limited_once ? $result['report']['limited'] : true;
			}
		}

		return $result;
	}

	/*
		Function: _render
			Renders the repeatable element.

	   Parameters:
            $params - AppData render parameter

		Returns:
			String - html
	*/
	protected function _render($params = array(), $mode=false, $opts=array())
	{
		// prepare the widgetkit mode
		$mode = "{$params->find('layout.widgetkit._widget')}.{$params->find('layout.widgetkit._style')}";
		
		// render layout or value
		$main_layout = basename($params->find('layout._layout', 'default.php'), '.php');
		if($layout = $this->getLayout('render/'.$main_layout.'/_sublayouts/'.$params->find('layout._sublayout', '_default.php')))
		{
			return $this->renderLayout($layout, compact('params'));
		}
		else
		{
			switch (preg_replace('/\..+/', '', $mode)) // return the mode without any post . text
			{
				// WK Slideset // WK Accordion
				case 'slideset': case 'accordion': case 'slideshow':
					$wkitem = array();
					$wkitem['set'] 			= '';
					$wkitem['title'] 		= $this->get('title');
					$wkitem['content'] 		= $this->get('value');
					$wkitem['navigation'] 	= $this->get('title');

					return $wkitem;
					break;

				default:
					return $this->get('value');
					break;
			}
		}
	}
	
	/*
		Function: _renderSubmission
			Renders the element in submission.

	   Parameters:
            $params - submission parameters

		Returns:
			String - html
	*/
	public function _renderSubmission($params = array()) {
        return $this->_edit();
	}

	/*
		Function: _validateSubmission
			Validates the submitted element

	   Parameters:
			$value  - AppData value
			$params - AppData submission parameters

		Returns:
			Array - cleaned value
	*/
	public function _validateSubmission($values, $params)
	{
		// init vars
		$required = $params->get('required');

		// validate values
		$value  = $this->app->validator->create('string', array('required' => $required))->clean($values->get('value'));
		$title = $this->app->validator->create('string', array('required' => false))->clean($values->get('title'));

		return compact('value', 'title');
	}
}