<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// init var
$default = $this->config->get('default');

// set default, if item is new
if ($default != '' && $this->_item != null && $this->_item->id == 0) {
	$this->set('value', $default);
}

?>

<div class="row">
	<?php echo $this->app->html->_('control.text', $this->getControlName('value'), $this->get('value', $default), 'size="60" maxlength="255"'); ?>
</div>

<?php if($this->config->find('specific._extra_options')) : ?>
<div class="more-options">
	<div class="trigger">
		<div>
			<div class="title button"><?php echo JText::_('PLG_ZLFRAMEWORK_TITLE'); ?></div>
		</div>
	</div>

	<div class="title options">
		<div class="row">
			<?php echo $this->app->html->_('control.text', $this->getControlName('title'), $this->get('title'), 'maxlength="255" title="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'" placeholder="'.JText::_('PLG_ZLFRAMEWORK_TITLE').'"'); ?>
		</div>
	</div>
</div>
<?php endif; ?>