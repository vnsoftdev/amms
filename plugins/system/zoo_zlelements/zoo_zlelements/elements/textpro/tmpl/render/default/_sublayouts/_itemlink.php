<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	$text = $this->get('value');
	echo $this->_item->getState() ? '<a href="' . $this->app->route->item($this->_item) . '">' . $text . '</a>' : $text;