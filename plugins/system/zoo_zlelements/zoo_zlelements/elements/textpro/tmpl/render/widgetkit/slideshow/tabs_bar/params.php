<?php
/**
* @package		ZL FrameWork
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"widget_separator":{
			"type": "separator",
			"text": "PLG_ZLFRAMEWORK_LAYOUT",
			"big":"1"
		},
		"widget_settings": {
			"type":"subfield",
			"path":"elements:pro\/tmpl\/render\/widgetkit\/slideshow\/tabs_bar\/settings.php",
			"control": "settings"
		},

		"text_separator":{
			"type": "separator",
			"text": "PLG_ZLFRAMEWORK_TEXT"
		},
		"text_options": {
			"type": "subfield",
			"path":"elements:'.$element->getElementType().'\/params\/text.php",
			"arguments":{
				"params":{
					"target":"true"
				}
			},
			"adjust_ctrl":{
				"pattern":'.json_encode('/\[layout\]\[widgetkit\]/').',
				"replacement":"[specific]"
			}
		}
		
	}}';