<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: ZL Helper
		The general ZOOlander helper Class for zoo
*/
class ZlHelper extends AppHelper {

	/*
		Function: renderView
			Renders an Item View

	   Parameters:
            $item - the Item Object
			$layoutName - the Item layout
			
		Returns:
			String - html
			
		Ex. of use:
			echo $this->app->zl->renderView($this->_item, 'LAYOUTNAME');
	*/
	public function renderView($item, $layoutName='full') {
	
		// create it, if is not object
		if (!is_object($item)){
			$item = $this->app->table->item->get($item);
		}
		
		if (is_object($item)){
	
			// init varr
			$application = $item->getApplication();
			$renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $application->getTemplate()->getPath()));
			$layoutName = str_replace(".php", "", $layoutName);

			$path   = 'item';
			$prefix = 'item.';
			$type   = $item->getType()->id;
			if ($renderer->pathExists($path.DIRECTORY_SEPARATOR.$type)) {
				$path   .= DIRECTORY_SEPARATOR.$type;
				$prefix .= $type.'.';
			}
			
			// Set view object
			$view = new AppView( array('name' => 'item') );
			$view->params = $application->getParams('site');

			return $renderer->render($prefix.$layoutName, array('view' => $view, 'item' => $item));
			
		} else {
			return JText::_('Item does not exist.');
		}
	}
	
	/*
		Function: renderModule
			Renders Joomla Module

		Parameters:
            $modID - Module ID
			
		HTML Styles:
			table - Wrapped by Table (Column)
			horz - Wrapped by Table (Horizontal)
			xhtml - Wrapped by Divs
			rounded - Wrapped by Multiple Divs
			none - No wrapping (raw output)

		Returns:
			String - html
			
		Ex. of use:
			echo $this->app->zl->renderModule($id);
	*/
	public function renderModule($modID = null) {

		// get modules
		$modules = $this->app->module->load();
		
		if ($modID && isset($modules[$modID])) {
			if ($modules[$modID]->published) {

				$attribs['style'] = 'xhtml';
				$rendered = JModuleHelper::renderModule($modules[$modID], $attribs);

				if (isset($modules[$modID]->params)) {
					$module_params = $this->app->parameter->create($modules[$modID]->params);
					if ($moduleclass_sfx = $module_params->get('moduleclass_sfx')) {
						$html[] = '<div class="'.$moduleclass_sfx.'">';
						$html[] = $rendered;
						$html[] = '</div>';

						return implode("\n", $html);
					}
				}

				return $rendered;
			}
		}
		
		return null;
	}
	
	/*
		Function: renderModulePosition
			Renders Joomla Module Position

	   Parameters:
            $position - render the position

		Returns:
			String - html
			
		Ex. of use:
			echo $this->app->zl->renderModulePosition('POSITION');
	*/
	public function renderModulePosition($position = null) {

		// get modules
		$modules = $this->app->module->load();
		$result = array();
		
		foreach ($modules as $mod) {
			if ($mod->position == $position) {
				$result[] = $this->renderModule($mod->id);
			}
		}
		
		if (!empty($result)) {
			return implode("\n", $result);
		}
		
		return null;
	}
	
	/*
		Function: resizeImage
			Resize and cache image file.

		Returns:
			String - image path
	*/
	public function resizeImage($file, $width, $height, $avoid_cropping = null, $unique = null) {

		// init vars
		$width      = (int) $width;
		$height     = (int) $height;
		$file_info  = pathinfo($file);
		$thumbfile  = $this->app->path->path('cache:').'/images/'. $file_info['filename'] . '_' . md5($file.$width.$height.$avoid_cropping.$unique) . '.' . $file_info['extension'];
		$cache_time = 86400; // cache time 24h
		$format = '';
		
		// ZL
		if ($avoid_cropping) {
			$file_size  = getimagesize($file);
			$format = ($file_size[0] > $file_size[1]) ? 'paisage' : 'portrait';
		}
		// ZL end

		// check thumbnail directory
		if (!JFolder::exists(dirname($thumbfile))) {
			JFolder::create(dirname($thumbfile));
		}

		// create or re-cache thumbnail
		if ($this->app->imagethumbnail->check() && (!is_file($thumbfile) || ($cache_time > 0 && time() > (filemtime($thumbfile) + $cache_time)))) {
			$thumbnail = $this->app->imagethumbnail->create($file);
			
			// ZL
			if ($avoid_cropping && $format == 'paisage' && $width > 0) {
				$thumbnail->sizeWidth($width);
				$thumbnail->save($thumbfile);
			} else if ($avoid_cropping && $format == 'portrait' && $height > 0) {
				$thumbnail->sizeHeight($height);
				$thumbnail->save($thumbfile);
			// ZL end
			} else if ($width > 0 && $height > 0) {
				$thumbnail->setSize($width, $height);
				$thumbnail->save($thumbfile);
			} else if ($width > 0 && $height == 0) {
				$thumbnail->sizeWidth($width);
				$thumbnail->save($thumbfile);
			} else if ($width == 0 && $height > 0) {
				$thumbnail->sizeHeight($height);
				$thumbnail->save($thumbfile);
			} else {
                if (JFile::exists($file)) {
                    JFile::copy($file, $thumbfile);
                }
            }

		}

		if (is_file($thumbfile)) {
			return $thumbfile;
		}

		return $file;
	}
	
	/*
		Function: applySeparators
			Separates the passed element values with a separator

		Parameters:
			$separated_by - Separator
			$values - Element values

		Returns:
			String
	*/
	public function applySeparators($separated_by, $values, $class = '') {

		if (!is_array($values)) {
			$values = array($values);
		}

		$separator = '';
		$tag = '';
		$enclosing_tag = '';
		if ($separated_by) {
			if (preg_match('/separator=\[(.*)\]/U', $separated_by, $result)) {
				$separator = $result[1];
			}

			if (preg_match('/tag=\[(.*)\]/U', $separated_by, $result)) {
				$tag = $result[1];
			}

			if (preg_match('/enclosing_tag=\[(.*)\]/U', $separated_by, $result)) {
				$enclosing_tag = $result[1];
			}
		}

		if (empty($separator) && empty($tag) && empty($enclosing_tag)) {
			$separator = ', ';
		}
		
		// add class to tag
		if (!empty($class) && preg_match('/(<.*>)/U', $tag, $result)) {
			$tag = str_replace($result[1], '<div class="'.$class.'">', $tag);
		}
		
		if (!empty($tag)) {
			foreach ($values as $key => $value) {
				$values[$key] = sprintf($tag, $values[$key]);
			}
		}

		$value = implode($separator, $values);

		if (!empty($enclosing_tag)) {
			$value = sprintf($enclosing_tag, $value);
		}

		return $value;
	}
	
	/*
		Function: isDateExpired
			Checks if some date has expired in relation to NOW or specified date
		Return: boolean
	*/
	public function isDateExpired($date, $exp_date = null) {
		
		// get dates
		$date = $this->app->date->create($date);
		$exp_date = $this->app->date->create();

		//return date('Y-m-d', $exp_date->toUnix(true)) > date('Y-m-d', $date->toUnix(true));
		return $exp_date->toUnix(true) > $date->toUnix(true);
	}
	
	
	/******************************************/
	
	/*
		Function: checkLogin
			Checks if the user is logued in
		Return: boolean
	*/
	public function checkLogin() {
		$user =& JFactory::getUser();
		return (!$user->get('guest')) ? true : false;
	}
	
	/* Depricated since ZLFW 2.5.6 */
	public function limitText($text, $limit, $etc = false)
	{
		$result = strip_tags($text);
		$etc = ($etc) ? '...' : '';
		
		if ($limit > 0 && $limit < strlen($result)) {
			return substr($result, 0, strrpos(substr($result, 0, $limit), ' ')).$etc;
		} else {
			return $result;
		}
	}

	/*
		Function: filterTypes
		prototype
			
	*/
	public function filterTypes($app, $types) {
	
		// init var
		$filtered_types = array();
		
		if ($app->getParams()->get('global.config.ct_filter', 0)) {
			// converts the string into an Array
			$selected_types = explode(', ', $app->getParams()->get('global.config.ct_filter'));
			
			foreach ($selected_types as $type) {
				$filtered_types[] = $types[$type];
			}
			
			return $filtered_types;
		}
		
		return $types;
	}
		
	/*
		Function: getElement
			Retrieve elements data knowing just the Element Type
			
		Variables:
			$item - the item where to loop
			$element_type - the type of element to search for
			$data_type - "config" or "data"
			$data_name - the name of data to return
			
		Returns the data
	*/
	public function getElementDataByType($item, $element_type, $data_type, $data_name) {
		
		// init vars
		$data = null;
		$config = null;
		$found = false;
		
		foreach ($item->getElements() as $element) {
			if ($element->getElementType() == $element_type) {
				$data = $element->getElementData();
				$config = $element->getConfig();
				$found = true;
			}
		}
		
		if ($found && $data_type == 'data' && $data->get($data_name)) {
			return $data->get($data_name);
		} elseif ($found && $data_type == 'config' && $config->get($data_name)) {
			return $config->get($data_name);
		}
		
		return null;
	}
	
	
/*
=== qTip ==============================================================================================================*/

	/*
		Function: loadqTip
			load qTip libraries
	*/
	public function loadqTip() {
		$this->app->document->addStylesheet('zlpath:assets/jquery.plugins/qtip/jquery.qtip.min.css');
		$this->app->document->addStylesheet('zlpath:assets/jquery.plugins/qtip/jquery.qtip.custom.css');
		$this->app->document->addScript('zlpath:assets/jquery.plugins/qtip/jquery.qtip.min.js');
	}
	
	/*
		Function: getQtipAjaxFunc
			return success ajax functions for qTip calls
	*/
	public function getqTipStyle($element = null, $params) {
		
		$classes = $params->find('qtip._class', '').($params->find('qtip._iframe') ? ' ui-tooltip-iframe' : '');
		$classes = explode(' ', $classes);
		$type = $element->getType();
		
		$style 	 = array('ui-tooltip-default', 'ui-tooltip-'.$element->getElementType(), 'ui-tooltip-custom');
		$style[] = $type->config->find('zl.qtip._style', '')   ? 'ui-tooltip-'.$type->config->find('zl.qtip._style') : '';
		$style[] = $type->config->find('zl.qtip._shadow', '')  ? 'ui-tooltip-shadow' : '';
		$style[] = $type->config->find('zl.qtip._rounded', '') ? 'ui-tooltip-rounded' : '';
		
		return implode(' ', array_merge($style, $classes));
	}
	
}