<?php
/*
* @package		ZOOaccess
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// init vars, some must be reset for ajax loading
	$fname	 			= 'zooaccess';
	$config  			= $element->config;
	$isCore  			= $element->getGroup() == 'Core';
	$overrided    		= isset($params) ? $params->find($fname.'._evaluate', 0) : 0;
	$evaluated 			= $config->find($fname.'._evaluate', 0);
	$options		 	= array();
	$conditions		 	= array();
	$label_evaluate 	= 'PLG_ZOOACCESS_EVALUATE';
	$help_evaluate 		= 'PLG_ZOOACCESS_EVALUATE_DESC';


	// change evaluate field text if overiding
	if ($evaluated && $enviroment == 'type-positions')
	{		
		$label_evaluate = 'PLG_ZLFRAMEWORK_OVERRIDE';
		$help_evaluate = '';
	}

	/*
	 * ========== User Access Levels ==========
	 */
	$user_access_levels = array();
	if($cache = $this->app->zlfield->cache->get('zooaccess.user-access-levels'))
	{
		$user_access_levels = $cache;
	}
	else
	{
		$query = 'SELECT a.id AS value, a.title AS text'
				.' FROM #__viewlevels AS a'
				.' GROUP BY a.id, a.title, a.ordering'
				.' ORDER BY a.ordering ASC';
		
		// Get the user access levels from the database.
		foreach ($this->app->database->queryObjectList($query) as $access){
			$user_access_levels[$access->text] = $access->value;
		}

		// store the value on cache var
		$this->app->zlfield->cache->set('zooaccess.user-access-levels', $user_access_levels);
	}
	
	$conditions[] =
	'"useraccess_fieldset": {
		"type":"fieldset",
		"control":"useraccess",
		"fields": {
			"_assignto":{
				"type":"radio",
				"label":"PLG_ZOOACCESS_USER_ACCESS",
				"help":"PLG_ZOOACCESS_USER_ACCESS_DESC||PLG_ZLFRAMEWORK_ACC_ASSIGN_DESC",
				"default":"0",
				"class":"special",
				"specific":{
					"options":{
						"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
						"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
						"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
					}
				},
				"dependents":"useraccess_params > 1 OR 2"
				'.($evaluated && $enviroment == 'type-positions' ? ',
				"state": {
					"init_state":"0",
					"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
				},
				"data_from_config":"'.($params->find($fname.'.conditions.useraccess._assignto_state') ? 0 : 1).'"' : '').'
			},
			"useraccess_params": {
				"type":"wrapper",
				"fields": {
					"_levels":{
						"type":"select",
						"label":"PLG_ZOOACCESS_LEVELS",
						"specific": {
							"options": '.json_encode($user_access_levels).',
							"multi":"true"
						}
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.useraccess._levels_state') ? 0 : 1).'"' : '').'
					},
					"_mode":{
						"type":"radio",
						"label":"PLG_ZLFRAMEWORK_ACC_MODE",
						"help":"PLG_ZLFRAMEWORK_ACC_MODE_DESC",
						"specific": {
							"options": {
								"PLG_ZLFRAMEWORK_AND":"1",
								"PLG_ZLFRAMEWORK_OR":"0"
							}
						},
						"default":"0"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.useraccess._mode_state') ? 0 : 1).'"' : '').'
					},
					"_user":{
						"type":"radio",
						"label":"PLG_ZOOACCESS_USER",
						"help":"PLG_ZOOACCESS_USER_DESC",
						"specific": {
							"options": {
								"PLG_ZOOACCESS_USER":"0",
								"PLG_ZOOACCESS_AUTHOR":"1"
							}
						},
						"default":"0"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.useraccess._user_state') ? 0 : 1).'"' : '').'
					}
				}
			}
		}
	}';

	
	/*
	 * ========== User Group Levels ==========
	 */
	$user_group_levels = array();
	if($cache = $this->app->zlfield->cache->get('zooaccess.user-group-levels'))
	{
		$user_group_levels = $cache;
	}
	else
	{
		$query = 'SELECT a.id, a.title, a.parent_id AS parent, COUNT(DISTINCT b.id) AS level'
			.' FROM #__usergroups AS a'
			.' LEFT JOIN `#__usergroups` AS b ON a.lft > b.lft AND a.rgt < b.rgt'
			.' GROUP BY a.id'
			.' ORDER BY a.lft ASC';	

		// Get the user group levels from the database.
		foreach ($this->app->database->queryObjectList($query) as $group)
		{
			$item_name = '-&nbsp;'.$group->title;

			for ($i = 1; $i <= $group->level; $i++) {
				$item_name = '.&nbsp;'.$item_name;
			}

			$user_group_levels[$item_name] = $group->id;
		}

		// store the value on cache var
		$this->app->zlfield->cache->set('zooaccess.user-group-levels', $user_group_levels);
	}

	$conditions[] =
	'"usergroup_fieldset": {
		"type":"fieldset",
		"control":"usergroup",
		"fields": {
			"_assignto":{
				"type":"radio",
				"label":"PLG_ZOOACCESS_USER_GROUP",
				"help":"PLG_ZOOACCESS_USER_GROUP_DESC||PLG_ZLFRAMEWORK_ACC_ASSIGN_DESC",
				"default":"0",
				"class":"special",
				"specific":{
					"options":{
						"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
						"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
						"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
					}
				},
				"dependents":"usergroup_params > 1 OR 2"
				'.($evaluated && $enviroment == 'type-positions' ? ',
				"state": {
					"init_state":"0",
					"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
				},
				"data_from_config":"'.($params->find($fname.'.conditions.usergroup._assignto_state') ? 0 : 1).'"' : '').'
			},
			"usergroup_params": {
				"type":"wrapper",
				"fields": {
					"_levels":{
						"type":"select",
						"label":"PLG_ZOOACCESS_LEVELS",
						"specific": {
							"options": '.json_encode($user_group_levels).',
							"multi":"true"
						}
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.usergroup._levels_state') ? 0 : 1).'"' : '').'
					},
					"_mode":{
						"type":"radio",
						"label":"PLG_ZLFRAMEWORK_ACC_MODE",
						"help":"PLG_ZLFRAMEWORK_ACC_MODE_DESC",
						"specific": {
							"options": {
								"PLG_ZLFRAMEWORK_AND":"1",
								"PLG_ZLFRAMEWORK_OR":"0"
							}
						},
						"default":"0"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.usergroup._mode_state') ? 0 : 1).'"' : '').'
					},
					"_user":{
						"type":"radio",
						"label":"PLG_ZOOACCESS_USER",
						"help":"PLG_ZOOACCESS_USER_DESC",
						"specific": {
							"options": {
								"PLG_ZOOACCESS_USER":"0",
								"PLG_ZOOACCESS_AUTHOR":"1"
							}
						},
						"default":"0"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.usergroup._user_state') ? 0 : 1).'"' : '').'
					}
				}
			}
		}
	}';


	/*
	 * ========== Date ==========
	 */
	$current_server_date = array();
	if($cache = $this->app->zlfield->cache->get('zooaccess.current-server-date'))
	{
		$current_server_date = $cache;
	}
	else
	{
		// get the current server date
		$date = JFactory::getDate();
		$tz = new DateTimeZone(JFactory::getApplication()->getCfg('offset'));
		$date->setTimeZone($tz);

		$current_server_date = $date->format('Y-m-d H:i', 1);

		// store the value on cache var
		$this->app->zlfield->cache->set('zooaccess.current-server-date', $current_server_date);
	}

	$conditions[] =
	'"date_fieldset": {
		"type":"fieldset",
		"control":"date",
		"fields": {
			"_assignto":{
				"type":"radio",
				"label":"PLG_ZOOACCESS_DATE",
				"help":"PLG_ZOOACCESS_DATE_DESC",
				"default":"0",
				"class":"special",
				"specific":{
					"options":{
						"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
						"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
						"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
					}
				},
				"dependents":"date_params > 1 OR 2"
				'.($evaluated && $enviroment == 'type-positions' ? ',
				"state": {
					"init_state":"0",
					"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
				},
				"data_from_config":"'.($params->find($fname.'.conditions.date._assignto_state') ? 0 : 1).'"' : '').'
			},
			"date_params": {
				"type":"wrapper",
				"fields": {
					"date_info":{
						"type":"info",
						"specific":{
							"text":"PLG_ZOOACCESS_DATE_INFO||'.$current_server_date.'"
						}
					},
					"_publish_up":{
						"type":"date",
						"label":"PLG_ZOOACCESS_DATE_START"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.date._publish_up_state') ? 0 : 1).'"' : '').'
					},
					"_publish_down":{
						"type":"date",
						"label":"PLG_ZOOACCESS_DATE_END"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.date._publish_down_state') ? 0 : 1).'"' : '').'
					}
				}
			}
		}
	}';


	/*
	 * ========== User Agent ========== TODO
	 */
	// $conditions[] =
	// '"useragent_fieldset": {
	// 	"type":"fieldset",
	// 	"control":"useragent",
	// 	"fields": {
	// 		"_assignto":{
	// 			"type":"radio",
	// 			"label":"PLG_ZOOACCESS_USER_AGENT",
	// 			"help":"PLG_ZOOACCESS_USER_AGENT_DESC||PLG_ZLFRAMEWORK_ACC_ASSIGN_DESC",
	// 			"default":"0",
	// 			"class":"special",
	// 			"specific":{
	// 				"options":{
	// 					"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
	// 					"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
	// 					"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
	// 				}
	// 			},
	// 			"dependents":"useragent_params > 1 OR 2"
	// 			'.($evaluated && $enviroment == 'type-positions' ? ',
	// 			"state": {
	// 				"init_state":"0",
	// 				"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
	// 			},
	// 			"data_from_config":"'.($params->find($fname.'.conditions.useragent._assignto_state') ? 0 : 1).'"' : '').'
	// 		},
	// 		"useragent_params": {
	// 			"type":"wrapper",
	// 			"fields": {
	// 				"_agents":{
	// 					"type":"select",
	// 					"label":"PLG_ZOOACCESS_AGENTS",
	// 					"specific": {
	// 						"options":{
	// 							"Search Bots":"searchbots"
	// 						},
	// 						"multi":"true"
	// 					}
	// 					'.($evaluated && $enviroment == 'type-positions' ? ',
	// 					"state": {
	// 						"init_state":"0",
	// 						"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
	// 					},
	// 					"data_from_config":"'.($params->find($fname.'.conditions.useragent._agents_state') ? 0 : 1).'"' : '').'
	// 				},
	// 				"_mode":{
	// 					"type":"radio",
	// 					"label":"PLG_ZLFRAMEWORK_ACC_MODE",
	// 					"help":"PLG_ZLFRAMEWORK_ACC_MODE_DESC",
	// 					"specific": {
	// 						"options": {
	// 							"PLG_ZLFRAMEWORK_AND":"1",
	// 							"PLG_ZLFRAMEWORK_OR":"0"
	// 						}
	// 					},
	// 					"default":"0"
	// 					'.($evaluated && $enviroment == 'type-positions' ? ',
	// 					"state": {
	// 						"init_state":"0",
	// 						"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
	// 					},
	// 					"data_from_config":"'.($params->find($fname.'.conditions.useragent._mode_state') ? 0 : 1).'"' : '').'
	// 				}
	// 			}
	// 		}
	// 	}
	// }';

	/*
	 * ========== Searchbot ==========
	 */
	$conditions[] =
	'"searchbot_fieldset": {
		"type":"fieldset",
		"control":"searchbot",
		"fields": {
			"_assignto":{
				"type":"radio",
				"label":"PLG_ZOOACCESS_SEARCHBOT",
				"help":"PLG_ZOOACCESS_SEARCHBOT_DESC||PLG_ZLFRAMEWORK_ACC_ASSIGN_DESC",
				"default":"0",
				"class":"special",
				"specific":{
					"options":{
						"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
						"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
						"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
					}
				},
				"dependents":"searchbot_params > 1 OR 2"
				'.($evaluated && $enviroment == 'type-positions' ? ',
				"state": {
					"init_state":"0",
					"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
				},
				"data_from_config":"'.($params->find($fname.'.conditions.searchbot._assignto_state') ? 0 : 1).'"' : '').'
			},
			"searchbot_params": {
				"type":"wrapper",
				"fields": {
					"_bots":{
						"type":"text",
						"label":"PLG_ZOOACCESS_BOTS",
						"help":"PLG_ZOOACCESS_BOTS_DESC",
						"default":"nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves/Teoma|ia_archiver"
						'.($evaluated && $enviroment == 'type-positions' ? ',
						"state": {
							"init_state":"0",
							"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
						},
						"data_from_config":"'.($params->find($fname.'.conditions.searchbot._bots_state') ? 0 : 1).'"' : '').'
					}
				}
			}
		}
	}';


	/*
	 * ========== Element Value ==========
	 */
	$elements = array();
	if($cache = $this->app->zlfield->cache->get('zooaccess.elements'))
	{
		$elements = $cache;
	}
	else
	{
		foreach ($type->getElements() as $key => $el){
			$elements[$el->getConfig()->name] = $key;
		}

		// store the value on cache var
		$this->app->zlfield->cache->set('zooaccess.elements', $elements);
	}
	
	$conditions[] =
	'"elementvalue_fieldset": {
		"type":"fieldset",
		"control":"elementvalue",
		"fields": {
			"_assignto":{
				"type":"radio",
				"label":"PLG_ZOOACCESS_ELEMENT_VALUE",
				"help":"PLG_ZOOACCESS_ELEMENT_VALUE_DESC||PLG_ZLFRAMEWORK_ACC_ASSIGN_DESC",
				"default":"0",
				"class":"special",
				"specific":{
					"options":{
						"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
						"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
						"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
					}
				},
				"dependents":"elementvalue_params > 1 OR 2"
				'.($evaluated && $enviroment == 'type-positions' ? ',
				"state": {
					"init_state":"0",
					"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
				},
				"data_from_config":"'.($params->find($fname.'.conditions.elementvalue._assignto_state') ? 0 : 1).'"' : '').'
			},
			"elementvalue_params": {
				"type":"wrapper",
				"fields": {
					"_element":{
						"type":"select",
						"label":"PLG_ZOOACCESS_ELEMENT",
						"help":"PLG_ZOOACCESS_ELEMENT_DESC",
						"specific": {
							"options":'.json_encode($elements).'
						}
					},
					"_value":{
						"type":"text",
						"label":"PLG_ZOOACCESS_VALUE",
						"help":"PLG_ZOOACCESS_VALUE_DESC"
					}
				}
			}
		}
	}';


	/*
	 * ========== Content Plugin ==========
	 */
	$conditions[] =
	'"contentplugin_fieldset": {
		"type":"fieldset",
		"control":"contentplugin",
		"fields": {
			"_assignto":{
				"type":"radio",
				"label":"PLG_ZOOACCESS_CONTENT_PLUGIN",
				"help":"PLG_ZOOACCESS_CONTENT_PLUGIN_DESC||PLG_ZLFRAMEWORK_ACC_ASSIGN_DESC",
				"default":"0",
				"class":"special",
				"specific":{
					"options":{
						"PLG_ZLFRAMEWORK_ACC_SELECTION":"1",
						"PLG_ZLFRAMEWORK_ACC_EXCLUDE_SELECTION":"2",
						"PLG_ZLFRAMEWORK_ACC_IGNORE":"0"
					}
				},
				"dependents":"contentplugin_params > 1 OR 2"
				'.($evaluated && $enviroment == 'type-positions' ? ',
				"state": {
					"init_state":"0",
					"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
				},
				"data_from_config":"'.($params->find($fname.'.conditions.contentplugin._assignto_state') ? 0 : 1).'"' : '').'
			},
			"contentplugin_params": {
				"type":"wrapper",
				"fields": {
					"_expression":{
						"type":"textarea",
						"label":"PLG_ZOOACCESS_EXPRESSION"
					}
				}
			}
		}
	}';


		// "_itemoveride":{
		// 	"type":"checkbox",
		// 	"label":"PLG_ZOOACCESS_ITEM_OVERRIDE",
		// 	"help":"PLG_ZOOACCESS_ITEM_OVERRIDE_DESC",
		// 	"specific":{
		// 		"label":"JYES"
		// 	},
		// 	"render":"'.(!$enviroment == 'type-positions' && !$isCore ? 1 : 0).'"
		// },

		// "conditions_message":{
		// 	"type":"info",
		// 	"specific":{
		// 		"text":"<div class=\"anotation\">assign this element to</div>"
		// 	}
		// },

	/*
	 * ========== JSON ==========
	 */
	return
	'{"fields": {
		
		"_evaluate":{
			"type":"checkbox",
			"label":"'.$label_evaluate.'",
			"help":"'.$help_evaluate.'",
			"specific":{
				"label":"JYES"
			},
			"dependents":"_options_wrapper > 1"
		},

		"_options_wrapper":{
			"type": "wrapper",
			"fields": {

				'./* conditions matching method */'
				"_match_method":{
					"type":"radio",
					"label":"PLG_ZLFRAMEWORK_ACC_MATCHING_METHOD",
					"help":"PLG_ZLFRAMEWORK_ACC_MATCHING_METHOD_DESC",
					"default":"1",
					"specific": {
						"options": {
							"PLG_ZLFRAMEWORK_ALL":"1",
							"PLG_ZLFRAMEWORK_ANY":"0"
						}
					}
					'.($evaluated && $enviroment == 'type-positions' ? ',
					"state": {
						"init_state":"0",
						"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
					},
					"data_from_config":"'.($params->find($fname.'._match_method_state') ? 0 : 1).'"' : '').'
				},

				'./* conditions action */'
				"_action":{
					"type":"select",
					"label":"PLG_ZLFRAMEWORK_ACC_ACTION",
					"help":"PLG_ZLFRAMEWORK_ACC_ACTION_DESC",
					"default":"1",
					"specific": {
						"options": {
							"PLG_ZLFRAMEWORK_ACC_RENDER":"1",
							"PLG_ZLFRAMEWORK_ACC_NOT_RENDER":"0"
						}
					}
					'.($evaluated && $enviroment == 'type-positions' ? ',
					"state": {
						"init_state":"0",
						"label":"PLG_ZLFRAMEWORK_OVERRIDE_THIS_FIELD"
					},
					"data_from_config":"'.($params->find($fname.'._action_state') ? 0 : 1).'"' : '').'
				},

				'./* conditions */'
				"conditions_wrapper": {
					"type":"control_wrapper",
					"control":"conditions",
					"fields": {
						'.implode(',', $conditions).'
					}
				}
				
			}
		}
		
	}}';
?>