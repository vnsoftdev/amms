<?php
/*
* @package		ZOOaccess
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: ZL Access
		The ZOOaccess base helper Class for zoo
*/
class ZooAccessHelper extends AppHelper 
{	
	/**
	 * Get final params
	 *
	 * @param object $element Element getting evaluated
	 * @param data object $params Element position params
	 *
	 * @return data object Final params
	 *
	 * @since 2.6
	 */
	public function getFinalParams($element, $params)
	{
		$config = $element->config;
		$params = $element->app->data->create($params);
	
		/* === get params from config === */
		$evaluate 				= $config->find('zooaccess._evaluate', 0);
		$match_method   		= $config->find('zooaccess._match_method', 1);
		$action			   		= $config->find('zooaccess._action', 1);
		$conditions				= $config->find('zooaccess.conditions', array());

		/* === override from item === */
		if ($element->getItem() && isset($data['zooaccess']['_evaluate']))
		{
			$data = $element->app->data->create($element->data());

			$match_method   	= $data->find('zooaccess._match_method', 1);
			$match_method   	= $data->find('zooaccess._action', 1);
			$conditions			= $data->find('zooaccess.conditions', array());
		}

		/* === override from position === */
		else if ($evaluate || $params->find('zooaccess._evaluate', 0))
		{
			$evaluate 			= 1;
			$match_method   	= strlen($params->find('zooaccess._match_method', '')) ? $params->find('zooaccess._match_method') : $match_method;
			$action   			= strlen($params->find('zooaccess._action', '')) ? $params->find('zooaccess._action') : $action;
			$conditions			= $this->mergeArrays($conditions, $params->find('zooaccess.conditions', array()));
		}

		// return result
		return $element->app->data->create(array('evaluate' => $evaluate, 'match_method' => $match_method, 'action' => $action, 'conditions' => $conditions));
	}

	/**
	 * mergeArrays
	 *
	 * @param array $Arr1 Array of values
	 * @param array $Arr1 Array of values
	 *
	 * @return merged array
	 *
	 * @since 2.6
	 */
	public function mergeArrays($Arr1, $Arr2)
	{
	  foreach($Arr2 as $key => $Value)
	  {
	    if(array_key_exists($key, $Arr1) && is_array($Value))
	      $Arr1[$key] = $this->mergeArrays($Arr1[$key], $Arr2[$key]);
	    else
	      $Arr1[$key] = $Value;
	  }
	  return $Arr1;
	}

	/**
	 * getAssignLogic
	 *
	 * @param array $params Array of options
	 * @param boolean $render The initial render value
	 *
	 * @return boolean
	 *
	 * @since 3.0.3
	 */
	public function getAssignLogic($params, $render)
	{
		// assign/exclude logic
		if ($params['_assignto'] == '1' && $render) {
			return true;
		} else if ($params['_assignto'] == '2') {
			return !$render; // return the oposite
		} else {
			return $render;
		}
	}

	/**
	 * evaluateUseraccess
	 *
	 * @param array $params Array of options
	 * @param object $element Element object being rendered
	 * @param object $user The current user object
	 *
	 * @return boolean
	 *
	 * @since 2.6
	 */
	public function evaluateUseraccess($params, $element, $user)
	{
		// if selected set author as user
		if (isset($params['_user']) && $params['_user']) {
			$user = $this->app->user->get($element->getItem()->created_by);
		}

		// if user not valid, deny access
		if (empty($user)) return false;

		// init vars
		$user_access = $user->getAuthorisedViewLevels();
		$allowedaccessids = $params['_levels'];
		$mode = $params['_mode'];

		// the selected group levels
		$render = false;
		if ( $user_access !== false )
		{    
			if ($mode==1) // if AND
			{
				foreach ($user_access as $key => $user_access_id){
					if(in_array($user_access_id, $allowedaccessids)) {
						unset($user_access[$key]);
					}
				}
				
				// if empty means user is part of all selected group levels
				if(empty($user_access)){
					$render = true;
				}
			}
			if ($mode==0) // if OR
			{
				foreach ($user_access as $user_access_id){
					if(in_array($user_access_id, $allowedaccessids)) {
						$render = true;
					}
				}
			}
		}

		// get assign logic and return
		return $this->getAssignLogic($params, $render);
	}

	/**
	 * evaluateUsergroup
	 *
	 * @param array $params Array of options
	 * @param object $element Element object being rendered
	 * @param object $user The current user object
	 *
	 * @return boolean
	 *
	 * @since 2.6
	 */
	public function evaluateUsergroup($params, $element, $user)
	{
		// if selected set author as user
		if (isset($params['_user']) && $params['_user']) {
			$user = $this->app->user->get($element->getItem()->created_by);
		}

		// if user not valid, deny access
		if (empty($user)) return false;

		// init vars
		$user_groups = $user->getAuthorisedGroups();
		$allowedgroupids = $params['_levels'];
		$mode = $params['_mode'];

		// the selected group levels
		$render = false;
		if ( $user_groups !== false )
		{    
			if ($mode==1) // if AND
			{
				foreach ($user_groups as $key => $user_group_id){
					if(in_array($user_group_id, $allowedgroupids)) {
						unset($user_groups[$key]);
					}
				}
				
				// if empty means user is part of all selected group levels
				if(empty($user_groups)){
					$render = true;
				}
			}
			if ($mode==0) // if OR
			{
				foreach ($user_groups as $user_group_id){
					if(in_array($user_group_id, $allowedgroupids)) {
						$render = true;
					}
				}
			}
		}

		// get assign logic and return
		return $this->getAssignLogic($params, $render);
	}

	/**
	 * evaluateDate
	 *
	 * @param array $params Array of options
	 * @param object $element Element object being rendered
	 * @param object $user The current user object
	 *
	 * @return boolean
	 *
	 * @since 3.0.4
	 */
	public function evaluateDate($params, $element, $user)
	{
		$render = true;
		$date = JFactory::getDate();
		$tz = new DateTimeZone(JFactory::getApplication()->getCfg('offset'));
		$date->setTimeZone($tz);

		if ($params['_publish_up'] || $params['_publish_down']) {
			$now = strtotime($date->format('Y-m-d H:i:s', 1));
			if ((int) $params['_publish_up']) {
				if (strtotime($params['_publish_up']) > $now) {
					// outside date range
					$render = false;
				}
			}
			if ((int) $params['_publish_down']) {
				if (strtotime($params['_publish_down']) < $now) {
					// outside date range
					$render = false;
				}
			}
		}

		// get assign logic and return
		return $this->getAssignLogic($params, $render);
	}

	/**
	 * evaluateElementvalue
	 *
	 * @param array $params Array of options
	 * @param object $element Element object being rendered
	 * @param object $user The current user object
	 *
	 * @return boolean
	 *
	 * @since 2.6
	 */
	public function evaluateElementvalue($params, $element, $user)
	{
		$render = false;
		$search_data = $element->getItem()->getElement($params['_element'])->getSearchData();

		if (preg_match('/'.$params['_value'].'/', $search_data)){
			$render = true;
		}

		// get assign logic and return
		return $this->getAssignLogic($params, $render);
	}

	/**
	 * evaluateContentplugin
	 *
	 * @param array $params Array of options
	 * @param object $element Element object being rendered
	 * @param object $user The current user object
	 *
	 * @return boolean
	 *
	 * @since 3.0.2
	 */
	public function evaluateContentplugin($params, $element, $user)
	{
		$result = trim($this->app->zoo->triggerContentPlugins($params['_expression']));

		// render if any content left
		$render = !empty($result);

		// get assign logic and return
		return $this->getAssignLogic($params, $render);
	}

	/**
	 * evaluateSearchbot
	 *
	 * @param array $params Array of options
	 * @param object $element Element object being rendered
	 * @param object $user The current user object
	 *
	 * @return boolean
	 *
	 * @since 2.6
	 */
	public function evaluateSearchbot($params, $element, $user)
	{
		// load dependencies
		jimport('joomla.environment.browser');

		// init vars
		$render = false;
		$bots = $params['_bots'];
		$browser = JBrowser::getInstance();
		$a = $browser->getAgentString();

		// chech the agent
		if (preg_match('#('.$bots.')#i', $a)) {
			$render = true;
		}

		// get assign logic and return
		return $this->getAssignLogic($params, $render);
	}
}