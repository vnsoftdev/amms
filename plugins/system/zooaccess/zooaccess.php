<?php
/*
* @package		ZOOaccess
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Import library dependencies
jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');

class plgSystemZooAccess extends JPlugin {

	public $joomla;
	public $app;
	
	/**
	 * onAfterInitialise handler
	 *
	 * Adds ZOO event listeners
	 *
	 * @access public
	 * @return null
	 */
	function onAfterInitialise()
	{
		// Get Joomla instances
		$this->joomla = JFactory::getApplication();
		$jlang = JFactory::getLanguage();
		
		// load default and current language
		$jlang->load('plg_system_zooaccess', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('plg_system_zooaccess', JPATH_ADMINISTRATOR, null, true);

		// check dependences
		if (!defined('ZLFW_DEPENDENCIES_CHECK_OK')){
			$this->checkDependencies();
			return; // abort
		}

		// Get the ZOO App instance
		$this->app = App::getInstance('zoo');
		
		// register plugin path
		if ( $path = $this->app->path->path( 'root:plugins/system/zooaccess' ) ) {
			$this->app->path->register($path, 'zooaccess');
		}
		
		// register fields - necesary since ZOO 2.5.13
		if ( $path = $this->app->path->path( 'zooaccess:fields' ) ) {
			$this->app->path->register($path, 'fields');
		}
			
		// register helpers
		if ( $path = $this->app->path->path( 'zooaccess:helpers' ) ) {
			$this->app->path->register($path, 'helpers');
			$this->app->loader->register('ZooAccessHelper', 'helpers:zooaccess.php');
		}
		
		// register events
		$this->app->event->dispatcher->connect('element:configparams', array($this, 'addElementConfig'));
		$this->app->event->dispatcher->connect('element:configform', array($this, 'configForm'));
		$this->app->event->dispatcher->connect('element:beforedisplay', array($this, 'beforeElementDisplay'));
		$this->app->event->dispatcher->connect('element:beforesubmissiondisplay', array($this, 'beforeElementDisplay'));
	}
	
	/*
		Function: initTypeLayouts
			Callback function for the zoo layouts

		Returns:
			void
	*/
	public function initTypeLayouts($event)
	{
		$extensions = (array) $event->getReturnValue();
		
		// add plugin layout
		$extensions[] = array('name' => 'ZOOaccess Plugin', 'path' => $this->app->path->path('zooaccess:'), 'type' => 'plugin');
		
		$event->setReturnValue($extensions);
	}
	
	/**
	 * Add Fields path
	 */
	public function configForm( $event, $arguments=array() )
	{
		$config = $event['form'];
		
		// add XML params path
		$config->addElementPath($this->app->path->path('zooaccess:fields'));
	}
	
	/** 
	 * New method for adding params to the element
	 * @since 2.5
	 */
	public static function addElementConfig($event)
	{
		// Get the ZOO App instance
		$zoo = App::getInstance( 'zoo' );
		
		// Custom Params File
		$file = $zoo->path->path( 'zooaccess:element.xml');
		$xml = simplexml_load_file( $file );
		
		// Old params
		$params = $event->getReturnValue();

		// add new params from custom params file
		$params[] = $xml->asXML();
		$event->setReturnValue($params);
	}

	/**
	 * beforeElementDisplay
	 *
	 * @param array $event Array of conditions
	 *
	 * @return boolean
	 */
	public function beforeElementDisplay($event)
	{
		$element = $event['element'];
		$params  = $event['params'];
		$user    = isset($event['user']) ? $event['user'] : $this->app->user->get();

		// get the final overided params
		$accparams = $this->app->zooaccess->getFinalParams($element, $params);

		// perform only if evaluate is checked
		if($accparams->get('evaluate'))
		{
			// init vars
			$conditions 	= $accparams->get('conditions', array());
			$match_method 	= $accparams->get('match_method', 1);
			$action			= $accparams->get('action', 1);

			// avoid rendering if result is false
			if(!$this->evaluate($conditions, $match_method, $action, $element, $user)){
				$event['render'] = false;
			}
		}
	}

	/**
	 * evaluate
	 *
	 * @param array $conditions Array of conditions
	 * @param array $match_method 1 = ALL | 0 = ANY
	 *
	 * @return boolean
	 */
	public function evaluate($conditions, $match_method, $action, $element, $user)
	{
		// init vars
		$match = false;

		// evaluate conditions if assignement is set and not ignored
		foreach ($conditions as $key => $condition) if(strlen($condition['_assignto']) && $condition['_assignto'] != 0)
		{
			$function = 'evaluate'.ucfirst($key);
			if ($match_method == 1)
			{
				// if at least one of conditions NOT met, there's no match
				if (!$this->app->zooaccess->$function($condition, $element, $user)) {
					$match = false;
					break; // stop further evaluation
				} else {
					$match = true;
				}
			}
			else if ($match_method == 0)
			{
				// if at least one of conditions DO met, there's a match
				if ($this->app->zooaccess->$function($condition, $element, $user)) {
					$match = true;
					break; // stop further evaluation
				}
			}
		}

		// evaluate match and action
		return $action == 1 ? $match : !$match;
	}
	
	/**
	 * add params for after edit
	 */
	public static function afterEdit($event) {
		
	}
	
	/*
	 *  checkDependencies
	 */
	public function checkDependencies()
	{
		if($this->joomla->isAdmin())
		{
			// if ZLFW not enabled
			if(!JPluginHelper::isEnabled('system', 'zlframework') || !JComponentHelper::getComponent('com_zoo', true)->enabled) {
				$this->joomla->enqueueMessage(JText::_('PLG_ZOOACCESS_MISSING_DEPENDENCIES'), 'notice');
			} else {
				// load zoo
				require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

				// fix plugins ordering
				$zoo = App::getInstance('zoo');
				$zoo->loader->register('ZlfwHelper', 'root:plugins/system/zlframework/zlframework/helpers/zlfwhelper.php');
				$zoo->zlfw->checkPluginOrder('zooaccess');
			}
		}
	}
}