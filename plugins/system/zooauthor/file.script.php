<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       file.script.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

class plgSystemZOOauthorInstallerScript {

	public function install($parent) {}

	public function uninstall($parent) {}

	public function update($parent) {}

	public function preflight($type, $parent) {

		if ($type == 'Update') {

			// load config
			require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

			// get app
			$zoo = App::getInstance('zoo');

			foreach ($zoo->filesystem->readDirectoryFiles($parent->getParent()->getPath('source'), $parent->getParent()->getPath('source').'/', '/(positions\.(config|xml)|metadata\.xml)$/', true) as $file) {
				JFile::delete($file);
			}

		}
	}

	public function postflight($type, $parent) {}
}
