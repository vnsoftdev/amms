<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       zooauthor.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: ZOOauthorHelper
		The helper class for the ZOOauthor
*/
		
class ZOOauthorHelper extends AppHelper {
	
	/* ZOOauthor plugin Params*/
	private $_params = null;
	
	/* ZOO Item Renderer*/
	private $_renderer = null;
	
	/*
	 *	Function that return ZOOauthor plugin Params
	 */
	public function getParams() {			
		if ($this->_params == null) {
			jimport('joomla.plugin.helper');				
			$zooauthor = JPluginHelper::getPlugin('system', 'zooauthor');
			$this->_params = $this->app->parameter->create($zooauthor->params);						
		} 		
		return $this->_params;
	}
	
	/*
	 *	Function that detect the Spammer by StopForumSpam
	*/
	public function checkSpammer(&$user) {
        if (!$user['block']) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $email = urlencode($user['email']);
            $username = urlencode($user['username']);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://www.stopforumspam.com/api?ip='.$ip.'&email='.$email.'&username='.$username.'&f=json');
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($httpCode == 200) {
                $response = json_decode($response);
                if ($response->ip->appears || $response->email->appears || $response->username->appears) {
                    $this->app->database->query("UPDATE #__users SET block = 1 WHERE id = ".$user['id']);                    
                    $user['notes'] = JText::_('PLG_ZOOAUTHOR_POSSIBLE_SPAMMER_DETECTED_BY_STOPFORUMSPAM');
                }
            }
        }
    }
		
	/*
	 *	Function that return the ZOO Item as User Profile
	*/
	public function getProfile($userId = null, $published = false) {

		// get database	
		$db   = $this->app->database;

		// get dates
		$date = $this->app->date->create();
		$now  = $db->Quote($date->toSQL());
		$null = $db->Quote($db->getNullDate());
		
		// set query options
		$conditions = 
			"a.application_id = ".(int) $this->getParams()->get('application')
		    ." AND a.type = ".$db->Quote($this->getParams()->get('type'))
			." AND a.created_by = ".(int) (empty($userId) ? $this->app->user->get()->id : $userId)
			.($published == true ? " AND a.state = 1"
			." AND (a.publish_up = ".$null." OR a.publish_up <= ".$now.")"
			." AND (a.publish_down = ".$null." OR a.publish_down >= ".$now.")": "");

		$options = array(
			'select' => 'a.*',
			'from' => ZOO_TABLE_ITEM.' AS a',
			'conditions' => $conditions,
			'order' => 'a.priority DESC',
			'limit' => '1');
			
		// get items
		$item = $this->app->table->item->first($options);
		
		return is_object($item) ? $item : false;
	}

	/*
	 *	Function that return new the ZOO Item as User Profile
	*/
	public function setProfile($user) {
		// init vars			
		$tzoffset   = $this->app->date->getOffset();
		// get item
		$item = $this->app->object->create('Item');
		$item->application_id = $this->getParams()->get('application');
		$item->type = $this->getParams()->get('type');
		$item->created_by = $user['id'];
		$item->access = $this->app->joomla->getDefaultAccess();
		// set name and alias
		$item->name  = isset($user['fullname']) ? $user['fullname'] : (isset($user['name']) ? $user['name'] : $user['username']);
		$item->alias = $this->app->string->sluggify($user['username']);
		$item->alias = $this->app->alias->item->getUniqueAlias($item->id, $this->app->string->sluggify($item->alias));
		// set modified
		$item->modified	   = $this->app->date->create()->toSQL();
		$item->modified_by = $user['id'];
		// set created date
		if ($item->created && strlen(trim($item->created)) <= 10) {
			$item->created .= ' 00:00:00';
		}
		$date = $this->app->date->create($item->created, $tzoffset);
		$item->created = $date->toSQL();
		// set publish up date
		if (strlen(trim($item->publish_up)) <= 10) {
			$item->publish_up .= ' 00:00:00';
		}
		$date = $this->app->date->create($item->publish_up, $tzoffset);
		$item->publish_up = $date->toSQL();
		// set publish down date
		if (trim($item->publish_down) == JText::_('Never') || trim($item->publish_down) == '') {
			$item->publish_down = $this->app->database->getNullDate();
		} else {
			if (strlen(trim($item->publish_down)) <= 10) {
				$item->publish_down .= ' 00:00:00';
			}
			$date = $this->app->date->create($item->publish_down, $tzoffset);
			$item->publish_down = $date->toSQL();
		}
		// set state
		//$item->state = isset($user['block']) ? ($user['block'] == 1 ? 0 : 1) : 1;
		$item->state = 1;
		// ML: tắt chức năng comment
		$item->params =  '{"config.enable_comments": 0}';
		// save item
		$this->app->table->item->save($item);
	}
	
	/*
	 *	Function that delete the ZOO Item as User Profile
	*/
	public function delProfile($userId) {
		if ($userId && $item = $this->getProfile($userId)) {
			return $this->app->table->item->delete($item);
		} else return false;
	}			
	
	/*
	 *	Function that return Item HTML Data
	 */
	public function getProfileHTML($item, $layout, $params = null) {		
		if ($this->_renderer == null) {			
			$this->_renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->app->path->path('zooauthor:')));						
		} 				
		return $this->_renderer->render('item.'.$layout,
										array('view' => $item->getApplication(), 'item' => $item, 'params' => $params));
	}

	/*
		Function: loadLanguage
			Load elements language file.

		Returns:
			Void
	*/
	public function loadLanguage() {
		// init vars						
		$jlang = $this->app->system->language;		
		// lets load first english, then joomla default standard, then user language
		$jlang->load('plg_system_zooauthor', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('plg_system_zooauthor', JPATH_ADMINISTRATOR, $jlang->getDefault(), true);
		$jlang->load('plg_system_zooauthor', JPATH_ADMINISTRATOR, null, true);
	}
}

/*
	Class: ZOOauthorHelperException
*/
class ZOOauthorHelperException extends AppException {}
