# ZOOauthor #

- Version: 3.0.0
- Date: June 2013
- Author: Attavus M.D.
- Website: <http://www.raslab.org>
- Copyright (C) 2013 R.A.S.Lab[.org]
- License:  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only

## Changelog
------------

	3.0.0
	+ Initial Release
	! The ZOOauthor is include user and system plugin, module, authorlink element

	* -> Security Fix
	# -> Bug Fix
	$ -> Language fix or change
	+ -> Addition
	^ -> Change
	- -> Removed
	! -> Note
