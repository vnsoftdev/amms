<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       element.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<?php if ($this->checkPosition('media')) : ?>
	<?php echo $this->renderPosition('media'); ?>
<?php endif; ?>
