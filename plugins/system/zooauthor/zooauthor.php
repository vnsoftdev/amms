<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       zooauthor.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: plgSystemZOOauthor
		the system plugin class for the ZOOauthor
*/

class plgSystemZOOauthor extends JPlugin {
	
	/** @var object of ZOO App instance */
	public $app;	

	/**
	 * onAfterInitialise handler
	 *
	 * This event is triggered after the framework has loaded and the application initialise method has been called
	 * Adds ZOO event listeners
	 *
	 * @access public
	 * @return null
	 */
	public function onAfterInitialise() {	
		
		// make sure ZOO exist
		jimport('joomla.filesystem.file');
		if (!JFile::exists(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php')
				|| !JComponentHelper::getComponent('com_zoo', true)->enabled) {
			return;
		}
				
		// load ZOO config
		require_once (JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');		
		
		// check if ZOO > 2.4 is loaded
		if (!class_exists('App')) {
			return;
		}
	
		// get the ZOO App instance
		$this->app = App::getInstance('zoo');

		// register plugin path
		if ($path = $this->app->path->path('root:plugins/system/zooauthor/')) {
			$this->app->path->register($path, 'zooauthor');
		}
					
		// register helpers path & load helper
		if ($path = $this->app->path->path('zooauthor:helpers/')) {
			$this->app->path->register($path, 'helpers');			
			$this->app->loader->register('ZOOauthorHelper', 'helpers:zooauthor.php');
		}		
		
		// register global events		
		$this->app->event->dispatcher->connect('item:saved', array($this, 'afterItemSave'));
		$this->app->event->dispatcher->connect('item:deleted', array($this, 'afterItemDelete'));
		$this->app->event->dispatcher->connect('item:stateChanged', array($this, 'afterItemStateChange'));		

		//load language.file to make translations work
		$this->loadLanguage();
	}

	/**
	 * onAfterRoute handler
	 *
	 * set redirect from joomla user profile to zoo user profile
	 *
	 * @access public
	 * @return null
	 */
	public function onAfterRoute() {		
		// set redirect from joomla user profile to zoo user profile
		if ($this->app->system->application->isSite()
			&& $this->app->request->getCmd('option') == 'com_users'
			&& $this->app->request->getCmd('view') == 'profile'
			&& $this->app->zooauthor->getParams()->get('redirect', 1)) {

			if($userId = $this->app->request->getCmd('user_id')) {
				$profile = $this->app->zooauthor->getProfile($userId);
			} else {
				$profile = $this->app->zooauthor->getProfile();
			}
					
			$this->app->system->application->redirect($this->app->route->item($profile));
			
		}		
	}	
	
	/**
	 * Check an Events after save the ZOO Items
	 * 
	 * @param AppEvent $event
	 * @param array $arguments
	 */
	public function afterItemSave($event) {
		// init vars
		$item = $event->getSubject();
		$new  = $event['new'];
		$db   = $this->app->database;
		// change user
		if($item->getApplication()->id == $this->app->zooauthor->getParams()->get('application')
			&& $item->getType()->id == $this->app->zooauthor->getParams()->get('type')) {
				
			$query = $db->getQuery(true);
			$query->update('#__users')
					->set('name = '.$db->Quote($item->name))
					->set('username = '.$db->Quote($item->alias))
					->set('block = '.($item->state == 1 ? 0 : 1))
					->where('id = '.$item->created_by);
			$db->query($query);
		}  
	}

	/**
	 * Check an Events after state change the ZOO Items
	 * 
	 * @param AppEvent $event
	 * @param array $arguments
	 */
	public function afterItemStateChange($event) {
		// init vars
		$item = $event->getSubject();
		$old_state = $event['old_state'];
		// change user
		if($item->getApplication()->id == $this->app->zooauthor->getParams()->get('application')
			&& $item->getType()->id == $this->app->zooauthor->getParams()->get('type')) {
				
			$this->app->database->query("UPDATE #__users SET block = ".($item->state == 1 ? 0 : 1)." WHERE id = ".$item->created_by);
		}
	}

	/**
	 * Check an Events after delete the ZOO Items
	 * 
	 * @param AppEvent $event
	 * @param array $arguments
	 */
	public function afterItemDelete($event) {
		// init vars
		$item = $event->getSubject();
		// delete user		
		if($item->getApplication()->id == $this->app->zooauthor->getParams()->get('application')
			&& $item->getType()->id == $this->app->zooauthor->getParams()->get('type')) {
				
			$this->app->user->get($item->created_by)->delete();
		}
	}	
}
