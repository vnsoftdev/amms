<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class plgSystemZoocartInstallerScript
{
	protected $_error;
	protected $_src;
	protected $_target;
	protected $_ext = 'zoocart';
	protected $_ext_name = 'ZOOcart';
	protected $_lng_prefix = 'PLG_ZOOCART_SYS';

	/**
	 * Called before any type of action
	 *
	 * @param   string  $type  Which action is happening (install|uninstall|discover_install)
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function preflight($type, $parent)
	{
		// init vars
		$db = JFactory::getDBO();
		$type = strtolower($type);
		$this->_src = $parent->getParent()->getPath('source'); // tmp folder
		$this->_target = JPATH_ROOT.'/plugins/system/zoocart'; // install folder

		// load ZLFW sys language file
		JFactory::getLanguage()->load('plg_system_zlframework.sys', JPATH_ADMINISTRATOR, 'en-GB', true);

		if($type == 'uninstall'){
			// save the sql files array while still exists
			$this->sqls = JFolder::files($this->_target . '/zoocart/sql');
		}
	}

	/**
	 * Called on installation
	 *
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	function install($parent)
	{
		// init vars
		$db = JFactory::getDBO();

		

		// enable plugin
		$db->setQuery("UPDATE `#__extensions` SET `enabled` = 1 WHERE `type` = 'plugin' AND `element` = '{$this->_ext}' AND `folder` = 'system'");
		$db->query();
	}

	/**
	 * Called on uninstallation
	 *
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	function uninstall($parent)
	{
		// init vars
		$db = JFactory::getDBO();
		
		// disable all zoocart modules
		$db->setQuery("UPDATE `#__extensions` SET `enabled` = 0 WHERE `element` LIKE '%zoocart%'")->query();

		// drop tables
		if(is_array($this->sqls)) foreach($this->sqls as $sql)
		{
			$sql = basename($sql, '.sql');
			$db->setQuery('DROP TABLE IF EXISTS `#__zoo_zl_zoocart_' . $sql . '`')->query();
		}

		// remove extra app config
		$zoo = App::getInstance('zoo');
		$path = $this->_target . '/zoocart/config/applications/config/';
		$files = $zoo->filesystem->readDirectoryFiles($path);
		$applications = $zoo->application->groups();
		foreach($applications as $application) {
			$group = $application->getGroup();
			foreach($files as $file) {
				$app_file = $zoo->path->path('applications:' . $group . '/config' ) . '/' . $file;
				// remove it if exists
				if ( JFile::exists($app_file) ) {
					JFile::delete($app_file);
				}
			}
		}

		// enqueue Message
		JFactory::getApplication()->enqueueMessage(JText::_($this->langString('_UNINSTALL')));
	}

	/**
	 * Called after install
	 *
	 * @param   string  $type  Which action is happening (install|uninstall|discover_install)
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function postflight($type, $parent)
	{
		// init vars
		$db = JFactory::getDBO();
		$type = strtolower($type);
		$release = $parent->get( "manifest" )->version;

		if($type == 'install'){
			echo JText::sprintf('PLG_ZLFRAMEWORK_SYS_INSTALL', $this->_ext_name, $release);
		}

		if($type == 'update'){
			echo JText::sprintf('PLG_ZLFRAMEWORK_SYS_UPDATE', $this->_ext_name, $release);
		}

		if($type != 'uninstall'){
		
			// create/update tables
			$sqls = JFolder::files($this->_src . '/zoocart/sql');
			if(is_array($sqls)) foreach($sqls as $sql)
			{
				$sql = JFile::read($this->_src . '/zoocart/sql/' . $sql);
				$queries = explode("-- QUERY SEPARATOR --", $sql);
				foreach($queries as $sql) {
					if ( !$db->setQuery($sql)->query() ) {
						$this->_error = 'ZL Error Query: ' . $sql . ' - ' . $db->getErrorMsg();
						return false;
					}
				}
			}

			// register extra app config
			$zoo = App::getInstance('zoo');
			$path = $this->_src . '/zoocart/config/applications/config/';
			$files = $zoo->filesystem->readDirectoryFiles($path);
			$applications = $zoo->application->groups();
			foreach($applications as $application) {
				$group = $application->getGroup();
				foreach($files as $file) {
					$app_file = $zoo->path->path('applications:' . $group . '/config' ) . '/' . $file;
					$plg_file = $this->_src . '/zoocart/config/applications/config/' . $file;
					// Copy only if not existent or newer
					if ( !JFile::exists($app_file) || (md5(JFile::read($app_file) != md5(JFile::read($plg_file))))) {
							JFile::copy($plg_file, $app_file);
						}
				}
			}
			
		}
	}

	/**
	 * creates the lang string
	 * @version 1.0
	 *
	 * @return  string
	 */
	protected function langString($string)
	{
		return $this->_lng_prefix.$string;
	}
}