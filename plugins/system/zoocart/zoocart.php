<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class plgSystemZoocart extends JPlugin { 

	// Just to easy the boring work of copy-paste
	protected $ext_name = 'zoocart';
	
	/**
	 * onAfterInitialise handler
	 *
	 * Adds ZOO event listeners
	 *
	 * @access	public
	 * @return null
	 */
	function onAfterInitialise()
	{
		// Get Joomla instances
		$this->joomla = JFactory::getApplication();
		$jlang = JFactory::getLanguage();

		// load default and current language
		$jlang->load('plg_system_zoocart', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('plg_system_zoocart', JPATH_ADMINISTRATOR, null, true);

		// check dependences
		if (!defined('ZLFW_DEPENDENCIES_CHECK_OK')){
			$this->checkDependencies();
			return; // abort
		}
		
		// Get the ZOO App instance
		$this->app = App::getInstance('zoo');
		
		// register plugin path
		if ( $path = $this->app->path->path( 'root:plugins/system/zoocart/zoocart' ) ) {
			$this->app->path->register($path, 'zoocart');
		}

		// register controllers
		if ( $path = $this->app->path->path( $this->ext_name.':controllers/' ) ) {
			$this->app->loader->register('ResourceController', $this->ext_name.':controllers/resource.php');
			$this->app->loader->register('AdminResourceController', $this->ext_name.':controllers/adminresource.php');
			$this->app->loader->register('SiteResourceController', $this->ext_name.':controllers/siteresource.php');
		}

		if ($this->joomla->isAdmin()) { 
			if ( $path = $this->app->path->path( $this->ext_name.':controllers/admin/' ) ) {
				$this->app->path->register($path, 'controllers');
			}
		} else {
			if ( $path = $this->app->path->path( $this->ext_name.':controllers/site/' ) ) {
				$this->app->path->register($path, 'controllers');
			}
		}

		// register assets
		if ( $path = $this->app->path->path( $this->ext_name.':assets/' ) ) {
			$this->app->path->register($path, 'assets');
		}
		
		// register helper
		if ( $path = $this->app->path->path( $this->ext_name.':helpers/' ) ) {
			$this->app->path->register($path, 'helpers');
		}

		// register classes
		if ( $path = $this->app->path->path( $this->ext_name.':classes/' ) ) {
			$this->app->path->register($path, 'classes');
			$this->app->loader->register('BootstrapPagination', 'classes:bspagination.php');
		}

		// register payment classes
		if ( $path = $this->app->path->path( $this->ext_name.':payment/' ) ) {
			$this->app->path->register($path, 'payment');
			$this->app->loader->register('JPaymentDriver', 'payment:driver.php');
		}

		// register shipping classes
		if ( $path = $this->app->path->path( $this->ext_name.':shipping/' ) ) {
			$this->app->path->register($path, 'shipping');
			$this->app->loader->register('JShippingDriver', 'shipping:driver.php');
		}

		// register tables
		if ( $path = $this->app->path->path( $this->ext_name.':tables/' ) ) {
			$this->app->path->register($path, 'tables');
			//$this->app->loader->register('LangHelper', 'helpers:lang.php');
		}

		// register elements
		if ( $path = $this->app->path->path( $this->ext_name.':elements/' ) ) {
			$this->app->path->register($path, 'elements');
		}
		
		// register fields
		if ( $path = $this->app->path->path( $this->ext_name.':fields/' ) ) {
			$this->app->path->register($path, 'fields');
		}

		// register renderer
		if ( $path = $this->app->path->path( $this->ext_name.':renderer/' ) ) {
			$this->app->loader->register('AddressRenderer', $this->ext_name.':renderer/address.php');
		}

		// register fields
		if ( $path = $this->app->path->path( $this->ext_name.':events/' ) ) {
			$this->app->path->register($path, 'events');
		}

		// Add menu item (First checking if ZOOcart enabled in app config)
		if ($this->joomla->isAdmin())
		{
			if ($id = $this->app->request->getInt('changeapp')) {
				$application = $this->app->table->application->get($id);
			} else {
				$application = $this->app->zoo->getApplication();
			}

			if($application && $application->getParams()->get('global.zoocart.enable_cart'))
				$this->app->event->dispatcher->connect('application:addmenuitems', array($this, 'addMenuItems'));
		}

		// ZOO Events
		$this->app->event->dispatcher->connect('layout:init', array($this, 'initTypeLayouts'));
		$this->app->event->dispatcher->connect('type:editdisplay', array($this, 'addTypeConfig'));
		$this->app->event->dispatcher->connect('type:beforesave', array($this, 'beforeTypeSave'));
		$this->app->event->dispatcher->connect('application:sefparseroute', array($this, 'sefParseRoute'));
		$this->app->event->dispatcher->connect('application:sefbuildroute', array($this, 'sefBuildRoute'));
		$this->app->event->dispatcher->connect('element:configparams', array($this, 'addElementConfig'));
		
		// ZOOCart Events
		$this->app->event->register('OrderEvent');
		$this->app->event->register('PaymentEvent');
		$this->app->event->dispatcher->connect('order:saved', array('OrderEvent', 'saved'));
		$this->app->event->dispatcher->connect('payment:saved', array('PaymentEvent', 'saved'));
		$this->app->event->dispatcher->connect('element:beforeaddressdisplay', array($this, 'beforeAddressDisplay'));
	}

	public function sefBuildRoute($event)
	{
		$segments = $event['segments'];
		$query 	  = $event['query'];

		$task = 'display';
		$controller = 'cart';

		if ((((@$query['task'] == $task || @$query['task'] == '') && (@$query['view'] == $controller || @$query['controller'] == $controller )))) {
			$segments[] = $this->app->alias->application->translateIDToAlias(@$query['app_id']);
			$segments[] = $controller;
			unset($query['task']);
			unset($query['view']);
			unset($query['controller']);
		}

		$task = 'display';
		$controller = 'orders';

		if ((((@$query['task'] == $task || @$query['task'] == '') && (@$query['view'] == $controller || @$query['controller'] == $controller )))) {
			if (!isset($query['app_id'])) {
				$query['app_id'] = $this->app->zoo->getApplication()->id;
			}
			$segments[] = $controller;
			unset($query['task']);
			unset($query['view']);
			unset($query['controller']);
		}

		$task = 'view';
		$controller = 'orders';

		if ((((@$query['task'] == $task) && (@$query['view'] == $controller || @$query['controller'] == $controller )))) {
			$segments[] = $controller;
			$segments[] = @$query['id'];
			unset($query['task']);
			unset($query['view']);
			unset($query['controller']);
			unset($query['id']);
		}

		$task = 'pay';
		$controller = 'orders';

		if ((((@$query['task'] == $task) && (@$query['view'] == $controller || @$query['controller'] == $controller )))) {
			$segments[] = $controller;
			$segments[] = $task;
			$segments[] = @$query['id'];
			unset($query['task']);
			unset($query['view']);
			unset($query['controller']);
			unset($query['id']);
		}

		$task = 'display';
		$controller = 'addresses';

		if ((((@$query['task'] == $task  || @$query['task'] == '') && (@$query['view'] == $controller || @$query['controller'] == $controller )))) {
			if (!isset($query['app_id'])) {
				$query['app_id'] = $this->app->zoo->getApplication()->id;
			}
			$segments[] = $controller;
			unset($query['task']);
			unset($query['view']);
			unset($query['controller']);
		}

		$task = 'edit';
		$controller = 'addresses';

		if ((((@$query['task'] == $task) && (@$query['view'] == $controller || @$query['controller'] == $controller )))) {
			$segments[] = $controller;
			$segments[] = isset($query['id']) ? $query['id'] : 'new';
			unset($query['task']);
			unset($query['view']);
			unset($query['controller']);
			unset($query['id']);
		}	

		$event['segments'] = $segments;
		$event['query']	   = $query;
	}

	public function sefParseRoute($event)
	{
		$segments = $event['segments'];
		$vars     = $event['vars'];

		$task = 'display';
		$controller = 'cart';

		$count = count($segments);

		if ($count == 2 && $segments[1] == $controller) {
			$vars['task']   = $task;
			$vars['view']	= $controller;
			$vars['controller'] = $controller;
			$vars['app_id'] = $this->app->alias->application->translateAliasToID($segments[0]);
		}

		$task = 'display';
		$controller = 'orders';

		if ($count == 1 && $segments[0] == $controller) {
			$vars['task']   = $task;
			$vars['view']	= $controller;
			$vars['controller'] = $controller;
		}


		$task = 'view';
		$controller = 'orders';

		if ($count == 2 && $segments[0] == $controller) {
			$vars['task']   = $task;
			$vars['view']	= $controller;
			$vars['controller'] = $controller;
			$vars['id'] = $segments[1];
		}

		$task = 'pay';
		$controller = 'orders';

		if (($count == 3 || $count == 2) && $segments[0] == $controller && $segments[1] == $task) {
			$vars['task']   = $task;
			$vars['view']	= $controller;
			$vars['controller'] = $controller;
			// Id can be passed via POST too
			if($count == 3) {
				$vars['id'] = $segments[2];
			}
		}


		$task = 'display';
		$controller = 'addresses';

		if ($count == 1 && $segments[0] == $controller) {
			$vars['task']   = $task;
			$vars['view']	= $controller;
			$vars['controller'] = $controller;
		}

		$task = 'edit';
		$controller = 'addresses';

		if ($count == 2 && $segments[0] == $controller) {
			$vars['task']   = $task;
			$vars['view']	= $controller;
			$vars['controller'] = $controller;
			if ($segments[1] != 'new') {
				$vars['id'] = $segments[1];
			}
		}

		$event['segments']	= $segments;
		$event['vars']		= $vars;
	}

	public function onUserLogin($user_data)
	{
		if ($user_data['status'] != 1) {
			return true;
		}

		if (get_class($this->app->table->cartitems) != 'CartitemsTable') {
			return true;
		}

		$user = JUser::getInstance($user_data['username']);

		$cartitems = $this->app->zoocart->table->cartitems->getByUser();

		if(!empty($cartitems))
		{
			// Clear user's existing cart items:
			$this->app->zoocart->table->cartitems->flushByUserId($user->id);

			// Change user_id in the guest's cart items
			foreach($cartitems as $cartitem) {
					$cartitem->user_id = $user->id;
					$this->app->zoocart->table->cartitems->save($cartitem);
			}
		}

		return true;
	}

	public function addMenuItems($event)
	{
		$application = $event->getSubject();
		$tab = $event['tab'];

		// Orders
		$controller = 'orders';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$orders = $this->app->object->create('AppMenuItem', array($application->id.'-orders', JText::_('PLG_ZOOCART_ORDERS'), $link));
		$tab->addChild($orders);

		// Configuration
		$controller = 'general';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes = $this->app->object->create('AppMenuItem', array($application->id.'-taxes', JText::_('PLG_ZOOCART_CONFIGURATION'), $link));

		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-general', JText::_('PLG_ZOOCART_CONFIG_GENERAL'), $link)));

		$controller = 'taxes';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-taxes', JText::_('PLG_ZOOCART_CONFIG_TAX_RULES'), $link)));

		$controller = 'taxclasses';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-taxclasses', JText::_('PLG_ZOOCART_CONFIG_TAXCLASSES'), $link)));

		$controller = 'addresses';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-addresses', JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPES'), $link)));

		$controller = 'currencies';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-currencies', JText::_('PLG_ZOOCART_CONFIG_CURRENCIES'), $link)));

		$controller = 'orderstates';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-orderstates', JText::_('PLG_ZOOCART_CONFIG_ORDERSTATES'), $link)));

		$controller = 'shippingrates';
		$link = $this->app->link(array('controller' => $controller, 'changeapp' => $application->id));
		$taxes->addChild($this->app->object->create('AppMenuItem', array($application->id.'-shippingrates', JText::_('PLG_ZOOCART_CONFIG_SHIPPINGRATES'), $link)));

		$tab->addChild($taxes);
	}

	/**
	 * Save ZOOcart Type config options
	 * 
	 * @param AppEvent $event The event triggered
	 */
	public static function beforeTypeSave($event) {
		$type = $event->getSubject();
		$type->config->set('zoocart', JRequest::getVar('zoocart'));
	}

	/**
	 * Add ZOOcart Type config form
	 * 
	 * @param AppEvent $event The event triggered
	 */
	public function addTypeConfig($event)
	{
		$html = $event['html'];
		$type = $event->getSubject();
		$group = $type->getApplication()->getGroup();

		// set fields form
		$price_list = $this->app->zoocart->getElementsList($group, $type->identifier, 'pricepro', 'zoocart[price_element]', $type->config->find('zoocart.price_element', ''));
		$quantity_list = $this->app->zoocart->getElementsList($group, $type->identifier, 'quantity', 'zoocart[quantity_element]', $type->config->find('zoocart.quantity_element', ''));

		// Check if generated lists contains no options:
		$empty_list = !preg_match('~<option~Uim',$price_list) || !preg_match('~<option~Uim',$quantity_list);

		if($empty_list)
		{
			$this->joomla->enqueueMessage(JText::_('PLG_ZOOCART_WARNING_NO_ELEMENTS'),'notice');
		}
		else
		{
			ob_start();
			$view = new AppView();
			$view->app = $this->app;
			$view->lists['price_element'] = $price_list;
			$view->lists['quantity_element'] = $quantity_list;
			$view->addTemplatePath($this->app->path->path('zoocart:views/admin/partials'));
			$view->setLayout('typeconfig')->display();
			$content = ob_get_contents();
			ob_end_clean();

			$pre_html = substr($html, 0, stripos($html, '</fieldset>') + strlen('</fieldset>'));
			$post_html = substr($html, stripos($html, '</fieldset>') + strlen('</fieldset>') + 1);

			$html = $pre_html . $content . $post_html;

			$event['html'] = $html;
		}
	}

	/*
		Function: initTypeLayouts
			Callback function for the zoo layouts

		Returns:
			void
	*/
	public function initTypeLayouts($event) 
	{
		$extensions = (array) $event->getReturnValue();
		
		// clean all ZOOfilter layout references
		$newextensions = array();
		foreach ($extensions as $ext) {
			if (strtolower($ext['name']) != 'zoocart') {
				$newextensions[] = $ext;
			}
		}
		
		// add new ones
		$newextensions[] = array('name' => 'ZOOcart', 'path' => $this->app->path->path('zoocart:'), 'type' => 'plugin');
		
		$event->setReturnValue($newextensions);
	}

	/** 
	 * New method for adding params to the element
	 * @since 2.5
	 */
	public function addElementConfig($event)
	{
		// apply only on Address view
		if ($this->app->request->getString('controller') != 'addresses') return;
		
		// Custom Params File
		$file = $this->app->path->path( 'zoocart:params/params.xml');
		$xml = simplexml_load_file( $file );

		// Old params
		$params = $event->getReturnValue();

		// add new params from custom params file if type = address
		$element = $event->getSubject();
		$type = $element->getType();

		$params[] = $xml->asXML();

		$event->setReturnValue($params);
	}

	/**
	 * Check the language before displaying the element
	 */
	public function beforeAddressDisplay($event)
	{
		$item 	 = $event->getSubject();
		$element = $event['element'];
		$config  = $element->config;
		$dparams = $this->app->data->create($event['params']);
			
		$label = $dparams->get('language', $config->get('language'));
		$language = JFactory::getLanguage()->get('tag');

		if (@$label[$language]) {
			$params = $event['params'];
			$params['altlabel'] = @$label[$language];
			$event['params'] = $params;
		}
	}

	/**
	 *  checkDependencies
	 */
	public function checkDependencies()
	{
		if($this->joomla->isAdmin())
		{
			// if ZLFW not enabled
			if(!JPluginHelper::isEnabled('system', 'zlframework') || !JComponentHelper::getComponent('com_zoo', true)->enabled) {
				$this->joomla->enqueueMessage(JText::_('PLG_ZOOCART_MISSING_DEPENDENCIES'), 'notice');
			} else {
				// load zoo
				require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

				// fix plugins ordering
				$this->app = App::getInstance('zoo');
				$this->app->loader->register('ZlfwHelper', 'plugins:system/zlframework/zlframework/helpers/zlfwhelper.php');
				$this->app->zlfw->checkPluginOrder($this->ext_name);
			}
		}
	}

}