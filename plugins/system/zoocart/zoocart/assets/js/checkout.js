(function($){

    var Plugin = function(){};

    $.extend(Plugin.prototype, {

        name: 'Checkout',

		options: {
			checkoutToggle: '.button-checkout',
			checkoutContainer: '#zoocart-checkout',
			loginToggle: '#zoocart-login',
			logoutToggle: '#zoocart-logout',
			registerToggle: '#zoocart-register',
			saveToggle: '.zoocart-save-order',
			cancelToggle: '.zoocart-cancel-checkout',
			loginForm: '#zoocart-login-form',
			registerForm: '#zoocart-register-form',
			orderIdField: '#zoocart-order-id',
			billingAddressForm: '#zoocart-billing-address-form',
			shippingAddressForm: '#zoocart-billing-address-form',
			selectAddressToggle: '.change-address',
			chooseAddressToggle: '.choose-address',
			editAddressToggle: '.edit-address',
			sameAsBilling: '.same-as-billing input[type="checkbox"]',
			loginUrl: '',
			logoutUrl: '',
			registerUrl: '',
			saveOrderUrl: '',
			validateAllUrl:'',
			validateAddressUrl: '',
			validateUserUrl: '',
			validateTaxesUrl: '',
			validateQuantitiesUrl: '',
			validatePaymentUrl: '',
			validateShippingUrl: '',
			saveOrderUrl: '',
			taxesAddressType: 'billing',
			terms: "#terms",
			termsText: '',
			payUrl: ''
		},

        initialize : function(element, options){
			this.options = $.extend({}, this.options, options);

			var $this = this;

			// Slider for checkout
			$(this.options.checkoutToggle, $(element)).click(function(){
				$($this.options.checkoutContainer).toggle('slow', function(){
					$('#zoocart-checkout' ).removeClass('hidden');
					$('#zoocart-checkout .row-fluid').each(function(){
						var highestCol = 0;
						$('.span6', $(this)).each(function(){
							highestCol = Math.max(highestCol, $(this).outerHeight());
						});
						$('.span6', $(this)).each(function(){
							$(this).css({'min-height': highestCol + 'px'});
						});
					});
				});
				$(this).parent().toggle('slow');
			});

			// Slider for cancel
			$(this.options.cancelToggle, $(element)).click(function(){
				$($this.options.checkoutContainer).toggle('slow');
				$($this.options.checkoutToggle, $(element)).parent().toggle('slow');
			});

			// Login Button
			$(this.options.loginToggle).click(function(){
				$this.login(element);
			});
			// Logout button
			$(this.options.logoutToggle).click(function(){
				$this.logout(element);
			});
			// Register button
			$(this.options.registerToggle).click(function(){
				$this.register(element);
			});
			// Validate & Save the Order
			$(this.options.saveToggle).click(function(){
				$this.validateOrder(element);
			});
			// Show other addresses
			$(this.options.selectAddressToggle, $(element)).click(function(){
				$(this).parent().find('.other-addresses').toggle('slow');
			});
			// Choose another address
			$(this.options.chooseAddressToggle, $(element)).click(function(){
				var id = $(this).parent().attr('data-address-id');
				var form = $($this.options.billingAddressForm);
				var div = form.parent().find('.address');
				div.html($(this).parent().html());
				div.find($($this.options.chooseAddressToggle)).remove();
				div.find($($this.options.editAddressToggle)).remove();
				form.find('[name="id"]').val(id);
				$(this).parents('.other-addresses').toggle('slow');
			});

			// Add validation to addresses
			this.bindValidate(element);

			// Same as billing
			$(this.options.sameAsBilling, $(element)).click(function(){
				if ($(this).is(':checked')) {
					
					var b_form = $('#zoocart-billing-address-form', $(element));
					var s_form = $('#zoocart-shipping-address-form', $(element));	

					// Clone values	and disable fields
					$('[name*="elements"]', b_form).each(function(){
						var input = $('[name="'+$(this).attr('name')+'"]', s_form);
						input.val($(this).val());
						input.attr('disabled', 'disabled');
					});
				} else {
					// Clone values	and disable fields
					$('[name*="elements"]', b_form).each(function(){
						var input = $('[name="'+$(this).attr('name')+'"]', s_form);
						input.attr('disabled', null);
					});
				}
			});

			// Validate Quantities
			$('.zoocart-cart [name*="quantity"]', $(element)).change(function(){
				var value = $(this).val();
  				// if it's not a number
  				if(!(!isNaN(parseFloat(value)) && isFinite(value)) || value <= 0){
  					$(this).val(1);
  				}

  				$this.validateQuantities(element);

			});

			$('#zoocart-choosepayment-form [name="payment_method"]').change(function(){
				$.when($this.validatePayment(element, {payment_method: $(this).val()})).done(function(data){
					$this.validateQuantities(element);
				});
			});

			$('#zoocart-chooseshipping-form [name="shipping_method"]').change(function(){
				$.when($this.validateShipping(element, {shipping_plugin: $(this).attr('data-shipping-plugin'), shipping_method: $(this).val()})).done(function(data){
					$this.validateQuantities(element);
				});
			})

			// Remove an item from the cart
			$('.zoocart-cart .remove a', $(element)).click(function(){
				var el = $(this).parents('tr');
				var id = el.attr('data-cartitem-id');

				$.when($this.removeItem(element, id)).then(el.remove());
			});

		},

		login: function(element) {

			var $this = this;

			$.ajax({
				url: $this.options.loginUrl,
				dataType: 'json',
				type: 'POST',
				data: {
					username : $($this.options.loginForm + ' input[name=username]').val(),
					password : $($this.options.loginForm + ' input[name=password]').val(),
					remember : ($($this.options.loginForm + ' [name=remember]:checked').length > 0)
				},
				success: function(data) {
					if(data === true) {
						window.location.reload(true);
					} else {
						// Deal with failed auth
						alert('WRONG!');
					}
				},
				error: function() {
					alert('WRONG!');
				}
			});
		},

		logout: function(element) {

			var $this = this;
			$.ajax({
				url: $this.options.logoutUrl,
				dataType: 'json',
				type: 'POST',
				success: function(data) {
					if(data) {
						window.location.reload(true);
					}
				}
			});
		},

		register: function(element) {	

			var $this = this;

			$.ajax({
				url: $this.options.registerUrl,
				dataType: 'json',
				type: 'POST',
				data: {
					username : $($this.options.registerForm + ' input[name=username]').val(),
					password : $($this.options.registerForm + ' input[name=password]').val(),
					password2 : $($this.options.registerForm + ' input[name=password2]').val(),
					email : $($this.options.registerForm + ' input[name=email]').val(),
					name : $($this.options.registerForm + ' input[name=name]').val()
				},
				success: function(data) {
					if(data.success) {
						window.location.reload(true);
					} else {
						// Deal with failed auth
						alert('WRONG!');
					}
				},
				error: function() {
					alert('WRONG!');
				}
			});
		},

		validateOrder: function(element) {
			var $this = this;

			/* The new validation behavior: */

			$this.validateAll(element);
			return;

			/* If you need to rollback to old (cascade) validation behavior - just remve previous two lines */

			$($this.options.saveToggle).button('loading');

			var billing_form = $('#zoocart-billing-address-form', $(element));			
			var billing_data = billing_form.serialize();

			var shipping_form = $('#zoocart-shipping-address-form', $(element));
			var shipping_data = shipping_form.serialize();

			var login_form = $('#zoocart-login-form', $(element));
			var login_data = login_form.serialize();

			var register_form = $('#zoocart-register-form', $(element));
			var register_data = register_form.serialize();

			var payment_form = $('#zoocart-choosepayment-form', $(element));
			var payment_data = payment_form.serialize();

			var sp_form = $('#zoocart-chooseshipping-form', $(element));
			var sp_data = sp_form.serialize();
			var sp_plugin =  $('[name="shipping_method"]:checked', sp_form).attr('data-shipping-plugin');
			sp_data = sp_data + '&shipping_plugin='+sp_plugin;

			var tax_data = billing_data;
			if($this.options.taxesAddressType == 'shipping') {
				tax_data = shipping_data;
			}

			$.when($this.validateAddress(element, billing_form, billing_data, 'billing')).then(function(data1){
				$.when($this.validateAddress(element, shipping_form, shipping_data, 'shipping')).then(function(data2){
					$.when($this.validateTaxes(element, tax_data, $this.options.taxesAddressType)).then(function(data3){
						$.when($this.validateUser(element, login_form, register_form, login_data, register_data)).then(function(data4){
							$.when($this.validatePayment(element, payment_data)).then(function(data5){
								$.when($this.validateShipping(element, sp_data)).then(function(data6){
									$.when($this.validateTerms()).then(function(data7){
										if(data1.success && data2.success && data3.success && data4.success && data5.success && data6.success && data7.success){
											$('#zoocart-errors').alert('close');
											var order_data = {};
											var b_data = billing_form.serializeArray();
											var s_data = shipping_form.serializeArray();
											var p_data = payment_form.serializeArray();
											var sp_data = sp_form.serializeArray();
											order_data.billing_data = {};
											$.each(b_data, function(k, v){
												name = v.name.replace(/elements/g, 'billing');
												order_data.billing_data[name] = v.value;
											});
											order_data.shipping_data = {};
											$.each(s_data, function(k, v){
												name = v.name.replace(/elements/g, 'shipping');
												order_data.shipping_data[name] = v.value;
											});
											order_data.payment_data = {};
											$.each(p_data, function(k, v){
												order_data.payment_data[v.name] = v.value;
											});
											order_data.shipping_rate_data = {
												shipping_plugin: sp_plugin
											};
											$.each(sp_data, function(k, v){
												order_data.shipping_rate_data[v.name] = v.value;
											});
											$.when($this.saveOrder(element, order_data)).then(function() { $($this.options.saveToggle).button('confirmed'); $($this.options.saveToggle).attr('disabled', 'disabled');});
										} else {
											$($this.options.saveToggle).button('reset'); 

											var errors = $.extend(data1.errors, data2.errors, data3.errors, data4.errors, data5.errors, data6.errors, data7.errors);
											$('#zoocart-errors').alert();
											$('#zoocart-errors').show();
											$('#zoocart-errors').bind('closed', function () {
											  	$('#zoocart-errors').hide();
											})
											$('#zoocart-errors .close').bind('click', function(){
												$('#zoocart-errors').hide();
												return false; 
											});
											$('#zoocart-errors').addClass('in');
											$('#zoocart-errors .errors').html('');
											$.each(errors, function(){
												$('#zoocart-errors .errors').append('<li>' + this +'</li>');
											});
											var offset = $('#zoocart-errors').offset();
											window.scrollTo(offset.left, offset.top);
										}
									});
								});
							});
						});
					});
				});
			});
		},

		saveOrder: function(element, data) {

			if(data.billing_data.id){
				data.billing_data.billing_id = data.billing_data.id;
			}
			if(data.shipping_data.id){
				data.shipping_data.shipping_id = data.shipping_data.id;
			}

			var notes = data.notes;

			var data = $.extend(data.billing_data, data.shipping_data, data.payment_data, data.shipping_rate_data);
			data.notes = notes;
			var $this = this;

			return $.ajax({
				url: this.options.saveOrderUrl,
				data: data, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					if(data.success) {
						window.location.href = $this.options.payUrl + '&id='+data.id;
						return;
					}
				}
			});
		},

		bindValidate: function(element) {
			var $this = this;
			
			// Validate Billing Address
			var form = $('#zoocart-billing-address-form', $(element));		
			$('[name*="elements"]', form).change(function(){
				var form = $('#zoocart-billing-address-form', $(element));
				$($this.options.saveToggle).button('loading');
				var billing_data = form.serialize();
				$.when(
					$this.validateAddress(element, form, billing_data, 'billing'),
					$this.validateTaxes(element, billing_data, 'billing')
				).then(function(data1, data2){ $($this.options.saveToggle).button('reset'); });

				// same as billing?
				if ($($this.options.sameAsBilling, $(element)).is(':checked')) {
					var s_form = $('#zoocart-shipping-address-form', $(element));
					var input = $('[name="'+$(this).attr('name')+'"]', s_form);
					input.val($(this).val());
					input.attr('disabled', 'disabled');
				}
			});

			// Validate Shipping Address
			var form = $('#zoocart-shipping-address-form', $(element));
			$('[name*="elements"]', form).change(function(){
				var form = $('#zoocart-shipping-address-form', $(element));
				$($this.options.saveToggle).button('loading');
				var shipping_data = form.serialize();
				$.when(
					$this.validateAddress(element, form, shipping_data, 'shipping'),
					$this.validateTaxes(element, shipping_data, 'shipping')
				).then(function(data1){ $($this.options.saveToggle).button('reset'); });
			});
		},

		showSpinner: function(element) {
			$('.spinner-validating', element).show();
			return $('.spinner', element).Spin({
				width: 2,
				length: 5,
				radius: 3,
				speed: 1.5,
				top: 0
			});
		},

		hideSpinner: function(element, spinner) {
			$('.spinner-validating', element).hide();
			spinner.stop();
		},

		validateUser: function(element, form_login, form_register, login_data, register_data) {

			var $this = this;

			if(login_data.length || register_data.length){
				var spinner_login = $this.showSpinner($('.login', $(element)));
				var spinner_register = $this.showSpinner($('.register', $(element)));
			}

			var login_data = form_login.serializeArray();
			var register_data = form_register.serializeArray();

			l_data = {};
			for (var i in login_data) {
				l_data[login_data[i].name] = login_data[i].value;
			}
			r_data = {};
			for (var i in register_data) {
				r_data[register_data[i].name] = register_data[i].value;
			}

			return $.ajax({
				url: this.options.validateUserUrl,
				data: {
					logindata: l_data,
					registerdata: r_data 
				}, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					if(login_data.length || register_data.length){
						$this.hideSpinner(element, spinner_login);
						$this.hideSpinner(element, spinner_register);
					}

					$.each($('input', form_login), function(){
						var input = $(this);
						input.closest('.control-group').removeClass('error').removeClass('warning');
						$('.help-block', input.parent()).html('');
					});

					$.each($('input', form_register), function(){
						var input = $(this);
						input.closest('.control-group').removeClass('error').removeClass('warning');
						$('.help-block', input.parent()).html('');
					});

					if(!data.success){
						$.each(data.errors, function(k, v){
							if(k == 'login') {
								var inputs = $('input', form_login);
								$.each(inputs, function(input){ 
									input.closest('.control-group').addClass('error');
									$('.help-block', input.parent()).html(v);
								});
							} else {
								var input = $('[name="'+k+'"]', form_register);
								input.closest('.control-group').addClass('error');
								$('.help-block', input.parent()).html(v);
							}
							
						});
					}
				}
			});
		},

		validateTerms: function() {
			var data = {
				success: false
			};

			var checkbox = $(this.options.terms);
			if (!checkbox.length) {
				data.success = true;
				return data;
			}
			checkbox.click(function(){
				
			});
			if (checkbox.is(':checked')) {
				data.success = true;
				return data;
			} 

			data.errors = [this.options.termsText];

			return data;
		},

		validatePayment: function(element, payment_data) {

			var $this = this;

			var spinner = $this.showSpinner($('.payments', $(element)));			
			
			return $.ajax({
				url: this.options.validatePaymentUrl,
				data: payment_data,
				dataType:'json',
				type: 'POST',
				success: function(data) {
					if (data.fee) {
						$('#zoocart-cart-payment', $(element)).html(data.fee);
					}
					$this.hideSpinner(element, spinner);
				}
			});
		},

		validateShipping: function(element, shipping_data) {

			var $this = this;

			var spinner = $this.showSpinner($('.shippings', $(element)));			
			
			return $.ajax({
				url: this.options.validateShippingUrl,
				data: shipping_data,
				dataType:'json',
				type: 'POST',
				success: function(data) {
					if (data.fee) {
						$('#zoocart-cart-shipping', $(element)).html(data.fee);
					}
					$this.hideSpinner(element, spinner);
				}
			});
		},


		validateTaxes: function(element, data, type) {

			if(!this.options.taxesAddressType == type) {
				return true;
			}

			var $this = this;
			var spinner = $this.showSpinner($('.zoocart-cart .taxes', $(element)));
			var spinner2 = $this.showSpinner($('.zoocart-cart .total', $(element)));
			var spinner3 = $this.showSpinner($('.zoocart-cart .subtotal', $(element)));

			var payment = $('#zoocart-choosepayment-form [name="payment_method"]:checked').val();
			var shipping_method = $('#zoocart-chooseshipping-form [name="shipping_method"]:checked').val();
			var shipping_plugin = $('#zoocart-chooseshipping-form [name="shipping_method"]:checked').attr('data-shipping-plugin');
			data = data + '&payment_method=' + payment + '&shipping_plugin=' + shipping_plugin + '&shipping_method=' + shipping_method;

			return $.ajax({
				url: this.options.validateTaxesUrl,
				data: data, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					if(data.taxes){
						$('#zoocart-cart-taxes', $(element)).html(data.taxes);
					}
					if(data.total){
						$('#zoocart-cart-total', $(element)).html(data.total);
					}
					if(data.subtotal){
						$('#zoocart-cart-subtotal', $(element)).html(data.subtotal);
					}
					$this.hideSpinner(element, spinner);
					$this.hideSpinner(element, spinner2);
					$this.hideSpinner(element, spinner3);
				}
			});
		},

		validateQuantities: function(element) {

			var $this = this;
			var form = $('#zoocart-cart', $(element));
			var data = form.serialize();

			var form = $('#zoocart-billing-address-form', $(element));
			var billing_data = form.serialize();

			return $.ajax({
				url: this.options.validateQuantitiesUrl,
				data: data, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					if(data.totals){
						$.each(data.totals, function(k,v){
							$('#zoocart-cart tr[data-cartitem-id="'+k+'"] .price-total', $(element)).html(v);
						});
					}	
					$this.validateTaxes(element, billing_data);
				}
			});
		},

		removeItem: function(element, id) {

			var $this = this;

			var form = $('#zoocart-billing-address-form', $(element));
			var billing_data = form.serialize();

			return $.ajax({
				url: this.options.removeItemUrl,
				data: {
					id: id
				}, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					// no more items, reload the page
					if($('#zoocart-cart tbody tr').length == 0) {
						window.location.reload(true);
					} else {
						$this.validateTaxes(element, billing_data);
					}
				}
			});
		},


		validateAddress: function(element, form, address_data, type) {
			var $this = this;
			var data = address_data + '&type=' + type;

			var spinner = $this.showSpinner($('.' + type + '-details', $(element)));
	
			return $.ajax({
				url: this.options.validateAddressUrl,
				data: data, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					$.each($('[name*="elements"]', form), function(){
						var input = $(this);
						input.closest('.control-group').removeClass('error').removeClass('warning');
						$('.help-block', input.parent()).html('');
					});

					$.each(data.notices, function(k, v){
						var input = $('[name*="elements\\['+k+'\\]"]', form);
						input.closest('.control-group').addClass('warning');
						$('.help-block', input.parent()).html(v);
					});

					if(!data.success){
						$.each(data.errors, function(k, v){
							var input = $('[name*="elements\\['+k+'\\]"]', form);
							input.closest('.control-group').addClass('error');
							$('.help-block', input.parent()).html(v);
						});
					}

					$this.hideSpinner(element, spinner);
				}
			});
		},

	    // Process order
	    _processOrder:function(element)
	    {
		    var $this = this;

		    $($this.options.saveToggle).button('loading');

		    var billing_form = $('#zoocart-billing-address-form', $(element));
		    var billing_data = billing_form.serialize();

		    var shipping_form = $('#zoocart-shipping-address-form', $(element));
		    var shipping_data = shipping_form.serialize();

		    var login_form = $('#zoocart-login-form', $(element));
		    var login_data = login_form.serialize();

		    var register_form = $('#zoocart-register-form', $(element));
		    var register_data = register_form.serialize();

		    var payment_form = $('#zoocart-choosepayment-form', $(element));
		    var payment_data = payment_form.serialize();

		    var sp_form = $('#zoocart-chooseshipping-form', $(element));
		    var sp_data = sp_form.serialize();
		    var sp_plugin =  $('[name="shipping_method"]:checked', sp_form).attr('data-shipping-plugin');
		    sp_data = sp_data + '&shipping_plugin='+sp_plugin;

		    var tax_data = billing_data;
		    if($this.options.taxesAddressType == 'shipping') {
			    tax_data = shipping_data;
		    }

		    //$('#zoocart-errors').alert('close');

		    var order_data = {};
		    var b_data = billing_form.serializeArray();
		    var s_data = shipping_form.serializeArray();
		    var p_data = payment_form.serializeArray();
		    var sp_data = sp_form.serializeArray();
		    order_data.billing_data = {};
		    $.each(b_data, function(k, v){
			    name = v.name.replace(/elements/g, 'billing');
			    order_data.billing_data[name] = v.value;
		    });
		    order_data.shipping_data = {};
		    $.each(s_data, function(k, v){
			    name = v.name.replace(/elements/g, 'shipping');
			    order_data.shipping_data[name] = v.value;
		    });
		    order_data.payment_data = {};
		    $.each(p_data, function(k, v){
			    order_data.payment_data[v.name] = v.value;
		    });
		    order_data.shipping_rate_data = {
			    shipping_plugin: sp_plugin
		    };
		    $.each(sp_data, function(k, v){
			    order_data.shipping_rate_data[v.name] = v.value;
		    });

		    order_data.notes = $('#notes').val();

		    $.when($this.saveOrder(element, order_data)).then(function() { $($this.options.saveToggle).button('confirmed'); $($this.options.saveToggle).attr('disabled', 'disabled');});
	    },

	    // Get addres form data and transform them to request string format:
	    _getAddressData:function(targetForm, type)
		{
			var data = '';
			var addrId = $('input[name="id"]');

			if(addrId.length && addrId.val())
			{
				data = type+'_addr_id='+parseInt(addrId.val());
			}else{
				var elements = targetForm.serialize();
				elements = elements.replace(/elements/g ,type+'_elements');
				data = elements;
			}
			return data;
		},

	    // Validate All function
	    validateAll: function(element){
		    var $this = this;

		    // Billing data
		    var billing_form = $('#zoocart-billing-address-form', $(element));
		    var billing_data = $this._getAddressData(billing_form,'bill');

		    // Shipping data
		    var shipping_form = $('#zoocart-shipping-address-form', $(element));
		    var shipping_data = $this._getAddressData(shipping_form,'ship');

		    // Shipping method data
		    var ship_method_form = $('#zoocart-chooseshipping-form', $(element));
		    var shipping_method = $('input[name="shipping_method"]:checked',ship_method_form);
		    var ship_method_data = 'shipping_method='+shipping_method.val()+'&shipping_plugin='+shipping_method.attr('data-shipping-plugin');

		    // Payment data
		    var payment_form = $('#zoocart-choosepayment-form', $(element));
		    var payment_data = payment_form.serialize();

		    // Quantities
		    var quantities_form = $('#zoocart-cart', $(element));
		    var quantities_data = quantities_form.serialize();

		    // User data validation:
		    var login_form = $('#zoocart-login-form', $(element));
		    var register_form = $('#zoocart-register-form', $(element));

		    var login_data = login_form.serializeArray();
		    var register_data = register_form.serializeArray();

		    var l_data = '';
		    for (var i in login_data) {
			    if(login_data[i].name && login_data[i].name!='Array')
			    {
			        if(l_data)
			            l_data += '&';
			        l_data += 'logindata['+login_data[i].name+']='+ login_data[i].value;

			    }
		    }
		    var r_data = '';
		    for (var i in register_data) {
			    if(register_data[i].name && register_data[i].name!='Array')
			    {
				    if(r_data)
					    r_data += '&';
				    r_data += 'registerdata['+register_data[i].name+']='+ register_data[i].value;

			    }
		    }

		    // Quantities
		    var form = $('#zoocart-cart', $(element));
		    var data = form.serialize();

		    var request = shipping_data+'&'+billing_data+'&'+ship_method_data+'&'+payment_data+'&'+quantities_data+'&'+l_data+'&'+r_data;

		    $.ajax({
			    url:$this.options.validateAllUrl,
			    type:'POST',
			    dataType:'json',
			    data:request,
			    success:function(response){
				    // Terms and conditions chack:
				    var termsValid = $this.validateTerms();

				    response.success = response.success && termsValid.success;
				    response.errors = $.extend(response.errors, termsValid.errors);

				    // Response processing
				    if(response.success && !response.errors.length)
				    {
						$this._processOrder(element);
				    }
				    else
				    {
					    // Errors displeying:
					    $($this.options.saveToggle).button('reset');

					    var errors = response.errors;
					    $('#zoocart-errors').alert();
					    $('#zoocart-errors').show();
					    $('#zoocart-errors').bind('closed', function () {
						    $('#zoocart-errors').hide();
					    })
					    $('#zoocart-errors .close').bind('click', function(){
						    $('#zoocart-errors').hide();
						    return false;
					    });
					    $('#zoocart-errors').addClass('in');
					    $('#zoocart-errors .errors').html('');
					    $.each(errors, function(){
						    $('#zoocart-errors .errors').append('<li>' + this +'</li>');
					    });
					    var offset = $('#zoocart-errors').offset();
					    window.scrollTo(offset.left, offset.top);
				    }
			    }
		    });
        }
			
    });

    // Don't touch
	$.fn[Plugin.prototype.name] = function() {

		var args   = arguments;
		var method = args[0] ? args[0] : null;

		return this.each(function() {
			var element = $(this);

			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();

				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}

				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}

		});
	};

})(jQuery);