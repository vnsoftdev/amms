<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Address {

	public $id;

	public $type;

	public $user_id;

	public $params;

	public $elements;

	public $default;

	public $app;

	protected $_elements;

 	/**
 	 * Class Constructor
 	 */
	public function __construct() {

		$this->app = App::getInstance('zoo');

		// decorate data as object
		$this->params = $this->app->parameter->create($this->params);

		// decorate data as object
		$this->elements = $this->app->data->create($this->elements);

	}

	public function __get($name){

		$billing_info = $this->app->zoocart->address->getBillingInfo();

		if(array_key_exists($name, $billing_info)){
			if($element = $this->getElement($billing_info[$name])) {
				$value = $element->get('value');
				if(!$value){
					$value = $element->get($name);
				}
				if($value){
					if(is_array($value)){
						$value = array_shift($value);
					}
					return $value;
				}
				return null;
			}
		}

		$properties = get_object_vars($this);
		if(array_key_exists($name,$properties))
			return $this->$name;

		return null;
	}

	/**
	 * Get the Application which the item belongs to
	 *
	 * @return Application The application
	 */
	public function getApplication() {
		return $this->app->zoo->getApplication();
	}

	/**
	 * Get an element object out of this item
	 *
	 * @param  string $identifier The element identifier
	 *
	 * @return Element             The element object
	 */
	public function getElement($identifier) {

		if (isset($this->_elements[$identifier])) {
			return $this->_elements[$identifier];
		};

		if ($element = $this->getType()->getElement($identifier)) {
			$element->setItem($this);
			$this->_elements[$identifier] = $element;
			return $element;
		}

		return null;
	}

	/**
	 * Get the list of elements
	 *
	 * @return array The element list
	 */
	public function getElements() {

		// get types elements
		if ($type = $this->getType()) {
			foreach ($type->getElements() as $element) {
				if (!isset($this->_elements[$element->identifier])) {
					$element->setItem($this);
					$this->_elements[$element->identifier] = $element;
				}
			}
			$this->_elements = $this->_elements ? $this->_elements : array();
		}

		return $this->_elements;
	}

	public function getParams() {
		return $this->params;
	}

	/**
	 * Get the item Type
	 *
	 * @return Type The Address Type
	 */
	public function getType() {
		$type = $this->app->zoocart->address->getAddressType();

		return $type;
	}

	/**
	 * Bind the Item Data
	 */
	public function bind($data, $skip_id = false) {
		$data = (array)$data;

		// the app object must be ignored
		unset($data['app']);

		foreach($data as $k => $v){
			if(isset($this->$k) && !($skip_id && $k == 'id')) {
				$this->$k = $v;
			}
		}

		// bind element data
		$this->elements = $this->app->data->create($this->elements);
		foreach ($this->getElements() as $id => $element) {
			if (isset($this->elements[$id])) {
				$element->bindData($this->elements[$id]);
			} else {
				$element->bindData();
			}
		}
	}
}

class AddressException extends AppException {}