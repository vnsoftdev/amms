<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$zoo = App::getInstance('zoo');
$zoo->loader->register('AppPagination', 'component.admin:framework/classes/pagination.php');

class BootstrapPagination extends AppPagination {

    public function render($url = 'index.php', $layout = null) {

		$html = '';

		// check if show all
		if ($this->_showall) {
			return $html;
		}

		// check if current page is valid
        if ($this->_current > $this->_pages) {
            $this->_current = $this->_pages;
		}

		if ($this->_pages > 1) {

			$html .= '<ul>';

			$range_start = max($this->_current - $this->_range, 1);
			$range_end   = min($this->_current + $this->_range - 1, $this->_pages);

            if ($this->_current > 1) {
				$link  = $url;
                $html .= '<li><a class="start" href="'.JRoute::_($link).'">&lt;&lt;</a></li>';
				$link  = $this->_current - 1 == 1 ? $url : $this->link($url, $this->_name.'='.($this->_current - 1));
				$html .= '<li><a class="previous" href="'.JRoute::_($link).'">&lt;</a></li>';
            }

            for ($i = $range_start; $i <= $range_end; $i++) {
                if ($i == $this->_current) {
	                $html .= '<li class="active disabled"><a>'.$i.'</a></li>';
                } else {
					$link  = $i == 1 ? $url : $this->link($url, $this->_name.'='.$i);
	                $html .= '<li><a href="'.JRoute::_($link).'">'.$i.'</a></li>';
                }
            }

            if ($this->_current < $this->_pages) {
				$link  = $this->link($url, $this->_name.'='.($this->_current + 1));
                $html .= '<li><a class="next" href="'.JRoute::_($link).'">&gt;&nbsp;</a></li>';
				$link  = $this->link($url, $this->_name.'='.($this->_pages));
                $html .= '<li><a class="end" href="'.JRoute::_($link).'">&gt;&gt;&nbsp;</a></li>';
            }

            $html .= '</ul>';

		}

        return $html;
    }

}