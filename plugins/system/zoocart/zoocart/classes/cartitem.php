<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Cartitem {

	public $id;
	
	public $user_id;
	
	public $session_id;
	
	public $item_id;

	public $quantity;

	public $params;

	public $modified_on;

	protected $_item;

	public function getNetTotal() {
		$zoo = App::getInstance('zoo');
		$item = $zoo->table->item->get($this->item_id);
		$price = $zoo->zoocart->price->getItemNetPrice($item);

		return $price * $this->quantity;
	}

	public function getGrossTotal($user_id = null) {
		$zoo = App::getInstance('zoo');
		$item = $zoo->table->item->get($this->item_id);
		$price = $zoo->zoocart->price->getItemGrossPrice($item, $user_id);

		return $price * $this->quantity;
	}

	public function getItem() {
		$app = App::getInstance('zoo');
		if (!$this->_item) {
			$this->_item = $app->table->item->get($this->item_id);
		}

		return $this->_item;
	}

}