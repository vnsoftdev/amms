<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Order {

	public $id;
	
	public $user_id;
	
	public $net;
	
	public $tax;

	public $shipping;

	public $payment;

	public $discount;

	public $shipping_tax;

	public $payment_tax;

	public $subtotal;

	public $tax_total;

	public $total;

	public $params;

	public $created_on;

	public $modified_on;

	public $notes;

	public $ip;

	public $shipping_address;

	public $billing_address;

	public $payment_method;

	public $shipping_method;

	public $state;

	protected $_state = null;

	protected $_billing_address = null;

	protected $_shipping_address = null;	

	protected $_items = null;

	protected $_payments = null;

	public function getSubtotal($force = false) {

		if (!$this->subtotal || $force) {
			$this->subtotal = $this->net + $this->shipping + $this->payment - $this->discount;
		}

		return $this->subtotal;
	}

	public function getPaymentTax($force = false) {

		$app = App::getInstance('zoo');
		if (!$this->payment_tax || $force) {
			$this->payment_tax = $app->zoocart->tax->getTaxes(null, $this->payment, null, $this->getTaxAddress());
		}

		return $this->payment_tax;
	}

	public function getShippingTax($force = false) {

		$app = App::getInstance('zoo');
		if (!$this->shipping_tax || $force) {
			$this->shipping_tax = $app->zoocart->tax->getTaxes(null, $this->shipping, null, $this->getTaxAddress());
		}

		return $this->shipping_tax;
	}

	public function getTaxTotal($force = false) {
		$app = App::getInstance('zoo');

		if (!$this->tax_total || $force) {
				$this->tax_total = $this->tax + $this->getShippingTax($force) + $this->getPaymentTax($force);
		}

		return $this->tax_total;
	}

	public function getTotal($force = false) {

		if (!$this->total || $force) {
			$this->total = $this->getSubtotal() + $this->getTaxTotal($force);
		}

		return $this->total;
	}

	public function getCurrency() {
		$app = App::getInstance('zoo');
		return $app->getConfig()->get('default_curency', 'EUR');
	}

	public function getState() {
		if(!$this->_state) {
			$app = App::getInstance('zoo');
			$this->_state = $app->zoocart->table->orderstates->get($this->state);
		}
		return $this->_state;
	}

	public function getBillingAddress() {

		if(!$this->_billing_address) {
			$this->_billing_address = $this->getAddress('billing');
		}
		return $this->_billing_address;
	}

	public function getShippingAddress() {
		if(!$this->_shipping_address) {
			$this->_shipping_address = $this->getAddress('shipping');
		}
		return $this->_shipping_address;
	}

	public function getTaxAddress() {
		$app = App::getInstance('zoo');
		$billing_type = $app->zoocart->getConfig()->get('billing_address_type', 'billing');

		if (strtolower($billing_type == 'shipping')) {
			return $this->getShippingAddress();
		}

		return $this->getBillingAddress();
	}

	protected function getAddress($type = 'billing') {
		$app = App::getInstance('zoo');
		$address = $app->object->create('Address');
		$address->type = $type;
		$key = $type . '_address';
		$data = json_decode($this->$key, true);

		$address->bind($data, true);

		return $address;
	}

	public function getItems() {
		$app = App::getInstance('zoo');
		if (!$this->_items) {
			$this->_items = $app->zoocart->table->orderitems->getByOrder($this->id);
		}

		return $this->_items;
	}

	public function getPayments() {
		$app = App::getInstance('zoo');
		if (!$this->_payments) {
			$this->_payments = $app->zoocart->table->payments->getByOrder($this->id);
		}

		return $this->_payments;
	}

	public function getShippingMethod() {
		$shipping = json_decode($this->shipping_method, true);
		return $shipping ? $shipping : array();
	}

}