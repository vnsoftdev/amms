<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Orderhistory {

	public $id;
	
	public $order_id;
	
	public $state;
	
	public $date;

	public $user_id;

	protected $_state = null;

	public function getState() {

		if (!$this->_state) {
			$zoo = App::getInstance('zoo');
			$this->_state = $zoo->zoocart->table->orderstates->get($this->state);
		}

		return $this->_state;
	}
}