<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Orderitem {

	public $id;
	
	public $order_id;
	
	public $item_id;
	
	public $name;

	public $quantity;

	public $elements;

	public $price;

	public $tax;

	public $total;

	public $params;

	protected $_item = null;

	public function getTax($address) {

		$app = App::getInstance('zoo');

		if (!$this->tax) {
			if($app->zoocart->tax->checkTaxEnabled()){
				$this->tax = $app->zoocart->tax->getTaxes($app->table->item->get($this->item_id)->application_id, $this->getPrice(), $app->user->get()->id, $address) * $this->quantity;
			}else{
				$this->tax = 0;
			}
		}

		return $this->tax;
	}

	public function getTotal($address) {

		if (!$this->total) {
			$this->total = $this->getTax($address) + ($this->quantity * $this->getPrice());
		}

		return $this->total;
	}

	public function getPrice() {

		$app = App::getInstance('zoo');

		if (!$this->price) {
			$this->price = $app->zoocart->price->getItemNetPrice($app->table->item->get($this->item_id));
		}

		return $this->price;
	}

	public function getItem() {
		$app = App::getInstance('zoo');
		if (!$this->_item) {
			$this->_item = $app->table->item->get($this->item_id);
		}

		return $this->_item;
	}

}