<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class AddressesController extends AdminResourceController {

	public $group;
	public $application;

	public function __construct($default = array()) {

		$this->resource_name = 'addresses';

		$this->resource_class = 'Address';

		parent::__construct($default);

		$this->registerDefaultTask('types');

		// get types
		$this->address = $this->app->zoocart->address->getAddressType($this->application);
		
		// register tasks
		$this->registerTask('applyelements', 'saveelements');
		$this->registerTask('applyassignelements', 'saveassignelements');
		$this->registerTask('applysubmission', 'savesubmission');
	}

	public function types() {

		// set toolbar items
		$this->app->system->application->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPE'));
		if ($this->app->joomla->isVersion('2.5')) {
			$this->app->toolbar->editListX('editElements');
		} else {
			$this->app->toolbar->editList('editElements');
		}

		// get templates
		$this->templates = $this->application->getTemplates();

		// get extensions / trigger layout init event
		$this->extensions = $this->app->event->dispatcher->notify($this->app->event->create($this->app, 'layout:init'))->getReturnValue();

		// display view
		$this->getView()->setLayout('addresstypes')->display();
	}

	public function editElements() {

		// disable menu
		$this->app->request->setVar('hidemainmenu', 1);

		// get request vars
		$cid = $this->app->request->get('cid.0', 'string', '');

		// get type
		$this->type = $this->address;

		// set toolbar items
		$this->app->system->application->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPE').': '.$this->type->name.' <small><small>[ '.JText::_('PLG_ZOOCART_EDIT_ELEMENTS').' ]</small></small>');
		$this->app->toolbar->save('saveelements');
		$this->app->toolbar->apply('applyelements');
		$this->app->toolbar->cancel('types', 'Close');

		// sort elements by group
		$this->elements = array();
		foreach ($this->app->element->getAll($this->application) as $element) {
			$this->elements[$element->getGroup()][$element->getElementType()] = $element;
		}
		ksort($this->elements);
		foreach ($this->elements as $group => $elements) {
			ksort($elements);
			$this->elements[$group] = $elements;
		}

		// display view
		$this->getView()->setLayout('editelements')->display();
	}

	public function addElement() {

		// get request vars
		$element = $this->app->request->getWord('element', 'text');

		// load element
		$this->element = $this->app->element->create($element, $this->application);
		$this->element->identifier = $this->app->utility->generateUUID();

		// display view
		$this->getView()->setLayout('addElement')->display();
	}

	public function saveElements() {

		// check for request forgeries
		$this->app->request->checkToken() or jexit('Invalid Token');

		// init vars
		$post = $this->app->request->get('post:', 'array', array());
		$cid  = $this->app->request->get('cid.0', 'string', '');

		try {

			// save types elements
			$type = $this->address;
			$type->bindElements($post);

			if ($cid == $this->app->zoocart->getConfig()->get('billing_address_type', 'billing') && !$this->checkAddressType($type)) {
				$this->app->error->raiseNotice(0, JText::_('PLG_ZOOCART_NOTICE_ADDRESS_TYPE_MUST_HAVE_COUNTRY_ELEM'));
				$this->_task = 'applyelements';
				$msg = null;
			} else {
				$this->saveAddressType($type);
				$msg = JText::_('PLG_ZOOCART_CONFIG_ADDRESS_ELEMENTS_SAVED');	
			}

		} catch (AppException $e) {

			$this->app->error->raiseNotice(0, JText::sprintf('PLG_ZOOCART_ERROR_SAVING_ADDRESS_ELEMENTS', $e));
			$this->_task = 'applyelements';
			$msg = null;

		}

		switch ($this->getTask()) {
			case 'applyelements':
				$link = $this->baseurl.'&task=editelements&cid[]='.$type->id;
				break;
			case 'saveelements':
			default:
				$link = $this->baseurl.'&task=types';
				break;
		}

		$this->setRedirect($link, $msg);
	}

	protected function checkAddressType($type) {
		
		$elements = $type->getElements();
		$count = 0;
		foreach($elements as $element) {
			if($element->getElementType() == 'country') {
				$count++;
			}
		}

		if ($count != 1) {
			return false;
		}

		return true;
	}

	protected function saveAddressType($type) {

		// save config file
		if ($file = $type->getConfigFile()) {
			$config_string = (string) $type->config;
			if (!JFile::write($file, $config_string)) {
				throw new TypeException(JText::_('PLG_ZOOCART_ERROR_WRITING_ADDRESS_TYPE_CONFIG_FILE'));
			}
		}
	}

	public function assignElements() {

		// disable menu
		$this->app->request->setVar('hidemainmenu', 1);

		// init vars
		$type				 = $this->app->request->getString('type');
		$this->relative_path = urldecode($this->app->request->getVar('path'));
		$this->path			 = $this->relative_path ? JPATH_ROOT . '/' . $this->relative_path : '';
		$this->layout		 = $this->app->request->getString('layout');

		$dispatcher = JDispatcher::getInstance();
		if (strpos($this->relative_path, 'plugins') === 0) {
			@list($_, $plugin_type, $plugin_name) = explode('/', $this->relative_path);
			JPluginHelper::importPlugin($plugin_type, $plugin_name);
		}
		$dispatcher->trigger('registerZOOEvents');

		// get type
		$this->type = $this->address;

        if ($this->type) {
            // set toolbar items
            $this->app->system->application->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPE').': '.$this->type->name.' <small><small>[ '.JText::_('PLG_ZOOCART_ASSIGN_ELEMENTS').': '. $this->layout .' ]</small></small>');
            $this->app->toolbar->save('saveassignelements');
            $this->app->toolbar->apply('applyassignelements');
            $this->app->toolbar->cancel('types');

            // get renderer
            $renderer = $this->app->renderer->create('address')->addPath($this->path);

            // get positions and config
            $this->config = $renderer->getConfig('address')->get($this->application->getGroup().'.'.$this->application->id.'.'.$type.'.'.$this->layout);

            $prefix = 'address.';
            if ($renderer->pathExists('address'.DIRECTORY_SEPARATOR.$type)) {
                $prefix .= $type.'.';
            }
            $this->positions = $renderer->getPositions($prefix.$this->layout);

			// display view
			ob_start();
			$this->getView()->setLayout('assignelements')->display();
			$output = ob_get_contents();
			ob_end_clean();

			// trigger edit event
			$this->app->event->dispatcher->notify($this->app->event->create($this->type, 'addresstype:assignelements', array('html' => &$output)));

			echo $output;


        } else {

			$this->app->error->raiseNotice(0, JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_FIND_ADDRESS_TYPE', $type));
			$this->setRedirect($this->baseurl . '&task=types');

		}
	}

	public function saveAssignElements() {

		// check for request forgeries
		$this->app->request->checkToken() or jexit('Invalid Token');

		// init vars
		$type		   = $this->app->request->getString('type');
		$layout		   = $this->app->request->getString('layout');
		$relative_path = $this->app->request->getVar('path');
		$path		   = $relative_path ? JPATH_ROOT . '/' . urldecode($relative_path) : '';
		$positions	   = $this->app->request->getVar('positions', array(), 'post', 'array');

		// unset unassigned position
		unset($positions['unassigned']);

		// get renderer
		$renderer = $this->app->renderer->create('address')->addPath($path);

		// clean config
		$config = $renderer->getConfig('address');
		foreach ($config as $key => $value) {
			$parts = explode('.', $key);
			if ($parts[0] == $this->group && !$this->application->getType($parts[1])) {
				$config->remove($key);
			}
		}

		// save config
		$config->set($this->application->getGroup().'.'.$this->application->id.'.'.$type.'.'.$layout, $positions);
		$renderer->saveConfig($config, $path.'/renderer/address/positions.config');

		switch ($this->getTask()) {
			case 'applyassignelements':
				$link  = $this->baseurl.'&task=assignelements&type='.$type.'&layout='.$layout.'&path='.$relative_path;
				break;
			default:
				$link = $this->baseurl.'&task=types';
				break;
		}

		$this->setRedirect($link, JText::_('PLG_ZOOCART_ELEMENTS_ASSIGNED'));
	}

	public function assignSubmission() {

		// disable menu
		$this->app->request->setVar('hidemainmenu', 1);

		// init vars
		$type				 = $this->app->request->getString('type');
		$this->relative_path = urldecode($this->app->request->getVar('path'));
		$this->path			 = $this->relative_path ? JPATH_ROOT . '/' . $this->relative_path : '';
		$this->layout		 = $this->app->request->getString('layout');
		$this->template		 = $this->app->request->getString('template');

		$dispatcher = JDispatcher::getInstance();
		if (strpos($this->relative_path, 'plugins') === 0) {
			@list($_, $plugin_type, $plugin_name) = explode('/', $this->relative_path);
			JPluginHelper::importPlugin($plugin_type, $plugin_name);
		}
		$dispatcher->trigger('registerZOOEvents');

		// get type
		$this->type = $this->address;

        if ($this->type) {

			// set toolbar items
            $this->app->system->application->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPE').': '.$this->type->name.' <small><small>[ '.JText::_('PLG_ZOOCART_ASSIGN_ELEMENTS').': '. $this->layout .' ]</small></small>');
			$this->app->toolbar->save('savesubmission');
			$this->app->toolbar->apply('applysubmission');
			$this->app->toolbar->cancel('types');

			// get renderer
            $renderer = $this->app->renderer->create('address')->addPath($this->path);

            // get positions and config
            $this->config = $renderer->getConfig('address')->get($this->application->getGroup().'.'.$this->application->id.'.'.$type.'.'.$this->layout);

            $prefix = 'address.';
            if ($renderer->pathExists('address'.DIRECTORY_SEPARATOR.$type)) {
                $prefix .= $type.'.';
            }
            $this->positions = $renderer->getPositions($prefix.$this->layout);

			// display view
			ob_start();
			$this->getView()->setLayout('assignsubmission')->display();
			$output = ob_get_contents();
			ob_end_clean();

			// trigger edit event
			$this->app->event->dispatcher->notify($this->app->event->create($this->type, 'addresstype:assignelements', array('html' => &$output)));

			echo $output;

        } else {

			$this->app->error->raiseNotice(0, JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_FIND_ADDRESS_TYPE', $type));
			$this->setRedirect($this->baseurl . '&task=types');

		}
	}

	public function saveSubmission() {

		// check for request forgeries
		$this->app->request->checkToken() or jexit('Invalid Token');

		// init vars
		$type		   = $this->app->request->getString('type');
		$layout		   = $this->app->request->getString('layout');
		$relative_path = $this->app->request->getVar('path');
		$path		   = $relative_path ? JPATH_ROOT . '/' . urldecode($relative_path) : '';
		$positions	   = $this->app->request->getVar('positions', array(), 'post', 'array');

		// unset unassigned position
		unset($positions['unassigned']);

		// get renderer
		$renderer = $this->app->renderer->create('address')->addPath($path);

		// clean config
		$config = $renderer->getConfig('address');
		foreach ($config as $key => $value) {
			$parts = explode('.', $key);
			if ($parts[0] == $this->group && !$this->application->getType($parts[1])) {
				$config->remove($key);
			}
		}

		// save config
		$config->set($this->application->getGroup().'.'.$this->application->id.'.'.$type.'.'.$layout, $positions);
		$renderer->saveConfig($config, $path.'/renderer/address/positions.config');

		switch ($this->getTask()) {
			case 'applysubmission':
				$link  = $this->baseurl.'&task=assignsubmission&type='.$type.'&layout='.$layout.'&path='.$relative_path;
				break;
			default:
				$link = $this->baseurl.'&task=types';
				break;
		}

		$this->setRedirect($link, JText::_('PLG_ZOOCART_ELEMENTS_ASSIGNED'));
	}


}

/*
	Class: ManagerControllerException
*/
class ManagerControllerException extends AppException {}