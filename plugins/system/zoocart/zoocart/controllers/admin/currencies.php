<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CurrenciesController extends AdminResourceController {

	public function __construct($default = array()) {

		$this->resource_name = 'currencies';

		$this->resource_class = 'Currency';

		parent::__construct($default);
	}

}

class CurrenciesControllerException extends AppException {}