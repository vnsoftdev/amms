<?php
/**
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class GeneralController extends AdminResourceController {

	public $group;
	public $application;

	public function __construct($default = array()) {

		$this->resource_name = 'general';

		$this->resource_class = 'General';

		parent::__construct($default);

		$this->registerDefaultTask('settingsList');

		// get types
		$this->address = $this->app->zoocart->address->getAddressType($this->application);

		// register tasks
		$this->registerTask('applyelements', 'saveelements');
		$this->registerTask('applyassignelements', 'saveassignelements');
		$this->registerTask('applysubmission', 'savesubmission');
	}

	public function settingsList() {

		// set toolbar items
		$this->app->system->application->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPE'));
		$this->app->toolbar->apply('saveSettings',JText::_('PLG_ZOOCART_APPLY'));
		$this->app->toolbar->cancel();

		// get templates
		$this->templates = $this->application->getTemplates();

		// get extensions / trigger layout init event
		$this->extensions = $this->app->event->dispatcher->notify($this->app->event->create($this->app, 'layout:init'))->getReturnValue();

		// display view
		$this->getView()->setLayout('settings')->display();
	}

	public function saveSettings() {

		// check for request forgeries
		$this->app->request->checkToken() or jexit('Invalid Token');
		$link = $this->baseurl."&task=settingsList";

		// Get the list of extra config values for zoocart
		$extra_config = $this->app->request->get('zoocart', 'array');

		foreach($extra_config as $key => $value ) {
			$zoo_app = $this->app->zoo;
			$zoocart_config = $this->app->zoocart->getConfig($zoo_app->getApplication()->id);
			$zoocart_config->set($key, $value);
		}

		$this->app->zoocart->setConfig($zoo_app->getApplication()->id, $zoocart_config);

		$msg = JText::_('PLG_ZOOCART_GENERAL_CONFIG_SAVED');
		$this->setRedirect($link, $msg);
	}
}

/*
	Class: ManagerControllerException
*/
class ManagerControllerException extends AppException {}