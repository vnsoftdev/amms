<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class ShippingratesController extends AdminResourceController {

	public function __construct($default = array()) {

		$this->resource_name = 'shippingrates';

		$this->resource_class = 'ShippingRate';

		parent::__construct($default);
	}

}

class ShippingratesControllerException extends AppException {}