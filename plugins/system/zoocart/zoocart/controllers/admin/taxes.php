<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class TaxesController extends AdminResourceController {

	public function __construct($default = array()) {

		$this->resource_name = 'taxes';

		$this->resource_class = 'Tax';

		parent::__construct($default);
	}

	protected function beforeEditDisplay() {
		// published select
		$this->lists['select_enabled'] = $this->app->html->_('select.booleanlist', 'enabled', null, $this->resource->enabled);

		// published searchable
		$this->lists['select_vies'] = $this->app->html->_('select.booleanlist', 'vies', null, $this->resource->vies);
	}
}

class TaxesControllerException extends AppException {}