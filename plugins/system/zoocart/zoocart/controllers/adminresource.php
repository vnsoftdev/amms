<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class AdminResourceController extends ResourceController {

	public function __construct($default = array()) {
		parent::__construct($default);

		if ($id = $this->app->request->getInt('changeapp')) {
			$this->application = $this->app->table->application->get($id);
		} else {
			$this->application = $this->app->zoo->getApplication();
		}

		// Access hook on ZOOcart is not enabled for this app:
		if(!$this->application->getParams()->get('global.zoocart.enable_cart'))
			$this->setRedirect('index.php?option=com_zoo');

		// Register
		$this->registerTask('add', 'edit');
		$this->registerTask('apply', 'save');
		$this->registerTask('saveandnew', 'save');
	}

	public function getView($name = '', $type = '', $prefix = '', $config = array()) {

		$view = parent::getView($name, $type, $prefix, $config);
		$view->addTemplatePath($this->app->path->path('zoocart:views/admin/partials'));
		$view->addTemplatePath($this->app->path->path('zoocart:views/admin/'.$this->resource_name.'/tmpl'));

		return $view;
	}

	public function display($cachable = false, $urlparams = false) 
	{
		parent::display($cachable, $urlparams );

		$state_prefix       = $this->option.'_'.$this->application->id.'.'.strtolower($this->getName()).'.';
		$filter_order	    = $this->app->system->application->getUserStateFromRequest($state_prefix.'filter_order', 'filter_order', 'a.created', 'cmd');
		$filter_order_Dir   = $this->app->system->application->getUserStateFromRequest($state_prefix.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		// set toolbar items
		$this->joomla->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_' . strtoupper($this->resource_name)));

		// set toolbar items
		$this->app->toolbar->addNew();
		$this->app->toolbar->editList();
		//$this->app->toolbar->custom('docopy', 'copy.png', 'copy_f2.png', 'Copy');
		$this->app->toolbar->deleteList();

		// Filters
		$state_prefix       = $this->option.'_'.$this->application->id.'.'.$this->resource_name;
		$limit		        = $this->joomla->getUserStateFromRequest('global.list.limit', 'limit', $this->joomla->getCfg('list_limit'), 'int');
		$limitstart			= $this->joomla->getUserStateFromRequest($state_prefix.'limitstart', 'limitstart', 0,	'int');

		$count = $this->table->count();

		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$limitstart = $limitstart > $count ? floor($count / $limit) * $limit : $limitstart;
	
		// Vars
		$this->pagination = $this->app->pagination->create($count, $limitstart, $limit);
		$this->resources = array_merge($this->getResources());

		// table ordering and search filter
		$this->lists['order_Dir'] = $filter_order_Dir;
		$this->lists['order']	  = $filter_order;

		// Display
		$this->getView()->setLayout('default')->display();
	}

	protected function getResources() {
		// Filters
		$state_prefix       = $this->option.'_'.$this->application->id.'.'.$this->resource_name;
		$limit		        = $this->joomla->getUserStateFromRequest('global.list.limit', 'limit', $this->joomla->getCfg('list_limit'), 'int');
		$limitstart			= $this->joomla->getUserStateFromRequest($state_prefix.'limitstart', 'limitstart', 0,	'int');

		$count = $this->table->count();

		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$limitstart = $limitstart > $count ? floor($count / $limit) * $limit : $limitstart;
		
		$options = $limit > 0 ? array('offset' => $limitstart, 'limit' => $limit, 'order' => $this->default_order) : array('order' => $this->default_order);

		return $this->table->all($options);
	}

	public function edit() {

		// disable menu
		$this->app->request->setVar('hidemainmenu', 1);

		// get request vars
		$cid  = $this->app->request->get('cid.0', 'int');
		$edit = $cid > 0;

		// get item
		if ($edit) {
			if (!$this->resource = $this->table->get($cid)) {
				$this->app->error->raiseError(500, JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_ACCESS_RESOURCE', $cid));
				return;
			}
		} else {
			$this->resource = $this->app->object->create($this->resource_class);
		}

		$this->app->system->application->JComponentTitle = $this->application->getToolbarTitle(JText::_('PLG_ZOOCART_CONFIG_' . strtoupper($this->resource_name)) . ' <small><small>[ '.($edit ? JText::_('PLG_ZOOCART_EDIT') : JText::_('PLG_ZOOCART_NEW')).' ]</small></small>');
		$this->getEditToolbar();

		$this->beforeEditDisplay();

		// display view
		$this->getView()->setLayout('edit')->display();
	}

	protected function getEditToolbar() {

		$cid  = $this->app->request->get('cid.0', 'int');
		$edit = $cid > 0;

		// set toolbar items
		$this->app->toolbar->apply();
		$this->app->toolbar->save();
		$this->app->toolbar->custom('saveandnew', 'save-new', 'saveandnew', 'PLG_ZOOCART_SAVE_AND_NEW', false);
		$this->app->toolbar->cancel('cancel', $edit ? 'PLG_ZOOCART_CLOSE' : 'PLG_ZOOCART_CANCEL');
		$this->app->zoo->toolbarHelp();
	}

	protected function beforeEditDisplay() {

	}

	public function save() {

		// check for request forgeries
		$this->app->request->checkToken() or jexit('Invalid Token');

		$post = $this->app->request->get('post:', 'array', array());
		$cid        = $this->app->request->get('cid.0', 'int');

		try {

			// get tax
			if ($cid) {
				$tax = $this->table->get($cid);
			} else {
				$tax = $this->app->object->create($this->resource_class);
			}

			// bind item data
			self::bind($tax, $post);

			// save item
			$this->table->save($tax);

			// set redirect message
			$msg = JText::_($this->resource_class . ' Saved');

		} catch (AppException $e) {

			// raise notice on exception
			$this->app->error->raiseNotice(0, JText::sprintf('PLG_ZOOCART_ERROR_SAVING', $this->resource_class).' ('.$e.')');
			$this->_task = 'apply';
			$msg = null;

		}

		$link = $this->baseurl;
		switch ($this->getTask()) {
			case 'apply' :
				$link .= '&task=edit&cid[]='.$tax->id;
				break;
			case 'saveandnew' :
				$link .= '&task=add';
				break;
		}

		$this->setRedirect($link, $msg);
	}

	protected function onAfterSave() {

	}

	public function remove() {

		// check for request forgeries
		$this->app->request->checkToken() or jexit('Invalid Token');

		// init vars
		$cid = $this->app->request->get('cid', 'array', array());

		if (count($cid) < 1) {
			$this->app->error->raiseError(500, JText::_('PLG_ZOOCART_ERROR_SELECT_A_ITEM_TO_DELETE'));
		}

		try {

			// delete items
			foreach ($cid as $id) {
				$this->table->delete($this->table->get($id));
			}

			// set redirect message
			$msg = JText::_($this->resource_class . ' Deleted');

		} catch (AppException $e) {

			// raise notice on exception
			$this->app->error->raiseWarning(0, JText::_('PLG_ZOOCART_ERROR_DELETING', $this->resource_class).' ('.$e.')');
			$msg = null;

		}

		$this->setRedirect($this->baseurl, $msg);
	}
}