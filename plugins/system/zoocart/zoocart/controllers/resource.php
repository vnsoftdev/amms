<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class ResourceController extends AppController {

	protected $resource_name = '';

	protected $resource_class = '';

	protected $default_order = 'id ASC';

	public function __construct($default = array()) {
		parent::__construct($default);

		// set table
		$table_name = $this->resource_name;
		$this->table = $this->app->table->$table_name;

		// set base url
		$this->baseurl = new JUri($this->app->link(array('controller' => $this->controller), false));
	}

	public function getView($name = '', $type = '', $prefix = '', $config = array()) {

		$view = parent::getView($name, $type, $prefix, $config);
		return $view;
	}

	public function display($cachable = false, $urlparams = false) 
	{
		// get database
		$this->db = $this->app->database;

		// get Joomla application
		$this->joomla = $this->app->system->application;
	}

	public function view() {

		// get request vars
		$cid  = $this->app->request->get('id', 'int');
		
		if (!$this->resource = $this->table->get($cid)) {
			$this->app->error->raiseError(500, JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_ACCESS_RESOURCE', $cid));
			return;
		}
		
		$this->template = $this->application->getTemplate();
		$this->renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->app->path->path('zoocart:'), $this->template->getPath()));
		
		// display view
		$this->getView()->setLayout('view')->display();
	}
}

class ZooResourceControllerException extends AppException {}