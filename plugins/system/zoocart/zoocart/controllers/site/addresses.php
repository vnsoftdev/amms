<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class AddressesController extends SiteResourceController {

	public function __construct($default = array()) {

		$this->resource_name = 'addresses';

		$this->resource_class = 'Address';

		parent::__construct($default);
	}

	public function save() {

		$id = $this->app->request->getInt('id');
		$type = $this->app->request->getString('type', 'billing');
		$post = $this->app->request->get('post:', 'array');

		$error = false;

		if ($id) {
			$address = $this->table->get($id);
			if ($address->user_id != $this->app->user->get()->id) {
				$msg = JText::_('PLG_ZOOCART_ERROR_CANNOT_SAVE_ADDRESS');
				$error = true;
			}
		} else {
			$address = $this->app->object->create('Address');
			$address->type = $type;
		}

		self::bind($address, $post);
		$address->user_id = ($user_id = $this->app->user->get()->id) ? $user_id : 0;

		$result = $this->validate();
		$error = $error && $result['success'];

		if ($this->app->request->get('format', 'string') == 'raw') {
			if($error){
				echo json_encode(false);
			} else {
				echo json_encode($this->table->save($address));
			}
			return;
		} else {
			if (!$error) {
				$msg = JText::_('PLG_ZOOCART_ADDRESS_SAVED');
				$this->table->save($address);
				$this->setRedirect($this->baseurl . '&task=display', $msg);
			} else {
				$msg = JText::_('PLG_ZOOCART_ERROR_CANNOT_SAVE_ADDRESS');
				$this->setRedirect($this->baseurl . '&task=edit&id='.$address->id, $msg, 'error');
			}
			return;
		}
	}

	public function validate() {

		$address = $this->app->request->get('elements', 'array');
		$type = $this->app->request->get('type', 'string', 'billing');

		$address = $this->app->zoocart->address->getFromValues($address);

		$result = $this->app->zoocart->address->validate($address, $type);
		if($this->app->request->get('format', 'string') == 'raw') {
			echo json_encode($result);
			die();
		}

		return $result;
	}

	public function remove() {
		$id = $this->app->request->getInt('id');
		$error = false;
		$msg = JText::_('PLG_ZOOCART_ADDRESS_DELETED');
		
		if (!$id) {
			$msg = JText::_('PLG_ZOOCART_EMPTY_ID_NOT_ALLOWED');
			$error = true;
		}

		$address = $this->table->get($id);
		if (!$address) {
			$msg = JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_ACCESS_RESOURCE', $id);
			$error = true;
		}

		if ($address->user_id != $this->app->user->get()->id) {
			$msg = JText::_('PLG_ZOOCART_ERROR_CANNOT_DELETE_ADDRESS');
			$error = true;
		}

		if ($address->default) {
			$msg = JText::_('PLG_ZOOCART_ERROR_CANNOT_DELETE_DEFAULT_ADDRESS');
			$error = true;
		}


		if (!$error) {
			$this->table->delete($address);
		}
		
		$this->setRedirect($this->baseurl . '&task=display', $msg);
	}

	public function setDefault() {
		$id = $this->app->request->getInt('id');
		$error = false;
		$msg = JText::_('PLG_ZOOCART_ADDRESS_DEFAULT_SET');
		
		if (!$id) {
			$msg = JText::_('PLG_ZOOCART_EMPTY_ID_NOT_ALLOWED');
			$error = true;
		}

		$address = $this->table->get($id);
		if (!$address) {
			$msg = JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_ACCESS_RESOURCE', $id);
			$error = true;
		}

		if ($address->user_id != $this->app->user->get()->id) {
			$msg = JText::_('PLG_ZOOCART_ERROR_CANNOT_SAVE_ADDRESS');
			$error = true;
		}

		if (!$error) {
			$default_address = $this->table->getDefaultAddress($this->app->user->get()->id, $address->type);
			$default_address->default = 0;
			$this->table->save($default_address);
			$address->default = 1;
			$this->table->save($address);
		}
		
		$uri = new JURI();

		$this->setRedirect($this->baseurl . 'task=display', $msg);
	}

	public function edit() {

		// get request vars
		$id  = $this->app->request->get('id', 'int');
		$edit = $id > 0;
		$error = false;

		// get item
		if ($edit) {
			if (!$this->resource = $this->table->get($id)) {
				$msg = JText::sprintf('PLG_ZOOCART_ERROR_UNABLE_ACCESS_RESOURCE', $id);
				$error = true;
			}
		} else {
			$this->resource = $this->app->object->create($this->resource_class);
		}

		if ($edit && $this->resource->user_id != $this->app->user->get()->id) {
			echo 'here';die();
			$msg = JText::_('PLG_ZOOCART_ADDRESS_CANNOT_EDIT');
			$error = true;
		}

		if (!$error) {
			// display view
			$this->getView()->setLayout('edit')->display();
		} else {
			$this->setRedirect($this->baseurl . '&task=display', $msg);
		}
	}
}

class AddressesControllerException extends AppException {}