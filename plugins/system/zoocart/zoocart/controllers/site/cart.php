<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CartController extends SiteResourceController {

	public function __construct($default = array()) {

		$this->resource_name = 'cartitems';

		$this->resource_class = 'Cartitem';

		parent::__construct($default);
	}

	public function login() {

		$username = trim($this->app->request->getString('username'));
		$password = trim($this->app->request->getString('password'));
		$remember = $this->app->request->getBool('remember');

		$success = false;
		if (strlen($username) && strlen($password)) {
			$success = JFactory::getApplication()->login(array('username' => $username, 'password' => $password, 'remember' => $remember));
		}

		echo json_encode($success);
		return;
	}

	public function register() {

		$data['username'] = trim($this->app->request->getString('username'));
		$data['password'] = trim($this->app->request->getString('password'));
		$data['password2'] = trim($this->app->request->getString('password2'));
		$data['email'] = trim($this->app->request->getString('email'));
		$data['name'] = trim($this->app->request->getString('name'));

		$errors = array();
		if (!strlen($data['username'])) {
			$errors[] = JText::_('PLG_ZOOCART_USERNAME_REQUIRED');
		}
		if (!strlen($data['password'])){
			$errors[] = JText::_('PLG_ZOOCART_PASSWORD_REQUIRED');
		}
		if (!strlen($data['email'])) {
			$errors[] = JText::_('PLG_ZOOCART_EMAIL_REQUIRED');
		} 
		if ($data['password'] != $data['password2']) {
			$errors[] = JText::_('PLG_ZOOCART_PASSWORDS_MUST_MATCH');
		}

		$success = false;
		if (!count($errors)) {
			$password = $data['password'];

			$user = new JUser();
			$user->id = 0;
			$user->bind($data);
			$user->groups = array(JComponentHelper::getParams('com_users')->get('new_usertype', 2));

			$success = $user->save();

			if(!$success) {
				$errors = $user->getErrors();
			} else {
				JFactory::getApplication()->login(array('username' => $data['username'], 'password' => $password));
			}
		}

		echo json_encode(array('success' => $success, 'errors' => $errors ));
		return;
	}
	
	public function display($cachable = false, $urlparams = false) {

		$this->application = $this->app->zoo->getApplication();
		$this->db = $this->app->database;

		$this->user = JFactory::getUser();

		$this->items = $this->table->getByUser();

		foreach($this->items as &$item) {
			$item->item = $this->app->table->item->get($item->item_id);
		}

		$this->template = $this->application->getTemplate();
		$this->renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->template->getPath(), $this->app->path->path('zoocart:')));

		// Assign addresses
		$this->address_renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->template->getPath(), $this->app->path->path('zoocart:')));
		$addresses = $this->app->zoocart->table->addresses->getByUser($this->user->id);

		$this->addresses['billing'] = array();
		$this->addresses['shipping'] = array();

		foreach( $addresses as $address ) {
			if ($address->type == 'billing') {
				$this->addresses['billing'][] = $address;
			} else {
				$this->addresses['shipping'][] = $address;
			}
		}

		// Default billing address
		$this->default_address = $this->app->zoocart->table->addresses->getDefaultAddress($this->user->id, 'billing');

		// Shipping methods
		$this->shipping_rates = $this->app->zoocart->shipping->getShippingRates(array('order' => null)); 

		// Display
		$view = $this->getView();
		$view->addTemplatePath($this->app->path->path('zoocart:views/site/cart/tmpl'));
		$view->addTemplatePath($this->template->getPath(). '/cart/');
		$view->setLayout('default')->display();
	}

	public function removeItem() {
		
		$id = $this->app->request->get('id', 'int');
		if ($id) {
			$cartitem = $this->app->zoocart->table->cartitems->get($id);
			$result = $this->app->zoocart->table->cartitems->delete($cartitem);
		} else {
			$result = false;
		}
		echo json_encode($result);
	}

	public function addToCart() {

		$item_id = $this->app->request->get('item_id', 'int');
		$quantity = $this->app->request->get('quantity', 'float');

		if(!$item_id) {
			return;
		}

		$user_id = ($user = $this->app->user->get()) ? $user->id : 0;
		$session_id = $this->app->session->getId();

		$cartitem = $this->table->getByItemId($item_id);

		if (!$cartitem) {
			$cartitem = $this->app->object->create('Cartitem');
		}

		$cartitem->item_id = $item_id;
		$cartitem->user_id = $user_id;
		$cartitem->session_id = $session_id;
		$cartitem->quantity += $quantity;

		$this->table->save($cartitem);

		if ($this->app->request->get('format', 'string') == 'raw') {
			echo json_encode((bool)$cartitem->id);
			return;
		}


		$this->setRedirect($this->app->link(array('controller' => $this->controller, 'app_id' => $this->application->id, 'task' => 'display'), false));
		$this->redirect();
	}

	public function cartModule() {

		jimport('joomla.application.module.helper');

		$id = $this->app->request->get('module_id', 'int');

		$module = JModuleHelper::getModule('mod_zoocart');
		$params = new JRegistry($module->params);

		ob_start();
		$zoo = $this->app;
		$items = $zoo->zoocart->table->cartitems->getByUser($zoo->user->get()->id);
		include ( JModuleHelper::getLayoutPath( 'mod_zoocart', 'default' ) );
		$html = ob_get_contents();
		ob_end_clean();

		echo json_encode($html);
		return;
	}
}

class ZooCartControllerException extends AppException {}