<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class OrdersController extends SiteResourceController {

	public function __construct($default = array()) {

		$this->resource_name = 'orders';

		$this->resource_class = 'Order';

		$this->default_order = 'created_on DESC';
 
		parent::__construct($default);
	}

	public function pay() {

		$id = $this->app->request->getInt('id');

		if (!$id) {
			throw new AppException(JText::_('PLG_ZOOCART_ERROR_ORDER_NOT_FOUND'), 500);
		}

		$this->order = $this->table->get($id);
		$config = $this->app->zoocart->getConfig();

		if (in_array($this->order->state, array($config->get('new_orderstate'), $config->get('payment_failed_orderstate')))) { 
			$this->app->session->set('com_zoo.zoocart.pay_order_id', $this->order->id);

			$dispatcher = JDispatcher::getInstance();
			JPluginHelper::importPlugin('zoocart_payment', $this->order->payment_method);

			$this->template = $this->application->getTemplate();
			$this->renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->template->getPath(), $this->app->path->path('zoocart:')));

			$this->payment_html = implode('', $dispatcher->trigger('render', array('data' => array('order' => $this->order))));
			$this->getView()->addTemplatePath($this->template->getPath().'/orders/')->setLayout('pay')->display();
		} else {
			// redirect to order view
			$this->app->system->application->enqueueMessage(JText::_('PLG_ZOOCART_ERROR_ORDER_ALREADY_PAYED'), 'notice');
			$this->setRedirect(JRoute::_($this->baseurl . '&task=view&id=' . $id));
			return;
		}
	}

	public function callback() {

		$data = $this->app->request->get('request:', 'array');
		$order_id = (int) $this->app->session->get('com_zoo.zoocart.pay_order_id');
		$payment_method = $this->app->request->get('payment_method', '');
		
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('zoocart_payment', $payment_method);

		$result = array_shift($dispatcher->trigger('callback', array('data' => $data)));
		$result = $this->app->data->create($result);

		$payment = $this->app->object->create('Payment');
		$payment->payment_method = $payment_method;
		$payment->status = $result->get('status', 0);
		$payment->order_id = ($id = $result->get('order_id', 0)) ? $id : $order_id;
		$payment->transaction_id = $result->get('transaction_id', '');
		$payment->data = json_encode($data);
		$payment->total = $result->get('total', 0);

		$this->app->zoocart->table->payments->save($payment);
		return;
	}

	public function message() {

		$data = $this->app->request->get('request:', 'array');
		$order_id = $this->app->session->get('com_zoo.zoocart.pay_order_id');
		$order = $this->app->zoocart->table->orders->get($order_id);

		$data['order'] = $order;

		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('zoocart_payment', $order->payment_method);

		$result = $dispatcher->trigger('message', array('data' => $data));

		$this->message = implode('', $result);
		$this->getView()->setLayout('message')->display();
	}

	public function cancel() {
		$this->order_id = $this->app->session->get('com_zoo.zoocart.pay_order_id');
		$this->getView()->setLayout('cancel')->display();
	}

	public function save() {

		$result = $this->_validate();
		$success = $result['success'];
		$errors = $result['errors'];

		if ($success) {

			$billing_data = $this->app->request->get('billing', 'array');
			$shipping_data = $this->app->request->get('shipping', 'array');

			if (!$billing_data) {
				$billing_data = $this->app->request->get('post:', 'array');
			}

			if (!$shipping_data) {
				$shipping_data = $this->app->request->get('post:', 'array');

				if ($this->app->request->get('same_as_billing', false)) {
					$shipping_data = $billing_data;
				}
			}

			// Save addresses First
			$billing_address = $this->_saveAddress($billing_data, 'billing');
			$shipping_address = $this->_saveAddress($shipping_data, 'shipping');

			// Get Payment fee
			$payment	 = $this->_validatePayment($this->app->request->getString('payment_method', ''));
			$payment_fee = $payment['fee'];

			// Get Shipping fee
			$shipping_method = $this->app->request->getString('shipping_method', '');
			$shipping_plugin = $this->app->request->getString('shipping_plugin', '');
			$shipping	 = $this->_validateShipping($shipping_plugin, $shipping_method);
			$shipping_fee = $shipping['fee'];

			// Save Order
			$order = $this->app->object->create('Order');
			
			$order->id = 0;
			$order->user_id = $this->app->user->get()->id;
			
			$order->billing_address = json_encode($billing_address);
			$order->shipping_address = json_encode($shipping_address);
			
			$order->net = $this->app->zoocart->cart->getNetTotal();
			$order->tax = $this->app->zoocart->cart->getTaxes($this->app->user->get()->id, $billing_address);
			
			$order->shipping = $shipping_fee;
			$order->shipping_tax = $order->getShippingTax();
			$order->payment = $payment_fee;
			$order->payment_tax = $order->getPaymentTax();

			$order->discount = 0;
			
			$order->notes = $this->app->request->getString('notes', '');
			$order->payment_method = $this->app->request->getString('payment_method', '');
			$order->shipping_method = json_encode($shipping['rate']);
			$order->state = $this->app->zoocart->getConfig()->get('new_orderstate', 1);

			$order->subtotal = $order->getSubtotal();
			$order->tax_total = $order->getTaxTotal();
			$order->total = $order->getTotal();

			$this->table->save($order);

			$success = $success && (bool)$order->id;

			if (!$success) {
				$errors[] = JText::_('PLG_ZOOCART_ERROR_ORDER_SAVE_FAILED');
			}


			// Save Items
			foreach ($this->app->zoocart->table->cartitems->getByUser() as $cartitem) {
				$item = $this->app->table->item->get($cartitem->item_id);

				$orderitem = $this->app->object->create('Orderitem');
				$orderitem->order_id = $order->id;
				$orderitem->item_id = $cartitem->item_id;
				$orderitem->name = $item->name;
				$orderitem->elements = $item->elements;
				$orderitem->params = $cartitem->params;
				$orderitem->quantity = $cartitem->quantity;
				$orderitem->price = $orderitem->getPrice();
				$orderitem->tax = $orderitem->getTax($billing_address);
				$orderitem->total = $orderitem->getTotal($billing_address);

				$this->app->zoocart->table->orderitems->save($orderitem);

				$success = $success && (bool)$orderitem->id;

				if (!$success) {
					$errors[] = JText::_('PLG_ZOOCART_ERROR_ORDER_ITEM_SAVE_FAILED');
				}
			}
		}

		if ($success) {
			$this->app->zoocart->cart->clear($this->app->user->get()->id);
		}

		if ($this->app->request->getCmd('format') == 'raw') {
			echo json_encode(array('success' => $success, 'errors' => $errors, 'id' => $order->id));
			return;
		}

		return $success;
	}

	protected function _saveAddress($data, $type = 'billing') {
		
		if (array_key_exists($type . '_id', $data)) {
 			return $this->app->zoocart->table->addresses->get((int)$data[$type . '_id']);
		}

		$address = $this->app->zoocart->address->getFromValues($data, $type);
		$address->user_id = $this->app->user->get()->id;

		$this->app->zoocart->table->addresses->save($address);

		return $address;
	}

	/**
	 * Validate address
	 *
	 * @param bool $silent
	 * @return mixed
	 */
	public function validateAddress() {

		$type = $this->app->request->getString('type');
		$data = $this->app->request->get('elements', 'array');

		if(!$data) {
			$data = $this->app->request->get('post:', 'array');
		}

		echo json_encode($this->_validateAddress($data, $type));
	}

	public function changePaymentMethod() {

		$method = $this->app->request->getString('payment_method');
		$order_id = $this->app->request->getInt('order_id');

		$order = $this->table->get($order_id);
		$plugin = JPluginHelper::getPlugin('zoocart_payment', $method);

		if(!$method || !$plugin) {
			echo json_encode($order->payment_method);
			return;
		}

		$order->payment_method = $method;
		$this->table->save($order);

		echo json_encode(ucfirst($method));
		return;
	}

	protected function _validateAddress($address, $type = 'billing') {

		if (array_key_exists('id', $address)) {
			$address = $this->app->zoocart->table->addresses->get((int)$address['id']);
		} else {
			$address = $this->app->zoocart->address->getFromValues($address);
		}

		return $this->app->zoocart->address->validate($address, $type);
	}

	public function validateTaxes() {

		$address = $this->app->request->get('elements', 'array');
		
		if(!$address) {
			$data = $this->app->request->get('post:', 'array');
			if (array_key_exists('id', $data)) {
				$address = $this->app->zoocart->table->addresses->get((int)$data['id']);
			}
		} else {
			$address = $this->app->zoocart->address->getFromValues($address, $this->app->zoocart->getConfig()->get('billing_address_type', 'billing'));
		}		

		$payment = $this->app->request->getString('payment_method');
		$payment = $this->_validatePayment($payment);
		$fee = $payment['fee'];

		$shipping_plugin = $this->app->request->getString('shipping_plugin');
		$shipping_method = $this->app->request->getString('shipping_method');
		$shipping = $this->_validateShipping($shipping_plugin, $shipping_method);
		$fee += $shipping['fee'];

		$response =  array(
			'success' => true, 
			'subtotal' => $this->app->zoocart->currency->format(null, $this->_validateSubtotal($fee)) , 
			'taxes' => $this->app->zoocart->currency->format(null, $this->_validateTaxes($address, $fee)), 
			'total' => $this->app->zoocart->currency->format(null, $this->_validateTotal($address, $fee))
		);

		echo json_encode($response);
	}

	protected function _validateTaxes($address, $fee = 0) {

		$taxes = $this->app->zoocart->cart->getTaxes( $this->app->user->get()->id, $address);
		$taxes = $taxes + ($fee * $this->app->zoocart->tax->getTaxes(null, $fee, null, $address));
		return $taxes;
	}

	protected function _validateSubtotal($fee = 0) {

		$subtotal = $this->app->zoocart->cart->getSubtotal($this->app->user->get()->id) + $fee;
		return $subtotal;
	}


	protected function _validateTotal($address, $fee = 0) {

		$total = $this->_validateSubtotal($fee) + $this->_validateTaxes($address, $fee);
		return $total;
	}

	public function validateQuantities() {
		
		$quantities = $this->app->request->get('quantity', 'array');
		$response = array('totals' => $this->_validateQuantities($quantities));

		echo json_encode($response);
	}

	protected function _validateQuantities($quantities) {
		$totals = array();

		foreach($quantities as $id => $quantity) {
			$cartitem = $this->app->zoocart->table->cartitems->get($id);
			$cartitem->quantity = $quantity;
			$this->app->zoocart->table->cartitems->save($cartitem);

			// Get payment properties to get taxes:
			$address = $this->app->request->get('elements', 'array');
			$payment = $this->app->request->getString('payment_method');
			$payment = $this->_validatePayment($payment);
			$fee = $payment['fee'];

			$taxes = $this->_validateTaxes($address, $fee);

			$totals[$id] = $this->app->zoocart->currency->format(null, $cartitem->getNetTotal()+($this->app->zoocart->tax->checkTaxEnabled()?$taxes:0));
		}
		return $totals;
	}

	public function validatePayment() {
		
		$payment = $this->app->request->get('payment_method', 'string');
		$errors = array();
		
		$result = $this->_validatePayment($payment);
		$success = $result['success'];

		if (!$success) {
			$errors[] = JText::_('PLG_ZOOCART_ERROR_SELECT_PAYMENT_METHOD');
		}

		$response = array(
			'success' => $success, 
			'errors' => $errors, 
			'pfee' => $this->app->zoocart->currency->format(null, $result['fee'])
		);

		echo json_encode($response);
	}

	protected function _validatePayment($payment) {

		if (strlen($payment) && JPluginHelper::getPlugin('zoocart_payment', $payment)) {
			JPluginHelper::importPlugin('zoocart_payment', $payment);
			$dispatcher = JDispatcher::getInstance();
			$fee = array_sum($dispatcher->trigger('getPaymentFee', array('order' => null)));
			return array('success' => true, 'fee' => $fee);
		}
		
		return array('success' => false, 'fee' => 0);
	}

	public function validateShipping() {
		
		$shipping_method = $this->app->request->get('shipping_method', 'string');
		$shipping_plugin = $this->app->request->get('shipping_plugin', 'string');
		$errors = array();
		
		$result = $this->_validateShipping($shipping_plugin, $shipping_method);
		$success = $result['success'];

		if (!$success) {
			$errors[] = JText::_('PLG_ZOOCART_ERROR_SELECT_SHIPPING_METHOD');
		}

		$response = array(
			'success' => $success, 
			'errors' => $errors, 
			'sfee' => $this->app->zoocart->currency->format(null, $result['fee'])
		);

		echo json_encode($response);
	}

	protected function _validateShipping($plugin, $method) {

		if ($this->app->zoocart->getConfig()->get('enable_shipping', true)) {
			if (strlen($plugin) && JPluginHelper::getPlugin('zoocart_shipping', $plugin)) {
				JPluginHelper::importPlugin('zoocart_shipping', $plugin);
				$dispatcher = JDispatcher::getInstance();
				$results = $dispatcher->trigger('getShippingRates', array('order' => null));

				foreach($results as $plugins) {
					foreach($plugins as $plugin => $rates) {
						if($plugin == $plugin) {
							foreach($rates as $rate) {
								if($rate['id'] == $method) {
									$fee = $rate['price'];
									$rate['plugin'] = $plugin;
									return array('success' => true, 'fee' => $fee, 'rate' => $rate);
								}
							}
						}
					}
				}
			}
		} else {
			return array('success' => true, 'fee' => 0, 'rate' => null);
		}
		
		return array('success' => false, 'fee' => 0, 'rate' => null);
	}

	public function validateUser() {

		$login_data = $this->app->request->get('logindata', 'array');
		$register_data = $this->app->request->get('registerdata', 'array');

		$result = $this->_validateUser($login_data, $register_data);

		echo json_encode($result);
	}

	protected function _validateUser($login_data, $register_data) {
		
		// init vars
		$errors = array();

		// if already logged validate right away
		if ($this->app->user->get()->id) {
			return array('success' => true, 'errors' => $errors);
		}
		
		// wrapp login data with Data object
		$login_data = $this->app->data->create($login_data);

		if (strlen($login_data->get('username', '')) && strlen($login_data->get('password', ''))) {
			
			$success = JFactory::getApplication()->login(array(
				'username' => $login_data->get('username'),
				'password' => $login_data->get('password'),
				'remember' => $login_data->get('remember')
			));

			if (!$success) {
				$errors['login'] = JText::_('PLG_ZOOCART_ERROR_LOGIN_FAILED');
			}
			
			return array('success' => $success, 'errors' => $errors);
		}

		$register_data = $this->app->data->create($register_data);

		$data = array();
		$data['username'] = trim($register_data->get('username', ''));
		$data['password'] = trim($register_data->get('password', ''));
		$data['password2'] = trim($register_data->get('password2', ''));
		$data['email'] = trim($register_data->get('email', ''));
		$data['name'] = trim($register_data->get('name', ''));

		if (!strlen($data['username'])) {
			$errors['username'] = JText::_('PLG_ZOOCART_USERNAME_REQUIRED');
		}
		if (!strlen($data['password'])){
			$errors['password'] = JText::_('PLG_ZOOCART_PASSWORD_REQUIRED');
		}
		if (!strlen($data['email'])) {
			$errors['email'] = JText::_('PLG_ZOOCART_EMAIL_REQUIRED');
		} 
		if ($data['password'] != $data['password2']) {
			$errors['password'] = JText::_('PLG_ZOOCART_PASSWORDS_MUST_MATCH');
		}

		$success = false;
		if (!count($errors)) {
			$password = $data['password'];

			$user = new JUser();
			$user->id = 0;
			$user->bind($data);
			$user->groups = array(JComponentHelper::getParams('com_users')->get('new_usertype', 2));

			$success = $user->save();

			if(!$success) {
				$errors = $user->getErrors();
			} else {
				JFactory::getApplication()->login(array('username' => $data['username'], 'password' => $password));
			}
		}

		return array('success' => $success, 'errors' => $errors);
	}

	protected function _validateVat($address, $element) {

		$billing_info = $this->app->zoocart->address->getBillingInfo();
		$validate = $this->app->zoocart->getConfig()->get('vies_validation', 1);
		$hard_validate = $this->app->zoocart->getConfig()->get('vies_validation_hard', 0);

		$success = true;
		$errors = array();

		$country = $address->country;

		if ($validate && $this->app->country->isEU($country)) {

			$vat = $this->app->data->create($this->app->data->create($address[$element])->get('0'))->get('value');

			$success = $this->app->zoocart->tax->isValidVat($country, $vat);
			
			if(!$success) {
				if ($hard_validate) {
					$errors[$element] = JText::_('PLG_ZOOCART_VIES_VAT_ID_NOT_REGISTERED'); 
				} else {
					$success = true;
					$notices[$element] = JText::_('PLG_ZOOCART_VIES_VAT_ID_NOT_REGISTERED'); 
				}
			}
		}

		return array('success' => $success, 'errors' => $errors, 'notices' => $notices);
	}

	/**
	 * Checkout form complex validation
	 *
	 * @return array
	 */
	protected function _validate() {
		$zconfig = $this->app->zoocart->getConfig();
		$success = true;
        $notifications = array('errors'=>array(),'notices'=>array());
		$data = array();

		// Addresses Validation:
		if($zconfig->get('require_address'))
		{
			// Get billing address data:
			if($bill_addr_id = $this->app->request->getInt('bill_addr_id'))
			{
				$bill_addr = array('id'=>$bill_addr_id);
			}else{
				$bill_addr = array('elements' => $this->app->request->get('bill_elements','array'));
			}

			// Get shipping address data:
			if($ship_addr_id = $this->app->request->getInt('ship_addr_id'))
			{
				$ship_addr = array('id'=>$ship_addr_id);
			}else{
				$ship_addr = array('elements' => $this->app->request->get('ship_elements','array'));
			}

            $billing_addr_valid = $this->_validateAddress($bill_addr,'billing');
            $shipping_addr_valid = $this->_validateAddress($ship_addr,'shipping');

			$success = $success && $billing_addr_valid['success'] && $shipping_addr_valid['success'];
            $notifications = array_merge_recursive($notifications,$billing_addr_valid,$shipping_addr_valid);
		}

		// Shipping validation:
		if($zconfig->get('enable_shipping')){
			$method = $this->app->request->getString('shipping_method');
			$plugin = $this->app->request->getString('shipping_plugin');

			$shipping_valid = $this->_validateShipping($plugin, $method);

			if(!$shipping_valid['success'])
				$notifications['errors'][] = JText::_('PLG_ZOOCART_ERROR_SELECT_SHIPPING_METHOD');

			$success = $success && $shipping_valid['success'];
		}else{
			$shipping_valid = array('success'=>true,'errors'=>true,'fee'=>0);
		}

		// Payment validation:
		$payment = $this->app->request->get('payment_method', 'string');
		$payment_valid = $this->_validatePayment($payment);

		if (!$payment_valid['success']) {
			$notifications['errors'][] = JText::_('PLG_ZOOCART_ERROR_SELECT_PAYMENT_METHOD');
		}

		$success = $success && $payment_valid['success'];
		$notifications['pfee'] = $payment_valid['fee'];

		// Taxes validation:
		if($zconfig->get('require_address')){
			$tax_address = 'billing'==$zconfig->get('billing_address_type')?$bill_addr:$ship_addr;
		}else{
			$tax_address = array();
		}

		if (array_key_exists('id', $data)) {
			$tax_address = $this->app->zoocart->table->addresses->get((int)$ship_addr['id']);
		} else {
			$tax_address = $this->app->zoocart->address->getFromValues($tax_address, $this->app->zoocart->getConfig()->get('billing_address_type', 'billing'));
		}

		$fee = $payment_valid['fee']+$shipping_valid['fee'];
		$taxes = $this->_validateTaxes($tax_address, $fee);
		$response =  array(
			'subtotal' => $this->app->zoocart->currency->format(null, $this->_validateSubtotal($fee)) ,
			'taxes' => $this->app->zoocart->currency->format(null, $taxes),
			'total' => $this->app->zoocart->currency->format(null, $this->_validateTotal($tax_address, $fee))
		);

		$notifications = array_merge_recursive($notifications,$response);

		// Quantities validation:
		$totals = array();
		$quantities = $this->app->request->get('quantity', 'array');
		if(!empty($quantities))
			foreach($quantities as $id => $quantity) {
				$cartitem = $this->app->zoocart->table->cartitems->get($id);
				$cartitem->quantity = $quantity;
				$this->app->zoocart->table->cartitems->save($cartitem);
				$totals[$id] = $this->app->zoocart->currency->format(null, $cartitem->getNetTotal()+($this->app->zoocart->tax->checkTaxEnabled()?$taxes:0));
			}

		$notifications['totals'] = $totals;

		// User validation:
		$login_data = $this->app->request->get('logindata', 'array');
		$register_data = $this->app->request->get('registerdata', 'array');

		$user_valid = $this->_validateUser($login_data, $register_data);

		$success = $success && $user_valid['success'];
		$notifications = array_merge_recursive($notifications,$user_valid);

		$notifications['success'] = $success;

		return $notifications;
	}

	/**
	 * Validate all incoming data
	 *
	 * @return bool
	 */
	public function validateAll()
	{
		echo json_encode($this->_validate());
	}
}

class OrderControllerException extends AppException {}