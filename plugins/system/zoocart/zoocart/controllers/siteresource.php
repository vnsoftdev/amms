<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class SiteResourceController extends ResourceController {

	public function __construct($default = array()) {
		parent::__construct($default);

		$this->application = $this->app->zoo->getApplication();

		// load bootstrap
		if ($this->app->zoocart->getConfig()->get('load_bootstrap', 1)) {
			$this->app->zlfw->zlux->loadBootstrap(true);
		}
	}

	public function getView($name = '', $type = '', $prefix = '', $config = array()) {

		$view = parent::getView($name, $type, $prefix, $config);
		$view->addTemplatePath($this->app->path->path('zoocart:views/site/'.$this->resource_name.'/tmpl'));
		$view->addTemplatePath($this->application->getTemplate()->getPath(). '/'.$this->resource_name.'/');

		return $view;
	}

	protected function getResources() {
		// Filters
		$state_prefix       = $this->option.'_'.$this->application->id.'.'.$this->resource_name;
		$per_page		    = $this->joomla->getCfg('list_limit');
		$page				= $this->app->request->getInt('page', 1);

		$count = $this->table->count();

		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($page - 1) * $per_page;
		
		$options = $per_page > 0 ? array('offset' => $limitstart, 'limit' => $per_page, 'order' => $this->default_order) : array('order' => $this->default_order);

		if(property_exists($this->resource_class, 'user_id')) {
			$options['conditions'] = 'user_id = ' . (int) $this->app->user->get()->id;
		}

		return $this->table->all($options);
	}

	public function display($cachable = false, $urlparams = false) 
	{
		parent::display($cachable, $urlparams );

		// Filters
		$per_page		    = $this->joomla->getCfg('list_limit');
		$page				= $this->app->request->getInt('page', 1);

		$count = $this->table->count();
	
		// Vars
		$this->pagination = $this->app->pagination->create($count, $page, $per_page, 'page', 'bootstrap');
		$this->pagination->setShowAll($per_page == 0);
		$this->pagination_link = $this->app->link(array('controller' => $this->controller, 'task' => 'display', 'app_id' => $this->application->id));
		$this->resources = array_merge($this->getResources());

		// Display
		$this->getView()->setLayout('default')->display();
	}

}