(function($){

    var Plugin = function(){};

    $.extend(Plugin.prototype, {

        name: 'AddToCart',

		options: {
			redirectUrl: '',
			updateModuleUrl: '',
			moduleContainer: '.zoocart-smallcart',
			avoid_readd: true
		},

        initialize : function(element, options){
			this.options = $.extend({}, this.options, options);

			var $this = this;

			$(element).click(function(){
				$('button', $(element)).button('loading');
				$.when($this.submitAddToCart(element)).done(function(){
					$.when($this.updateCartModule()).done(function(){
						if($this.options.avoid_readd) {
							$('button', $(element)).button('complete');
							setTimeout(function(){
								$('button', $(element)).attr('disabled', 'disabled');
							}, 50);
						} else {
							$('button', $(element)).button('reset');
						}

						if($this.options.redirectUrl.length) {
							if($this.options.redirectUrl == '_reload_') {
								window.location.reload(true);
							} else {
								window.location.href = $this.options.redirectUrl;
							}
						}
					});
				});
				return false;
			});
		},

		submitAddToCart: function(element) {
			var form = $('form', $(element))
			var data = form.serialize() + '&format=raw';
			
			return $.ajax({
				url: form.attr('action'),
				data: data,
				type: 'post'
			})
		},

		updateCartModule: function() {

			if(!this.options.updateModuleUrl.length){
				return true;
			}

			var $this = this;

			if(!$($this.options.moduleContainer)) {
				return true;
			}

			var module_id = $($this.options.moduleContainer).attr('data-module-id');

			return $.ajax({
				url: this.options.updateModuleUrl,
				data: {
					module_id : module_id
				},
				type: 'get',
				dataType: 'json',
				success: function(data) {
					$($this.options.moduleContainer).html(data);
				}
			});
		}
			
    });

    // Don't touch
	$.fn[Plugin.prototype.name] = function() {

		var args   = arguments;
		var method = args[0] ? args[0] : null;

		return this.each(function() {
			var element = $(this);

			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();

				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}

				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}

		});
	};

})(jQuery);