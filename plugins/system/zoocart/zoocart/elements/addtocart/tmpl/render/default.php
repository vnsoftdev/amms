<?php
/**
* @package   com_zoo
* @author    ZOOlanders http://www.zoolanders.com
* @copyright Copyright (C) ZOOlanders.com
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$this->app->document->addStylesheet('zoocart:assets/css/bootstrap.min.css');
$this->app->document->addStylesheet('zoocart:assets/css/site.css');	
$this->app->document->addScript('zoocart:elements/addtocart/assets/js/addtocart.js');
$this->app->document->addScript('zoocart:assets/js/bootstrap.min.js');	

$check_quantities = $this->app->zoocart->getConfig()->get('check_quantities', true);
$itemid = $params->find('layout._itemid','');

switch($params->find('layout._action', 'cart')) {
	case 'reload':
		$action = '_reload_';
		break;
	case 'none':
		$action = '';
		break;
	case 'cart':
	default:
		$action = $this->app->link(array('Itemid'=>$itemid, 'controller' => 'cart', 'task' => 'display', 'app_id' => $this->getItem()->getApplication()->id), false);
		break;
}

$action_url = 'index.php?option=com_zoo&app_id='.$this->getItem()->getApplication()->id.'&controller=cart&task=addtocart';

$module_url = $params->find('layout._update_module', true) ? $this->app->link(array("controller" => "cart", "task" => "cartModule", "app_id" => $this->getItem()->getApplication()->id, "format" =>"raw"), false) : '';
$avoid_readd = $params->find('layout._avoid_readd', true);
$in_cart = (bool)$this->app->zoocart->table->cartitems->getByItemId($this->getItem()->id);
$complete_label = $params->find('layout._complete_label', '');
if(!$complete_label) {
	$complete_label = 'PLG_ZOOCART_ADDTOCART_COMPLETE_LABEL';
}

?>

<div class="zoocart-addtocart" id="zoocart-addtocart-<?php echo $this->identifier; ?>-<?php echo $this->getItem()->id; ?>">
	<?php if (!$check_quantities || ($check_quantities && $this->app->zoocart->quantity->getQuantity($this->getItem()))): ?>
	<form method="POST" name="zoocart-addtocart" action="<?php echo JRoute::_($action_url);?>" class="form">
		<button type="submit" class="btn btn-success"  <?php echo ($in_cart && $avoid_readd) ? 'disabled="disabled"' : ''; ?> data-complete-text="<?php echo JText::_($complete_label); ?>" data-loading-text="<?php echo JText::_('PLG_ZOOCART_ADDTOCART_ADDING'); ?>">
			<i class="icon-shopping-cart icon-white"></i>
			<?php echo ($avoid_readd && $in_cart) ? JText::_($complete_label) : $params->find('layout._label', JText::_('PLG_ZOOCART_ADDTOCART_ADD_TO_CART')); ?>
		</button>
		<input type="hidden" name="item_id" value="<?php echo $this->getItem()->id; ?>" />
		<input type="hidden" name="quantity" value="1" />
	</form>
	<?php else : ?>
	<?php echo JText::_('PLG_ZOOCART_ADDTOCART_PRODUCT_NOT_AVAILABLE'); ?>
	<?php endif; ?>
</div>

<?php if(!$avoid_readd || ($avoid_readd && !$in_cart)) : ?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-addtocart-<?php echo $this->identifier; ?>-<?php echo $this->getItem()->id; ?>').AddToCart({
		redirectUrl: '<?php echo $action; ?>',
		updateModuleUrl: '<?php echo $module_url; ?>',
		avoid_readd: <?php echo $avoid_readd; ?>
	});
});
</script>
<?php endif; ?>