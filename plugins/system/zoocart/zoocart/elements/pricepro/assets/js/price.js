/* Copyright (C) 2007 - 2011 ZOOlanders.com - Inspired by YOOtheme GmbH code */

(function ($) {
    var b = function () {};
    $.extend(b.prototype, {
        name: "EditElementPrice",
        options: {
			symbol: '$',
			decimalsAmount: 2,
			url: ''
        },
        initialize: function (b, c) {
            this.options = $.extend({}, this.options, c);
			var d = this;
			
            b.delegate("p.add a", "click", function () {
				d.apply(b.find("input"));
            });
			
			d.apply(b.find("input.input-price"));
        },
		apply: function (inputs) {
			var op = this.options;
			var $this = this;
			inputs.each( function() {
				
				var input = $(this);
				var pretty = input.next('.input-price-pretty');
				
				// show the price
				input.formatCurrency(pretty, {region: 'es-ES', symbol: op.symbol});
			
				if ( !$(this).data("initialized-price") ){
				
					pretty.numeric({allow:'.,'}).blur(function() {
						// sync empty value
						if (pretty.val() == '') {
							input.val('');
						} else {
							pretty.formatCurrency({
								region: 'es-ES',
								symbol: op.symbol,
								roundToDecimalPlace: op.decimalsAmount
							});
							var input_newval = pretty.asNumber({ region: 'es-ES' });
							input.val(input_newval);
							$this.writeCalculateGross(input_newval, input);
						}
					});
					
					pretty.click(function() {
						$(this).formatCurrency({
							region: 'es-ES',
							symbol: '',
							roundToDecimalPlace: op.decimalsAmount
						}).val($.trim($(this).val()));
					});
					
				} $(this).data("initialized-price", !0);
			});
		},
		writeCalculateGross: function(net, origin) {
			var target = origin.parent().find('.input-price-gross');
			this.calculateGross(net, function(result){
				target.html(result);
			});
		},
		calculateGross: function(net, callback) 
		{
			$.ajax({
				url: this.options.url,
				type: 'GET',
				dataType: 'json',
				data:{
					net_price: net
				},
				success: function(data){
					callback(data.gross);
				}
			})
		}
    });
    $.fn[b.prototype.name] = function () {
        var e = arguments,
            c = e[0] ? e[0] : null;
        return this.each(function () {
            var d = $(this);
            if (b.prototype[c] && d.data(b.prototype.name) && c != "initialize") d.data(b.prototype.name)[c].apply(d.data(b.prototype.name), Array.prototype.slice.call(e, 1));
            else if (!c || $.isPlainObject(c)) {
                var f = new b;
                b.prototype.initialize && f.initialize.apply(f, $.merge([d], e));
                d.data(b.prototype.name, f)
            } else $.error("Method " + c + " does not exist on jQuery." + b.name)
        })
    }
})(jQuery);