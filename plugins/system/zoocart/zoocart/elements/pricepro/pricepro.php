<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class ElementPricePro extends ElementRepeatablePro implements iRepeatSubmittable {

	public $currency = null;

	public function __construct() {

		// call parent constructor
		parent::__construct();

		$this->registerCallback('getGrossPriceAjax');

	}

	public function getGrossPrice($net_price = null) {
		if ($net_price == null) {
			$net_price = $this->get('value');
		}
		return $this->app->zoocart->price->getGrossPrice($this->getItem()->getApplication()->id, $net_price);

	}

	public function getGrossPriceAjax() {
		$net_price = $this->app->request->get('net_price', '');
		return json_encode(array('gross' => $this->getGrossPrice($net_price)));
	}
	
	/*
		Function: hasValue
			Checks if the element's value is set.

	   Parameters:
			$params - AppData render parameter

		Returns:
			Boolean - true, on success
	*/
	public function hasValue($params = array()) {
		
		foreach($this as $self) {
			return $this->_hasValue($params);
		}
		return false;
	}
	
	/*
		Function: _hasValue
			Checks if the repeatables element's value is set.

	   Parameters:
			$params - AppData render parameter

		Returns:
			Boolean - true, on success
	*/
	protected function _hasValue($params = array()) {
		$value = $this->get('value', $this->config->find('specific._default'));
		return !empty($value);
	}

	/*
	   Function: _edit
	       Renders the repeatable edit form field.

	   Returns:
	       String - html
	*/
	protected function _edit() {

        if ($layout = $this->getLayout('edit/_edit.php')) {
            return $this->renderLayout($layout,
                array(
					'default' => $this->config->find('specific._default'),
					'value' => $this->get('value')
                )
            );
        }
	}

	public function edit() {

		if(!$this->app->zoocart->informer->checkEnabled())
		{
			$this->app->zoocart->informer->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_ENABLE_TO_USE_ELEMENTS'), $this->app->zoocart->informer->_getZooLink('controller=configuration')),'print_once');
			$this->app->zoocart->informer->popout('print_once');
		}

		$this->currency = $this->app->zoocart->table->currencies->get($this->config->find('specific._currency', $this->app->zoocart->getConfig()->get('default_currency')));
		return parent::edit();
	}
	
	/*
		Function: loadAssets
			Load elements css/js assets.

		Returns:
			Void
	*/
	public function loadAssets() {
		
		parent::loadAssets();
		$this->app->document->addScript('elements:pricepro/assets/js/jquery.alphanumeric.min.js');
		$this->app->document->addScript('elements:pricepro/assets/js/formatCurrency/jquery.formatCurrency.min.js');
		$this->app->document->addScript('elements:pricepro/assets/js/price.js');
		$this->app->document->addStylesheet('elements:pricepro/assets/css/price-admin.css');
		return $this;
	}
	
	/*
		Function: render
			Renders the element.

	   Parameters:
            $params - AppData render parameter

		Returns:
			String - html
	*/
	public function render($params = array()) {

		$this->currency = $this->app->zoocart->table->currencies->get($this->config->find('specific._currency'), $this->app->zoocart->getConfig()->get('default_currency'));

		$params = $this->app->data->create($params);
		// render layout
		if ($layout = $this->getLayout($params->find('layout._layout', 'default.php'))) {
			return $this->renderLayout($layout, compact('params'));
		}
	}

	/*
		Function: render
			Renders the repeatable element.

	   Parameters:
            $params - AppData render parameter

		Returns:
			String - html
	*/
	protected function _render($params = array()) {
	
		if ($this->app->zoocart->getConfig()->get('show_price_with_tax', 1)) {
			$price = $this->getGrossPrice();
		} else {
			$price = $this->get('value', 0);
		}
		
		return $this->formatNumber($price, $params);
	}
	
	/*
		Function: renderSubmission
			Renders the element in submission.

	   Parameters:
            $params - submission parameters

		Returns:
			String - html
	*/
	public function formatNumber($price, $params) {
        return $this->app->zoocart->currency->format($this->_item->getApplication()->id, $price, $this->currency);
	}
	
	/*
		Function: renderSubmission
			Renders the element in submission.

	   Parameters:
            $params - submission parameters

		Returns:
			String - html
	*/
	public function _renderSubmission($params = array()) {
        return $this->edit();
	}
	
	/*
		Function: _renderRepeatable
			Renders the repeatable

		Returns:
			String - output
	*/
    protected function _renderRepeatable($function, $params = array()) {
		return $this->renderLayout($this->app->path->path("elements:pricepro/tmpl/edit/edit.php"), compact('function', 'params'));
    }
	
	/*
		Function: getConfigForm
			Get parameter form object to render input form.

		Returns:
			Parameter Object
	*/
	public function getConfigForm() {
		
		$form = parent::getConfigForm();
		$form->addElementPath($this->app->path->path( 'zoocart:fields'));

		return $form;
	}

}