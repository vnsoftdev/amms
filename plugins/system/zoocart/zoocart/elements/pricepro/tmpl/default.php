<?php
/**
* @package   com_zoo
* @author    ZOOlanders http://www.zoolanders.com
* @copyright Copyright (C) ZOOlanders.com
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
	
	$result = array();
	foreach ($this as $self) {
		$result[] = $this->_render($params);
	}

	echo $this->app->element->applySeparators($params->get('separated_by'), $result);
	
?>