<?php
/**
* @package   com_zoo
* @author    ZOOlanders http://www.zoolanders.com
* @copyright Copyright (C) ZOOlanders.com
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$link = $this->app->link(array('task' => 'callelement', 'format' => 'raw', 'type' => $this->_item->getType()->id, 'item_id' => $this->_item->id, 'elm_id' => $this->identifier), false);
?>

<?php if ($this->config->get('repeatable')) : ?>

	<div id="<?php echo $this->identifier; ?>" class="repeat-elements">
		<ul class="repeatable-list">

			<?php $this->rewind(); ?>
			<?php foreach($this as $self) : ?>
				<li class="repeatable-element">
					<?php echo $this->$function($params); ?>
				</li>
			<?php endforeach; ?>

			<?php $this->rewind(); ?>

			<li class="repeatable-element hidden">
				<?php echo preg_replace('/(elements\[\S+])\[(\d+)\]/', '$1[-1]', $this->$function($params)); ?>
			</li>

		</ul>
		<p class="add">
			<a href="javascript:void(0);"><?php echo JText::_('Add another'); ?></a>
		</p>

	</div>
	
	<script type="text/javascript">
		jQuery('#<?php echo $this->identifier; ?>').ElementRepeatable({ msgDeleteElement : '<?php echo JText::_('PLG_ZOOCART_DELETE_ELEMENT'); ?>', msgSortElement : '<?php echo JText::_('Sort Element'); ?>' });
		jQuery('#<?php echo $this->identifier; ?>').EditElementPrice({ symbol: '<?php echo $this->currency->symbol_left; ?>', decimalsAmount: <?php echo $this->currency->num_decimals; ?> });
	</script>
	
<?php else : ?>

	<div id="<?php echo $this->identifier; ?>" class="repeatable-element">
		<?php echo $this->$function($params); ?>
	</div>

	<script type="text/javascript">
		jQuery('#<?php echo $this->identifier; ?>').EditElementPrice({ symbol: '<?php echo $this->currency->symbol_left; ?>', decimalsAmount: <?php echo $this->currency->num_decimals; ?>,  url: '<?php echo $this->app->link(array('task' => 'callelement', 'format' => 'raw', 'type' => $this->_item->getType()->id, 'item_id' => $this->_item->id, 'elm_id' => $this->identifier, 'method' => 'getGrossPriceAjax'), false); ?>' });
	</script>

<?php endif; ?>