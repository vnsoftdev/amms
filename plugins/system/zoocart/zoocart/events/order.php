<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class OrderEvent {

	public static function saved($event, $args = array()) {
		
		$app = App::getInstance('zoo');

		$order = $event->getSubject();
		$new = $event['new'];
		$old_state = $event['old']->state;

		if ($event['old']->id) {
			// Calculate the payment method fee
			if ($event['old']->payment_method != $order->payment_method ) {
				JPluginHelper::importPlugin('payment', $order->payment_method);
				$dispatcher = JDispatcher::getInstance();
				$result = array_sum($dispatcher->trigger('getPaymentFee'));
				$order->payment = $result;
			}

			// Calculate totals if somthing has changed
			if ($event['old']->getTotal(true) != $order->getTotal(true)) {
				$app->zoocart->table->orders->save($order);
			}
		}

		// Deal with quantities
		$config = $app->zoocart->getConfig();
		$update_state = $config->get('quantity_update_state');
		
		// Completed order -> remove quantity
		if (($order->state == $update_state ) && ($old_state != $order->state ) && $config->get('update_quantities', false)) {
			foreach($order->getItems() as $item) {
				$app->zoocart->quantity->removeQuantity($item->item_id, $item->quantity);
			}
		}

		// Replenish on reverting order state
		if (($order->state == $config->get('canceled_orderstate')) && ($old_state != $order->state) && $config->get('update_quantities', false)) {
			foreach($order->getItems() as $item) {
				$app->zoocart->quantity->addQuantity($item->item_id, $item->quantity);
			}
		}

		// Store order change in history
		$history = $app->object->create('Orderhistory');
		$history->state = $order->state;
		$history->order_id = $order->id;
		$app->zoocart->table->orderhistories->save($history);

		// Now deal with notification emails
		if (!$new && $old_state != $order->state && $app->request->getBool('notify_user', 0)) {
			$app->zoocart->order->sendNotificationMail($order, 'mail.order.statechange.php');
		}

		// New order notification
		if ($new) {
			$app->zoocart->order->sendNotificationMail($order, 'mail.order.new.php');
		}
	}

}
