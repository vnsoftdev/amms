<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class ZoocartHelper extends AppHelper {

	/* prefix */
	protected $_prefix;

	/* models */
	protected $_helpers = array();

	/*
		Function: __construct
			Class Constructor.
	*/
	public function __construct($app) {
		parent::__construct($app);

		// set helper prefix
		$this->_prefix = 'zoocart';
	}

	/*
		Function: get
			Retrieve a helper

		Parameters:
			$name - Helper name
			$prefix - Helper prefix

		Returns:
			Mixed
	*/
	public function get($name, $prefix = null) {
		
		// set prefix
		if ($prefix == null) {
			$prefix = $this->_prefix;
		}

		// load class
		$class = $prefix . $name . 'Helper';
		
		$this->app->loader->register($class, 'zoocart:helpers/zoocart/'.strtolower($name).'.php');

		// add helper, if not exists
		if (!isset($this->_helpers[$name])) {
			$this->_helpers[$name] = class_exists($class) ? new $class($this->app) : new AppHelper($this->app, $class);
		}

		return $this->_helpers[$name];
	}
	
	/*
		Function: __get
			Retrieve a helper

		Parameters:
			$name - Helper name

		Returns:
			Mixed
	*/
	public function __get($name) {
		return $this->get($name);
	}

	/**
	 * Retrieve ZOOcart config
	 * 
	 * @param string $app_id The App id, optional
	 * 
	 * @return object The config data wrapped with DATA class
	 */
	static $config = array();
	public function getConfig($app_id = null) {
		
		// set Application object or use current one
		$application = $app_id ? $this->app->table->application->get($app_id) : $this->app->zoo->getApplication();

		// basic check
		if (!$application) return false;

		// save retrieved config in cache
		if(!isset(self::$config[$app_id])) {
			self::$config[$app_id] = $this->app->data->create($application->getParams()->get('global.zoocart.'));
		}

		// return config
		return self::$config[$app_id];
	}

	/**
	 * Set ZOOcart config
	 * 
	 * @param string $app_id The App id, optional
	 * @param array $config The config data to be set
	 * 
	 * @return boolean True on success
	 */
	public function setConfig($app_id = null, $config = array()) {

		// set Application object or use current one
		$application = $app_id ? $this->app->table->application->get($app_id) : $this->app->zoo->getApplication();

		// basic check
		if (!$application) return false;

		// wrapp the config with DATA class
		if (is_array($config)) {
			$config = $this->app->data->create($config);
		}

		// save config
		self::$config[$app_id] = $config;
		$application->params->set('global.zoocart.', $config);
		$this->app->table->application->save($application);

		return true;
	}

	public function getElementsList($app_groups = array(), $type_ids = array(), $element_type = null, $name = 'zoocart', $selected = '', $add_none = false ) {

		$list = $this->app->zlfield->elementsList($app_groups, $element_type, $type_ids);

		$options = array();
		if ($add_none) {
			$options[] = $this->app->html->_('select.option', '', JText::_('PLG_ZOOCART_NONE'));
		}

		foreach($list as $label => $id) {
			$options[] = $this->app->html->_('select.option', $id, $label);
 		}

		return $this->app->html->_('select.genericlist', $options, $name, '', 'value', 'text', $selected);
	}

	public function orderstatesList($name, $selected, $attribs = '', $add_none = false) {

		$options = array();		

		if ($add_none) {
			$options[] = $this->app->html->_('select.option', '', '- ' . JText::_('PLG_ZOOCART_SELECT_STATE') . ' -');
		}

		foreach ($this->app->zoocart->table->orderstates->all() as $orderstate) {
			$options[] = $this->app->html->_('select.option', $orderstate->id, JText::_($orderstate->name));
		}

		return $this->app->html->_('select.genericlist', $options, $name, $attribs, 'value', 'text', $selected);
	}

	public function userGroupsList($name, $selected, $attribs = '')
	{
		// Initialise variables.
		$db		= $this->app->database;
		$query	= $db->getQuery(true)
			->select('a.id AS value, a.title AS text, COUNT(DISTINCT b.id) AS level, a.parent_id')
			->from('#__usergroups AS a')
			->leftJoin('`#__usergroups` AS b ON a.lft > b.lft AND a.rgt < b.rgt')
			->group('a.id')
			->order('a.lft ASC');

		$db->setQuery($query);
		$options = $db->loadObjectList();

		return $this->app->html->_('select.genericlist', $options, $name, $attribs, 'value', 'text', $selected);
	}

	/**
	 * Check if a product has been purchased
	 * 
	 * @param array $items_id The list of Items ID to be checked
	 * @param int $user_id The user ID to check the order from
	 * @param int $mode The query where mode. 0=OR / 1=AND
	 * 
	 * @return boolean True on success
	 */
	public function hasPurchased($items_id, $user_id = null, $mode = 0)
	{
		// validate data
		settype($items_id, 'array');
		settype($user_id, 'int');

		if (is_null($user_id)) {
			$user_id = $this->app->user->get()->id;
		}

		// init vars
		$db	= $this->app->database;
		$state = 5; // order completed state
		$mode = $mode ? 'AND' : 'OR';

		// set wheres
		$wheres = array();
		foreach ($items_id as $id) {
			$wheres[] = 'oi.item_id = ' . $id;
		}

		// set query
		$query = $db->getQuery(true)
			->select('COUNT(*)')
			->from('`#__zoo_zl_zoocart_orders` AS o')
			->leftJoin('`#__zoo_zl_zoocart_orderitems` AS oi ON oi.order_id = o.id')
			->where('o.state = '.$state.' AND o.user_id = '.$user_id.' AND (' . implode(" $mode ", $wheres) . ')')
			->group('order_id');
		$db->setQuery($query);

		// query
		$result = $db->loadObjectList();

		return !empty($result);
	}
}