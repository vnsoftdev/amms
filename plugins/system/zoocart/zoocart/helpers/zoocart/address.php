<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartAddressHelper extends AppHelper {

	static $billing_info = array();

	/**
	 * Get the Item Net price
	 * 
	 * @param object $item The Item
	 * 
	 * @return float The Net price
	 */
	public function validate($address, $type) {

		$layout = $type . '-form';
		$elements_config = $this->app->zoocart->address->getAddressConfig($layout);
		$billing_info = $this->getBillingInfo();

		// If Addresses is not required in the system, always return positive check result:
		if(!$this->app->zoocart->getConfig()->get('require_address',1))
			return array('success' => true, 'errors'=>array(), 'notices'=>array());

		$errors = array();
		$notices = array();

		$element_fields = $elements_config->get('fields');
		
		if ($element_fields) {
			foreach ($element_fields as $element_data) {

				try {
					if (($element = $address->getElement($element_data['element']))) {

						// get paramss
						$params = $this->app->data->create($element_data);
						$element->validateSubmission($this->app->data->create($address->getElement($element->identifier)->data()), $params);

						// Validate also more specific things
						$billing_type = $this->app->zoocart->getConfig()->get('billing_address_type', 'billing');
						if($type == $billing_type) {
							if ($key = array_search($element->identifier, $billing_info)) {
								$method = "_validate" . ucfirst(strtolower($key));
								if(method_exists($this, $method)) {
									$check = $this->$method($address, $element->identifier);
									if (!$check['success']) {
										$errors = array_merge($errors, $check['errors']);
									}
									$notices = array_merge($notices, $check['notices']);
								}
							}
						}
					}

				} catch (AppValidatorException $e) {
					$errors[$element->identifier] = (string) $e;
				}
			}
		}

		$success = true;
		if (count($errors)) {
			$success = false;
		}

		return array('success' => $success, 'errors' => $errors, 'notices' => $notices);
	}

	/**
	 * Get Billing info
	 * 
	 * @return object The info
	 */
	public function getBillingInfo() {

		if (!self::$billing_info) {
			
			$address = $this->getAddressType();

			self::$billing_info = array(
				'name' => '',
				'address' => '',
				'company' => '',
				'country' => '',
				'state' => '',
				'city' => '',
				'zip' => '',
				'vat' => '',
				'personal_id' => '',
				'other' => ''
			);

			$elements = $address->getElements();
			foreach($elements as $key => $element) {
				$key = $element->config->get('billing', '');
				if ($key) {
					self::$billing_info[$key] = $element->identifier;
				}
			}
		}
		
		return self::$billing_info;
	}

	/**
	 * Get the Address data
	 *
	 * @param object $address The Address Object
	 * 
	 * @return array The data
	 */
	public function getAddressData($address) {
		$billing_info = $this->getBillingInfo();
		$result = array();
		foreach($billing_info as $key => $info) {
			if($key == 'country') {
				$result[$key] = array_shift($address->getElement($info)->get('country'));
			} else {
				$value = $address->getElement($info)->data();
				while(is_array($value)) {
					$value = array_shift($value);
				}
				$result[$key] = $value;
			}
		}

		return $result;
	}

	/**
	 * Get from values
	 *
	 * @param object $address The Address Object
	 * @param string $type The Address Type
	 * 
	 * @return object The From values
	 */
	public function getFromValues($address, $type = 'billing') {
		$addr = $this->app->object->create('Address');
		$addr->type = $type;

		// bind element data
		$addr->elements = $this->app->data->create();
		foreach ($addr->getElements() as $id => $element) {
			if (isset($address[$id])) {
				$element->bindData($address[$id]);
			} else {
				$element->bindData();
			}
		}

		return $addr;
	}

	/**
	 * Get the Address Type
	 * 
	 * @param object $application The App object
	 * 
	 * @return string The Address type
	 */
	public function getAddressType($application = null) {

		// register AddressType
		$this->app->loader->register('AddressType', 'zoocart:classes/addresstype.php');

		$application = $application ? $application : $this->app->zoo->getApplication();
		$address = $this->app->object->create('AddressType', array('address', $application));

		if (($file = $address->getConfigFile()) && JFile::exists($file)) {
			$address->config = $this->app->data->create(JFile::read($file));
		} else {
			$address->config = $this->app->data->create();
		}

		$address->name = JText::_('PLG_ZOOCART_ADDRESS');
		return $address;
	}

	/**
	 * Get the Address config data
	 * 
	 * @param string $layout The Layout name
	 * 
	 * @return array The Config Data
	 */
	public function getAddressConfig($layout) {
		$template = $this->app->zoo->getApplication()->getTemplate();
		$address_renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $template->getPath(), $this->app->path->path('zoocart:')));

		return $address_renderer->getConfigPositions('address', $layout);
	}
}