<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartCartHelper extends AppHelper {

	static $cartitems = array();

	/**
	 * Get the cart total net price
	 * 
	 * @param int $user_id The user id to get the price from
	 * 
	 * @return float The total net price
	 */
	public function getNetTotal($user_id = null) {

		if(!self::$cartitems){
			self::$cartitems = $this->app->zoocart->table->cartitems->getByUser($user_id);
		}		

		$subtotal = 0;
		foreach (self::$cartitems as $cartitem) {
			$item = $this->app->table->item->get($cartitem->item_id);
			$subtotal += $this->app->zoocart->price->getItemNetPrice($item) * $cartitem->quantity;
		}

		return $subtotal;
	}

	/**
	 * Clear the cart items
	 * 
	 * @param int $user_id The user id
	 * 
	 * @return boolean True on success
	 */
	public function clear($user_id = null) {
		$session_id = $this->app->session->getId();
		$user_id = ($user_id  === null) ? $this->app->user->get()->id : $user_id;

		$options = "user_id = " . (int) $user_id;
		if (!$user_id) {
			$options .= " AND session_id = ".$this->app->database->q($session_id);
		}


		if(!self::$cartitems){
			self::$cartitems = $this->app->zoocart->table->cartitems->getByUser($user_id);
		}

		foreach (self::$cartitems as $cartitem) {
			$this->app->zoocart->table->cartitems->delete($cartitem);
		}

		return true;
	}

	/**
	 * Get the cart sub total price
	 * 
	 * @param int $user_id The user id
	 * 
	 * @return float The subtotal net price
	 */
	public function getSubtotal($user_id = null) {
		return $this->getNetTotal($user_id);
	}

	/**
	 * Get the cart taxes price
	 * 
	 * @param int $user_id The user id
	 * @param object $address The address info
	 * 
	 * @return float The taxes price
	 */
	public function getTaxes( $user_id = null, $address = null ) {

		if(!self::$cartitems){
			self::$cartitems = $this->app->zoocart->table->cartitems->getByUser($user_id);
		}

		$taxes = 0;
		foreach (self::$cartitems as $cartitem) {
			$item = $this->app->table->item->get($cartitem->item_id);
			$taxes += $this->app->zoocart->tax->getItemTaxes($item, $user_id, $address) * $cartitem->quantity;
		}

		return $taxes;
	}

	/**
	 * Get the cart total price
	 * 
	 * @param int $app_id The app id
	 * @param int $user_id The user id
	 * @param object $address The address info
	 * 
	 * @return float The total price
	 */
	public function getTotal($app_id = null, $user_id = null, $address = null ) {

		// init vars
		$total = $this->getSubtotal($user_id);
		$total += $this->getTaxes($user_id, $address);

		return  $total;
	}
}