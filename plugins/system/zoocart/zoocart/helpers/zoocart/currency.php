<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartCurrencyHelper extends AppHelper {

	/**
	 * Get the currencies list HTML field
	 * 
	 * @param string $name The field name value
	 * @param array $selected The selected options
	 * 
	 * @return string The HTML field
	 */
	public function currenciesList($name, $selected) {

		$options = array();		

		foreach ($this->app->zoocart->table->currencies->all() as $currency) {
			$options[] = $this->app->html->_('select.option', $currency->id, JText::_($currency->name));
		}

		$attribs = '';

		return $this->app->html->_('select.genericlist', $options, $name, $attribs, 'value', 'text', $selected);
	}

	/**
	 * Format currency
	 * 
	 * @param int $price The price to be formated
	 * @param object $currency The currency object, optional
	 * @param string $app_id The App id, optional
	 * 
	 * @return float The formated price with provided currency
	 */
	public function format($app_id = null, $price, $currency = null) {

		// use default currency if none provided
		if (!$currency) {
			$currency = $this->getDefaultCurrency($app_id);
		}

		// if currency set
		if (is_object($currency)) {
			return $currency->symbol_left . number_format($price, $currency->num_decimals_show, $currency->decimal_sep, $currency->thousand_sep) . $currency->symbol_right;

		} else {
			// return basic format
			return number_format($price, 2, ',', '');
		}
	}

	/**
	 * Retrieve the default currency
	 *
	 * @param string $app_id The App id
	 * 
	 * @return object The currency object
	 */
	public function getDefaultCurrency($app_id = null) {
		return $this->app->zoocart->table->currencies->get($this->app->zoocart->getConfig($app_id)->get('default_currency'));
	}
}