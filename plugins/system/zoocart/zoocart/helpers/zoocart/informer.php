<?php
/**
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Class InformerHelper
 * Implements backend tips system to help users configure zoocart
 */
class zoocartInformerHelper extends AppHelper {

	/**
	 * @var array Array of internal notifications
	 */
	protected $_messages = array();

	/**
	 * @var null Current application object
	 */
	protected $_application = null;

	/**
	 * @var Flag for Print Once
	 */
	protected static $_once = false;

	/**
	 * Class instance constructor
	 *
	 * @param App $app
	 * @internal param \Application $Object object
	 * @return \InformerHelper
	 */
	public function __construct($app)
	{
		parent::__construct($app);

		$this->_application = $this->app->zoo->getApplication();
		$this->checkCfg();

		return $this;
	}

	/**
	 * Genarate internal link
	 *
	 * @param string $query
	 * @param bool Set true to not include changeapp to query
	 * @return string
	 */
	public function _getZooLink($query='',$no_app_id=false)
	{
		$url = 'index.php?option=com_zoo';
		if(!empty($this->_application) && !$no_app_id)
			$url .= '&changeapp='.(int)$this->_application->id;
		if(!empty($query))
			$url .= '&'.$query;

		return JRoute::_($url,false);
	}

	/**
	 * Pop out first tip from message queue
	 *
	 * @return string Tip message
	 */
	public function popout($area='default')
	{
		$msg = '';

		if(!empty($this->_messages[$area]))
			$msg = array_shift($this->_messages[$area]);

		if('print_once'==$area)
		{
			if(self::$_once)
				return;

			$joomla = JFactory::getApplication();
			$joomla->enqueueMessage($msg,'notice');

			self::$_once = true;
			return;
		}

		return $msg;
	}

	/**
	 * Add new tip message to stack
	 *
	 * @param $message
	 * @return object
	 */
	public function enqueue($message, $area='default')
	{
		if($message)
			$this->_messages[$area][] = $message;

		return $this;
	}

	/**
	 * Check configuration
	 */
	public function checkCfg()
	{
		$billing_addr_layouts = array('billing','billing-form');
		$shipping_addr_layouts = array('shipping','shipping-form');

		if(!$this->_application)
			return;

		// Check type config:
		$types = $this->_application->getTypes();

		if(!empty($types))
		{
			$qty_check = false;
			$price_check = false;
			$atc_check = false;
			$required_elements = array();

			$price_elements_count = 0;
			$quantity_elements_count = 0;
			$multielem_types = array();

			foreach($types as $type)
			{
				$price_elements = $this->app->zlfield->elementsList($this->_application->getGroup(), 'pricepro', $type->identifier);
				//@TODO: for ZOO versions over 3.1.1 could be replaced with $type->getElementsByType(...);

				$price_check = $price_check || !empty($price_elements);
				$price_elements_count = count($price_elements);

				$quantity_elements = $this->app->zlfield->elementsList($this->_application->getGroup(), 'quantity', $type->identifier);
				//@TODO: for ZOO versions over 3.1.1 could be replaced with $type->getElementsByType(...);

				$qty_check = $qty_check || !empty($quantity_elements);
				$quantity_elements_count = count($quantity_elements);

				$addtocart_elements = $this->app->zlfield->elementsList($this->_application->getGroup(), 'addtocart', $type->identifier);
				//@TODO: for ZOO versions over 3.1.1 could be replaced with $type->getElementsByType(...);

				$atc_check = $atc_check || !empty($addtocart_elements);

				// Check if type has more than one zoocart elements:
				if(($price_elements_count>1) || ($quantity_elements_count>1))
				{
					$multielem_types[] = '<b>'.ucfirst($type->identifier).'</b>';
				}
			}

			if(!empty($multielem_types))
				$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_CHECK_TYPE_CFG'),implode(', ',$multielem_types),$this->_getZooLink('controller=manager&task=types&group='.$this->_application->getGroup(),true)));

			// Set list or required elements:
			if(!$price_check)
				$required_elements[] = '<b>'.JText::_('PLG_ZOOCART_INFORMER_PRICEPRO').'</b>';
			if(!$qty_check)
				$required_elements[] = '<b>'.JText::_('PLG_ZOOCART_INFORMER_QUANTITYPRO').'</b>';
			if(!$atc_check)
				$required_elements[] = '<b>'.JText::_('PLG_ZOOCART_INFORMER_ADDTOCART').'</b>';

			if(!$qty_check || !$price_check || !$atc_check){
				$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_TYPE_CFG'),implode(', ',$required_elements),$this->_getZooLink('controller=manager&task=types&group='.$this->_application->getGroup(),true)));
			}
		}

		// Check tax classes:
		$tc = $this->app->zoocart->table->taxclasses->all();
		if(empty($tc)){
			$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_TAXCLASSES'),$this->_getZooLink('controller=taxclasses')));
		}

		// Check currencies:
		$cc = $this->app->zoocart->table->currencies->all();
		if(empty($cc)){
			$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_CURRENCIES'),$this->_getZooLink('controller=currencies')));
		}

		// Check payment plugins installed and enabled:
		$pplugins = JPluginHelper::isEnabled('zoocart_payment');
		if(empty($pplugins)){
			$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_PAYMENT_PLUGINS'),JRoute::_('index.php?option=com_installer'),JRoute::_('index.php?option=com_plugins')));
		}

		if($this->app->zoocart->getConfig()->get('require_address',1))
		{
			// Check address type configured
			$address = $this->app->zoocart->address->getAddressType($this->_application);
			$addr = $address->getElements();
			if(empty($addr)){
				$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_ADDRESS'),$this->_getZooLink('controller=addresses&task=editelements&cid[]=address')));
			}

			// Check address layouts configured:
			$renderer = $this->app->renderer->create('address')->addPath(JPATH_ROOT.'/plugins/system/zoocart/zoocart');
			$layouts = $billing_addr_layouts;

			if($this->app->zoocart->getConfig()->get('enable_shipping', true))
				$layouts = array_merge($layouts,$shipping_addr_layouts);
			$lc = false;

			foreach($layouts as $layout)
			{
				$config = $renderer->getConfig('address')->get($this->_application->getGroup().'.'.$this->_application->id.'.address.'.$layout);
				$lc = $lc || empty($config);
			}

			if($lc){
				$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_ADDRESS_LAYOUTS'),$this->_getZooLink('controller=addresses')));
			}
		}

		// Check shipping plugins installed and enabled and shipping rates configured on shipping option is ON:
		if($this->app->zoocart->getConfig()->get('enable_shipping', true)){
			// Shipping plugins check:
			$splugins = JPluginHelper::isEnabled('zoocart_shipping');
			if(empty($splugins)){
				$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_SHIPPING_PLUGINS'),JRoute::_('index.php?option=com_installer'),JRoute::_('index.php?option=com_plugins')));
			}

			// Shipping rates check:
			$sr = $this->app->zoocart->table->shippingrates->all();
			if(empty($sr)){
				$this->enqueue(sprintf(JText::_('PLG_ZOOCART_INFORMER_SET_SHIPPING_RATES'),$this->_getZooLink('controller=shippingrates')));
			}
		}
	}

	/**
	 * Check if Zoocart enabled
	 */
	public function checkEnabled()
	{
		return $this->app->zoocart->getConfig()->get('enable_cart');
	}
}