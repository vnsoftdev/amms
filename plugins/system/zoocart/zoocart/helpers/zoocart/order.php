<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartOrderHelper extends AppHelper {
	
	/**
	 * Send notification email
	 *
	 * @param object $order The Order object
	 * @param string $layout The layout
	 *
	 * @return boolean True on success
	 */
	public function sendNotificationMail($order, $layout) {

		// workaround to make sure JSite is loaded
		$this->app->loader->register('JSite', 'root:includes/application.php');

		// init vars
		$website_name = $this->app->system->application->getCfg('sitename');
		$order_link = JURI::root().'index.php?'.http_build_query(array(
				'option' => $this->app->component->self->name,
				'controller' => 'orders',
				'task' => 'view',
			    'app_id'=>$this->app->zoo->getApplication()->id,
				'id' => $order->id,
			), '', '&');

		$user = $this->app->user->get($order->user_id);
		$recipients = array();
		$recipients[$user->email] = $user->name;

		// send email to $recipients
		foreach ($recipients as $email => $name) {

			if (empty($email)) {
				continue;
			}

			$mail = $this->app->mail->create();
			$mail->setSubject(JText::sprintf('PLG_ZOOCART_ORDER_STATE_CHANGE', $order->id));
			$file = $this->app->zoo->getApplication()->getTemplate()->resource.$layout;
			if (!JFile::exists($file)) {
				$file ='zoocart:views/admin/orders/tmpl/'.$layout;
			}
			$mail->setBodyFromTemplate($file, compact(
				'order', 'website_name', 'email', 'name', 'order_link'
			));
			$mail->addRecipient($email);
			$mail->Send();
		}

		return true;
	}

	/**
	 * Get the Order States list field
	 *
	 * @param string $name The field control name
	 * @param array $selected The selected values
	 *
	 * @return string The HTML form field
	 */
	public function orderStatesList($name, $selected) {

		// init vars
		$attribs = '';
		$options = array();

		// populate options
		foreach ($this->app->zoocart->table->orderstates->all() as $orderstate) {
			$options[] = $this->app->html->_('select.option', $orderstate->id, JText::_($orderstate->name));
		}

		return $this->app->html->_('select.genericlist', $options, $name, $attribs, 'value', 'text', $selected);
	}

	/**
	 * Check if the Order is payed
	 * 
	 * @param object $order The Order object
	 * 
	 * @return boolean
	 */
	public function isPayed($order) {
		
		$config = $this->app->zoocart->getConfig();
		$yet_to_pay = in_array($order->getState()->id, array($config->get('new_orderstate'), $config->get('payment_failed_orderstate')));
		
		return !$yet_to_pay;
	}
}