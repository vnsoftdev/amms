<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartPaymentHelper extends AppHelper {

	/**
	 * Get a list of the ZOOcart Shipping Plugins
	 *
	 * @param null $name
	 * @return array The list
	 */
	public function getPaymentPlugins($name = null) {
		return JPluginHelper::getPlugin('zoocart_payment',$name);
	}

	/**
	 * Get the return URL
	 * 
	 * @return string The url
	 */
	public function getReturnUrl() {
		$uri = JUri::getInstance();
		$url = $uri->toString(array('scheme', 'host', 'port'));
		return $url . str_replace('//', '/', $this->app->link(array('controller' => 'orders', 'task' => 'message', 'app_id' => $this->app->zoo->getApplication()->id), false));
	}

	/**
	 * Get the cancel URL
	 * 
	 * @return string The url
	 */
	public function getCancelUrl() {
		$uri = JUri::getInstance();
		$url = $uri->toString(array('scheme', 'host', 'port'));
		return $url . str_replace('//', '/', $this->app->link(array('controller' => 'orders', 'task' => 'cancel', 'app_id' => $this->app->zoo->getApplication()->id), false));
	}

	/**
	 * Get the callback URL
	 * 
	 * @return string The url
	 */
	public function getCallbackUrl($payment_method) {
		$uri = JUri::getInstance();
		$url = $uri->toString(array('scheme', 'host', 'port'));
		return $url . str_replace('//', '/', $this->app->link(array('controller' => 'orders', 'task' => 'callback', 'app_id' => $this->app->zoo->getApplication()->id, 'format' => 'raw', 'payment_method' => $payment_method), false));
	}
}