<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartPriceHelper extends AppHelper {

	/**
	 * Get the price with taxes based on a given user
	 *
	 * @param int $app_id The app id (default: current)* 
	 * @param float $price The net price
	 * @param int $user_id The user id to apply the tax from (default: 0 => guest user)
	 * 
	 * @return float The gross price
	 */
	public function getGrossPrice($app_id = null, $price, $user_id = 0) {
		return $price + $this->app->zoocart->tax->getTaxes($app_id, $price, $user_id);
	}

	/**
	 * Get the Price element object
	 * 
	 * @param object $item The Item containing the Element
	 * 
	 * @return object The Price Element
	 */
	public function getPriceElement($item) {

		// if element set on Type config
		if ( ($identifier = $item->getType()->config->find('zoocart.price_element', false))
			&& $element = $item->getElement($identifier) ) {
			return $element;

		// else get the first element on the type
		} else {
			$elements = $item->getElementsByType('pricepro');
			return array_shift($elements);	
		}
	}

	/**
	 * Get the Item Net price
	 * 
	 * @param object $item The Item
	 * 
	 * @return float The Net price
	 */
	public function getItemNetPrice($item) {

		$price = 0;
		foreach((array)$this->getPriceElement($item)->data() as $self){
			$price = $this->app->data->create($self)->get('value');
		}

		return $price;
	}

	/**
	 * Get the Item Gross price
	 * 
	 * @param object $item The Item
	 * @param int $user_id The User ID
	 * 
	 * @return float The Gross price
	 */
	public function getItemGrossPrice($item, $user_id = null) {
		$price = $this->getItemNetPrice($item);
		$price = $this->getGrossPrice($item->getApplication()->id, $price, $user_id);

		return $price;
	}
}