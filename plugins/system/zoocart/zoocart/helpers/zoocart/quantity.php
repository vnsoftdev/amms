<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartQuantityHelper extends AppHelper {

	/**
	 * Remove quantity
	 * 
	 * @param int $item_id The Item ID
	 * @param int $quantity The quantity value
	 * 
	 * @return int The new quantity value
	 */
	public function removeQuantity($item_id, $quantity) {
		// init vars
		$item = $this->app->table->item->get($item_id);
		$element = $this->getQuantityElement($item);
		$old_quantity = $element->get('value');
		$new_quantity = $old_quantity + $quantity;
		$element->set('value', $new_quantity);

		// save updated quantity value
		$this->app->table->item->save($item);

		return $new_quantity;
	}

	/**
	 * Add quantity
	 * 
	 * @param int $item_id The Item ID
	 * @param int $quantity The quantity value
	 * 
	 * @return int The new quantity value
	 */
	public function addQuantity($item_id, $quantity) {
		return $this->removeQuantity($item_id, $quantity);
	}

	/**
	 * Get the Quantity element object
	 * 
	 * @param object $item The Item containing the Element
	 * 
	 * @return object The Quantity Element
	 */
	public function getQuantityElement($item) {
		
		// if element set on Type config
		if ( ($identifier = $item->getType()->config->find('zoocart.quantity_element', false))
			&& $element = $item->getElement($identifier) ) {
			return $element;

		// else get the first element on the type
		} else {
			$elements = $item->getElementsByType('quantity');
			return array_shift($elements);	
		}
	}

	/**
	 * Get quantity
	 * 
	 * @param object $item The Item object
	 * 
	 * @return int The quantity value
	 */
	public function getQuantity($item) {
		if ($element = $this->getQuantityElement($item)) {
			return $element->get('value', 0);
		}

		return '';
	}
}