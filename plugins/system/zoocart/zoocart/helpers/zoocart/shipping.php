<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class zoocartShippingHelper extends AppHelper {
	
	/**
	 * Get a list of the ZOOcart Shipping Plugins
	 * 
	 * @return array The list
	 */
	public function getShippingPlugins($name = null) {
		return JPluginHelper::getPlugin('zoocart_shipping', $name);
	}

	/**
	 * Get plugin by it's name
	 *
	 * @param string Plugin name
	 */
	public function getPluginByName($name)
	{
		$plugins = $this->getShippingPlugins();
		$plg = null;

		if(!empty($plugins))
			foreach($plugins as $plugin){
				if($plugin->name==$name)
				{
					$plg = $plugin;
					break;
				}
			}

		return $plg;
	}

	/**
	 * Get the Shipping rates
	 * 
	 * @param array $data 
	 * 
	 * @return array The rates list
	 */
	public function getShippingRates($data = array()) {
		
		// init vars
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('zoocart_shipping');
		$shipping_rates = array();
		$plugins = array_merge($dispatcher->trigger('getShippingRates', $data));

		$shipping_rates = array();
		foreach ($plugins as $rates) {
			$shipping_rates = array_merge($shipping_rates, $rates);
		}

		return $shipping_rates;
	}

	/**
	 * Get the Shipping rates Types field
	 * 
	 * @param string $name The field control name
	 * @param arrat $selected The selected options
	 * 
	 * @return string HTML form field
	 */
	public function shippingRateTypes($name, $selected) {

		// init vars
		$attribs = '';
		$options = array();		

		// set options
		$options[] = $this->app->html->_('select.option', 'order', JText::_('PLG_ZOOCART_CONFIG_SHIPPINGRATE_ORDER_TYPE'));
		$options[] = $this->app->html->_('select.option', 'item', JText::_('PLG_ZOOCART_CONFIG_SHIPPINGRATE_ITEM_TYPE'));

		return $this->app->html->_('select.genericlist', $options, $name, $attribs, 'value', 'text', $selected);
	}
}