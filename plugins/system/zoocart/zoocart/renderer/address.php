<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$zoo = App::getInstance('zoo');
$zoo->path->register($zoo->path->path('classes:renderer'), 'renderer');

$class = 'SubmissionRenderer';
$zoo->loader->register($class, 'renderer:submission.php');

class AddressRenderer extends SubmissionRenderer {

	public function getLayouts($dir) {

		$layouts = parent::getLayouts($dir);

		return $layouts;
	}

	public function renderPosition($position, $args = array()) {

		// init vars
		$elements = array();
		$output   = array();

		// get style
		$style = isset($args['style']) ? $args['style'] : 'submission.bootstrap';

		// store layout
		$layout = $this->_layout;

		// render elements
        foreach ($this->_getConfigPosition($position) as $data) {
            if (($element = $this->_item->getElement($data['element']))) {

				if (!$element->canAccess()) {
					continue;
				}

                // set params
                $params = array_merge((array) $data, $args);

                // check value
                $elements[] = compact('element', 'params');
            }
        }

        foreach ($elements as $i => $data) {
        	$element = $data['element'];
            $params = array_merge(array('first' => ($i == 0), 'last' => ($i == count($elements)-1)), array('trusted_mode' => false, 'show_tooltip'=> false), $data['params']);

			// trigger elements beforedisplay event
			$render = true;
			$this->app->event->dispatcher->notify($this->app->event->create($this->_item, 'element:beforeaddressdisplay', array('render' => &$render, 'element' => $element, 'params' => &$params)));

			if ($render) {
				$element->setItem($this->_item);
				$output[$i] = parent::render("element.$style", array('element' => $element, 'params' => $params, 'item' => $this->_item));

				// trigger elements afterdisplay event
				$this->app->event->dispatcher->notify($this->app->event->create($this->_item, 'element:afteraddressdisplay', array('html' => &$output[$i], 'element' => $element, 'params' => $params)));
			}

        }

		// restore layout
		$this->_layout = $layout;

		return implode("\n", $output);
	}

	protected function _getConfigPosition($position) {
		$application = $this->app->zoo->getApplication();
		$config	= $this->getConfig('address')->get($application->getGroup().'.'.$application->id.'.address.'.$this->_layout);
        return $config && isset($config[$position]) ? $config[$position] : array();
    }

    public function getConfigPositions($type, $layout) {
    	$application = $this->app->zoo->getApplication();
		$config	= $this->getConfig('address')->get($application->getGroup().'.'.$application->id.'.address.'.$layout);
		return $this->app->data->create($config);
    }
}