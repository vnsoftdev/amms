<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php if($this->checkPosition('name')): ?>
	<strong><?php echo $this->renderPosition('name', array('style' => 'default')); ?></strong>
	<br />
<?php endif; ?>

<?php if($this->checkPosition('address')): ?>
	<?php echo $this->renderPosition('address', array('style' => 'comma')); ?>
	<br />
<?php endif; ?>

<?php if($this->checkPosition('details')): ?>
	<?php echo $this->renderPosition('details', array('style' => 'comma')); ?>
<?php endif; ?>