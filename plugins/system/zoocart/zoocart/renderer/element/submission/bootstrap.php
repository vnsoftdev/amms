<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$params = $this->app->data->create($params);

// add tooltip
$tooltip = '';
if ($params->get('show_tooltip') && ($description = $element->config->get('description'))) {
	$tooltip = ' class="hasTip" title="'.$description.'"';
}

// create label
$label = $params->get('altlabel') ? $params->get('altlabel') : $element->config->get('name');

// create class attribute
$class = 'element-'.$element->getElementType().($params->get('first') ? ' first' : '').($params->get('last') ? ' last' : '').($params->get('required') ? ' required' : '').(@$element->error ? ' error' : '');

$element->loadAssets();

$item = $element->getItem(); 
$item->elements = $this->app->data->create($item->elements); 
$element->setItem($item); 

?>
<div class="control-group <?php echo $class; ?>">
	<label class="control-label">
		<?php echo $label; ?>
	</label>
	<div class="controls">
		<?php echo $element->renderSubmission($params); ?>
		<span class="help-block"></span>
	</div>
</div>