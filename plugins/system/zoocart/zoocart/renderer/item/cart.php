<?php
/**
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<tr data-cartitem-id="<?php echo $cartitem->id; ?>">
	<td class="remove">
		<a class="btn btn-danger"><i class="icon-trash icon-white"></i></a>
	</td>
	<td class="name">
		<?php if ($this->checkPosition('name')) : ?>
			<?php echo $this->renderPosition('name', array('style' => 'block')); ?>
		<?php endif; ?>
	</td>
	<td class="price-total">
		<?php
		if ($this->app->zoocart->getConfig()->get('show_price_with_tax', 1)) {
			$price = $cartitem->getGrossTotal();
		} else {
			$price = $cartitem->getNetTotal();
		}

		echo $this->app->zoocart->currency->format(null, $price);
		?>
	</td>
</tr>