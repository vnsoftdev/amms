<?php
/**
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<tr data-orderitem-id="<?php echo $cartitem->id; ?>">
	<td class="name">
		<?php if ($this->checkPosition('name')) : ?>
			<?php echo $this->renderPosition('name', array('style' => 'block')); ?>
		<?php endif; ?>
	</td>
	<td class="description">
		<?php if ($this->checkPosition('description')) : ?>
			<?php echo $this->renderPosition('description', array('style' => 'block')); ?>
		<?php endif; ?>
	</td>
	<td class="quantity">
		<?php echo $cartitem->quantity; ?>
	</td>
	<td class="price">
		<?php if ($this->checkPosition('price')) : ?>
			<?php echo $this->renderPosition('price', array('style' => 'block')); ?>
		<?php endif; ?>
	</td>
	<td class="price-total">
		<?php echo $this->app->zoocart->currency->format(null, $cartitem->total); ?>
	</td>
</tr>