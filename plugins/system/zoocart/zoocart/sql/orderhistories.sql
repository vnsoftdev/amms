CREATE TABLE IF NOT EXISTS `#__zoo_zl_zoocart_orderhistories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;