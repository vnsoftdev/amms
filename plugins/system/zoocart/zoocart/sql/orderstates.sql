CREATE TABLE IF NOT EXISTS `#__zoo_zl_zoocart_orderstates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;

-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(1, 'Pending', 'This is the status of an order that has yet to be payed, and has just been saved from your cart');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(2, 'Payment Received', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(3, 'Processing', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(4, 'Shipped', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(5, 'Completed', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(6, 'Payment Failed', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(7, 'Canceled', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(8, 'Refunded', '');
-- QUERY SEPARATOR --
INSERT IGNORE INTO `#__zoo_zl_zoocart_orderstates` VALUES(9, 'Validating Payment ', 'This state represents an order that is either waiting for a scheduled payment or the confirmation of an already performed payment');