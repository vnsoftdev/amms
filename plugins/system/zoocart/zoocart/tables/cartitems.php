<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CartitemsTable extends AppTable {

	public function __construct($app) {
		parent::__construct($app, '#__zoo_zl_zoocart_cartitems', 'id');

		$this->app->loader->register('Cartitem', 'classes:cartitem.php');
		$this->class = 'Cartitem';
	}

	protected function _initObject($object) {

		parent::_initObject($object);

		// trigger init event
		$this->app->event->dispatcher->notify($this->app->event->create($object, 'cartitem:init'));

		return $object;
	}

	public function save($object) {

		$object->modified_on = $this->app->date->create('now', $this->app->date->getOffset())->toSql();
		
		$new = !(bool) $object->id;
		
		$result = parent::save($object);

		// trigger save event
		$this->app->event->dispatcher->notify($this->app->event->create($object, 'cartitem:saved', compact('new')));

		return $result;
	}

	public function delete($object) {

		$result = parent::delete($object);

		// trigger deleted event
		$this->app->event->dispatcher->notify($this->app->event->create($object, 'cartitem:deleted'));

		return $result;
	}

	public function getByUser($user_id = null) {

		$session_id = $this->app->session->getId();
		$user_id = ($user_id  === null) ? $this->app->user->get()->id : $user_id;

		$options = "user_id = " . (int) $user_id;
		if (!$user_id) {
			$options .= " AND session_id = ".$this->app->database->q($session_id);
		}

		return $this->all(array('conditions' => $options));
	}

	/**
	 * Flush cart items, related to provided user
	 *
	 * @param $user_id
	 * @return void
	 */
	public function flushByUserId($user_id)
	{
		if(!$user_id)
			return;

		$items_to_flush = $this->getByUser($user_id);
		if(!empty($items_to_flush))
			foreach($items_to_flush as $item)
				$this->delete($item);
	}

	public function getByItemId($item_id, $user = null) {

		if ($user == null) {
			$user = $this->app->user->get();
		}

		$user_id = 0;
		if ($user) {
			$user_id = $user->id;
		}

		$session_id = $this->app->session->getId();

		$options = "user_id = " . (int) $user_id . " AND item_id = " . (int) $item_id;
		if (!$user_id) {
			$options .= " AND session_id = ".$this->app->database->q($session_id);
		}
		return $this->first(array('conditions' => $options));
	}

}

class CartitemTableException extends AppTableException {}