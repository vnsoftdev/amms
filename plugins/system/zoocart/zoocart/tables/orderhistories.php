<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class OrderhistoriesTable extends AppTable {

	public function __construct($app) {
		parent::__construct($app, '#__zoo_zl_zoocart_orderhistories', 'id');

		$this->app->loader->register('Orderhistory', 'classes:orderhistory.php');
		$this->class = 'Orderhistory';
	}

	public function save($object) {

		$object->date = $this->app->date->create('now', $this->app->date->getOffset())->toSql();
		$object->user_id = JFactory::getUser()->id;

		$result = parent::save($object);

		return $result;
	}

	public function getByOrder($order_id) {
		return $this->all(array('conditions' => 'order_id = ' . (int) $order_id));
	}
	
}

class OrderhistoriesTableException extends AppTableException {}