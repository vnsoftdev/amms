<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class OrderitemsTable extends AppTable {

	public function __construct($app) {
		parent::__construct($app, '#__zoo_zl_zoocart_orderitems', 'id');

		$this->app->loader->register('Orderitem', 'classes:orderitem.php');
		$this->class = 'Orderitem';
	}

	public function getByOrder($order_id) {
		return $this->all(array('conditions' => 'order_id = ' . (int) $order_id));
	}
	
}

class OrderitemTableException extends AppTableException {}