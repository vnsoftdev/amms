<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class OrdersTable extends AppTable {

	public function __construct($app) {
		parent::__construct($app, '#__zoo_zl_zoocart_orders', 'id');

		$this->app->loader->register('Order', 'classes:order.php');
		$this->class = 'Order';
	}

	protected function _initObject($object) {

		parent::_initObject($object);

		// trigger init event
		$this->app->event->dispatcher->notify($this->app->event->create($object, 'order:init'));

		return $object;
	}

	public function save($object) {

		$new = !(bool) $object->id;
		$old = $new ? $this->app->object->create('Order') : clone $this->get($object->id, true);

		$object->modified_on = $this->app->date->create('now', $this->app->date->getOffset())->toSql();

		if ($new) {
			$object->created_on = $this->app->date->create('now', $this->app->date->getOffset())->toSql();
			$object->ip = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';	
		}

		$result = parent::save($object);

		// trigger save event
		$this->app->event->dispatcher->notify($this->app->event->create($object, 'order:saved', compact('new', 'old')));

		return $result;
	}

	public function delete($object) {

		$result = parent::delete($object);

		// trigger deleted event
		$this->app->event->dispatcher->notify($this->app->event->create($object, 'order:deleted'));

		return $result;
	}
	
}

class OrderTableException extends AppTableException {}