<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$this->app->html->_('behavior.tooltip');
$this->app->document->addStyleSheet('assets:css/admin.css');

?>

<form id="adminForm" class="menu-has-level3" action="index.php" method="post" name="adminForm" accept-charset="utf-8">

<?php echo $this->partial('zoocartmenu'); ?>

<div class="box-bottom">
	<?php echo $this->partial('informer'); ?>

	<table id="actionlist" class="list stripe">
	<thead>
		<tr>
			<th class="checkbox">
				<input type="checkbox" class="check-all" />
			</th>
			<th class="name" colspan="2">
				<?php echo JText::_('PLG_ZOOCART_NAME'); ?>
			</th>
			<th class="template">
				<?php echo JText::_('PLG_ZOOCART_TEMPLATE_LAYOUTS'); ?>
			</th>
			<th class="extension">
				<?php echo JText::_('PLG_ZOOCART_EXTENSION_LAYOUTS'); ?>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$type = $this->address;
			$edit = $this->app->link(array('controller' => $this->controller, 'group' => $this->group, 'task' => 'editelements', 'cid[]' => $type->id));
		?>
		<tr>
			<td class="checkbox">
				<input type="checkbox" name="cid[]" value="<?php echo $type->id; ?>" />
			</td>
			<td class="icon"></td>
			<td class="name">
				<span class="editlink hasTip" title="<?php echo JText::_('PLG_ZOOCART_EDIT_ELEMENTS');?>::<?php echo $type->name; ?>">
					<a href="<?php echo $edit; ?>"><?php echo $type->name; ?></a>
				</span>
			</td>
			<td class="template">
				<?php foreach ($this->templates as $template) {
					echo '<div>'.$template->getMetadata('name').': ';

					$renderer = $this->app->renderer->create('address')->addPath($template->getPath());

					$path   = 'address';
					$prefix = 'address.';
					if ($renderer->pathExists($path.DIRECTORY_SEPARATOR.$type->id)) {
						$path   .= DIRECTORY_SEPARATOR.$type->id;
						$prefix .= $type->id.'.';
					}

					$links = array();
					foreach ($renderer->getLayouts($path) as $layout) {

						// get layout metadata
						$metadata = $renderer->getLayoutMetaData($prefix.$layout);

                        // create link
						$path = $this->app->path->relative($template->getPath());
                        $link = '<a href="'.$this->app->link(array('controller' => $this->controller, 'task' => 'assignelements', 'group' => $this->group, 'type' => $type->id, 'path' => urlencode($path), 'layout' => $layout)).'">'.$metadata->get('name', $layout).'</a>';

						// create tooltip
						if ($description = $metadata->get('description')) {
							$link = '<span class="editlinktip hasTip" title="'.$metadata->get('name', $layout).'::'.$description.'">'.$link.'</span>';
						}

						$links[] = $link;

					}
					echo implode(' | ', $links);
					echo '</div>';
				} ?>
			</td>
			<td class="extension">
				<?php foreach ($this->extensions as $extension) {

					$renderer = $this->app->renderer->create()->addPath($extension['path']);

					if (count($renderer->getLayouts('address'))) {
						echo '<div>'.ucfirst($extension['name']).': ';					

						$links = array();
						foreach ($renderer->getLayouts('address') as $layout) {

							// get layout metadata
							$metadata = $renderer->getLayoutMetaData("address.$layout");

							$layout_type = $metadata->get('type');

							$task = 'assignsubmission';
							if($layout_type == 'display') {
								$task = 'assignelements';
							} 

							// create link
							$path = $this->app->path->relative($extension['path']);
							$link = '<a href="'.$this->app->link(array('controller' => $this->controller, 'task' => $task, 'group' => $this->group, 'type' => $type->id, 'path' => urlencode($path), 'layout' => $layout)).'">'.$metadata->get('name', $layout).'</a>';

							// create tooltip
							if ($description = $metadata->get('description')) {
								$link = '<span class="editlinktip hasTip" title="'.$metadata->get('name', $layout).'::'.$description.'">'.$link.'</span>';
							}

							$links[] = $link;
						}

						echo implode(' | ', $links);
						echo '</div>';
					}
				} ?>
			</td>
		</tr>
	</tbody>
	</table>

</div>

<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>