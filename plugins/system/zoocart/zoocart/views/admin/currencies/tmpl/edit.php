<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	$this->app->html->_('behavior.tooltip');

	// Keepalive behavior
	JHTML::_('behavior.keepalive');
	$this->app->document->addStylesheet('zoocart:assets/css/admin.css');
?>

<form id="adminForm" class="menu-has-level3" action="index.php" method="post" name="adminForm" accept-charset="utf-8">

	<?php echo $this->partial('zoocartmenu'); ?>

	<div class="box-bottom">
		<?php echo $this->partial('informer'); ?>

		<div class="col col-left width-100">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_DETAILS'); ?></legend>
				<div class="element element-name">
					<strong><?php echo JText::_('PLG_ZOOCART_NAME'); ?></strong>
					<div id="name-edit">
						<div class="row">
							<?php echo $this->app->html->text('name', $this->resource->name, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-code">
					<strong><?php echo JText::_('PLG_ZOOCART_CODE'); ?></strong>
					<div id="code-edit">
						<div class="row">
							<?php echo $this->app->html->text('code', $this->resource->code, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-symbol-left">
					<strong><?php echo JText::_('PLG_ZOOCART_SYMBOL_LEFT'); ?></strong>
					<div id="symbol-left-edit">
						<div class="row">
							<?php echo $this->app->html->text('symbol_left', $this->resource->symbol_left, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-symbol-right">
					<strong><?php echo JText::_('PLG_ZOOCART_SYMBOL_RIGHT'); ?></strong>
					<div id="symbol-right-edit">
						<div class="row">
							<?php echo $this->app->html->text('symbol_right', $this->resource->symbol_right, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-num-decimals">
					<strong><?php echo JText::_('PLG_ZOOCART_CONFIG_NUM_DECIMAL'); ?></strong>
					<div id="num-decimals-edit">
						<div class="row">
							<?php echo $this->app->html->text('num_decimals', $this->resource->num_decimals, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-num-decimals">
					<strong><?php echo JText::_('PLG_ZOOCART_CONFIG_NUM_DECIMAL_TO_SHOW'); ?></strong>
					<div id="num-decimals-edit">
						<div class="row">
							<?php echo $this->app->html->text('num_decimals_show', $this->resource->num_decimals_show, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-decimal-sep">
					<strong><?php echo JText::_('PLG_ZOOCART_CONFIG_DECIMAL_SEPARATOR'); ?></strong>
					<div id="decimals-sep-edit">
						<div class="row">
							<?php echo $this->app->html->text('decimal_sep', $this->resource->decimal_sep, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-thousands-sep">
					<strong><?php echo JText::_('PLG_ZOOCART_CONFIG_THOUSAND_SEPARATOR'); ?></strong>
					<div id="thousand-sep-edit">
						<div class="row">
							<?php echo $this->app->html->text('thousand_sep', $this->resource->thousand_sep, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-conversion-rate">
					<strong><?php echo JText::_('PLG_ZOOCART_CONFIG_CONVERSION_RATE'); ?></strong>
					<div id="conversion-rate-edit">
						<div class="row">
							<?php echo $this->app->html->text('conversion_rate', $this->resource->conversion_rate, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
			</fieldset>

		</div>

		
	</div>

<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="cid[]" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>