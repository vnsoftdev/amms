<?php
/**
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$this->app->html->_('behavior.tooltip');
$settings = $this->app->zoocart->getConfig($this->application->id);
$this->app->document->addStyleSheet('assets:css/admin.css');

?>
<form id="adminForm" class="menu-has-level3" action="index.php" method="post" name="adminForm" accept-charset="utf-8">
	<?php echo $this->partial('zoocartmenu'); ?>

	<div class="box-bottom">
		<?php echo $this->partial('informer'); ?>

		<div class="col col-left width-100">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_GENERAL_SETTINGS'); ?></legend>
				<div class="col col-left width-50">
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_SHOW_PRICE_WITH_TAX_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_SHOW_PRICE_WITH_TAX'); ?></strong>
						<div id="p1-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[show_price_with_tax]','',(int)$settings->get('show_price_with_tax')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_DEFAULT_TAX_CLASS_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_DEFAULT_TAX_CLASS'); ?></strong>
						<div id="p2-edit">
							<div class="row">
								<?php echo $this->app->zoocart->tax->taxClassesList('zoocart[default_tax_class]',$settings->get('default_tax_class')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_BILLING_ADDRESS_TYPE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_BILLING_ADDRESS_TYPE'); ?></strong>
						<div id="p3-edit">
							<div class="row">
								<?php
								    $addr_options = array(
									    array('text'=>'PLG_ZOOCART_BILLING','value'=>'billing'),
									    array('text'=>'PLG_ZOOCART_SHIPPING','value'=>'shipping')
								    );
									echo $this->app->html->genericList($addr_options,'zoocart[billing_address_type]','','value','text',$settings->get('billing_address_type'),false,true);
								?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_LOAD_BOOTSTRAP_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_LOAD_BOOTSTRAP'); ?></strong>
						<div id="p4-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[load_bootstrap]','',(int)$settings->get('load_bootstrap')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_VIES_VALIDATION_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_VIES_VALIDATION'); ?></strong>
						<div id="p5-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[vies_validation]','',(int)$settings->get('vies_validation')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_VIES_VALIDATION_HARD_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_VIES_VALIDATION_HARD'); ?></strong>
						<div id="p6-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[vies_validation_hard]','',(int)$settings->get('vies_validation_hard')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_ACCEPT_TERMS_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_ACCEPT_TERMS'); ?></strong>
						<div id="p7-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[accept_terms]','',(int)$settings->get('accept_terms')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_TERMS_URL_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_TERMS_URL'); ?></strong>
						<div id="p8-edit">
							<div class="row">
								<?php echo $this->app->html->text('zoocart[terms_url]',$settings->get('terms_url'),'size="20" style="max-width:300px;"'); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_ENABLED_SHIPPING_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_ENABLED_SHIPPING'); ?></strong>
						<div id="p9-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[enable_shipping]','',(int)$settings->get('enable_shipping')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_CHECK_QUANTITIES_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_CHECK_QUANTITIES'); ?></strong>
						<div id="p10-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[check_quantities]','',(int)$settings->get('check_quantities')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_UPDATE_QUANTITIES_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_UPDATE_QUANTITIES'); ?></strong>
						<div id="p11-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[update_quantities]','',(int)$settings->get('update_quantities')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_REQUIRE_ADDRESS_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_REQUIRE_ADDRESS'); ?></strong>
						<div id="p12-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[require_address]','',(int)$settings->get('require_address',1)); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col col-left width-50">
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_UPDATE_QUANTITIES_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_UPDATE_QUANTITIES_ORDERSTATE'); ?></strong>
						<div id="p12-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[quantity_update_state]',$settings->get('quantity_update_state')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_DEFAULT_COUNTRY_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_DEFAULT_COUNTRY'); ?></strong>
						<div id="p13-edit">
							<div class="row">
                                <?php echo $this->app->html->countrySelectList($this->app->country->getIsoToNameMapping(), 'zoocart[default_country]', $settings->get('default_country'),false ); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_DEFAULT_CURRENCY_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_DEFAULT_CURRENCY'); ?></strong>
						<div id="p14-edit">
							<div class="row">
								<?php echo $this->app->zoocart->currency->currenciesList('zoocart[default_currency]',$settings->get('default_currency')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_NEW_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_NEW_ORDERSTATE'); ?></strong>
						<div id="p15-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[new_orderstate]',$settings->get('new_orderstate')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_PAYMENT_RECEIVED_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_PAYMENT_RECEIVED_ORDERSTATE'); ?></strong>
						<div id="p16-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[payment_received_orderstate]',$settings->get('payment_received_orderstate')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_PAYMENT_PENDING_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_PAYMENT_PENDING_ORDERSTATE'); ?></strong>
						<div id="p17-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[payment_pending_orderstate]',$settings->get('payment_pending_orderstate')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_PAYMENT_FAILED_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_PAYMENT_FAILED_ORDERSTATE'); ?></strong>
						<div id="p18-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[payment_failed_orderstate]',$settings->get('payment_failed_orderstate')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_CANCELED_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_CANCELED_ORDERSTATE'); ?></strong>
						<div id="p19-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[canceled_orderstate]',$settings->get('canceled_orderstate')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_FINISHED_ORDERSTATE_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_FINISHED_ORDERSTATE'); ?></strong>
						<div id="p20-edit">
							<div class="row">
								<?php echo $this->app->zoocart->order->orderStatesList('zoocart[finished_orderstate]',$settings->get('finished_orderstate')); ?>
							</div>
						</div>
					</div>
					<div class="element element-name">
						<strong class="hasTip" title="<?php echo JText::_('PLG_ZOOCART_CONFIG_CHECKOUT_OPENED_DESC'); ?>"><?php echo JText::_('PLG_ZOOCART_CONFIG_CHECKOUT_OPENED'); ?></strong>
						<div id="p21-edit">
							<div class="row">
								<?php echo $this->app->html->booleanlist('zoocart[checkout_opened]','',(int)$settings->get('checkout_opened')); ?>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>

		<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
		<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />

		<?php echo $this->app->html->_('form.token'); ?>
	</div>
</form>