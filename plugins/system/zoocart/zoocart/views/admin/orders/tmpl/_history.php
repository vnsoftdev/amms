<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<table class="table list">
	<thead>
		<tr>
			<th><?php echo JText::_('PLG_ZOOCART_STATE'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_DATE'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_USER'); ?></th>
		</tr>
	</thead>
	<tbody>		
		<?php foreach($histories as $history) :?>
		<tr>
			<td><?php echo JText::_($history->getState()->name); ?></td>
			<td><?php echo JFactory::getDate($history->date)->format('d/m/Y H:i:s'); ?></td>
			<td><?php echo JFactory::getUser($history->user_id)->name; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>