<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<table class="table list">
	<thead>
		<tr>
			<th><?php echo JText::_('PLG_ZOOCART_ITEM_NAME'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_QUANTITY'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_UNIT_PRICE'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_TOTAL'); ?></th>
		</tr>
	</thead>
	<tbody>		
		<?php foreach($items as $item) :?>
		<tr>
			<td><a href="<?php echo JRoute::_('index.php?option=com_zoo&view=item&task=edit&cid[]='.$item->getItem()->id);?>" target="_blank"><?php echo $item->getItem()->name; ?></a></td>
			<td><?php echo $item->quantity; ?></td>
			<td><?php echo $this->app->zoocart->currency->format(null, $item->price); ?></td>
			<td><?php echo $this->app->zoocart->currency->format(null, $item->price * $item->quantity); ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr class="payment">
			<td colspan="3">
				<strong><?php echo JText::_('PLG_ZOOCART_PAYMENT_FEE'); ?></strong>
			</td>
			<td id="zoocart-cart-payment"><strong><?php echo $this->app->zoocart->currency->format(null, $this->resource->payment);?></strong></td>
		</tr>
		<tr class="shipping">
			<td colspan="3">
				<strong><?php echo JText::_('PLG_ZOOCART_SHIPPING_FEE'); ?></strong>
			</td>
			<td id="zoocart-cart-shipping"><strong><?php echo $this->app->zoocart->currency->format(null, $this->resource->shipping);?></strong></td>
		</tr>
		<tr class="subtotal">
			<td colspan="3">
				<strong><?php echo JText::_('PLG_ZOOCART_SUBTOTAL'); ?></strong>
			</td>
			<td id="zoocart-cart-subtotal"><strong><?php echo $this->app->zoocart->currency->format(null, $this->resource->getSubtotal());?></strong></td>
		</tr>
		<tr class="taxes">
			<td colspan="3">
				<strong><?php echo JText::_('PLG_ZOOCART_TAXES'); ?></strong>
			</td>
			<td id="zoocart-cart-taxes"><strong><?php echo $this->app->zoocart->currency->format(null, $this->resource->getTaxTotal());?></strong></td>
		</tr>
		<tr class="total">
			<td colspan="3">
				<strong><?php echo JText::_('PLG_ZOOCART_TOTAL'); ?></strong>
			</td>			
			<td id="zoocart-cart-total"><strong><?php echo $this->app->zoocart->currency->format(null, $this->resource->getTotal());?></strong></td>
		</tr>
	</tfoot>
</table>