<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php if (count($payments)) : ?>
<table class="table list">
	<thead>
		<tr>
			<th><?php echo JText::_('PLG_ZOOCART_TRANSACTION_ID'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_STATUS'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_PAYMENT_METHOD'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_TOTAL'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_DATE'); ?></th>
		</tr>
	</thead>
	<tbody>		
		<?php foreach($payments as $payment) :?>
		<tr>
			<td><?php echo $payment->transaction_id; ?></td>
			<td><?php echo $payment->status; ?></td>
			<td><?php echo $payment->payment_method; ?></td>
			<td><?php echo $this->app->zoocart->currency->format(null, $payment->total); ?></td>
			<td><?php echo $this->app->date->create($payment->created_on)->format('%Y-%m-%d %H:%M:%S', $this->app->date->getOffset()); ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php else : ?>

<?php echo JText::_('PLG_ZOOCART_NO_PAYMENTS_YET'); ?>

<?php endif; ?>