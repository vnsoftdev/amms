<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// add js
$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

$address_renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->application->getTemplate()->getPath(), $this->app->path->path('zoocart:')));

?>

<form id="adminForm" class="menu-has-level3" action="<?php echo $this->app->link(); ?>" method="post" name="adminForm" accept-charset="utf-8">

<?php echo $this->partial('zoocartmenu'); ?>

<div class="box-bottom">
	<?php echo $this->partial('informer'); ?>

	<?php
	if($this->pagination->total > 0) : ?>


		<ul class="filter">
			
			<li class="filter-right">
				<?php echo $this->lists['select_state'];?>
			</li>
			<li class="filter-right">
				<label><?php echo JText::_('PLG_ZOOCART_FROM'); ?></label> <?php echo $this->app->html->_('zoo.calendar', $this->lists['created_on_from'], 'created_on_from', 'created_on_from', '', true); ?>
				<label><?php echo JText::_('PLG_ZOOCART_TO'); ?></label> <?php echo $this->app->html->_('zoo.calendar', $this->lists['created_on_to'], 'created_on_to', 'created_on_to', '', true); ?>
				<button onclick="this.form.submit();"><?php echo JText::_('PLG_ZOOCART_FILTER'); ?></button>
				<button onclick="document.getElementById('created_on_from').value='';document.getElementById('created_on_to').value='';this.form.submit();"><?php echo JText::_('PLG_ZOOCART_RESET'); ?></button>
			</li>
			
		</ul>

		<table class="list stripe">
			<thead>
				<tr>
					<th class="checkbox">
						<input type="checkbox" class="check-all" />
					</th>
					<th class="id">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ORDER_ID', 'id', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="id">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_USER', 'user_id', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="billing_address">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_BILLING', 'billing_address', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="shipping_address">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_SHIPPING', 'shipping_address', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="net">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_NET_TOTAL', 'net', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="total">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_TOTAL', 'net', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="state">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ORDER_STATE', 'state', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="payment_method">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_PAYMENT_METHOD', 'payment_method', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="created_on">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_CREATED_ON', 'created_on', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="modified_on">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_MODIFIED_ON', 'modified_on', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			for ($i=0, $n=count($this->resources); $i < $n; $i++) :

				$row = $this->resources[$i];
				
			?>
				<tr>
					<td class="checkbox">
						<input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>" />
					</td>
					<td class="id">
						<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'changeapp' => $this->application->id, 'task' => 'edit', 'cid[]' => $row->id));  ?>"><?php echo $row->id; ?></a>
					</td>
					<td class="id">
						<?php echo JFactory::getUser($row->user_id)->name; ?>
					</td>
					<td class="billing_address">
						<?php echo $address_renderer->render('address.billing', array('item' => $row->getBillingAddress())); ?>
					</td>
					<td class="shipping_address">
						<?php echo $address_renderer->render('address.shipping', array('item' => $row->getShippingAddress())); ?>
					</td>
					<td class="net">
						<?php echo $this->app->zoocart->currency->format(null, $row->getSubtotal()); ?>
					</td>
					<td class="total">
						<?php echo $this->app->zoocart->currency->format(null, $row->getTotal()); ?>
					</td>
					<td class="state">
						<?php echo $row->getState()->name; ?>
					</td>
					<td class="payment_method">
						<?php echo ucfirst($row->payment_method); ?>
					</td>
					<td class="created_on">
						<?php echo $this->app->date->create($row->created_on)->format('l, d/m/Y H:m:s'); ?>
					</td>
					<td class="modified_on">
						<?php echo $this->app->date->create($row->modified_on)->format('l, d/m/Y H:m:s'); ?>
					</td>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>

	<?php 
		else :

			$title   = JText::_('PLG_ZOOCART_CONFIG_NO_ORDERS_YET');
			$message = JText::_('PLG_ZOOCART_CONFIG_ORDERS_MANAGER_DESC');
			echo $this->partial('message', compact('title', 'message'));

		endif;
	?>
</div>

<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>