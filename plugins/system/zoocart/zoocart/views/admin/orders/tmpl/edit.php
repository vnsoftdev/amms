<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	$this->app->html->_('behavior.tooltip');
	$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

	// Keepalive behavior
	JHTML::_('behavior.keepalive');

	// filter output
	JFilterOutput::objectHTMLSafe($this->resource, ENT_QUOTES, array('params', 'billing_address', 'shipping_address', 'shipping_method'));

	$renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->application->getTemplate()->getPath(), $this->app->path->path('zoocart:')));

?>

<form id="adminForm" action="index.php" class="menu-has-level3" method="post" name="adminForm" accept-charset="utf-8">

	<?php echo $this->partial('zoocartmenu'); ?>

	<div class="box-bottom">
		<?php echo $this->partial('informer'); ?>

		<div class="col col-left width-50">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_DETAILS'); ?></legend>

				<div class="element element-id">
					<strong><?php echo JText::_('PLG_ZOOCART_ID'); ?></strong>
					<div>
						<div class="row">
							<?php echo $this->resource->id; ?>
						</div>
					</div>
				</div>

				<div class="element element-id">
					<strong><?php echo JText::_('PLG_ZOOCART_USER'); ?></strong>
					<div>
						<div class="row">
							<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id='.$this->resource->user_id); ?>" target="_blank"><?php echo $this->app->user->get($this->resource->user_id)->name; ?></a>
						</div>
					</div>
				</div>

				<div class="element element-address">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_BILLING'); ?></strong>
					<div>
						<div class="row">
							<?php echo $renderer->render('address.billing', array('item' => $this->resource->getBillingAddress())); ?>
						</div>
					</div>
				</div>

				<div class="element element-address">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_SHIPPING'); ?></strong>
					<div>
						<div class="row">
							<?php echo $renderer->render('address.shipping', array('item' => $this->resource->getShippingAddress())); ?>
						</div>
					</div>
				</div>

				<div class="element element-created-on">
					<strong><?php echo JText::_('PLG_ZOOCART_CREATED_ON'); ?></strong>
					<div>
						<div class="row">
							<?php echo $this->app->date->create($this->resource->created_on)->format('l, d/m/Y H:m:s'); ?>
						</div>
					</div>
				</div>


				<div class="element element-modified-on">
					<strong><?php echo JText::_('PLG_ZOOCART_MODIFIED_ON'); ?></strong>
					<div>
						<div class="row">
							<?php echo $this->app->date->create($this->resource->modified_on)->format('l, d/m/Y H:m:s'); ?>
						</div>
					</div>
				</div>

				<div class="element element-payment">
					<strong><?php echo JText::_('PLG_ZOOCART_PAYMENT_METHOD'); ?></strong>
					<div>
						<div class="row">
							<?php echo ucfirst($this->resource->payment_method); ?>
							<?php if(!$this->app->zoocart->order->isPayed($this->resource)): ?>
								<a href="javascript:void(0);" class="change-payment-method-toggle"><?php echo JText::_('PLG_ZOOCART_CHANGE'); ?></a>
								<div class="change-payment-method">
									<?php 
									$plugins = $this->app->zoocart->payment->getPaymentPlugins(); 
									foreach($plugins as $plugin) : ?>
										<input type="radio" name="payment_method" value="<?php echo $plugin->name;?>" <?php echo ($plugin->name == $this->resource->payment_method) ? 'checked="checked"' : ''; ?>/> <?php echo JText::_(ucfirst($plugin->name));?>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="element element-shipping">
					<strong><?php echo JText::_('PLG_ZOOCART_SHIPPING_METHOD'); ?></strong>
					<div>
						<div class="row">
							<?php foreach($this->resource->getShippingMethod() as $key => $value): ?>
								<strong><?php echo ucfirst($key); ?>:</strong> <?php echo ($key == 'price') ? $this->app->zoocart->currency->format(null, $value) : $value; ?><br />
							<?php endforeach; ?>
						</div>
					</div>
				</div>

				<div class="element element-notes">
					<strong><?php echo JText::_('PLG_ZOOCART_CUSTOMER_NOTES'); ?></strong>
					<div>
						<div class="row">
							<?php echo $this->resource->notes; ?>
						</div>
					</div>
				</div>
				
			</fieldset>

		</div>

		<div class="col col-right width-50">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_ACTIONS'); ?></legend>
				<div class="element element-state">
					<strong><?php echo JText::_('PLG_ZOOCART_ORDER_STATE'); ?></strong>
					<div>
						<div class="row">
							<?php echo $this->app->zoocart->orderstatesList('state', $this->resource->state, '', false); ?>
						</div>
						<div class="row notify-user">
							<input type="checkbox" checked="checked" name="notify_user" value="1" /> <?php echo JText::_('PLG_ZOOCART_ORDER_STATE_CHANGE_NOTIFY_USER'); ?>
						</div>
					</div>
				</div>
				
			</fieldset>

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_HISTORY'); ?></legend>
				<?php echo $this->partial('history', array('histories' => $this->app->zoocart->table->orderhistories->getByOrder($this->resource->id))); ?>
				
			</fieldset>

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_ITEMS'); ?></legend>
				<?php echo $this->partial('items', array('items' => $this->resource->getItems())); ?>
				
			</fieldset>

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_PAYMENTS'); ?></legend>
				<?php echo $this->partial('payments', array('payments' => $this->resource->getPayments())); ?>
				
			</fieldset>

		</div>

		
	</div>

<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="cid[]" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('.change-payment-method-toggle').click(function(){
		$('.change-payment-method').toggle('slow');
	});
});
</script>

<?php echo ZOO_COPYRIGHT; ?>