<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<html>
	<body>
		<p>Hi <?php echo $name; ?>,</p>

		<p>Your order at <?php echo $website_name; ?> has just been updated!</p>

		<p>If you want to check your order status, click the following link: <a href="<?php echo $order_link; ?>"><?php echo $order_link; ?></a></p>
	</body>
</html>