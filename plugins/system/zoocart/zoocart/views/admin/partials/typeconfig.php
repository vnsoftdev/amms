<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<fieldset class="creation-form">
	<legend>ZOOcart</legend>
	<div class="element element-price">
		<strong><?php echo JText::_('PLG_ZOOCART_PRICE_ELEMENT'); ?></strong>
		<div id="price-edit">
			<div class="row">
				<?php 
					echo $this->lists['price_element'];
				?>
			</div>
		</div>
	</div>
	<div class="element element-quantity">
		<strong><?php echo JText::_('PLG_ZOOCART_QUANTITY_ELEMENT'); ?></strong>
		<div id="quantity-edit">
			<div class="row">
				<?php 
					echo $this->lists['quantity_element'];
				?>
			</div>
		</div>
	</div>
</fieldset>