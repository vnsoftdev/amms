<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

?>

<form id="adminForm" class="menu-has-level3" action="<?php echo $this->app->link(); ?>" method="post" name="adminForm" accept-charset="utf-8">

<?php echo $this->partial('zoocartmenu'); ?>

<div class="box-bottom">
	<?php echo $this->partial('informer'); ?>

	<?php
	if($this->pagination->total > 0) : ?>

		<table class="list stripe">
			<thead>
				<tr>
					<th class="checkbox">
						<input type="checkbox" class="check-all" />
					</th>
					<th class="name">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_NAME', 'name', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="type">
						<?php echo JText::_('Type'); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			for ($i=0, $n=count($this->resources); $i < $n; $i++) :

				$row		  = $this->resources[$i];
				
			?>
				<tr>
					<td class="checkbox">
						<input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>" />
					</td>
					<td class="name">
						<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'changeapp' => $this->application->id, 'task' => 'edit', 'cid[]' => $row->id));  ?>"><?php echo $row->name; ?></a>
					</td>
					<td class="description">
						<?php echo $row->type; ?>
					</td>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>

	<?php 
		else :

			$title   = JText::_('PLG_ZOOCART_CONFIG_NO_SHIPPINGRATES_YET');
			$message = JText::_('PLG_ZOOCART_CONFIG_SHIPPINGRATES_MANAGER_DESC');
			echo $this->partial('message', compact('title', 'message'));

		endif;
	?>
</div>

<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>