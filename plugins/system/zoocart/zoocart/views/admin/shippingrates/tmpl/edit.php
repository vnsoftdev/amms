<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	$this->app->html->_('behavior.tooltip');
	$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

	// Keepalive behavior
	JHTML::_('behavior.keepalive');

	// Transform user groups and countries:
    $this->resource->user_groups = explode(',',$this->resource->user_groups);
    $this->resource->countries = explode(',',$this->resource->countries);
?>

<form id="adminForm" action="index.php" class="menu-has-level3" method="post" name="adminForm" accept-charset="utf-8">

	<?php echo $this->partial('zoocartmenu'); ?>

	<div class="box-bottom">
		<?php echo $this->partial('informer'); ?>

		<div class="col col-left width-100">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_DETAILS'); ?></legend>
				<div class="element element-name">
					<strong><?php echo JText::_('PLG_ZOOCART_NAME'); ?></strong>
					<div id="country-edit">
						<div class="row">
							<?php echo $this->app->html->text('name', $this->resource->name, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_TYPE'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->zoocart->shipping->shippingRateTypes('type', $this->resource->type); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_PRICE'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('price', $this->resource->price, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_PRICE_FROM'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('price_from', $this->resource->price_from, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_PRICE_TO'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('price_to', $this->resource->price_to, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_QUANTITY_FROM'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('quantity_from', $this->resource->quantity_from, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_QUANTITY_TO'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('quantity_to', $this->resource->quantity_to, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_COUNTRIES'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->countrySelectList($this->app->country->getIsoToNameMapping(), 'countries[]', $this->resource->countries, true ); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_STATES'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('states', $this->resource->states, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_CITIES'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('cities', $this->resource->cities, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_ZIPS'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->html->text('zips', $this->resource->zips, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>

				<div class="element element-type">
					<strong><?php echo JText::_('PLG_ZOOCART_USER_GROUPS'); ?></strong>
					<div id="type-edit">
						<div class="row">
							<?php echo $this->app->zoocart->userGroupsList('user_groups[]', $this->resource->user_groups, 'multiple="multiple"'); ?>
						</div>
					</div>
				</div>
				
			</fieldset>
		</div>
	</div>

<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="cid[]" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>