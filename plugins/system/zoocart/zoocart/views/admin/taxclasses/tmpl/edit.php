<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$this->app->html->_('behavior.tooltip');

// Keepalive behavior
JHTML::_('behavior.keepalive');
$this->app->document->addStyleSheet('assets:css/admin.css');

?>

<form id="adminForm" action="index.php" class="menu-has-level3" method="post" name="adminForm" accept-charset="utf-8">

	<?php echo $this->partial('zoocartmenu'); ?>

	<div class="box-bottom">
		<?php echo $this->partial('informer'); ?>

		<div class="col col-left width-100">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_DETAILS'); ?></legend>
				<div class="element element-name">
					<strong><?php echo JText::_('PLG_ZOOCART_NAME'); ?></strong>
					<div id="country-edit">
						<div class="row">
							<?php echo $this->app->html->text('name', $this->resource->name, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				
			</fieldset>

		</div>

		
	</div>

<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="cid[]" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>