<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

?>

<form id="adminForm" class="menu-has-level3" action="<?php echo $this->app->link(); ?>" method="post" name="adminForm" accept-charset="utf-8">

<?php echo $this->partial('zoocartmenu'); ?>

<div class="box-bottom">
	<?php echo $this->partial('informer'); ?>

	<?php
	if($this->pagination->total > 0) : ?>

		<table class="list stripe">
			<thead>
				<tr>
					<th class="checkbox">
						<input type="checkbox" class="check-all" />
					</th>
					<th class="country" colspan="2">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_COUNTRY', 'country', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="state">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_STATE', 'state', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="city">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_CITY', 'city', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="zip">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_ZIP', 'zip', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="vies">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_VIES_REGISTERED', 'vies', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="enabled">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ENABLED', 'enabled', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="tax_class_id">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_TAX_CLASS', 'tax_class_id', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
					<th class="ordering">
						<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ORDERING', 'ordering', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			for ($i=0, $n=count($this->resources); $i < $n; $i++) :

				$row		  = $this->resources[$i];
				$img = '';
				$alt = '';
				if ($row->enabled == 1) {
					$img = 'publish_g.png';
					$alt = JText::_('PLG_ZOOCART_PUBLISHED');
				} else {
					$img = 'publish_r.png';
					$alt = JText::_('PLG_ZOOCART_UNPUBLISHED');
				}

				$vies_img = '';
				$vies_alt = '';
				if ($row->vies == 1) {
					$vies_img = 'publish_y.png';
					$vies_alt = JText::_('PLG_ZOOCART_VIES_REGISTERED');
				} else {
					$vies_img = 'publish_x.png';
					$vies_alt = JText::_('PLG_ZOOCART_VIES_REGISTERED_NOT');
				}

				
			?>
				<tr>
					<td class="checkbox">
						<input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>" />
					</td>
					<td class="icon"></td>
					<td class="country">
						<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'changeapp' => $this->application->id, 'task' => 'edit', 'cid[]' => $row->id));  ?>"><?php echo $row->country; ?></a>
					</td>
					<td class="state">
						<?php echo $row->state; ?>
					</td>
					<td class="city">
						<?php echo $row->city; ?>
					</td>
					<td class="zip">
						<?php echo $row->zip; ?>
					</td>
					<td class="vies">
						<img src="<?php echo $this->app->path->url('assets:images/'.$vies_img) ;?>" width="16" height="16" border="0" alt="<?php echo $vies_alt; ?>" />
					</td>
					<td class="published">
						<a href="#" rel="task-<?php echo $row->state ? 'unpublish' : 'publish'; ?>">
							<img src="<?php echo $this->app->path->url('assets:images/'.$img) ;?>" width="16" height="16" border="0" alt="<?php echo $alt; ?>" />
						</a>
					</td>
					<td class="tax_class_id">
						<?php echo ($tax_class = $row->getTaxClass()) ? $tax_class->name : ''; ?>
					</td>
					<td class="ordering">
						<span class="minus"></span>
						<input type="text" class="value" value="<?php echo $row->ordering; ?>" size="5" name="ordering[<?php echo $row->id; ?>]"/>
						<span class="plus"></span>
					</td>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>

	<?php 
		else :

			$title   = JText::_('PLG_ZOOCART_CONFIG_NO_TAXRULES_YET');
			$message = JText::_('PLG_ZOOCART_CONFIG_TAXRULES_MANAGER_DESC');
			echo $this->partial('message', compact('title', 'message'));

		endif;
	?>
</div>

<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>