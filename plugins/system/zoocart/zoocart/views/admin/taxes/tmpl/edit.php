<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	$this->app->html->_('behavior.tooltip');
	$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

	// Keepalive behavior
	JHTML::_('behavior.keepalive');

?>

<form id="adminForm" action="index.php" class="menu-has-level3" method="post" name="adminForm" accept-charset="utf-8">

	<?php echo $this->partial('zoocartmenu'); ?>

	<div class="box-bottom">
		<?php echo $this->partial('informer'); ?>

		<div class="col col-left width-100">

			<fieldset class="creation-form">
				<legend><?php echo JText::_('PLG_ZOOCART_DETAILS'); ?></legend>
				<div class="element element-country">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_COUNTRY'); ?></strong>
					<div id="country-edit">
						<div class="row">
							<?php echo $this->app->html->countrySelectList($this->app->country->getIsoToNameMapping(), 'country', $this->resource->country, false); ?>
						</div>
					</div>
				</div>
				<div class="element element-state">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_STATE'); ?></strong>
					<div id="state-edit">
						<div class="row">
							<?php echo $this->app->html->text('state', $this->resource->state, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-city">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_CITY'); ?></strong>
					<div id="city-edit">
						<div class="row">
							<?php echo $this->app->html->text('city', $this->resource->city, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-zip">
					<strong><?php echo JText::_('PLG_ZOOCART_ADDRESS_ZIP'); ?></strong>
					<div id="zip-edit">
						<div class="row">
							<?php echo $this->app->html->text('zip', $this->resource->zip, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-taxrate">
					<strong><?php echo JText::_('PLG_ZOOCART_TAX_RATE'); ?></strong>
					<div id="taxrate-edit">
						<div class="row">
							<?php echo $this->app->html->text('taxrate', $this->resource->taxrate, 'class="inputbox"'); ?> %
						</div>
					</div>
				</div>
				<div class="element element-taxrate">
					<strong><?php echo JText::_('PLG_ZOOCART_ORDERING'); ?></strong>
					<div id="ordering-edit">
						<div class="row">
							<?php echo $this->app->html->text('ordering', $this->resource->ordering, 'class="inputbox"'); ?>
						</div>
					</div>
				</div>
				<div class="element element-published">
					<strong><?php echo JText::_('PLG_ZOOCART_PUBLISHED'); ?></strong>
					<?php echo $this->lists['select_enabled']; ?>
				</div>

				<div class="element element-vies">
					<strong><?php echo JText::_('PLG_ZOOCART_VIES_REGISTERED'); ?></strong>
					<?php echo $this->lists['select_vies']; ?>
				</div>

				<div class="element element-taxclass">
					<strong><?php echo JText::_('PLG_ZOOCART_TAX_CLASS'); ?></strong>
					<div id="ordering-edit">
						<div class="row">
							<?php echo $this->app->zoocart->tax->taxClassesList('tax_class_id', $this->resource->tax_class_id); ?><br />
						</div>
					</div>
				</div>
				
			</fieldset>

		</div>

		
	</div>

<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="id" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="cid[]" value="<?php echo $this->resource->id; ?>" />
<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
<?php echo $this->app->html->_('form.token'); ?>

</form>

<?php echo ZOO_COPYRIGHT; ?>