<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/site.css');

$renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->application->getTemplate()->getPath(), $this->app->path->path('zoocart:')));

?>

<div id="zoocart-container" class="zl-bootstrap">

	<div class="row add-address">
		<a href="<?php echo $this->app->link(array('task' => 'edit', 'controller' => 'addresses', 'type' => 'billing', 'app_id' => $this->application->id)); ?>" class="btn btn-success pull-right">
			<i class="icon-plus icon-white"></i>
			<?php echo JText::_('PLG_ZOOCART_ADDRESS_ADD_NEW'); ?>
		</a>
	</div>
	
	<?php if (count($this->resources)): ?>
	<form id="zoocart-admin-default" action="<?php echo $this->app->link(); ?>" method="post" name="adminForm" accept-charset="utf-8">


			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="span1">
						</th>
						<th class="id span2">
							<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS_ID', 'id', @$this->lists['order_Dir'], @$this->lists['order']); ?>
						</th>
						<th class="type span2">
							<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_TYPE', 'type', @$this->lists['order_Dir'], @$this->lists['order']); ?>
						</th>
						<th class="default span1">
							<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_DEFAULT', 'default', @$this->lists['order_Dir'], @$this->lists['order']); ?>
						</th>
						<th class="address">
							<?php echo $this->app->html->_('grid.sort', 'PLG_ZOOCART_ADDRESS', 'address', @$this->lists['order_Dir'], @$this->lists['order']); ?>
						</th>
					</tr>
				</thead>
				<tbody>
				<?php
				for ($i=0, $n=count($this->resources); $i < $n; $i++) :

					$row = $this->resources[$i];
					
				?>
					<tr>
						<td>
							<a class="btn btn-danger" onclick="javascript: return confirm('<?php echo JText::_('PLG_ZOOCART_ADDRESS_CONFIRM_DELETE'); ?>');" href="<?php echo $this->app->link(array('controller' => $this->controller, 'app_id' => $this->application->id, 'task' => 'remove', 'id' => $row->id));  ?>"><i class="icon-trash icon-white"></i></a>
						</td>
						<td class="id">
							<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'app_id' => $this->application->id, 'task' => 'edit', 'id' => $row->id));  ?>"><?php echo $row->id; ?></a>
						</td>
						<td class="type">
							<span class="btn btn-<?php echo ($row->type == 'shipping') ? 'warning' : 'primary'; ?> disabled"><?php echo JText::_('PLG_ZOOCART_' . strtoupper($row->type)); ?></span>
						</td>
						<td class="default">
							<?php if ($row->default) : ?>
							<a href="javascript: void(0);" class="btn btn-success"><i class="icon-ok icon-white"></i></a>
							<?php else : ?>
							<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'app_id' => $this->application->id, 'task' => 'setDefault', 'id' => $row->id));  ?>" class="btn"><i class="icon-remove icon"></i></a>
							<?php endif; ?>
						</td>
						<td class="address">
							<?php echo $renderer->render('address.' . $row->type, array('item' => $row)); ?>
						</td>
					</tr>
					<?php endfor; ?>
				</tbody>
			</table>

			<?php if ($pagination = $this->pagination->render($this->pagination_link)) : ?>
				<div class="pagination pagination-centered">
					<?php echo $pagination; ?>
				</div>
			<?php endif; ?>

	<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="app_id" value="<?php echo $this->application->id; ?>" />
	<?php echo $this->app->html->_('form.token'); ?>

	</form>

	<?php else: ?>

	<div class="alert alert-warning">
		<?php echo JText::_('PLG_ZOOCART_ADDRESS_NO_ADDRESSES_YET'); ?>
	</div>

	<?php endif; ?>
</div>