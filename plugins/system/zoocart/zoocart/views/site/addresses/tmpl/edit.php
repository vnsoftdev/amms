<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/admin.css');

$type = $this->resource->type;
if (!$type) {
	$type = $this->app->request->getCmd('type', 'billing');
}

$renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->application->getTemplate()->getPath(), $this->app->path->path('zoocart:')));
?>

<div id="zoocart-container" class="zl-bootstrap">
	<div class="address-edit well">
		<form class="form form-horizontal" id="zoocart-address" action="<?php echo $this->app->link();?>" method="post">
			<div class="control-group type">
				<label class="control-label">
					<?php echo JText::_('PLG_ZOOCART_CONFIG_ADDRESS_TYPE'); ?>
				</label>
				<div class="controls">
					<?php if ($this->resource->id): ?>
					<?php echo JText::_('PLG_ZOOCART_' . strtoupper($this->resource->type)); ?>
					<?php else : ?>
					<select name="type">
						<option value="billing" <?php if ($type== 'billing') { echo 'selected="selected"'; } ?>><?php echo JText::_('PLG_ZOOCART_BILLING'); ?>
						<option value="shipping" <?php if ($type == 'shipping') { echo 'selected="selected"'; } ?>><?php echo JText::_('PLG_ZOOCART_SHIPPING'); ?>
					</select>
					<?php endif; ?>
				</div>
			</div>
			<?php 
			echo $renderer->render('address.'.$type.'-form', array('item' => $this->resource)); 
			?>
			<div class="form-actions">
				<button type="submit" id="zoocart-address-save" class="btn btn-primary"><?php echo JText::_('PLG_ZOOCART_SAVE'); ?></button>
				<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'task' => 'display', 'app_id' => $this->application->id)); ?>" class="btn"><?php echo JText::_('PLG_ZOOCART_CANCEL'); ?></a>
			</div>
			<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
			<input type="hidden" name="task" value="save" />
			<input type="hidden" name="app_id" value="<?php echo $this->application->id; ?>" />
			<input type="hidden" name="id" value="<?php echo $this->resource->id; ?>" />
		</form>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($){

		function getQueryVariable(variable) {
		    var query = window.location.search.substring(1);
		    var vars = query.split('&');
		    for (var i = 0; i < vars.length; i++) {
		        var pair = vars[i].split('=');
		        if (decodeURIComponent(pair[0]) == variable) {
		            return decodeURIComponent(pair[1]);
		        }
		    }
		    return '';
		}

		function setQueryVariable(variable, value) {
		    var query = window.location.search.substring(1);
		    var vars = query.split('&');
		    var vars_new = [];
		    for (var i = 0; i < vars.length; i++) {
		        var pair = vars[i].split('=');
		        if (decodeURIComponent(pair[0]) == variable) {
		            vars_new[i] = pair[0] + '=' + value;
		        } else {
		        	vars_new[i] = pair[0] + '=' + pair[1];
		        }
		    }
		    return window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + vars_new.join('&');
		}

		$('#zoocart-container [name="type"]').change(function(){
			var value = $(':selected', $(this)).val();
			if (window.location.href.indexOf('type=') != -1) {
				window.location.href = setQueryVariable('type', value);
			} else {
				window.location.href = window.location.href + '&type=' + value;
			}
		});

		function zcValidateAddress(type){
			var form = $('#zoocart-address');
			var data = form.serialize();

			$('#zoocart-address-save').addClass('disabled');
	
			return $.ajax({
				url: '<?php echo $this->app->link(array('controller' => $this->controller, 'task' => 'validate', 'format' => 'raw')); ?>',
				data: data, 
				dataType:'json',
				type: 'POST',
				success: function(data) {
					$.each($('[name*="elements"]', form), function(){
						var input = $(this);
						input.closest('.control-group').removeClass('error').removeClass('warning');
						$('.help-block', input.parent()).html('');
					});

					$.each(data.notices, function(k, v){
						var input = $('[name*="elements\\['+k+'\\]"]', form);
						input.closest('.control-group').addClass('warning');
						$('.help-block', input.parent()).html(v);
					});

					if(!data.success){
						$.each(data.errors, function(k, v){
							var input = $('[name*="elements\\['+k+'\\]"]', form);
							input.closest('.control-group').addClass('error');
							$('.help-block', input.parent()).html(v);
						});
					} else {
						$('#zoocart-address-save').removeClass('disabled');
					}

					return data;
				}
			});
		}

		$('#zoocart-address [name*="elements"]').change(function(){
			zcValidateAddress('<?php echo $this->resource->type; ?>');
		});

		$('#zoocart-address-save').click(function(){
			$.when(zcValidateAddress('<?php echo $this->resource->type; ?>')).done(function(data){
				if(data.success){
					$('#zoocart-address').submit();
				}
			});
			return false;
		});
	});
</script>