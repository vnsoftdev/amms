<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$require_address = $this->app->zoocart->getConfig()->get('require_address', 1);
?>
<?php if($require_address):?>
	<?php if (!count($addresses) ) : ?>
		<form class="form form-horizontal" id="zoocart-<?php echo $type; ?>-address-form" name="zoocart-<?php echo $type;?>-address">
		<?php if ($type == 'shipping') : ?>
			<div class="control-group same-as-billing">
				<input type="checkbox" name="same_as_billing" value="1" /> <?php echo JText::_('PLG_ZOOCART_ADDRESS_SAME_AS_BILLING');?>
			</div>
		<?php endif; ?>
		<?php
			$address = $this->app->object->create('Address');
			$address->type = $type;
			echo $this->address_renderer->render('address.' . $address->type . '-form', array('item' => $address));
		?>
		</form>
	<?php else : ?>
			<small class="change-address pull-right"><a href="javascript: void(0);" class="btn btn-mini btn-warning"><?php echo JText::_('PLG_ZOOCART_CHANGE'); ?></a></small>
			<?php foreach($addresses as $address) : ?>
			<?php if ($address->default) : ?>
					<div class="address">
						<?php echo $this->address_renderer->render('address.' . $address->type, array('item' => $address)); ?>
					</div>
					<form class="form form-horizontal" id="zoocart-<?php echo $type; ?>-address-form" name="zoocart-<?php echo $type;?>-address">
						<input type="hidden" name="id" value="<?php echo $address->id; ?>" />
					</form>
				<?php endif;?>
			<?php endforeach; ?>

			<div class="other-addresses">
				<?php foreach($addresses as $address) : ?>
					<div data-address-id="<?php echo $address->id; ?>" class="row">
						<?php echo $this->address_renderer->render('address.' . $address->type, array('item' => $address)); ?>
						<a class="pull-right btn btn-mini btn-info choose-address" href="javascript: void(0);"><?php echo JText::_('PLG_ZOOCART_CHOOSE'); ?></a>
						<a class="pull-right btn btn-mini btn-warning edit-address" href="<?php echo $this->app->link(array('controller' => 'addresses', 'task' => 'edit', 'id' => $address->id, 'app_id' => $this->application->id )); ?>" target="_blank"><?php echo JText::_('PLG_ZOOCART_EDIT'); ?></a>
					</div>
				<?php endforeach; ?>
				<div class="add-address-row">
					<a class="pull-right btn btn-mini add-address" href="<?php echo $this->app->link(array('task' => 'edit', 'controller' => 'addresses', 'type' => $type, 'app_id' => $this->application->id)); ?>" target="_blank"><?php echo JText::_('PLG_ZOOCART_ADDRESS_ADD'); ?></a>
				</div>
			</div>
	<?php endif; ?>
<?php endif; ?>