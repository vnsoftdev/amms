<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<h3 class="title"><?php echo JText::_('PLG_ZOOCART_YOUR_CART'); ?></h3>

<form id="zoocart-cart" class="zoocart-cart form form-horizontal" action="<?php echo $this->app->link(); ?>" method="post" name="zoocart-cart-form" accept-charset="utf-8">

<table class="table table-bordered zoocart-cart">
	<thead>
		<tr>
			<th></th>
			<th class="span8"><?php echo JText::_('PLG_ZOOCART_ITEM_NAME'); ?></th>
			<!--<th><?php echo JText::_('PLG_ZOOCART_DESCRIPTION'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_QUANTITY'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_UNIT_PRICE'); ?></th>-->
			<th style="text-align:right;"><?php echo JText::_('PLG_ZOOCART_TOTAL'); ?></th>
		</tr>
	</thead>
	<tbody>		
		<?php foreach($this->items as $item) :?>
			<?php 
			$path = $this->template->getPath() . '/renderer/item/' . $item->item->getType()->identifier . '/cart.php';
			if (JFile::exists($path)) {
				$path = '.' . $item->item->getType()->identifier;
			} else {
				$path = '';
			} 
			?>
			<?php echo $this->renderer->render('item'.$path.'.cart', array('view' => $this, 'item' => $item->item, 'cartitem' => $item)); ?>	
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr class="payment">
			<td colspan="2">
				<small class="spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_CALCULATING');?></span><span class="spinner pull-right"></span></small>
				<?php echo JText::_('PLG_ZOOCART_PAYMENT_FEE'); ?>
			</td>
			<td id="zoocart-cart-payment"><?php echo $this->app->zoocart->currency->format(null, 0);?></td>
		</tr>
		<tr class="shipping">
			<td colspan="2">
				<small class="spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_CALCULATING');?></span><span class="spinner pull-right"></span></small>
				<?php echo JText::_('PLG_ZOOCART_SHIPPING_FEE'); ?>
			</td>
			<td id="zoocart-cart-shipping"><?php echo $this->app->zoocart->currency->format(null, 0);?></td>
		</tr>
		<tr class="subtotal">
			<td colspan="2">
				<small class="spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_CALCULATING');?></span><span class="spinner pull-right"></span></small>
				<?php echo JText::_('PLG_ZOOCART_SUBTOTAL'); ?>
			</td>
			<td id="zoocart-cart-subtotal"><?php echo $this->app->zoocart->currency->format(null, $this->app->zoocart->cart->getSubtotal());?></td>
		</tr>
		<tr class="taxes">
			<td colspan="2">
				<small class="spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_CALCULATING');?></span><span class="spinner pull-right"></span></small>
				<?php echo JText::_('PLG_ZOOCART_TAXES'); ?>
			</td>
			<td id="zoocart-cart-taxes"><?php echo $this->app->zoocart->currency->format(null, $this->app->zoocart->cart->getTaxes($this->app->user->get()->id, $this->default_address));?></td>
		</tr>
		<tr class="total">
			<td colspan="2">
				<small class="spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_CALCULATING');?></span><span class="spinner pull-right"></span></small>
				<?php echo JText::_('PLG_ZOOCART_TOTAL'); ?>
			</td>			
			<td id="zoocart-cart-total"><?php echo $this->app->zoocart->currency->format(null, $this->app->zoocart->cart->getTotal(null, JFactory::getUser()->id, $this->default_address));?></td>
		</tr>
	</tfoot>
</table>

</form>