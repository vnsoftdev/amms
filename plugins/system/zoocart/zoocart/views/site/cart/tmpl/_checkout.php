<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/site.css');
$this->app->document->addScript('zoocart:assets/js/spin.min.js');
$this->app->document->addScript('zoocart:assets/js/checkout.js');

$enable_shipping = $this->app->zoocart->getConfig()->get('enable_shipping', true);
$require_address = $this->app->zoocart->getConfig()->get('require_address', 1);
?>

<?php if (!$this->app->user->get()->id) :?>
<div class="login-register row-fluid">
	<div class="login span6 well">
		<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
		<h3><?php echo JText::_('PLG_ZOOCART_LOGIN');?></h3>
		<?php echo $this->partial('login'); ?>
	</div>

	<div class="register span6 well">
		<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
		<h3><?php echo JText::_('PLG_ZOOCART_REGISTER');?></h3>
		<?php echo $this->partial('register'); ?>
	</div>
</div>
<?php endif; ?>

<?php if($require_address):?>
<div class="addresses row-fluid">
	<div class="billing-details span6 well">
		<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
		<h3><?php echo JText::_('PLG_ZOOCART_ADDRESS_BILLING');?></h3>
		<?php echo $this->partial('address', array('addresses' => $this->addresses['billing'], 'type' => 'billing')); ?>
	</div>

	<div class="shipping-details span6 well">
		<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
		<h3><?php echo JText::_('PLG_ZOOCART_ADDRESS_SHIPPING');?></h3>
		<?php echo $this->partial('address', array('addresses' => $this->addresses['shipping'], 'type' => 'shipping')); ?>
	</div>
</div>
<?php endif; ?>

<div class="payment-shipping row-fluid">
	<div class="payments span6 well">
		<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
		<h3><?php echo JText::_('PLG_ZOOCART_PAYMENT_METHOD');?></h3>
		<?php echo $this->partial('choosepayment'); ?>
	</div>

	<?php if ($enable_shipping) : ?>
	<div class="shippings span6 well">
		<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
		<h3><?php echo JText::_('PLG_ZOOCART_SHIPPING_METHOD');?></h3>
		<?php echo $this->partial('chooseshipping'); ?>
	</div>
	<?php else: ?>
	<div class="notes span6 well">
		<h3><?php echo JText::_('PLG_ZOOCART_CUSTOMER_NOTES');?></h3>
		<textarea rows="3" id="notes" class="span12" name="notes" placeholder="<?php echo JText::_('PLG_ZOOCART_CUSTOMER_NOTES_PLACEHOLDER');?>"></textarea>
	</div>
	<?php endif; ?>
</div>

<?php if ($enable_shipping) : ?>
<div class="order-notes row-fluid">
	<div class="notes span12 well">
		<h3><?php echo JText::_('PLG_ZOOCART_CUSTOMER_NOTES');?></h3>
		<textarea rows="3" class="span12" name="notes" id="notes" placeholder="<?php echo JText::_('PLG_ZOOCART_CUSTOMER_NOTES_PLACEHOLDER');?>"></textarea>
	</div>
</div>
<?php endif; ?>

<?php if ($this->app->zoocart->getConfig()->get('accept_terms', 1)): ?>
<div class="terms-and-conditions row-fluid">
	<div class="terms span12 well">
		<h3><?php echo JText::_('PLG_ZOOCART_TERMS_AND_CONDITION');?></h3>
		<label class="checkbox">
			<input type="checkbox" name="terms" value="1" id="terms" />
			<?php if ($url = $this->app->zoocart->getConfig()->get('terms_url', '')) : ?>
			<a href="<?php echo JRoute::_($url); ?>" target="_blank">
				<?php echo JText::_('PLG_ZOOCART_AGREE_TERMS'); ?> 
			</a>
			<?php else : ?>
			<?php echo JText::_('PLG_ZOOCART_AGREE_TERMS'); ?> 
			<?php endif; ?>
		</label>
	</div>
</div>
<?php endif; ?>


<div id="zoocart-errors" class="well fade">
	<a class="close" href="#">&times;</a>
	<h3><?php echo JText::_('PLG_ZOOCART_CORRECT_ERRORS'); ?></h3>
	<ul class="errors"></ul>
</div>

<input type="button" data-confirmed-text="<?php echo JText::_('PLG_ZOOCART_ORDER_CONFIRMED');?>" data-loading-text="<?php echo JText::_('PLG_ZOOCART_VALIDATING');?>" class="btn btn-primary button-save-order zoocart-save-order pull-right" name="checkout" value="<?php echo JText::_('PLG_ZOOCART_CHECKOUT');?>" />

<form action="index.php" method="post">
	<input type="hidden" id="zoocart-order-id" name="id" value="" />
	<input type="hidden" name="option" value="com_zoo" />
	<input type="hidden" name="controller" value="orders" />
	<input type="hidden" name="task" value="pay" />
	<input type="hidden" name="app_id" value="<?php echo $this->app->zoo->getApplication()->id; ?>" />
</form>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-container').Checkout({
		'loginUrl': '<?php echo $this->app->link(array('controller' => 'cart', 'task' => 'login', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'logoutUrl': '<?php echo $this->app->link(array('controller' => 'cart', 'task' => 'logout', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'registerUrl': '<?php echo $this->app->link(array('controller' => 'cart', 'task' => 'register', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validateAllUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validateAll', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validateAddressUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validateAddress', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validateTaxesUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validateTaxes', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validateUserUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validateUser', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validateQuantitiesUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validateQuantities', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validatePaymentUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validatePayment', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'validateShippingUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'validateShipping', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'removeItemUrl': '<?php echo $this->app->link(array('controller' => 'cart', 'task' => 'removeItem', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'saveOrderUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'save', 'format' => 'raw', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'taxesAddressType': '<?php echo $this->app->zoocart->getConfig()->get('billing_address_type'); ?>',
		'payUrl': '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'pay', 'app_id' => $this->app->zoo->getApplication()->id ), false);?>',
		'terms' : '#terms',
		'termsText': '<?php echo JText::_("PLG_ZOOCART_TERMS_REQUIRED"); ?>'
	});

	$('#zoocart-checkout .row-fluid').each(function(){
		var highestCol = 0;
		$('.span6', $(this)).each(function(){
			highestCol = Math.max(highestCol, $(this).outerHeight());
		});
		$('.span6', $(this)).each(function(){
			$(this).css({'min-height': highestCol + 'px'});
		});
	}) 
});
</script>