<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form class="form form-horizontal" id="zoocart-choosepayment-form" name="zoocart-choosepayment">
	<div class="control-group">
		<?php 
		$plugins = $this->app->zoocart->payment->getPaymentPlugins(); 
		foreach($plugins as $plugin) : $params = $this->app->data->create($plugin->params); ?>
			<label class="radio">
				<input type="radio" name="payment_method" value="<?php echo $plugin->name;?>" /> 
				<?php echo JText::_( $params->get('title', ucfirst($plugin->name)) ); ?>
			</label>
		<?php endforeach; ?>
	</div>
</form>