<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$multiple_shipping_plugins = count($this->shipping_rates)>1;
?>

<form class="form form-horizontal" id="zoocart-chooseshipping-form" name="zoocart-chooseshipping">
	<div class="control-group">
		<?php 
		foreach($this->shipping_rates as $plugin => $rates) : ?>
			<?php
			if($multiple_shipping_plugins)
			{
				$plg = $this->app->zoocart->shipping->getPluginByName($plugin);
				$params = $this->app->data->create($plg->params);
				$plugin_name = $params->get('title', ucfirst($plg->name));
			}
			foreach($rates as $rate) : ?>
				<label class="radio">
					<input type="radio" name="shipping_method" data-shipping-plugin="<?php echo $plugin; ?>" value="<?php echo $rate['id'];?>" /> 
					<?php echo JText::_(ucfirst($rate['name']));?><?php echo ($multiple_shipping_plugins?' ('.$plugin_name.')':''); ?> - <?php echo $this->app->zoocart->currency->format(null, $rate['price']); ?>
				</label>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</div>
</form>