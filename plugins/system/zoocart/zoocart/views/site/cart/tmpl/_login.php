<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form class="form form-horizontal" id="zoocart-login-form" name="zoocart-login"> 
	<div class="control-group">
		<label for="username" class="control-label"><?php echo JText::_('PLG_ZOOCART_USERNAME'); ?></label>
		<div class="controls">
			<input type="text" id="zoocart-username" class="span12" name="username" value="" />
			<span class="help-block"></span>
		</div>
	</div>	
	<div class="control-group">
		<label for="password" class="control-label"><?php echo JText::_('PLG_ZOOCART_PASSWORD'); ?></label>
		<div class="controls">
			<input type="password" id="zoocart-password" class="span12" name="password" value="" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="control-group">
		<label for="remember" class="control-label"><?php echo JText::_('PLG_ZOOCART_REMEMBER_ME'); ?></label>
		<div class="controls">
			<input type="checkbox" id="zoocart-remember" class="span12" name="remember" value="1" />
		</div>
	</div>
	<div class="control-group">
		<input type="button" class="btn btn-primary" id="zoocart-login" value="<?php echo JText::_('PLG_ZOOCART_LOGIN'); ?>" />
	</div>
</form>
