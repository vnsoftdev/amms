<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form class="form form-horizontal" id="zoocart-register-form" name="zoocart-register"> 
	<div class="control-group">
		<label for="name" class="control-label"><?php echo JText::_('PLG_ZOOCART_FULL_NAME'); ?></label>
		<div class="controls">
			<input type="text" class="span12" name="name" value="" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="control-group">
		<label for="email" class="control-label"><?php echo JText::_('PLG_ZOOCART_EMAIL'); ?></label>
		<div class="controls">
			<input type="text" class="span12" name="email" value="" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="control-group">
		<label for="username" class="control-label"><?php echo JText::_('PLG_ZOOCART_USERNAME'); ?></label>
		<div class="controls">
			<input type="text" class="span12" name="username" value="" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="control-group">
		<label for="password" class="control-label"><?php echo JText::_('PLG_ZOOCART_PASSWORD'); ?></label>
		<div class="controls">
			<input type="password" class="span12" name="password" value="" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="control-group">
		<label for="password" class="control-label"><?php echo JText::_('PLG_ZOOCART_PASSWORD_CONFIRM'); ?></label>
		<div class="controls">
			<input type="password" class="span12" name="password2" value="" />
			<span class="help-block"></span>
		</div>
	</div>
	<div class="control-group">
		<input type="button" class="btn btn-primary" id="zoocart-register" value="<?php echo JText::_('PLG_ZOOCART_REGISTER'); ?>" />
	</div>
</form>