<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/site.css');
$opened = $this->app->zoocart->getConfig()->get('checkout_opened', 1);
?>

<div id="zoocart-container" class="zl-bootstrap">
	<?php if(count($this->items)):?>
		
		<div class="zoocart-cart">
			<?php echo $this->partial('cart', array('items' => $this->items, 'default_address' => $this->default_address)); ?>
		</div>

		<div class="zoocart-checkout-buttons"<?php if ($opened) : ?> style="display:none;"<?php endif; ?>>
			<a href="<?php echo $this->app->route->frontpage($this->app->zoo->getApplication()->id);?>" class="btn"><?php echo JText::_('PLG_ZOOCART_CANCEL_MORE'); ?></a>
			<a class="btn btn-primary button-checkout float-right"><?php echo JText::_('PLG_ZOOCART_CHECKOUT'); ?></a>
		</div>

		<div id="zoocart-checkout" class="container-fluid<?php if (!$opened) : ?> hidden<?php endif; ?>">

			<div class="zoocart-checkout-buttons-checkout">
				<a class="btn zoocart-cancel-checkout"><?php echo JText::_('PLG_ZOOCART_CANCEL_LESS'); ?></a>
				<a class="btn btn-primary zoocart-save-order float-right"><?php echo JText::_('PLG_ZOOCART_CHECKOUT'); ?></a>
			</div>
			<br />
			<div class="zoocart-checkout-checkout">
				<?php echo $this->partial('checkout', array('items' => $this->items)); ?>
			</div>
		</div>

	<?php else : ?>
		<?php echo JText::_('PLG_ZOOCART_EMPTY_CART'); ?>
	<?php endif;?>
</div>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-checkout .row-fluid').each(function(){
		var highestCol = 0;
		$('.span6', $(this)).each(function(){
			highestCol = Math.max(highestCol, $(this).outerHeight());
		});
		$('.span6', $(this)).each(function(){
			$(this).css({'min-height': highestCol + 'px'});
		});
		//console.log(highestCol);
	});
});
</script>