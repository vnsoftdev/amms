<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<h3><?php echo JText::_('PLG_ZOOCART_ORDER_ITEMS'); ?></h3>

<table class="table table-bordered zoocart-order-items">
	<thead>
		<tr>
			<th><?php echo JText::_('PLG_ZOOCART_ITEM_NAME'); ?></th>
			<th class="span6"><?php echo JText::_('PLG_ZOOCART_DESCRIPTION'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_QUANTITY'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_UNIT_PRICE'); ?></th>
			<th><?php echo JText::_('PLG_ZOOCART_TOTAL'); ?></th>
		</tr>
	</thead>
	<tbody>		
		<?php foreach($items as $item) :?>
			<?php 
			$path = $this->template->getPath() . '/renderer/item/' . $item->getItem()->getType()->identifier . '/order.php';
			if (JFile::exists($path)) {
				$path = '.' . $item->getItem()->getType()->identifier;
			} else {
				$path = '';
			} 
			?>
			<?php echo $this->renderer->render('item'.$path.'.order', array('view' => $this, 'item' => $item->getItem(), 'cartitem' => $item, 'edit' => false)); ?>	
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr class="payment">
			<td colspan="4">
				<?php echo JText::_('PLG_ZOOCART_PAYMENT_FEE'); ?>
			</td>
			<td id="zoocart-cart-payment"><?php echo $this->app->zoocart->currency->format(null, $this->resource->payment);?></td>
		</tr>
		<tr class="subtotal">
			<td colspan="4">
				<?php echo JText::_('PLG_ZOOCART_SUBTOTAL'); ?>
			</td>
			<td id="zoocart-cart-subtotal"><?php echo $this->app->zoocart->currency->format(null, $this->resource->getSubtotal());?></td>
		</tr>
		<tr class="taxes">
			<td colspan="4">
				<?php echo JText::_('PLG_ZOOCART_TAXES'); ?>
			</td>
			<td id="zoocart-cart-taxes"><?php echo $this->app->zoocart->currency->format(null, $this->resource->getTaxTotal());?></td>
		</tr>
		<tr class="total">
			<td colspan="4">
				<?php echo JText::_('PLG_ZOOCART_TOTAL'); ?>
			</td>			
			<td id="zoocart-cart-total"><?php echo $this->app->zoocart->currency->format(null, $this->resource->getTotal());?></td>
		</tr>
	</tfoot>
</table>