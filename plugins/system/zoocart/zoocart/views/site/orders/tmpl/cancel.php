<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
	$link = $this->app->link(array('controller' => 'orders', 'task' => 'view', 'id' => $this->order_id));

	$this->app->document->addStylesheet('zoocart:assets/css/site.css');
?>


<div id="zoocart-container" class="zl-bootstrap">
	<div class="alert"><?php echo JText::_('PLG_ZOOCART_ORDER_CANCEL_MESSAGE'); ?></div>
	<a class="btn btn-primary" href="<?php echo $link; ?>"><?php echo JText::_('PLG_ZOOCART_GO_TO_ORDER'); ?></a>
</div>