<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/site.css');

$address_renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->application->getTemplate()->getPath(), $this->app->path->path('zoocart:')));

$config = $this->app->zoocart->getConfig();

?>

<div id="zoocart-container" class="zl-bootstrap">
	<?php if (count($this->resources)): ?>
	<form id="zoocart-site-default" class="form form-vertical zoocart-orders" action="<?php echo $this->app->link(); ?>" method="post" accept-charset="utf-8">
			<table class="table table-bordered zoocart-table-orders">
				<thead>
					<tr>
						<th>
						</th>
						<th class="id">
							<?php echo JText::_('PLG_ZOOCART_ORDER_ID'); ?>
						</th>
						<th class="billing_address">
							<?php echo JText::_('PLG_ZOOCART_ADDRESS_BILLING'); ?>
						</th>
						<th class="shipping_address">
							<?php echo JText::_('PLG_ZOOCART_ADDRESS_SHIPPING'); ?>
						</th>
						<th class="net">
							<?php echo JText::_('PLG_ZOOCART_NET_TOTAL'); ?>
						</th>
						<th class="total">
							<?php echo JText::_('PLG_ZOOCART_TOTAL'); ?>
						</th>
						<th class="state">
							<?php echo JText::_('PLG_ZOOCART_ORDER_STATE'); ?>
						</th>
						<th class="payment_method">
							<?php echo JText::_('PLG_ZOOCART_PAYMENT_METHOD'); ?>
						</th>
					</tr>
				</thead>
				<tbody>
				<?php
				for ($i=0, $n=count($this->resources); $i < $n; $i++) :

					$row		  = $this->resources[$i];
					switch ($row->getState()->id) {
						case $config->get('payment_received_orderstate'): 
						case $config->get('finished_orderstate'): 
								$suffix = 'success';
								break;
						case $config->get('payment_failed_orderstate'): 
						case $config->get('canceled_orderstate'): 
								$suffix = 'danger'; 
								break;
						case $config->get('payment_pending_orderstate'): 
								$suffix = 'warning'; 
								break;
						default: $suffix = 'default';
								break;
					}
				?>
					<tr>
						<td>
							<input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>" />
						</td>
						<td class="id">
							<a href="<?php echo $this->app->link(array('controller' => $this->controller, 'app_id' => $this->application->id, 'task' => 'view', 'id' => $row->id));  ?>"><?php echo $row->id; ?></a>
						</td>
						<td class="billing_address">
							<?php echo $address_renderer->render('address.billing', array('item' => $row->getBillingAddress())); ?>
						</td>
						<td class="shipping_address">
							<?php echo $address_renderer->render('address.shipping', array('item' => $row->getShippingAddress())); ?>
						</td>
						<td class="net">
							<?php echo $this->app->zoocart->currency->format(null, $row->getSubtotal()); ?>
						</td>
						<td class="total">
							<?php echo $this->app->zoocart->currency->format(null, $row->getTotal()); ?>
						</td>
						<td class="state">
							<span class="btn btn-<?php echo $suffix; ?> disabled" rel="popover" 
								data-content="<?php echo JText::_($row->getState()->description); ?>" 
								data-original-title="<?php echo JText::_($row->getState()->name); ?>">
									<?php echo JText::_($row->getState()->name); ?>
							</span>
						</td>
						<td class="payment_method">
							<?php echo ucfirst($row->payment_method); ?>
						</td>
					</tr>
					<?php endfor; ?>
				</tbody>
			</table>

			<?php if ($pagination = $this->pagination->render($this->pagination_link)) : ?>
				<div class="pagination pagination-centered">
					<?php echo $pagination; ?>
				</div>
			<?php endif; ?>

	<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="changeapp" value="<?php echo $this->application->id; ?>" />
	<?php echo $this->app->html->_('form.token'); ?>

	</form>

	<?php else: ?>

	<div class="alert alert-warning">
		<?php echo JText::_('PLG_ZOOCART_ADDRESS_NO_ADDRESSES_YET'); ?>
	</div>

	<?php endif; ?>
</div>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('[rel="popover"]').popover();
});
</script>
