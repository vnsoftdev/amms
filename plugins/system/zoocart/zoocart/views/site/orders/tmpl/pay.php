<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/site.css');;
$this->address_renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->template->getPath(), $this->app->path->path('zoocart:')));
$this->resource = $this->order;

$require_address = $this->app->zoocart->getConfig()->get('require_address', 1);

$payment_plugin = $this->app->zoocart->payment->getPaymentPlugins($this->order->payment_method);
$payment_params = $this->app->data->create($payment_plugin->params);
$pname = $payment_params->get('title',ucfirst($this->order->payment_method));
$shipping_plugin = $this->app->zoocart->shipping->getShippingPlugins(json_decode($this->order->shipping_method)->plugin);
$shipping_params = $this->app->data->create($shipping_plugin->params);
$sname = $shipping_params->get('title',ucfirst(json_decode($this->order->shipping_method)->name));
?>

<div id="zoocart-container" class="zl-bootstrap">

	<?php echo $this->payment_html; ?>

	<hr />
	<h3><?php echo JText::_('PLG_ZOOCART_ORDER_SUMMARY'); ?></h3>
	<hr />
		
	<div class="zoocart-order-items">
		<?php echo $this->partial('items', array('items' => $this->order->getItems(), 'resource')); ?>
	</div>

	<?php if($require_address):?>
	<div class="addresses row-fluid">
		<div class="billing-details span6 well">
			<h3><?php echo JText::_('PLG_ZOOCART_ADDRESS_BILLING');?></h3>
			<div data-address-id="<?php echo $address->id; ?>">
				<?php echo $this->address_renderer->render('address.billing', array('item' => $this->order->getBillingAddress())); ?>
			</div>
		</div>

		<div class="shipping-details span6 well">
			<h3><?php echo JText::_('PLG_ZOOCART_ADDRESS_SHIPPING');?></h3>
			<div data-address-id="<?php echo $address->id; ?>">
				<?php echo $this->address_renderer->render('address.billing', array('item' => $this->order->getShippingAddress())); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="payment-shipping row-fluid">
		<div class="payments span<?php echo ($this->order->shipping_method && $this->order->shipping_method != 'null')?6:12; ?> well">
			<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
			<h3><?php echo JText::_('PLG_ZOOCART_PAYMENT_METHOD');?></h3>
			<span class="payment-name"><?php echo $pname; ?></span>
		</div>

		<?php if ($this->order->shipping_method && $this->order->shipping_method != 'null') : ?>
		<div class="shippings span6 well">
			<h3><?php echo JText::_('PLG_ZOOCART_SHIPPING_METHOD');?></h3>
			<span class="shipping-name"><?php echo $sname; ?></span>
		</div>
		<?php endif; ?>
	</div>

	<?php echo $this->payment_html; ?>
	
</div>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-container .row-fluid').each(function(){
		var highestCol = 0;
		$('.span6', $(this)).each(function(){
			highestCol = Math.max(highestCol, $(this).outerHeight());
		});
		$('.span6', $(this)).each(function(){
			$(this).css({'min-height': highestCol + 'px'});
		});
	});
});
</script>