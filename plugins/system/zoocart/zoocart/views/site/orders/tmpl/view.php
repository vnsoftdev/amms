<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// add js
$this->app->document->addStylesheet('zoocart:assets/css/site.css');;
$this->address_renderer = $this->app->renderer->create('address')->addPath(array($this->app->path->path('component.site:'), $this->template->getPath(), $this->app->path->path('zoocart:')));

$config = $this->app->zoocart->getConfig();

switch ($this->resource->getState()->id) {
	case $config->get('payment_received_orderstate'): 
	case $config->get('finished_orderstate'): 
			$suffix = 'success';
			break;
	case $config->get('payment_failed_orderstate'): 
	case $config->get('canceled_orderstate'): 
			$suffix = 'danger'; 
			break;
	case $config->get('payment_pending_orderstate'): 
			$suffix = 'warning'; 
			break;
	default: $suffix = 'default';
			break;
}

$yet_to_pay = !$this->app->zoocart->order->isPayed($this->resource);
$require_address = $this->app->zoocart->getConfig()->get('require_address', 1);
?>

<div id="zoocart-container" class="zl-bootstrap">

	<a role="button" href="#order-history" href="#data-target" class="pull-right btn btn-<?php echo $suffix; ?>" rel="popover" 
		data-content="<?php echo JText::_($this->resource->getState()->description); ?>" 
		data-original-title="<?php echo JText::_($this->resource->getState()->name); ?>"
		data-toggle="modal" >
			<?php echo JText::_($this->resource->getState()->name); ?>
	</a>

	<?php if ($yet_to_pay) : ?>
	<a class="pull-right btn btn-success pay-again" href="<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'pay', 'id' => $this->resource->id, 'app_id' => $this->application->id )); ?>">
		<?php echo JText::_('PLG_ZOOCART_ORDER_PAY'); ?>
	</a>
	<?php endif;?>

	<h2><?php echo JText::_('PLG_ZOOCART_ORDER'); ?> <?php echo $this->resource->id; ?></h2>

	<hr />
		
	<div class="zoocart-order-items">

		<?php echo $this->partial('items', array('items' => $this->resource->getItems())); ?>

	</div>

<?php if($require_address):?>
	<div class="addresses row-fluid">
		<div class="billing-details span6 well">
			<h3><?php echo JText::_('PLG_ZOOCART_ADDRESS_BILLING');?></h3>
			<div data-address-id="<?php echo $address->id; ?>">
				<?php echo $this->address_renderer->render('address.billing', array('item' => $this->resource->getBillingAddress())); ?>
			</div>
		</div>

		<div class="shipping-details span6 well">
			<h3><?php echo JText::_('PLG_ZOOCART_ADDRESS_SHIPPING');?></h3>
			<div data-address-id="<?php echo $address->id; ?>">
				<?php echo $this->address_renderer->render('address.billing', array('item' => $this->resource->getShippingAddress())); ?>
			</div>
		</div>
	</div>
<?php endif; ?>

	<div class="payment-shipping row-fluid">
		<div class="payments span6 well">
			<small class="pull-right spinner-validating"><span class="validating"><?php echo JText::_('PLG_ZOOCART_VALIDATING');?></span><span class="spinner pull-right"></span></small>
			<h3><?php echo JText::_('PLG_ZOOCART_PAYMENT_METHOD');?></h3>
			<span class="payment-name"><?php echo $this->resource->payment_method; ?></span>
			<?php if ($yet_to_pay) : ?>
				<small class="change-payment pull-right"><a href="javascript: void(0);" class="btn btn-mini btn-warning"><?php echo JText::_('PLG_ZOOCART_CHANGE'); ?></a></small>
				<div class="payment-list">
					<form class="form form-vertical" id="zoocart-choosepayment-form" name="zoocart-choosepayment">
						<div class="control-group">
							<?php 
							$plugins = $this->app->zoocart->payment->getPaymentPlugins(); 
							foreach($plugins as $plugin) : ?>
								<div class="controls">
									<input type="radio" name="payment_method" value="<?php echo $plugin->name;?>" <?php if ($this->resource->payment_method == $plugin->name) echo 'checked="checked"';?> /> <?php echo JText::_(ucfirst($plugin->name));?>
								</div>
							<?php endforeach; ?>
						</div>
					</form>
				</div>
			<?php endif; ?>
		</div>

		<?php if ($this->resource->shipping_method && $this->resource->shipping_method != 'null') : ?>
		<div class="shippings span6 well">
			<h3><?php echo JText::_('PLG_ZOOCART_SHIPPING_METHOD');?></h3>
		</div>
		<?php endif; ?>
	</div>

	<div class="modal hide" id="order-history" tabindex="-1" role="dialog">
	  	<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h3><?php echo JText::_('PLG_ZOOCART_ORDER_HISTORY'); ?></h3>
	  	</div>
	  	<div class="modal-body">
	  	  <table class="table table-bordered zoocart-order-history">
			<thead>
				<tr>
					<th><?php echo JText::_('PLG_ZOOCART_ORDER_STATE'); ?></th>
					<th><?php echo JText::_('PLG_ZOOCART_DATE'); ?></th>
				</tr>
			</thead>
			<tbody>		
				<?php foreach($this->app->zoocart->table->orderhistories->getByOrder($this->resource->id) as $history) :?>
				<tr>
					<td><?php echo JText::_($history->getState()->name); ?></td>
					<td><?php echo JFactory::getDate($history->date)->format('d/m/Y H:i:s'); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	  	</div>
	  	<div class="modal-footer">
	    	<a href="javascript: void(0);" class="btn btn-primary" data-dismiss="modal"><?php echo JText::_('PLG_ZOOCART_CLOSE'); ?></a>
	  	</div>
	</div>
	
</div>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-container .row-fluid').each(function(){
		var highestCol = 0;
		$('.span6', $(this)).each(function(){
			highestCol = Math.max(highestCol, $(this).outerHeight());
		});
		$('.span6', $(this)).each(function(){
			$(this).css({'min-height': highestCol + 'px'});
		});
	});

	$('[rel="popover"]').popover({placement: 'left'});

	// Show payment methods
	$('.change-payment').click(function(){
		$(this).parent().find('.payment-list').toggle('slow');
	});

	$('[name="payment_method"]').change(function(){
		$.ajax({
			url: '<?php echo $this->app->link(array('controller' => 'orders', 'task' => 'changePaymentMethod', 'format' => 'raw', 'app_id' => $this->application->id)); ?>',
			data: {
				payment_method: $('[name="payment_method"]:checked').val(),
				order_id: <?php echo $this->resource->id; ?>
			},
			dataType: 'json',
			success: function(data){
				window.location.reload(true);
			}
		});
	})
});
</script>