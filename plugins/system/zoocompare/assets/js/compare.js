/* ===================================================
 * ZOOcompare
 * https://zoolanders.com/extensions/zoocompare
 * ===================================================
 * Copyright (C) JOOlanders SL 
 * http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * ========================================================== */
(function ($) {
	var Plugin = function(){};
	Plugin.prototype = $.extend(Plugin.prototype, {
		name: 'ZOOcompare',
		options: {
			app_id: null,
			url: '',
			total_items: 0,
			app_params: {},
			txtAddToCompare: 'Add To Compare',
			txtCompare: 'Compare',
			txtRemove: 'Remove'
		},
		initialize: function(body, options) {
			this.options = $.extend({}, this.options, options);
			var $this = this;

			// create the ajax requests cointainer
			$this.options.requests = [];
			$this.options.ajaxloading = false;
			$this.options.itemsToRemove = [];
			$this.options.itemsToAdd = [];

			// addto button action, current and new ones
			$('body').on('click', 'button.zoocompare.addto', function() {
				$this.addToCompare($(this).parent().data('item-data'));
			});

			// remove button action, current and new ones
			$('body').on('click', 'button.zoocompare.remove', function() {
				$this.removeFromCompare($(this).parent().data('item-data'));
			});

			// module compare button action
			$('.zoocompare-module').on('click', 'button.zoocompare.compare', function() {
				// get module type
				var type = $(this).closest('.zoocompare-module').data('zc-type');

				// redirect to compare view
				window.location = $this.getURL('task=compare&app_id='+$this.options.app_id+'&type='+type);
			});

			// compare button action, current and new ones
			$('body').on('click', 'button.zoocompare.compare', function() {
				// get item data
				var $item = $(this).parent().data('item-data');

				// redirect to compare view
				window.location = $this.getURL('task=compare&app_id='+$this.options.app_id+'&type='+$item.type);
			});

			// remove ALL button action
			$('button.zoocompare.removeall').on('click', function() {
				// get module type
				var type = $(this).closest('.zoocompare-module').data('zc-type');
				$this.removeAllFromCompare(type);
			});
		},
		getURL: function(params) {
			var concat = this.options.url.match(/\?/) ? '&' : '?';
			return this.options.url + concat + params;
		},
		addToCompare: function($item) {
			var $this = this,
				module_opts = {render: 0};

			// switch compare/addto
			$this.switchButton($item);

			// disable all addTo buttons if limit reached
			if($this.options.app_params && $this.options.app_params._max_items && ($this.options.total_items + 1) >= $this.options.app_params._max_items){
				$('.addto').attr('disabled', 'disabled');
			};

			// add to ajax queue
			$this.queueAdding($item, module_opts);
		},
		removeFromCompare: function($item){
			var $this = this,
				module = $('.zoocompare-module-'+$item.type);

			// if module present
			if(module.length)
			{
				// remove related row
				$('div.container-fluid .row[data-item-id="'+$item.id+'"]', module).fadeOut(200, function()
				{
					// remove row
					$(this).remove();

					// hide action buttons if now row left
					if(!$('div.container-fluid .row', module).length){
						$('.btns', module).hide();
					}
				})
			}

			// enable all addTo buttons
			$('.addto').removeAttr('disabled');

			// swtich compare/addto
			$this.switchButton($item);

			// add to ajax queue
			$this.queueRemoving($item);
		},
		removeAllFromCompare: function(type){
			var $this = this,
				module = $('.zoocompare-module-'+type);

			// switch all buttons state
			$('button.zoocompare.compare').not('.btns button').each(function(){
				$this.switchButton($(this).parent().data('item-data'));
			})

			// enable all addTo buttons
			$('.addto').removeAttr('disabled');

			// reset the total items count
			$this.options.total_items = 0;

			// if module present
			if(module.length)
			{
				// remove all rows
				$('div.container-fluid .row, .btns', module).fadeOut(200, function(){
					$(this).not('.btns').remove();
				})
			}
			
			// remove all items from session
			jQuery.ajax({
				url : $this.getURL('format=raw'),
				type : 'GET',
				data: {
					type: type,
					task: 'removeAllFromCompare'
				}
			})
		},
		switchButton : function($item) {
			var $this = this,
				button = $('[data-item-id="'+$item.id+'"] button.zoocompare').not('.remove');

			if(button.hasClass('addto')) {
				// change to compare
				button.removeClass('addto btn-primary').addClass('compare btn-success')
				.html($this.options.txtCompare);
				
				// show remove_btn
				button.siblings('.remove').show();
			} else {
				// change to addto
				button.removeClass('compare btn-success').addClass('addto btn-primary')
				.html($this.options.txtAddToCompare);

				// hide remove_btn
				button.siblings('.remove').hide();
			}
		},
		queueAdding : function($item) {
			var $this = this,
				$module_opts = {},
				$module = $('.zoocompare-module-'+$item.type);

			// add new request if any
			$item.id && $this.options.itemsToAdd.push($item.id);

			// if no ajax running launch next request
			if ($this.options.itemsToAdd.length && !$this.options.ajaxloading) {
				$this.options.ajaxloading = true; // lock ajax

				// if module present
				if($module.length){
					$('div.container-fluid', $module).append($('<div class="row new"><span class="zl-loader"></span></div>'));
					$module_opts = {render: 1, item_layout: $module.data('zc-renderer-layout')}
				}

				// save items for ajax and empty for next use
				var $items = $this.options.itemsToAdd;
				$this.options.itemsToAdd = [];
				
				$.ajax({
					url : $this.getURL('format=raw'),
					type : 'POST',
					data: {
						task: 'addToCompare',
						type: $item.type,
						items_id: $items,
						module: $module_opts,
						app_id: $this.options.app_id
					},
					success : function(data) {
						data = $.parseJSON(data);

						// update total items
						$this.options.total_items = data.items.length;

						// if module row returned
						data.module_rows.length && $.each(data.module_rows, function(i, row)
						{
							// remove loading row
							$('div.container-fluid .row.new', $module).remove();

							// set row item data
							$item_data = JSON.stringify(row.itemdata, null, 2).replace(/\s/g, '');

							// render it
							var remove_btn = '<button type="button" class="btn btn-mini zoocompare remove">'+$this.options.txtRemove+'</button>';
							$('div.container-fluid', $module).append(
								$('<div class="row" />').attr('data-item-data', $item_data).hide().html(row.html+remove_btn).removeClass('new').attr('data-item-id', row.itemdata.id).fadeIn(200)
							);
							
							// show module action buttons
							$('.btns', $module).show();
						})

						// unlock ajax
						$this.options.ajaxloading = false; 

						// run queue again
						$this.queueAdding(false);
					}
				});


			} else if ($this.options.itemsToAdd.length){
				var t=setInterval(function(){$this.queueAdding(false)},500);
			}
		},
		queueRemoving : function($item) {
			var $this = this;

			// add new request if any
			$item.id && $this.options.itemsToRemove.push($item.id);

			// if no ajax running launch next request
			if ($this.options.itemsToRemove.length && !$this.options.ajaxloading) {
				$this.options.ajaxloading = true; // lock ajax
				
				// save items for ajax and empty for next use
				var $items = $this.options.itemsToRemove;
				$this.options.itemsToRemove = [];

				$.ajax({
					url : $this.getURL('format=raw'),
					type : 'POST',
					data: {
						task: 'removeFromCompare',
						type: $item.type,
						items_id: $items,
						app_id: $this.options.app_id
					},
					success : function(data) {
						data = $.parseJSON(data);

						// update total items
						$this.options.total_items = data.items.length;
						
						// unlock ajax
						$this.options.ajaxloading = false; 

						// run queue again
						$this.queueRemoving(false);
					}
				});

			} else if ($this.options.itemsToRemove.length){
				var t=setInterval(function(){$this.queueRemoving(false)},500);
			}
		}
	});
	// Don't touch
	$.fn[Plugin.prototype.name] = function() {
		var args   = arguments;
		var method = args[0] ? args[0] : null;
		return this.each(function() {
			var element = $(this);
			if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
				element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
			} else if (!method || $.isPlainObject(method)) {
				var plugin = new Plugin();
				if (Plugin.prototype['initialize']) {
					plugin.initialize.apply(plugin, $.merge([element], args));
				}
				element.data(Plugin.prototype.name, plugin);
			} else {
				$.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
			}
		});
	};
})(jQuery);