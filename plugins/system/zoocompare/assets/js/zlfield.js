/* ===================================================
 * ZOOcompareZLField
 * https://zoolanders.com/extensions/zoocompare
 * ===================================================
 * Copyright (C) JOOlanders SL 
 * http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * ========================================================== */
(function ($) {
    var Plugin = function(){};
    Plugin.prototype = $.extend(Plugin.prototype, {
        name: 'ZOOcompareZLField',
        options: {},
        initialize: function(body, options) {
            this.options = $.extend({}, this.options, options);
            var $this = this;

            $(document).ready(function() {
                // Toolbar position handling
                $('.ui-sortable').on('sortstop', function(event, ui)
                {
                    var $element = ui.item,
                        $element_position = $element.closest('ul.element-list').data('position'),
                        $element_type = $element.find('.zlinfo').data('element-type');
    
                    if ($element_type == 'compare'){
                        
                        // set layout visibility
                        $this.layoutVisibility($element);

                        // launch the Element Title feature
                        $this.setCompareTitle($element);

                    } else if ($element_position == 'toolbar'){
                        $element.remove();
                    } else {
                        $this.setTitle($element);
                    }
                    
                });

                // first time must be triggered manually
                $('.ui-sortable li.element').each(function(){
                    $(this).parent().trigger('sortstop', { item: $(this) });
                });
            })
        },
        setTitle: function($element) {
            var $name = $element.find('.name'),
                $subtitle = $name.find('span').text() ? $name.find('span').text() : $name.text();

            // hide the Show Label parameter
            $element.find('.parameter-form input[name$="[showlabel]"]').parent().parent('li.parameter').hide();
            $element.find('.parameter-form .zlfield input[name$="[showlabel]"]').closest('.row').hide();

            // match the title with alternate label if not empty
            $element.find('.parameter-form input[name$="[altlabel]"]').on('keyup zlinit', function(){
                $(this).val() && $name.html($(this).val() + ' <span>(' + $.trim($subtitle.replace(/(|)/g, '')) + ')</span>');
            }).trigger('zlinit');
        },
        setCompareTitle: function($element) {
            var $select = $element.find('.row-layout .row[data-id=_layout] select'),
                $element_position = $element.closest('ul.element-list').data('position'),
                $name = $element.find('.name');
           
            if ($element_position == 'elements'){
                $select.on('loaded.zlfield', function(){
                    if ($select.val() == 'main_header.php'){
                        $name.html('Main Header <span>(Compare)</span>');

                    } else if ($select.val() == 'header.php'){
                        var $title_input = $element.find('.zlfield.row-layout .placeholder .row[data-id=_title] input');
                        // change the title and listen to new inputs
                        $title_input.on('keyup zlinit', function(){
                            $name.html('** ' + $title_input.val() + ' ** <span>(Compare)</span>');
                        }).trigger('zlinit');
                    }
                }).trigger('loaded.zlfield');
            }
        },
        layoutVisibility: function($element) {
            var $this = this,
                $element_position = $element.closest('ul.element-list').data('position'),
                $name = $element.find('.name');
            
            // reset the Element title value
            $name.html('Compare <span>(Compare)</span>');

            // show/hide the appropiate zlfield
            if ($element_position == 'toolbar'){
                $element.find('.zlfield-main.row-layout').closest('li.parameter').hide();
                $element.find('.zlfield-main.toolbar-layout').closest('li.parameter').show();
                $element.find('.parameter-form > div.field input').attr('value', 'tool');

            } else if ($element_position == 'elements') {
                $element.find('.zlfield-main.toolbar-layout').closest('li.parameter').hide();
                $element.find('.zlfield-main.row-layout').closest('li.parameter').show();
                $element.find('.parameter-form > div.field input').attr('value', 'row');
            }
        }
    });
    /* Copyright (C) YOOtheme GmbH http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only */
    $.fn[Plugin.prototype.name] = function() {
        var args   = arguments;
        var method = args[0] ? args[0] : null;
        return this.each(function() {
            var element = $(this);
            if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
                element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
            } else if (!method || $.isPlainObject(method)) {
                var plugin = new Plugin();
                if (Plugin.prototype['initialize']) {
                    plugin.initialize.apply(plugin, $.merge([element], args));
                }
                element.data(Plugin.prototype.name, plugin);
            } else {
                $.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
            }
        });
    };
})(jQuery);