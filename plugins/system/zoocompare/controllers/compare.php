<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

/*
	Class: CompareController
		The controller class for zoocompare tasks
*/
class CompareController extends AppController {

	protected $items = array();
	protected $compare_params = null;
	protected $compare_id = 0;

	/*
		Function: compare
			Do the actual comparing

		Returns:
			HTML
	*/
	public function compare()
	{
		// init vars
		$db 		= $this->app->database;
		$type 		= $this->app->request->getCmd('type', '');
		$menuId		= JRequest::getInt('Itemid') ? '&Itemid='.JRequest::getInt('Itemid') : '';
		$app 		= JRequest::getInt('app_id');

		// get compare ID
		$id = $this->getCompareId();
		
		$this->setRedirect(JRoute::_('index.php?option=com_zoo&controller=compare&task=docompare&compare_id='.$id.$menuId.'&app_id='.$app, false));
		$this->redirect();
	}

	/**
	 * Method to render the compare layout view
	 *
	 * @return html
	 */
	public function docompare()
	{
		// init vars
		$compare_id 			= JRequest::getInt('compare_id', null);
		$compare_params 		= $this->getCompareParams($compare_id);
		$app 					= $this->app->system->session->get('com_zoo.zoocompare.app_id');
		$type 					= $compare_params->get('type', '');

		// get application object
		$app = $app ? $this->app->table->application->get($app) : $this->app->zoo->getApplication();

		// save params for global use
		$this->compare_params 	= $compare_params;

		// get items from params, not from session
		$this->items = $compare_params->get('items', array());

		// if there are items to process
		if(!empty($this->items))
		{
			// save the items to CHART session
			$this->app->system->session->set('com_zoo.compare.'.$type.'.chart.items', $this->items);

			// get items objects
			foreach ($this->items as &$item) {
				$item = $this->app->table->item->get($item);
			}

			// layout used for the order config
			$elayout = JFile::stripExt('compare.php');

			// set renderer
			$this->type = $app->getType($type);
			$this->renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->app->path->path('zoocompare:')));

			$config = $this->renderer->getConfig('item')->get($app->getGroup().'.'.$this->type->id.'.'.$elayout);
			$this->rows = @$config['elements'];
			$this->tools = @$config['toolbar'];
		}

		$app_params = $app->params->get('global.zoocompare.layout');
		$layout = strlen($app_params['_layout']) ? basename($app_params['_layout'], '.php') : 'default';

		$this->getView()->addTemplatePath($this->app->path->path('zoocompare:layouts'))->setLayout($layout)->display();
	}

	/*
		Function: getCompareId
	*/
	protected function getCompareId()
	{
		// init vars
		$params 	= $this->app->data->create(array());
		$user_id 	= JFactory::getUser()->id;
		$db 		= $this->app->database;
		
		// Use joomla query builder
		$query = $db->getQuery(true);

		// save params data
		$params->set('app_id', $this->app->request->getCmd('app_id'));
		$params->set('type', $this->app->request->getCmd('type'));
		$params->set('items', $this->getItems(@$params['type'], false));

		// encode params
		$params = json_encode($params);
		
		// set hash
		$uuid = md5($params);

		// search for saved compare
		$query->select('compare_id')->from('#__zoo_zl_compares')->where('compare_uuid LIKE '.$db->Quote($uuid));
		$db->setQuery($query);
		
		$this->compare_id = $db->loadResult();

		// generate one if first time comparing this configuration
		if(!$this->compare_id)
		{
			$query = "INSERT INTO #__zoo_zl_compares (compare_uuid, compare_params, user_id, datetime) ";
			$query .= "VALUES (".$db->quote($uuid).", ".$db->quote($params).", ".$user_id.", NOW())";
			$db->query($query);
			
			$this->compare_id = $db->insertid();
		}
		
		return $this->compare_id;
	}

	/*
		Function: getCompareParams
			Returns the search params from ZF database
		
		Parameters:
			$compare_id
	*/
	public function getCompareParams($id)
	{
		// init vars
		$db 	= $this->app->database;
		
		// Use joomla query builder
		$query = $db->getQuery(true);

		$query->select('compare_params')->from('#__zoo_zl_compares')->where('compare_id = '.(int)($id));
		$db->setQuery($query);
		
		jimport( 'joomla.html.parameter' );
		$params = json_decode($db->loadResult(), true); 
		
		if($params){
			$this->decodeParams($params);
		} else {
			$params = array();
		}

		return $this->app->data->create($params);
	}

	protected function decodeParams(&$params)
	{
		foreach($params as &$param)
		{
			if(is_array($param))
			{
				$this->decodeParams($param);
			}
			else
			{
				$param = urldecode($param);
			}	
		}
	}

	/**
	 * Method to add the item to compare session
	 *
	 * @return json
	 */
	public function addToCompare()
	{
		// init vars
		$type 			= $this->app->request->getCmd('type', '');
		$items 			= (array)$this->app->request->getVar('items_id', array());
		$module 		= $this->app->request->getVar('module', array('render' => 0));
		$module_rows 	= array();
		
		// get items stored in session
		$session_items = $this->app->system->session->get('com_zoo.compare.'.$type.'.items', array());

		foreach ($items as $item) 
		{
			// get item
			$item = $this->app->table->item->get($item);

			// get app params
			$app_params = $item->getApplication()->params;
			$max_items  = $app_params->find('global.zoocompare.layout => _max_items', 0, ' => ');
			
			if (count($session_items) >= $max_items && $max_items != 0)
			{
				$error = sprintf(JText::_('PLG_ZOOCOMPARE_CANNOT_COMPARE_MORE'), $max_items);
			} 
			else 
			{
				if (!in_array($item->id, $session_items))
				{
					// add item to the compare list
					$session_items[] = $item->id;

					// if module present, let's render the new item module row
					if ($module['render']) {
						$path = $this->app->path->path('modules:mod_zoocompare');
						$renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $path));
						$renderer->render('item.'.$module['item_layout'], compact('item')); // workaround
						
						$positions = $renderer->getPositions('item.'.$module['item_layout']);
						$positions = array_pop($positions);

						// check if the rendered layout has been populated
						$populated = $renderer->checkPositions('item.'.$module['item_layout'], $item);

						$module_row = '';
						if($populated){
							$module_row = $renderer->render('item.'.$module['item_layout'], compact('item'));
						} else {
							$module_row = $item->name;
						}
						
						$module_rows[] = array('html' => $module_row, 'itemdata' => array('id' => $item->id, 'type' => $item->type));
					}
				}
				$error = '';
			}
		}

		// check limit
		$limit_reached = count($session_items) >= $max_items;
		
		// save the items back to session
		$this->app->system->session->set('com_zoo.compare.'.$type.'.items', $session_items);
		
		echo json_encode(array('items' => array_values($session_items), 'module_rows' => $module_rows, 'error' => utf8_encode($error), 'limit_reached' => $limit_reached));
		return;
	}
	
	/**
	 * Method to remove the item/s from compare session
	 *
	 * @return json
	 */
	public function removeFromCompare()
	{
		$type  = $this->app->request->getCmd('type', '');
		$items = (array)$this->app->request->getVar('items_id', array());
		
		// get items stored in session
		$session_items = $this->app->system->session->get('com_zoo.compare.'.$type.'.items', array());

		// check if the item to remove is present and proceede
		foreach ($items as $item) if(in_array($item, $session_items)){
			foreach($session_items as $k => $v){
				if($v == $item){
					unset($session_items[$k]);
				}
			}
		}
		
		// save the items left back to session
		$this->app->system->session->set('com_zoo.compare.'.$type.'.items', $session_items);
		
		// return the new list of items in session
		echo json_encode(array('items' => array_values($session_items)));
		return;
	}

	/**
	 * Method to remove ALL items from compare session
	 *
	 * @return json
	 */
	public function removeAllFromCompare()
	{
		$type = $this->app->request->getCmd('type', '');
		
		$this->app->system->session->set('com_zoo.compare.'.$type.'.items', array());
		return;
	}

	/**
	 * Exports comparing chart to CSV file
	 *
	 * @return json
	 *
	 * @since 2.0
	 */
	public function toCSV()
	{
		$app 				= $this->app->zoo->getApplication();
		$type 				= $app->getType($this->app->request->getCmd('type', ''));
		$compare_id		 	= $this->app->request->getCmd('compare_id', '');
		$hidden_elements 	= $this->app->request->getVar('hidden_elements', array());

		// get the items being compared
		$items = $this->getItems($type->id);

		// get elements
		$renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->app->path->path('zoocompare:')));
		$config = $renderer->getConfig('item')->get($app->getGroup().'.'.$type->id.'.compare');
		$elements = @$config['elements'];
		
		$i = 1;
		$data = $valid_rows = $maxima = array();
		// process rows
		foreach ($elements as $element)
		{
			$identifier = $element['element'];
			$el_params  = $element;

			// don't process hidden elements row
			if (in_array($identifier, $hidden_elements)){
				continue;
			}

			$element = $type->getElement($identifier);

			$el_params['showlabel'] = false; // avoid rendering label
			$params = $this->app->data->create(array_merge(array('first' => true, 'last' => true), $el_params));

			// set row name
			$name = $params->get('altlabel') ? $params->get('altlabel') : $element->config->get('name');
			$data[$i]['Element'] = utf8_decode($name);

			// header rows
			if ($element->getElementType() == 'compare')
			{
				if ($params->find('layout._layout') != 'header.php'){
					continue;
				}

				$valid_rows[$i] = $i;

				// overide row name
				$name = $params->find('layout._title') ? $params->find('layout._title') : $element->config->get('name');
				$data[$i]['Element'] = utf8_decode("**$name**");
			}

			// process columns
			else foreach ($items as $item) // columns
			{
				if (!$element = $item->getElement($identifier)){
					continue;
				}
				
				$value = trim(strip_tags($renderer->render('element.default', array('element' => $element, 'params' => $params))));
				
				if (!ctype_space($value) && !empty($value) || $value = $params->find('zoocompare._ifnovalue', 0)){
					$data[$i][$item->alias] = utf8_decode($value);
					$valid_rows[$i] = $i;
				} else {
					$data[$i][$item->alias] = ''; // important, as we don't know if other items DO have valid value
				}
			}

			foreach ($data[$i] as $key => $value) {
				if (is_array($value)) {
					$maxima[$key] = max(1, @$maxima[$key], count($value));
				}
			}

			$i++;
		}

		// remove unvalid rows
		foreach ($data as $i => $row) {
			if (!in_array($i, $valid_rows)) {
				unset($data[$i]);
			}
		}

		if (empty($data)) {
			return false;
		}

		// use maxima to pad arrays
		foreach ($maxima as $key => $num) {
			foreach (array_keys($data) as $i) {
				$data[$i][$key] = array_pad($data[$i][$key], $num, '');
			}
		}

		// set header, leavint first column empty
		array_unshift($data, array(0 => ''));
		foreach ($items as $item) {
			$data[0][] = $item->name;
		}

		// set file name
		$file_name = str_replace(' ', '_', JText::sprintf('PLG_ZOOCOMPARE_TOOL_DWNCSV_FILENAME', $compare_id));

		$file = rtrim($this->app->system->application->getCfg('tmp_path'), '\/')."/{$file_name}.csv";
		if (($handle = fopen($file, "w")) !== false) {
			foreach ($data as $row) {
				fputcsv($handle, $this->app->data->create($row)->flattenRecursive(), ';');
			}
			fclose($handle);
		} else {
			throw new AppExporterException(sprintf('Unable to write to file %s.', $file));
		}

		echo json_encode(array('file' => basename($file)));
	}

	/**
	 * Output a file to the browser
	 */
	public function download() {
		$file = $this->app->request->getCmd('file', '');
		$this->app->filesystem->output($this->app->path->path('tmp:'.$file));
	}

	/**
	 * Get current Items Object stored in session
	 *
	 * @return array
	 *
	 * @since 3.0
	 */
	public function getItems($type, $asObjects=true)
	{
		$items = $this->app->system->session->get('com_zoo.compare.'.$type.'.items', array());
		if($asObjects) foreach($items as &$item){
			$item = $this->app->table->item->get($item);
		}

		return $items;
	}

	/**
	 * HORIZONTAL VERSION - TODO
	 * Exports comparing chart to CSV file
	 *
	 * @return json
	 *
	 * @since 2.0
	 */
	// public function toCSV()
	// {
	// 	$app = $this->app->zoo->getApplication();
	// 	$type = $app->getType($this->app->request->getCmd('type', ''));
	// 	$hidden_elements = $this->app->request->getVar('hidden_elements', array());

	// 	// Fetch items to compare of this type
	// 	$items = $this->app->system->session->get('com_zoo.compare.'.$type->id.'.items', array());

	// 	// get elements
	// 	$renderer = $this->app->renderer->create('item')->addPath(array($this->app->path->path('component.site:'), $this->app->path->path('zoocompare:')));
	// 	$config = $renderer->getConfig('item')->get($app->getGroup().'.'.$type->id.'.compare');
	// 	$elements = @$config['elements'];
		
	// 	$i = 1;
	// 	$data = $valid_rows = $maxima = array();
	// 	foreach($items as &$item)
	// 	{
	// 		$item = $this->app->table->item->get($item);

	// 		// item properties ()
	// 		$data[$i]['Item'] = $item->name;

	// 		// elements
	// 		foreach($elements as $element)
	// 		{
	// 			$identifier = $element['element'];

	// 			// don't process hidden elements row
	// 			if(in_array($identifier, $hidden_elements)){
	// 				continue;
	// 			}

	// 			$element['showlabel'] = false; // avoid rendering label
	// 			$params = $this->app->data->create(array_merge(array('first' => true, 'last' => true), $element));

	// 			if((!$element = $item->getElement($identifier)) || $element->getElementType() == 'compare'){
	// 				continue;
	// 			}

	// 			$name = $params->get('altlabel') ? $params->get('altlabel') : $element->config->get('name');
	// 			$value = trim(strip_tags($renderer->render('element.default', array('element' => $element, 'params' => $params))));
				
	// 			if(!ctype_space($value) && !empty($value) || $value = $params->find('zoocompare._ifnovalue', 0)){
	// 				$data[$i][$name] = $value;
	// 				$valid_rows[$name] = $name;
	// 			} else {
	// 				$data[$i][$name] = ''; // important, as we don't know if other items DO have valid value
	// 			}
	// 		}

	// 		foreach($data[$i] as $key => $value) {
	// 			if(is_array($value)) {
	// 				$maxima[$key] = max(1, @$maxima[$key], count($value));
	// 			}
	// 		}

	// 		$i++;
	// 	}

	// 	// remove unvalid rows
	// 	foreach($data as $i => $item) {
	// 		foreach($item as $key => $value) {
	// 			if(!in_array($key, $valid_rows)) {
	// 				unset($data[$i][$key]);
	// 			}
	// 		}
	// 	}

	// 	if (empty($data)) {
	// 		return false;
	// 	}

	// 	// use maxima to pad arrays
	// 	foreach ($maxima as $key => $num) {
	// 		foreach (array_keys($data) as $i) {
	// 			$data[$i][$key] = array_pad($data[$i][$key], $num, '');
	// 		}
	// 	}

	// 	// set header
	// 	array_unshift($data, array());
	// 	foreach ($data[1] as $key => $value) {
	// 		$num = is_array($value) ? count($value) : 1;
	// 		$data[0] = array_merge($data[0], array_fill(0, max(1, $num), $key));
	// 	}

	// 	$file = rtrim($this->app->system->application->getCfg('tmp_path'), '\/')."/ZOOcompare.csv";
	// 	if (($handle = fopen($file, "w")) !== false) {
	// 		foreach ($data as $row) {
	// 			fputcsv($handle, $this->app->data->create($row)->flattenRecursive());
	// 		}
	// 		fclose($handle);
	// 	} else {
	// 		throw new AppExporterException(sprintf('Unable to write to file %s.', $file));
	// 	}

	// 	$file = $this->app->path->url('root:'.$this->app->path->relative($file));

	// 	echo json_encode(array('file' => $file));
	// }

}

/*
	Class: CompareControllerException
*/
class CompareControllerException extends AppException {}