<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: ElementCompare
		The compare element class
*/
class ElementCompare extends ElementPro {
	
	public function __construct( )
	{
		parent::__construct( );
		$this->registerCallback( 'compare' );

		// set defaults
		$this->config->set('name', 'Compare');
	}

	/*
		Function: hasValue
			Checks if the element's value is set.

	   Parameters:
			$params - render parameter

		Returns:
			Boolean - true, on success
	*/
	public function hasValue($params = array()) {
		return true;
	}
	
	/*
		Function: getSearchData
			Get elements search data.
					
		Returns:
			String - Search data
	*/
	public function getSearchData() {
		return null;
	}

	/*
	   Function: edit
	       Renders the edit form field.

	   Returns:
	       String - html
	*/		
	public function edit(){
		return null;
	}

	/*
		Function: render
			Renders the element.

	   Parameters:
            $params - AppData render parameter

		Returns:
			String - html
	*/
	public function render($params = array()) {
		$params = $this->app->data->create($params);

		// rows
		if ($params->get('layout_mode') == 'row'){
			$path = 'zoocompare:layouts/_rows/'.$params->find('layout._layout', 'main_header.php');
			if ($layout = $this->app->path->path($path)){
				return $this->renderLayout($layout, compact('params'));
			}
		}

		// tools
		else if ($params->get('layout_mode') == 'tool'){

			$path = 'zoocompare:layouts/_tools/'.$params->find('toolbar._layout', '');
			if ($layout = $this->app->path->path($path)){
				return $this->renderLayout($layout, compact('params'));
			}
		}
		
		// default
		else if ($layout = $this->getLayout('render/'.$params->find('layout._layout', 'default.php'))) {
			return $this->renderLayout($layout, compact('params'));
		}
	}

	/*
		Function: getLayout
			Get element layout path and use override if exists.

		Returns:
			String - Layout path
	*/
	public function getLayout($layout = null) {

		// init vars
		$type = $this->getElementType();

		// set default
		if ($layout == null) {
			$layout = "{$type}.php";
		}

		// find layout
		return $this->app->path->path("elements:{$type}/tmpl/{$layout}");

	}

}