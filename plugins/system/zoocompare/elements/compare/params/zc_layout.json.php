<?php
/**
* @package		ZL Framework
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// init vars
	$path = json_encode($params->find('layout.path'));

	return
	'{
		"_layout":{
			"type":"layout",
			"label":"Layout",
			"default":"default.php",
			"specific":{
				"label":"'.$params->find('layout.label').'",
				"help":"'.$params->find('layout.help').'",
				"path":'.$path.',
				"regex":'.json_encode('^([^_][_A-Za-z0-9]*)\.php$').'
			},
			"childs":{
				"loadfields": {
					
					"_rowlayout":{
						"type":"layout",
						"label":"'.$params->find('rowlayout.label').'",
						"help":"'.$params->find('rowlayout.help').'",
						"default":"main_header.php",
						"specific":{
							"mode":"'.$params->find('rowlayout.mode', 'files').'",
							"path":"'.trim($path, '"').'\/{_layout}\/rows",
							"regex":'.json_encode($params->find('rowlayout.mode') == 'folders' ? '' : $params->find('rowlayout.regex', '^([^_][_A-Za-z0-9]*)\.php$')).'
						},
						"childs":{
							"loadfields":{
								"layout_options":{
									"type":"wrapper",
									"fields":{
										"subfield":{
											"type":"subfield",
											"path":"'.trim($path, '"').'\/{_layout}\/rows\/{_rowlayout}\/params.php"
										}
									}
								}
							}
						}
					}

				}
			}
		}
	}';
?>