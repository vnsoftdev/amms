<?php
/*
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// load assets
	$this->app->document->addScript('zoocompare:assets/js/compare.min.js');
	$this->app->document->addStylesheet('zoocompare:assets/css/zoocompare.css');

	// init vars
	$item		= $this->getItem();
	$app_id 	= $item->application_id;
	$type_id 	= $this->getType()->identifier;
	$item_id	= $item->id;
	$url		= $this->app->link(array('controller' => 'compare'), false);

	$items  	= $this->app->system->session->get('com_zoo.compare.'.$type_id.'.items', array());
	$active		= in_array($item->id, $items);
	$class		= $active ? 'compare btn-success' : 'addto btn-primary';
	$text		= $active ? 'PLG_ZOOCOMPARE_COMPARE' : 'PLG_ZOOCOMPARE_ADD_TO_COMPARE';
	$display	= $active ? 'block' : 'none';
	
	$app_params = $item->getApplication()->params->get('global.zoocompare.layout');
	$max_items  = isset($app_params['_max_items']) ? $app_params['_max_items'] : '';
	$state   	= !$active && $max_items && count($items) >= $max_items ? ' disabled="disabled"' : '';

	// item data
	$item_data  = array(
		'id' => $item_id,
		'type' => $type_id
	);

	// call once time the script
	if (!defined('ZOOCOMPARE_ELEMENTS_SCRIPT_DECLARATION')) {
		define('ZOOCOMPARE_ELEMENTS_SCRIPT_DECLARATION', true);

		// save the app id in session for comparing later use
		$this->app->system->session->set('com_zoo.zoocompare.app_id', $app_id);

		// init the js functions
		$javascript = 'jQuery(function($) { $("body").ZOOcompare({ app_id: '.$app_id.', url: "'.$url.'", app_params: '.json_encode($app_params).', total_items: '.count($items).', txtAddToCompare: "'.JText::_('PLG_ZOOCOMPARE_ADD_TO_COMPARE').'", txtCompare: "'.JText::_('PLG_ZOOCOMPARE_COMPARE').'", txtRemove: "'.JText::_('PLG_ZOOCOMPARE_REMOVE').'" }) });';
		
		$this->app->document->addScriptDeclaration($javascript);
	}
?>

<span class="zoocompare-element zl-bootstrap" data-item-id="<?php echo $item_id ?>" data-item-data='<?php echo json_encode($item_data) ?>'>
	<button type="button" class="btn btn-mini zoocompare <?php echo $class ?>"<?php echo $state ?>>
		<?php echo JText::_($text) ?>
	</button>
	<button type="button" class="btn btn-mini zoocompare remove" style="display: <?php echo $display ?>;">
		<?php echo JText::_('PLG_ZOOCOMPARE_REMOVE') ?>
	</button>
</span>