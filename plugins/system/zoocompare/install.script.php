<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class plgSystemZoocompareInstallerScript
{
	protected $_error;
	protected $_src;
	protected $_target;
	protected $_ext = 'zoocompare';
	protected $_ext_name = 'ZOOcompare';
	protected $_lng_prefix = 'PLG_ZOOCOMPARE_SYS';

	/**
	 * Called before any type of action
	 *
	 * @param   string  $type  Which action is happening (install|uninstall|discover_install)
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function preflight($type, $parent)
	{
		// init vars
		$db = JFactory::getDBO();
		$type = strtolower($type);
		$this->_src = $parent->getParent()->getPath('source'); // tmp folder
		$this->_target = JPATH_ROOT.'/plugins/system/zoocompare'; // install folder

		// load ZLFW sys language file
		JFactory::getLanguage()->load('plg_system_zlframework.sys', JPATH_ADMINISTRATOR, 'en-GB', true);

		if($type == 'update')
		{
			/*
			 * deny updating from v2.x
			 */
			$zoocompare_manifest = simplexml_load_file(JPATH_ROOT.'/plugins/system/zoocompare/zoocompare.xml');

			if($zoocompare_manifest && version_compare((string)$zoocompare_manifest->version, 3.0, '<=') ) {
				Jerror::raiseWarning(null, JText::_('PLG_ZOOCOMPARE_SYS_V2UPDATE'));
				return false;
			}


			/* 
			 * when updating we don't wont to override renderer/item folder,
			 * so let's delete the temp folder before install only if it already exists
			 */
			JFolder::exists($this->_target.'/zoocompare/renderer/item') && 
			JFolder::delete($this->_src.'/zoocompare/renderer/item');
		}
	}

	/**
	 * Called on installation
	 *
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	function install($parent)
	{
		// init vars
		$db = JFactory::getDBO();

        // enable plugin
        $db->setQuery("UPDATE `#__extensions` SET `enabled` = 1 WHERE `type` = 'plugin' AND `element` = '{$this->_ext}' AND `folder` = 'system'");
        $db->query();
    }

    /**
	 * Called on uninstallation
	 *
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	function uninstall($parent)
	{
		// init vars
		$db = JFactory::getDBO();
		
		// remove App config files
		if(JFolder::exists(JPATH_ROOT.'/media/zoo/applications')){
			foreach(JFolder::Folders(JPATH_ROOT.'/media/zoo/applications') as $app){
				if(JFile::exists(JPATH_ROOT.'/media/zoo/applications/'.$app.'/config/zoocompare.xml'))
					JFile::delete(JPATH_ROOT.'/media/zoo/applications/'.$app.'/config/zoocompare.xml');
			}
		}

        // disable all zoocompare modules
        $db->setQuery("UPDATE `#__extensions` SET `enabled` = 0 WHERE `element` LIKE '%zoocompare%'")->query();

        // drop table
        $db->setQuery('DROP TABLE IF EXISTS `#__zoo_zl_compares`')->query();

        // enqueue Message
        JFactory::getApplication()->enqueueMessage(JText::_($this->langString('_UNINSTALL')));
    }

	/**
	 * Called after install
	 *
	 * @param   string  $type  Which action is happening (install|uninstall|discover_install)
	 * @param   object  $parent  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	public function postflight($type, $parent)
	{
		// load zoo config
		require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

		// init vars
		$type = strtolower($type);
		$release = $parent->get( "manifest" )->version;
        $zoo = App::getInstance('zoo');
        $db = JFactory::getDBO();

        // create table
		$db->setQuery('CREATE TABLE IF NOT EXISTS `#__zoo_zl_compares` ('
			.'`compare_id` int(11) NOT NULL AUTO_INCREMENT,'
			.'`compare_uuid` varchar(32) NOT NULL,'
			.'`compare_params` text NOT NULL,'
			.'`user_id` int(11) NOT NULL,'
			.'`datetime` datetime NOT NULL,'
			.'`hits` int(11) NOT NULL,'
			.'PRIMARY KEY (`compare_id`),'
			.'UNIQUE KEY `compare_uuid` (`compare_uuid`)'
			.') ENGINE=MyISAM DEFAULT CHARSET=utf8;');
		$db->query();

		if($type == 'install'){
			echo JText::sprintf('PLG_ZLFRAMEWORK_SYS_INSTALL', $this->_ext_name, $release);
		}

		if($type == 'update'){
			echo JText::sprintf('PLG_ZLFRAMEWORK_SYS_UPDATE', $this->_ext_name, $release);
		}

        // copy extra app config
		if($path = $zoo->path->path('plugins:system/zoocompare/config/application')){
			$files = $zoo->filesystem->readDirectoryFiles($path);
			$applications = $zoo->application->groups();
			foreach($applications as $application){
				$group = $application->getGroup();
				foreach($files as $file){
					$app_file = $zoo->path->path("applications:$group/config")."/$file";
					$plg_file = $zoo->path->path("plugins:system/zoocompare/config/application/$file");
					if(!JFile::exists($app_file) || (md5(JFile::read($app_file) != md5(JFile::read($plg_file))))) {
						JFile::copy($plg_file, $app_file);
					}
				}
			}
		}
	}

	/**
	 * creates the lang string
	 * @version 1.0
	 *
	 * @return  string
	 */
	protected function langString($string)
	{
		return $this->_lng_prefix.$string;
	}

}