<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	if ($element->canAccess() && $element->hasValue($params)) {

		// if no rendering from item, create empty one
		if(!isset($item)) $item = $this->app->renderer->create('item');

		// trigger elements beforedisplay event
		$render = true;
		$this->app->event->dispatcher->notify($this->app->event->create($item, 'element:beforedisplay', array('render' => &$render, 'element' => $element, 'params' => $params)));

		if ($render) {
			$output = $this->renderer->render("element.default", array('element' => $element, 'params' => $params));

			// trigger elements afterdisplay event
			$this->app->event->dispatcher->notify($this->app->event->create($item, 'element:afterdisplay', array('html' => &$output, 'element' => $element, 'params' => $params)));

			// render result
			echo $output;
		}

	}
?>