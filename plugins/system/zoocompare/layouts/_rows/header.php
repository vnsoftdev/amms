<?php
/*
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$folding = $params->find('layout._folding');
$title   = $params->find('layout._title') ? $params->find('layout._title') : $this->config->get('name');
?>

	<div class="header<?php echo $folding == 2 ? ' folded' : '' ?>">
		<header>

			<div class="content">
				<?php if($folding) : ?>
				<i class="icon-<?php echo $folding == 2 ? 'plus' : 'minus' ?>-sign"></i>
				<?php endif; ?>
				<?php echo $title ?>
			</div>
			
		</header>
	</div>