<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"layout_wrapper":{
			"type": "fieldset",
			"fields": {

				"_title":{
					"type":"text",
					"label":"PLG_ZLFRAMEWORK_TITLE",
					"specific":{
						"placeholder":"Header Title"
					}
				},
				"_folding":{
					"type":"select",
					"label":"PLG_ZOOCOMPARE_FOLD_FEATURE",
					"specific": {
						"options": {
							"PLG_ZLFRAMEWORK_DISABLED":"",
							"PLG_ZOOCOMPARE_START_UNFOLDED":"1",
							"PLG_ZOOCOMPARE_START_FOLDED":"2"
						}
					}
				}

			}
		}

	}}';