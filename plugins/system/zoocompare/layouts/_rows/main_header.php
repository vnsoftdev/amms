<?php
/*
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

	// init vars
	$items  	= $this->app->system->session->get('com_zoo.compare.'.$this->getType()->id.'.chart.items', array());
	$total_items = count($items);
	$total_col   = 12/($total_items);
?>

	<!-- header -->
	<div class="row-fluid level1 main-header hidden-phone">

		<div class="span3">&nbsp;</div>
		<div class="span9">
			<div class="row-fluid level2">
				<?php $i=0; foreach($items as $item) : $item = $this->app->table->item->get($item); ?>
				<div class="span<?php echo $total_col; ?>" data-item-id="<?php echo $item->id ?>">
					<div class="content">
						<?php if($params->find('layout._remove_button')) : ?><i class="icon-remove"></i><?php endif; ?>

						<?php if($params->find('layout._link_to_item')) : ?>
							<a title="<?php echo $item->name; ?>" href="<?php echo $this->app->route->item($item);?> "><?php echo $item->name; ?></a>
						<?php else : ?>
							<?php echo $item->name; ?>
						<?php endif; ?>
					</div>
				</div>
				<?php $i++; endforeach; ?>
			</div>
		</div>
	</div>