<?php
/**
* @package		ZL Elements
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

	return 
	'{"fields": {

		"layout_wrapper":{
			"type": "fieldset",
			"fields": {

				"_link_to_item":{
					"type":"radio",
					"label":"PLG_ZLFRAMEWORK_LINK_TO_ITEM",
					"help":"PLG_ZOOCOMPARE_LINK_TO_ITEM_DESC",
					"default":"1"
				},
				"_remove_button":{
					"type":"radio",
					"label":"PLG_ZOOCOMPARE_REMOVE_BUTTON",
					"help":"PLG_ZOOCOMPARE_REMOVE_BUTTON_DESC",
					"default":"1"
				}

			}
		}

	}}';