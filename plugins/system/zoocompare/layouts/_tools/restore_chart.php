<?php
/*
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
	<button type="button" class="btn btn-mini zoocompare zoocompare-tool-restore-chart">
		<i class="icon-retweet"></i> <?php echo JText::_('PLG_ZOOCOMPARE_TOOL_RESTORE_CHART') ?>
	</button>