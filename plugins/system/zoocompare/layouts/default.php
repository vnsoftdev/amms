<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

if(!count($this->items)){
	echo JText::_('PLG_ZOOCOMPARE_NO_ITEMS_TO_COMPARE');
	return;
}

if(!count($this->rows)){
	echo JText::_('PLG_ZOOCOMPARE_LAYOUT_ISSUES');
	return;
}

// load assets
$this->app->zlfw->loadLibrary('zlux-front');
$this->app->document->addStylesheet('zoocompare:assets/css/zoocompare.css');
$this->app->document->addStylesheet('zoocompare:layouts/default/style.css');
$this->app->document->addScript('zoocompare:layouts/default/script.min.js');
$this->app->document->addScript('zoocompare:assets/js/compare.min.js');
$this->app->document->addScript('zlfw:assets/js/jquery.plugins/jquery.equalheights.custom.js');

// init vars
$items = $this->items;
$compare_id	= $this->app->request->getCmd('compare_id', '');

// call once time the script
if (!defined('ZOOCOMPARE_ELEMENTS_SCRIPT_DECLARATION')) {
	define('ZOOCOMPARE_ELEMENTS_SCRIPT_DECLARATION', true);

	$app 		= $this->type->getApplication();
	$type_id 	= $this->type->identifier;
	$url		= $this->app->link(array('controller' => 'compare'), false);
	$app_params = $app->params->get('global.zoocompare.layout');

	$javascript = 'jQuery(function($) { $("body").ZOOcompare({ app_id: '.$app->id.', type_id: "'.$type_id.'", url: "'.$url.'", app_params: '.json_encode($app_params).', total_items: '.count($items).', txtAddToCompare: "'.JText::_('PLG_ZOOCOMPARE_ADD_TO_COMPARE').'", txtCompare: "'.JText::_('PLG_ZOOCOMPARE_COMPARE').'", txtRemove: "'.JText::_('PLG_ZOOCOMPARE_REMOVE').'" }) });';
	
	$this->app->document->addScriptDeclaration($javascript);
}

// load style js
$javascript = 'jQuery(function($){ $(".zoocompare-default-layout").ZOOcompareDefaultLayout({ total_items: '.count($items).', type_id: "'.$type_id.'", url: "'.$url.'", compare_id: '.$compare_id.' }) });';
$this->app->document->addScriptDeclaration($javascript);

// init vars
$show_labels = true;
$total_items = count($this->items);
$total_col   = round(12/($total_items)); // limited to max 4 items

// reverse as they are floating right
$this->tools = array_reverse((array)$this->tools);

?>

<div class="zoocompare zoocompare-default-layout zl-bootstrap">
	<div class="container-fluid <?php echo $total_items > 4 ? 'vertical' : 'horizontal' ?>-mode">

		<!-- Buffer it, to avoid printing empty rows -->
		<?php foreach ($this->rows as $i => $element) : if($el = $this->type->getElement($element['element'])) : ob_start(); $this->rows[$i] = array(); $render_row = false; ?>

			<?php if($el->getElementType() == 'compare') : ?>

				<?php
					$element['showlabel'] = false; // avoid rendering label here
					$params  = $this->app->data->create(array_merge(array('first' => true, 'last' => true), $element));

					// save row layout
					$this->rows[$i]['layout'] = basename($params->find('layout._layout'), '.php');

					$output = $this->partial('element', array('element' => $el, 'params' => $params));

					// set render row state as true if it has content
					if(!ctype_space($output) && !empty($output)){
						echo $output;
						$render_row = true;
					}
				?>

			<?php else : ?>
			<div class="row-fluid level1 element" data-identifier="<?php echo $el->identifier ?>">

				<!-- save row layout -->
				<?php $this->rows[$i]['layout'] = 'element'; ?>

				<!-- label -->
				<div class="span3">
					<div class="content">
						<i class="icon-minus-sign"></i>
						<?php if(true) echo $element['altlabel'] ? $element['altlabel'] : $el->config->get('name'); ?>
					</div>
				</div>

				<!-- elements -->
				<div class="span9">
					<div class="row-fluid level2">
						<?php foreach($this->items as $item) : ?>
							<div class="span<?php echo $total_col ?>" data-item-id="<?php echo $item->id ?>">
								
								<div class="row-fluid level3">

									<!-- mobiles view -->
									<div class="span6 mobile-header">
										<?php if(true) : ?><i class="icon-remove"></i><?php endif; ?>
										<a title="<?php echo $item->name; ?>" href="<?php echo $this->app->route->item($item);?> "><?php echo $item->name; ?></a>
									</div><!-- mobiles view end -->

									<div class="span6 data">
										<div class="content"><?php
											$element_object = $item->getElement($element['element']);
											$element['showlabel'] = false; // avoid rendering label here
											$params  = $this->app->data->create(array_merge(array('first' => true, 'last' => true), $element));

											$output = $this->partial('element', array('item' => $item, 'element' => $element_object, 'params' => $params));

											// set render row state as true if it has content
											if(!ctype_space($output) && !empty($output) || $output = $params->find('zoocompare._ifnovalue', 0)){
												echo $output;
												$render_row = true;
											}
										?></div>
									</div>
								</div>

							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<!-- end elements -->
			</div>
			<?php endif; ?>
		
			<?php 
				$html = ob_get_contents();
				ob_end_clean();
				if($render_row) $this->rows[$i]['output'] = $html;
			?>
		<?php endif; endforeach; ?> <!-- end body -->

		<?php 
			// remove empty rows
			$this->rows = array_filter($this->rows, create_function('$row', 'return isset($row[\'output\']);'));

			// reindex rows
			$this->rows = array_values($this->rows);
		
			// remove headers without childs
			foreach ($this->rows as $i => $row) {

				// check if next row is its child
				if ($row['layout'] == 'header' && isset($this->rows[$i+1])) {
					
					// remove header if next row is not its child
					if ($this->rows[$i+1]['layout'] != 'element') {
						unset($this->rows[$i]);
					}
				}
			}

			// render rows
			foreach ($this->rows as $row) {
				echo $row['output'];
			}
		?>

		<div class="rows"></div> <!-- important -->
	</div>

	<?php if ($this->tools) : ?>
	<div class="container-fluid tools">
		<?php foreach ($this->tools as $element) {
			$el_params = $element;
			$params  = $this->app->data->create(array_merge(array('first' => true, 'last' => true), $element));

			if ($element = $this->type->getElement($element['element'])){
				echo $this->partial('element', array('element' => $element, 'params' => $params));
			}
		} ?>
	</div>
	<?php endif; ?>
</div>