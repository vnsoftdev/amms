<?php
/**
* @package		ZL Framework
* @author    	JOOlanders, SL http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders, SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

	return
	'{"fields": {

		"_max_items":{
			"type":"text",
			"label":"Max Items",
			"help":"Set the Max Items for this App"
		}

	}}';
?>