/* ===================================================
 * ZOOcompareZLField
 * https://zoolanders.com/extensions/zoocompare
 * ===================================================
 * Copyright (C) JOOlanders SL 
 * http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * ========================================================== */
(function ($) {
    var Plugin = function(){};
    Plugin.prototype = $.extend(Plugin.prototype, {
        name: 'ZOOcompareDefaultLayout',
        options: {
        	total_items: 0,
        	type_id: '',
			compare_id: '',
			url: ''
        },
        initialize: function(layout, options) {
            this.options = $.extend({}, this.options, options);
            this.layout = layout;
            var $this = this;

			// module integration
			$('.btn.zoocompare.remove').on('click', function(){
				var item = $(this).parent().data('item-id');
				$('[data-item-id='+item+']', b).fadeOut('fast', function(){
					$(this).remove();
				});
			})

			// if widht less than
			$.onMediaQuery('(max-width: 768px)', {
				valid: function() {
					$this.switchMode(true);
				}
				// the 'novalid' trigger is not executed at start, that's why using an extra query
			});

			// if width less than
			$.onMediaQuery('(min-width: 768px)', {
				valid: function() {
					$this.switchMode(false);
				}
			});

			// adjust columns heights on window resizing if horizontal mode
			$this.resized();
			$(window).resize(function() {
				$this.resized();
			});

			// ie8 fix
			$this.layout.find('.row-fluid.level1:last-child').addClass('last');

			// init functions
			$this.initMainHeaders();
			$this.initHeaders();
			$this.initRows();
			$this.initTools();
		},
		resized: function(toVertical)
		{
			var $this = this;

			if($(window).width() >= 761-10 && $this.options.total_items <= 4){
				// match rows data heights
				$('.row-fluid.level1, .row-fluid.level2', $this.layout).equalChildHeights();

				// math rows data height with rows label
				$('.row-fluid.level1.element', $this.layout).each(function() {
					var $main = $(this).find('> .span3');;
					$(this).find('> .span9 .row-fluid.level2 > [class*=span]').matchHeightsAs($main);
				});
			} else {
				// reset
				$('.row-fluid, .row-fluid > [class*=span]', $this.layout).css({'min-height': 0});
			}

			
		},
		switchMode: function(toVertical)
		{
			var $this = this;

			// switch to vertical-mode if true or more than 4 items
			if(toVertical || $this.options.total_items > 4){
				$this.layout.find('.container-fluid').removeClass('horizontal-mode').addClass('vertical-mode');
			} else {
				$this.layout.find('.container-fluid').removeClass('vertical-mode').addClass('horizontal-mode');
			}

		},
		initMainHeaders: function()
		{
			var $this = this,
				$remove_buttons = $('.row-fluid.main-header i, .row-fluid .mobile-header i', $this.layout),
				$total_columns = $('.row-fluid.main-header i', $this.layout).length;

			// for both row and mobile main header
			$remove_buttons.on('click', function()
			{
				// proceede only if more than 2 items
				if($total_columns > 2)
				{
					var item = $(this).closest('[data-item-id]').data('item-id');

					// seek all related dom to remove
					$('[data-item-id='+item+']', $this.layout)

					// perform actions when the first one is removed
					.first().fadeOut('fast', function()
					{
						$(this).remove();
						$total_columns = $total_columns-1;

						// reset grid with new total columns
						var span = 'span' + (12 / $total_columns);
						$('.row-fluid.level2 > div', $this.layout).each(function(){
							$(this).attr('class', $(this).attr('class').replace(/span[0-9]/g, span));
						})

						// if only 2 the items left, delete the remove button
						if($total_columns == 2){
							$remove_buttons.remove();
						}
					})

					// remove the rest
					.end().fadeOut('fast');
				} else {
					// delete the the remove button
					$remove_buttons.remove();
				}
			})
		},
		initHeaders: function()
		{
			var $this = this;

			$('.header', $this.layout).each(function()
			{
				var header = $(this).addClass('has-childs'),
					header_trigger = header.find('header'),
					icon = header.find('i'),
					nextHeader = header.nextAll().not('.element').first(),
					childs = header.nextUntil(nextHeader).wrapAll('<div class="childs" />').parent().appendTo(header);

				// hide if start folded is on
				if(header.hasClass('folded')) {
					header.data('folded', 1);
					childs.hide();
				}

				// set button actions
				$.onMediaQuery('(max-width: 768px)', {
					valid: function() {
						header_trigger.on('mouseenter', function(){
							childs.css('opacity', .4 )
						}).on('mouseleave', function(){
							childs.css('opacity', 1 )
						}).on('click', function(){
							$this.toggleHeader(header);
						})

						// remove actions
						icon.off('mouseenter mouseleave click');
					}
				});

				$.onMediaQuery('(min-width: 768px)', {
					valid: function() {
						icon.on('mouseenter', function(){
							childs.css('opacity', .4 )
						}).on('mouseleave', function(){
							childs.css('opacity', 1 )
						}).on('click', function(){
							$this.toggleHeader(header);
						})

						// remove actions
						header_trigger.off('mouseenter mouseleave click');
					}
				});

				// set child actions
				childs.find('.element').on('click', function(){
					if($(this).siblings(':visible').length == 0){
						header.hide();
					}
				})
			})

			// mark the last header
			$('.header', $this.layout).last().addClass('last-header')
		},
		toggleHeader: function(header)
		{
			var icon = header.find('header i'),
				childs = header.find('.childs');

			// toggle icon sign
			icon.toggleClass('icon-minus-sign icon-plus-sign');

			// reset the opacity
			childs.css('opacity', 1);

			// toggle childs
			if(header.data('folded')){
				childs.slideDown('fast', function(){
					header.data('unfolding', 0).data('folded', 0).removeClass('folded');
				}); 
			} else {
				childs.slideUp('fast', function(){
					header.data('folding', 0).data('folded', 1).addClass('folded');
				}); 
			}
		},
		initRows: function(toVertical)
		{
			var $this = this;

			$('.element > .span3 i', $this.layout).each(function()
			{
				var close = $(this),
					element = close.closest('.element'),
					content = element.find('.span3, .span9');

				close.on('mouseenter', function(){
					content.css('opacity', .4 )
				}).on('mouseleave', function(){
					content.css('opacity', 1 )
				}).on('click', function(){
					element.hide();
				})
			});
		},
		initTools: function()
		{
			var $this = this;

			// restore hidden
			$('.btn.zoocompare-tool-restore-hidden', $this.layout).on('click', function(){

				// unhide header/rows
				$('.row-fluid.level1.element, .header', $this.layout).show();

				// unfold headers
				$('.header.folded', $this.layout).each(function(){
					$this.toggleHeader($(this));
				});
			});

			// restore chart
			$('.btn.zoocompare-tool-restore-chart', $this.layout).on('click', function(){
				$(this).addClass('btn-working');
				location.reload();
			});

			// download CSV
			$('.zoocompare-tool-download-csv', $this.layout).on('click', function(e){
				e.preventDefault();

				var hidden_elements = [],
					btn = $(this);

				// loading indication
				btn.addClass('btn-working');

				// find hidden rows
				$('.row-fluid.element:hidden', $this.layout).each(function(){
					hidden_elements.push($(this).data('identifier'))
				});

				jQuery.ajax({
					url : $this.options.url+'&format=raw',
					type : 'POST',
					data: {
						task: 'toCSV',
						type: $this.options.type_id,
						hidden_elements: hidden_elements,
						compare_id: $this.options.compare_id
					},
					success : function(data) {
						data = $.parseJSON(data);

						btn.removeClass('btn-working');

						// redirect to the file
        				window.location.href = $this.options.url+'&format=raw&task=download&file='+data.file;
					}
				})
			})
		}
	});
	/* Copyright (C) YOOtheme GmbH http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only */
    $.fn[Plugin.prototype.name] = function() {
        var args   = arguments;
        var method = args[0] ? args[0] : null;
        return this.each(function() {
            var element = $(this);
            if (Plugin.prototype[method] && element.data(Plugin.prototype.name) && method != 'initialize') {
                element.data(Plugin.prototype.name)[method].apply(element.data(Plugin.prototype.name), Array.prototype.slice.call(args, 1));
            } else if (!method || $.isPlainObject(method)) {
                var plugin = new Plugin();
                if (Plugin.prototype['initialize']) {
                    plugin.initialize.apply(plugin, $.merge([element], args));
                }
                element.data(Plugin.prototype.name, plugin);
            } else {
                $.error('Method ' +  method + ' does not exist on jQuery.' + Plugin.name);
            }
        });
    };
})(jQuery);