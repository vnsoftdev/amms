<?php
/**
* @package		ZOOcompare
* @author    	ZOOlanders http://www.zoolanders.com
* @copyright 	Copyright (C) JOOlanders SL
* @license   	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class plgSystemZoocompare extends JPlugin {

	public $joomla;
	public $app;
	
	/**
	 * onAfterInitialise handler
	 *
	 * Adds ZOO event listeners
	 *
	 * @access	public
	 * @return null
	 */
	public function onAfterInitialise()
	{
		// Get Joomla instances
		$this->joomla = JFactory::getApplication();
		$jlang = JFactory::getLanguage();
		
		// load default and current language
		$jlang->load('plg_system_zoocompare', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('plg_system_zoocompare', JPATH_ADMINISTRATOR, null, true);

		// check dependences
		if (!defined('ZLFW_DEPENDENCIES_CHECK_OK')){
			$this->checkDependencies();
			return; // abort
		}

		// Get the ZOO App instance
		$this->app = App::getInstance('zoo');
		
		// register plugin path
		if ( $path = $this->app->path->path( 'root:plugins/system/zoocompare' ) ) {
			$this->app->path->register($path, 'zoocompare');
		}

		// Register elements path
		if ( $path = $this->app->path->path( 'zoocompare:elements' ) ){
			$this->app->path->register( $path, 'elements' );
		}
		
		// register controllers
		if ( $path = $this->app->path->path( 'zoocompare:controllers' ) ) {
			$this->app->path->register( $path, 'controllers' );
		}
		
		// register events
		$this->app->event->dispatcher->connect('element:configform', array($this, 'configForm'));
		$this->app->event->dispatcher->connect('layout:init', array($this, 'initTypeLayouts'));
		$this->app->event->dispatcher->connect('application:sh404sef', array($this, 'sh404sef'));
		$this->app->event->dispatcher->connect('application:sefbuildroute', array($this, 'sefBuildRoute'));
		$this->app->event->dispatcher->connect('application:sefparseroute', array($this, 'sefParseRoute')); 

		// load frontend assets
		$this->joomla->isSite() && $this->app->zlfw->loadLibrary('bootstrap');

		// load ZOOcompare specific ZLfield integrations
		if($this->joomla->isAdmin() && strstr(JRequest::getVar('path'), 'zoocompare')){
			$this->app->document->addScript('zoocompare:assets/js/zlfield.min.js');
			$this->app->document->addScriptDeclaration( 'jQuery(function($) { $("body").ZOOcompareZLField({}) });' );
		}
		
	}

	/*
		Functions: sefBuildRoute / sefParseRoute
	*/
	public function sefBuildRoute($event)
	{
		$segments = $event['segments'];
		$query 	  = $event['query'];

		$task = 'docompare';
		$controller = 'compare';

		if ((((@$query['task'] == $task) && @$query['controller'] == $controller ))) {
			$segments[] = $controller;
			$segments[] = @$query['compare_id'];
			unset($query['task']);
			unset($query['controller']);
			unset($query['compare_id']);
			unset($query['app_id']);
		}

		$event['segments'] = $segments;
		$event['query']	   = $query;
	}

	public function sefParseRoute($event)
	{	
		$segments = $event['segments'];
		$vars     = $event['vars'];

		$count = count($segments);

		$task = 'docompare';
		$controller = 'compare';

		if ($count == 2 && $segments[0] == $controller) {
			$vars['task']   = $task;
			$vars['controller'] = $controller;
			$vars['compare_id'] = $segments[1];
		}

		$event['segments']	= $segments;
		$event['vars']		= $vars;
	}
	
	/*
		Function: initTypeLayouts
			Callback function for the zoo layouts

		Returns:
			void
	*/
	public function initTypeLayouts($event)
	{
		$extensions = (array) $event->getReturnValue();
		
		// clean all ZOOfilter layout references
		$newextensions = array();
		foreach ($extensions as $ext) {
			if (stripos($ext['name'],'zoocompare') === false) {
				$newextensions[] = $ext;
			}
		}
		
		// add new ones
		$newextensions[] = array('name' => 'ZOOcompare Chart', 'path' => $this->app->path->path('zoocompare:'), 'type' => 'plugin');
		$newextensions[] = array('name' => 'ZOOcompare Module', 'path' => $this->app->path->path('modules:mod_zoocompare'), 'type' => 'module');
		
		$event->setReturnValue($newextensions);
	}
	
	public function getCompareLayout($event)
	{	
		$layout = (JRequest::getVar('compare_layout', false)) ? JRequest::getVar('compare_layout', 'vertical') : $this->params->get('layout', 'vertical');
		$return = (array) $event->getReturnValue();
		$return[] = array('name' => $layout, 'class' => $this->params->get('class', 'tablesorter'));
		
		$event->setReturnValue($return);
	}
	
	/**
	 * Add parameter to the elements config form
	 */
	public function configForm($event, $arguments = array())
	{	
		$element = $event->getSubject();
		$type	 = $element->getType();
		$config  = $event['form'];
		$files	 = array();

		// if type has value, we are in Backend and the module path is correct
		if (!empty($type) && $this->joomla->isAdmin())
		{
			$el_type = $element->getElementType();
			
			if (strstr(JRequest::getVar('path'), 'zoocompare'))
			{
				switch($el_type) {
					case 'compare':
						$files[] = $this->app->path->path('zoocompare:elements/compare/params/compare_layout.xml');
						$this->app->zlxml->loadElementParams($config, $files);
						break;

					default:
						$files[] = $this->app->path->path('zoocompare:elements/compare/params/alternative_content.xml');
						$this->app->zlxml->addElementParams($config, $files);
						break;						
				}
			}
		}
	}

	/*
	 *  sh404sef
	 */
	public function sh404sef($event, $arguments = array())
	{
		$query = $event['query'];
		$title = $event['title'];
		
		// Add tasks and variables here
		if(array_key_exists('controller', $query))
		{
			if(in_array($query['controller'], array('compare')))
			{
				$event['dosef'] = false;
			}
		}
		
		$event['query'] = $query;
		$event['title'] = $title;
	}

	/*
	 *  checkDependences
	 */
	public function checkDependencies()
	{
		if($this->joomla->isAdmin())
		{
			// if ZLFW not enabled
			if(!JPluginHelper::isEnabled('system', 'zlframework') || !JComponentHelper::getComponent('com_zoo', true)->enabled) {
				$this->joomla->enqueueMessage(JText::_('PLG_ZOOCOMPARE_MISSING_DEPENDENCIES'), 'notice');
			} else {
				// load zoo
				require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

				// fix plugins ordering
				$zoo = App::getInstance('zoo');
				$zoo->loader->register('ZlfwHelper', 'root:plugins/system/zlframework/zlframework/helpers/zlfwhelper.php');
				$zoo->zlfw->checkPluginOrder('zoocompare');
			}
		}
	}
}