<?php
/**************************
* @package    ZOO Component
* @subpackage ZOOauthor
* @file       zooauthor.php
* @version    3.0.0 June 2013
* @author     Attavus M.D. http://www.raslab.org
* @copyright  Copyright (C) 2013 R.A.S.Lab[.org]
* @license    http://opensource.org/licenses/GPL-2.0 GNU/GPLv2 only
******************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');

/*
	Class: plgUserZOOauthor
		the user plugin class for the ZOOauthor
*/

class plgUserZOOauthor extends JPlugin {
	
	/** @var object of ZOO App instance */
	public $app;	
	
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		
		// make sure ZOO exist
		jimport('joomla.filesystem.file');
		if (!JFile::exists(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php')
				|| !JComponentHelper::getComponent('com_zoo', true)->enabled) {
			return;
		}
				
		// load ZOO config
		require_once (JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');		
		
		// check if ZOO > 2.4 is loaded
		if (!class_exists('App')) {
			return;
		}
	
		// get the ZOO App instance
		$this->app = App::getInstance('zoo');	
					
		// register helpers path & load helper
		if (!$this->app->zooauthor->getParams()
			&& ($path = $this->app->path->path('root:plugins/system/zooauthor/helpers/'))) {
			$this->app->path->register($path, 'helpers');			
			$this->app->loader->register('ZOOauthorHelper', 'helpers:zooauthor.php');
		}		
	
		//load language.file to make translations work
		$this->app->zooauthor->loadLanguage();
	}

	/**
	 * Remove ZOO User Profile for the user name
	 *
	 * Method is called after user data is deleted from the database
	 *
	 * @param	array		$user	Holds the user data
	 * @param	boolean		$succes	True if user was succesfully stored in the database
	 * @param	string		$msg	Message
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	public function onUserAfterDelete($user, $succes, $msg) {
		
		if (!$succes) {
			return false;
		}
				
		$result = $this->app->zooauthor->delProfile((int) $user['id']);

		//JDEBUG ? $this->app->system->application->enqueueMessage(__LINE__.': onUserAfterDelete : '.$result) : '';

		return !$result ? false : true;
	}

	/**
	 * Create ZOO User Profile for the user after it has been saved.
	 *
	 * @param	array		$user		Holds the new user data.
	 * @param	boolean		$isnew		True if a new user is stored.
	 * @param	boolean		$success	True if user was succesfully stored in the database.
	 * @param	string		$msg		Message.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function onUserAfterSave($user, $isnew, $success, $msg) {

		//JDEBUG ? $this->app->system->application->enqueueMessage(__LINE__.': onUserAfterSave : '.json_encode($user)) : '';
		
		if (!isset($user['id'])) {
			die('invalid userid');
			return; // if the user id appears invalid then bail out just in case
		}
		
		$profile = $this->app->zooauthor->getProfile($user['id']);

		//JDEBUG ? $this->app->system->application->enqueueMessage(__LINE__.': onUserAfterSave : '.json_encode($profile)) : '';
		
		if (($isnew == true && $profile == false) || $profile == false) {
			// check
			if ($this->app->system->application->isSite() && $this->app->zooauthor->getParams()->get('stopforumspam')) {
				$this->app->zooauthor->checkSpammer($user);
			}
			// save item
			$this->app->zooauthor->setProfile($user);
			
			// redirect
			if($this->app->system->application->isSite()) {
				$this->app->system->application->redirect($this->app->route->item($this->app->zooauthor->getProfile($user['id'])),
														JText::_('PLG_ZOOAUTHOR_FILL_PROFILE'));
			}
		} else {			
			// set name
			$profile->name  = isset($user['fullname']) ? $user['fullname'] : (isset($user['name']) ? $user['name'] : $user['username']);
			// set username
			$profile->alias = $this->app->alias->item->getUniqueAlias($profile->id, $this->app->string->sluggify($user['username']));
			// set block
			$profile->state = isset($user['block']) ? ($user['block'] == 1 ? 0 : 1) : 1;
			// save item
			$this->app->table->item->save($profile);			
		}
	}

	/**
	 * This method should handle any login logic and report back to the subject
	 *
	 * @param	array	$user		Holds the user data
	 * @param	array	$options	Array holding options (remember, autoregister, group)
	 *
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	public function onUserLogin($user, $options = array()) {

		//JDEBUG ? $this->app->system->application->enqueueMessage(__LINE__.': onUserLogin Before: '.json_encode($user)) : '';

		jimport('joomla.user.helper');
		$id = intval(JUserHelper::getUserId($user['username']));
		$user['id'] = (int) $id;
		
		// check
		if ($this->app->zooauthor->getProfile($user['id']) == false) {
			// save item
			$this->app->zooauthor->setProfile($user);
		}
		
		//JDEBUG ? $this->app->system->application->enqueueMessage(__LINE__.': onUserLogin After: '.json_encode($user)) : '';
		
		return true;
	}
}
