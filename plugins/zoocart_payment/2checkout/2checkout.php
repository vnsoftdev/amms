<?php
/**
 * @package        ZOOcart
 * @author        ZOOlanders http://www.zoolanders.com
 * @copyright    Copyright (C) JOOlanders, SL
 * @license        http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class plgZoocart_Payment2checkout extends JPaymentDriver
{

	/**
	 * Render single order item partial for
	 */
	private function renderOrderDetails( $order = null )
	{
		$html = '';

		if ( null !== $order ) {
			$html = $this->renderLayout( 'orderitems', array( 'items' => $items = $order->getItems() ) );
		}

		return $html;
	}

	/**
	 * Get payment fee value for this type payments:
	 *
	 * @param array $data
	 * @return float|int
	 */
	public function getPaymentFee( $data = array() )
	{

		if ( $this->params->get( 'fee_type', 'net' ) == 'perc' ) {
			$perc = ( (float) $this->params->get( 'fee', 0 ) ) / 100;

			if ( $data['order'] ) {
				return $data['order']->net * $perc;
			}
			else {
				return $this->app->cart->getSubtotal( $this->app->user->get()->id ) * $perc;
			}
		}

		return (float) $this->params->get( 'fee', 0 );
	}

	/**
	 * Get data for rendiring in payment request form
	 *
	 * @param array $data
	 * @return array
	 */
	protected function getRenderData( $data = array() )
	{

		$zoo = App::getInstance( 'zoo' );
		$user = JFactory::getUser();
		$data = parent::getRenderData( $data );

		$data['url'] = 'https://www.2checkout.com/checkout/spurchase';
		$data['sid'] = $this->params->get( 'sid', '' );
		$data['x_receipt_link_url'] = $zoo->payment->getCallbackUrl( '2checkout','html' );
		$data['name'] = $user->name;
		$data['email'] = $user->email;

		$data['billing_address'] = $data['order']->getBillingAddress();
		$data['shipping_address'] = $data['order']->getShippingAddress();

		$data['content'] = $this->renderOrderDetails( $data['order'] );

		return $data;
	}

	/**
	 * Payment handle callback
	 *
	 * @param array $data
	 * @return array
	 */
	public function callback( $data = array() )
	{

		$app = App::getInstance( 'zoo' );
		$data = $app->data->create( $data );

		if ( $id = (int) $data->get( 'merchant_order_id', null ) ) {
			$order = $app->table->orders->get( $id );
		}
		else {
			$order = $data->get( 'order', null );
		}

		// Check against frauds
		// If operation is suspected to be fraud or demo mode - returns false
		$valid = $this->isValidIPN( $data );

		if ( $valid ) {
			$order_num = $data->get( 'order_number' );
			$credit_card_processed = $data->get( 'credit_card_processed' );

			// Set order status based on cart processed or not:
			// If  card processed - set status as processed (1),
			// otherwise - just pending (-1)
			$status = $order_num ? ( $credit_card_processed ? 1 : -1 ) : 0;

			$total = $data->get( 'total' );

			return array( 'status' => $status, 'transaction_id' => $data->get( 'order_number' ), 'order_id' => $order->id, 'total' => $total );
		}

		return array( 'status' => 0, 'transaction_id' => '', 'order_id' => $order->id, 'total' => $order->subtotal );
	}

	/**
	 * Validates the incoming data against 2CO's IPN to make sure this is not a
	 * fraudelent request
	 *
	 * @param object $data
	 * @return bool True on Ok
	 */
	private function isValidIPN( $data )
	{
		$incoming_md5 = strtoupper( $data['key'] );
		$calculated_md5 = md5(
			$this->params->get( 'secret', '' ) .
				$this->params->get( 'sid', 0 ) .
				$data['order_number'] .
				$data['total']
		);
		$calculated_md5 = strtoupper( $calculated_md5 );

		return ( $calculated_md5 == $incoming_md5 );
	}
}