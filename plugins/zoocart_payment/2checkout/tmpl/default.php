<?php
/**
 * 2checkout payment plugin for ZOOcart
 *
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form action="<?php echo $url ;?>" method="post" id="zoocart-2co">
		<!-- Required params according to 2checkout API documentation: https://www.2checkout.com/documentation/checkout/parameter-sets/pass-through-products/ -->
		<input type="hidden" name="sid" value="<?php echo $sid; ?>" />
		<input type="hidden" name="mode" value="2CO" />

        <?php if($order->shipping): ?>
        <input type="hidden" name="li_0_type" value="shipping" />
        <input type="hidden" name="li_0_name" value="Shipping" />
        <input type="hidden" name="li_0_quantity" value="1" />
        <input type="hidden" name="li_0_price" value="<?php echo number_format($order->shipping+$order->getShippingTax(),2); ?>" />
        <input type="hidden" name="li_0_tangible" value="Y" />
        <?php endif; ?>

		<?php if($order->tax || $order->getPaymentTax()): ?>
		<input type="hidden" name="li_1_type" value="tax" />
		<input type="hidden" name="li_1_name" value="Tax" />
		<input type="hidden" name="li_1_quantity" value="1" />
		<input type="hidden" name="li_1_price" value="<?php echo number_format($order->tax+$order->getPaymentTax(),2); ?>" />
		<input type="hidden" name="li_1_tangible" value="N" />
		<?php endif; ?>

		<?php echo $content; ?>

        <!-- Additional params according to 2checkout API documentation: https://www.2checkout.com/documentation/checkout/parameter-sets/pass-through-products/ -->
		<?php if($params->get('demo',0)): ?>
			<input type="hidden" name="demo" value="Y" />
		<?php endif;?>
		<?php if($params->get('skip_landing',0)): ?>
			<input type="hidden" name="skip_landing" value="1" />
		<?php endif; ?>
		<input type="hidden" name="fixed" value="Y" />
		<input type="hidden" name="currency_code" value="<?php echo $order->getCurrency(); ?>" />
		<input type="hidden" name="lang" value="<?php echo $params->get('lang','en') ?>" />
		<input type="hidden" name="merchant_order_id" value="<?php echo (int)$order->id; ?>" />
		<input type="hidden" name="pay_method" value="<?php echo strtoupper($params->get('pay_method','cc')) ?>" />
		<input type="hidden" name="total" value="<?php echo number_format($order->getTotal(),2); ?>" />

		<!-- Prepopulate billing information: -->
		<input type="hidden" name="card_holder_name" value="<?php echo $billing_address->name; ?>" />
		<input type="hidden" name="street_address" value="<?php echo $billing_address->address; ?>">
		<input type="hidden" name="street_address2" value="<?php echo $billing_address->address; ?>">
		<input type="hidden" name="city" value="<?php echo $billing_address->city; ?>">
		<input type="hidden" name="state" value="<?php echo $billing_address->state; ?>">
		<input type="hidden" name="zip" value="<?php echo $billing_address->zip; ?>">
		<input type="hidden" name="country" value="<?php echo $billing_address->country; ?>">
		<input type="hidden" name="email" value="<?php echo $email; ?>">
		<input type="hidden" name="phone" value="<?php echo $billing_address->phone; ?>">

		<!-- Prepopulate shipping information: -->
		<input type="hidden" name="ship_name" value="<?php echo $shipping_address->name; ?>" />
		<input type="hidden" name="ship_street_address" value="<?php echo $shipping_address->address; ?>">
		<input type="hidden" name="ship_street_address2" value="<?php echo $shipping_address->address; ?>">
		<input type="hidden" name="ship_city" value="<?php echo $shipping_address->city; ?>">
		<input type="hidden" name="ship_state" value="<?php echo $shipping_address->state; ?>">
		<input type="hidden" name="ship_zip" value="<?php echo $shipping_address->zip; ?>">
		<input type="hidden" name="ship_country" value="<?php echo $shipping_address->country; ?>">

		<input type="hidden" name="x_receipt_link_url" value="<?php echo $x_receipt_link_url; ?>" />

		<input class="btn" type="submit" value="<?php echo JText::_('PLG_ZOOCART_PAYMENT_2CO_CHECKOUT_PROCESS_LABEL');?>"/>
</form>
