<?php
/**
 * 2checkout payment plugin for ZOOcart
 *
 * @package		ZOOcart
 * @author		ZOOlanders http://www.zoolanders.com
 * @copyright	Copyright (C) JOOlanders, SL
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$i = 2;
?>
<!-- Render single rder position for 2checkout: -->
<?php if(!empty($items)):?>
	<?php foreach($items as $item):?>
		<input type="hidden" name="li_<?php echo $i;?>_type" value="product" />
		<input type="hidden" name="li_<?php echo $i;?>_name" value="<?php echo $item->name; ?>" />
		<input type="hidden" name="li_<?php echo $i;?>_quantity" value="<?php echo number_format($item->quantity,2); ?>" />
		<input type="hidden" name="li_<?php echo $i;?>_price" value="<?php echo $item->price; ?>" />
		<input type="hidden" name="li_<?php echo $i;?>_tangible" value="Y" />
	<?php $i++; endforeach; ?>
<?php endif; ?>