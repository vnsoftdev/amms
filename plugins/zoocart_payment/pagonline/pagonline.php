<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class plgZoocart_PaymentPagonline extends JPaymentDriver {

	public function getPaymentFee($data = array()) {
		if ($this->params->get('fee_type' ,'net') == 'perc') {
			$perc = ((float) $this->params->get('fee', 0)) / 100;

			if ($data['order']) {
				return $data['order']->net * $perc;
			} else {
				return $this->app->zoocart->cart->getSubtotal($this->app->user->get()->id) * $perc;
			}
		}

		return (float) $this->params->get('fee', 0);
	}
	
	protected function getRenderData($data = array()) {

		$zoo = App::getInstance('zoo');

		$data = parent::getRenderData($data);
		
		$data['url'] = 'https://pagamenti.unicredito.it/initInsert.do?';
		
		$input['numeroCommerciante'] = $this->params->get('numero_commerciante', '');
		$input['stabilimento'] = $this->params->get('stabilimento', '');
		$input['userID'] = $this->params->get('user_id', '');
		$input['password'] = $this->params->get('password', '');
		$input['numeroOrdine'] = $data['order']->id;
		$input['totaleOrdine'] = floor($data['order']->total * 100);
		$input['valuta'] = $this->getCurrencyCode($zoo->zoocart->currency->getDefaultCurrency()->code);
		$input['flagRiciclaOrdine'] = 'Y';
		$input['flagDeposito'] = $this->params->get('flag_deposito', 'Y');
		$input['tipoRispostaApv'] = $this->params->get('redirect', 'wait');
		$input['urlOk'] = $zoo->zoocart->payment->getReturnUrl();
		$input['urlKo'] = $zoo->zoocart->payment->getCancelUrl();

		$input['mac'] = urlencode($this->getMac($input));

		// Fix urlencode and password masking
		$input['urlOk'] = urlencode($input['urlOk']);
		$input['urlKo'] = urlencode($input['urlKo']);
		$input['password'] = 'Password';

		$vars = array();
		foreach($input as $key => $value) {
			$vars[] = $key . '=' . $value;
		}
		$data['url'] .= implode('&', $vars);


		$data['postback_url'] = $zoo->zoocart->payment->getCallbackUrl('pagonline');
		$data['auto'] = $this->params->get('auto', 1);

		$data = array_merge($data, $input);

		return $data;
	}

	public function callback($data = array()) {

		$app = App::getInstance('zoo');
		$data = $app->data->create($data);

		$id = (int) $data->get('numeroOrdine', null);
		if($id) {
			$order = $app->zoocart->table->orders->get($id);
		} else {
			$order = $data->get('order', null);
		}

		if ((bool) $order->id) {

		    if ($this->isValidMac($data)) {
		    	if(in_array($data->get('statoattuale'), array('IC', 'OK'))) {
		    		$payed = $data->get('importototale');
		    		$payed = $payed / 100;
					$total = $order->total;
					
					// A positive value means "payment". The prices MUST match!
					// Important: NEVER, EVER compare two floating point values for equality.
					if( ($total - $payed) < 0.01 ) {
						return array('status' => 1, 'transaction_id' => $data->get('idautorizzazione'), 'order_id' => $order->id, 'total' => $data->get('importototale') /100);
					}
		    	}
		    }
		}

		return array('status' => 0, 'transaction_id' => $data->get('idautorizzazione'), 'order_id' => $order->id, 'total' => $data->get('importototale') / 100);
	}

	private function isValidMac($data) {

		$secretString = $this->params->get('secret' , '');
		$queryString = urldecode($_SERVER["QUERY_STRING"]);

	    $stringToBeMacVerified = substr($queryString,strpos($queryString, "?", 0)+1, strpos($queryString,"mac=",0) - 1);

	    return true;

		//Calcolo della firma digitale della stringa in input
	    $MAC = md5($stringToBeMacVerified."&".$secretString);
	    $MACtemp = "";
	    for($i=0;$i<strlen($MAC);$i=$i+2) {
		  $MACtemp .= chr(hexdec(substr($MAC,$i,2)));
	    }
	    $MAC = $MACtemp;
	    // Codifica del MAC con lo standard BASE64
	    $MACcode = base64_encode($MAC);
		
		$macGiunto = $data['mac'];

		return $macGiunto .' '.$MACcode;
		
		if ($macGiunto==$MACcode) {
			return true;
		}

		return false;
	}

	private function getMac($input) {

		$input_string = '';
		$i = 0;
		foreach ($input as $k => $v) {
			if ($i != 0) {
				$input_string .= '&';
			}
			$input_string .= $k . '=' . $v;
			$i++;
		} 
		$input_string .= '&' .$this->params->get('secret', '');

		//Calcolo della firma digitale della stringa in input
		$MAC = md5($input_string);
		$MACtemp = "";
		for($i=0;$i<strlen($MAC);$i=$i+2) {
			$MACtemp .= chr(hexdec(substr($MAC,$i,2)));
		}
		$MAC = $MACtemp;

		// Codifica del MAC con lo standard BASE64
		$MACcode = base64_encode($MAC);

		return $MACcode;
	}

	private function getCurrencyCode($code) {
		switch($code) {
			case 'USD':
				return '840';
			case 'EUR':
			default:
				return '978';
		}
	}
}