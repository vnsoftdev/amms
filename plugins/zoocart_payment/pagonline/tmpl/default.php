<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<a id="#zoocart-pagonline" href="<?php echo $url; ?>" class="btn btn-success btn-large">
	<?php echo JText::_('PLG_ZOOCART_PAY_WITH_CREDIT_CARD'); ?>
</a>

<?php if($auto): ?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-pagonline').trigger('click');
})
</script>
<?php endif; ?>