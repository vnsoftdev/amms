<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class plgZoocart_PaymentPaypal extends JPaymentDriver {

	public function getPaymentFee($data = array()) {
		if ($this->params->get('fee_type' ,'net') == 'perc') {
			$perc = ((float) $this->params->get('fee', 0)) / 100;

			if ($data['order']) {
				return $data['order']->net * $perc;
			} else {
				return $this->app->zoocart->cart->getSubtotal($this->app->user->get()->id) * $perc;
			}
		}

		return (float) $this->params->get('fee', 0);
	}
	
	protected function getRenderData($data = array()) {

		$data = parent::getRenderData($data);
		$data['test'] = $this->params->get('test', 0);
		
		if ($data['test']) {
			$data['url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
			$data['account'] = $this->params->get('test_account', '');
		} else {
			$data['url'] = 'https://www.paypal.com/cgi-bin/webscr';
			$data['account'] = $this->params->get('account', '');
		}

		$zoo = App::getInstance('zoo');
		$data['currency'] = $zoo->zoocart->currency->getDefaultCurrency()->code;
		$data['custom'] = $data['order']->id;
		$data['item_number'] = $data['order']->id;
		$data['item_name'] = JText::_('PLG_ZOOCART_ORDER') . ' ' . $data['order']->id;
		$data['amount'] = $data['order']->subtotal;
		$data['tax']	= $data['order']->tax_total;
		$data['return_url'] = $zoo->zoocart->payment->getReturnUrl();
		$data['cancel_url'] = $zoo->zoocart->payment->getCancelUrl();
		$data['postback_url'] = $zoo->zoocart->payment->getCallbackUrl('paypal');
		$data['auto'] = $this->params->get('auto', 1);

		return $data;
	}

	public function callback($data = array()) {

		$app = App::getInstance('zoo');
		$data = $app->data->create($data);

		$id = (int) $data->get('custom', null);
		if($id) {
			$order = $app->zoocart->table->orders->get($id);
		} else {
			$order = $data->get('order', null);
		}

		// Check against frauds
		$valid = $this->isValidIPN($data);
		
		if ($valid) {
			$valid = ($data->get('txn_type') == 'web_accept');
			
			if($valid) {
				$valid = (bool) $order->id;
			}
			
			// Check that total is correct
			if ($valid) {
				$mc_gross = floatval($data->get('mc_gross', 0));
				
				$total = $order->total;
				// A positive value means "payment". The prices MUST match!
				// Important: NEVER, EVER compare two floating point values for equality.
				$isValid = ($total - $mc_gross) < 0.01;
			}
			
			if (!$valid) {
				$status = 0;
			} else {
				// Check the payment_status
				switch($data->get('payment_status'))
				{
					case 'Canceled_Reversal':
					case 'Completed':
						$status = 1;
						break;
					
					case 'Created':
					case 'Pending':
					case 'Processed':
						$status = 1;
						break;
					
					case 'Denied':
					case 'Expired':
					case 'Failed':
					case 'Refunded':
					case 'Reversed':
					case 'Voided':
					default:
						$status = 0;
						break;
				}
			}

			return array('status' => $status, 'transaction_id' => $data->get('txn_id'), 'order_id' => $order->id, 'total' => $mc_gross);
		}

		return array('status' => 0, 'transaction_id' => $data->get('txn_id'), 'order_id' => $order->id, 'total' => $mc_gross);
	}

	/**
	 * Validates the incoming data against PayPal's IPN to make sure this is not a
	 * fraudelent request.
	 *
	 * Code credits goes to Nicholas from Akeebabackup.com
	 */
	private function isValidIPN($data)
	{	
		if ($this->params->get('test', 0)) {
			$url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		} else {
			$url = 'https://www.paypal.com/cgi-bin/webscr';
		}
		
		$exclude_vars = array('option', 'Itemid', 'controller', 'task', 'app_id', 'format', 'payment_method');

		$req = 'cmd=_notify-validate';
		foreach($data as $key => $value) {
			if(!in_array($key, $exclude_vars)) {
				$value = urlencode($value);
				$req .= "&$key=$value";
			}
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.sandbox.paypal.com/cgi-bin/webscr');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Host: www.sandbox.paypal.com'));
		$res = curl_exec($ch);
		curl_close($ch);

		if (!$res) {
			// HTTP ERROR
			return false;
		} else {
			if (strcmp ($res, "VERIFIED") == 0) {
				return true;
			} else if (strcmp ($res, "INVALID") == 0) {
				return false;
			}
		}

		return false;
	}
}