<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form action="<?php echo $url ;?>" method="post" id="zoocart-paypal">
	<input type="hidden" name="cmd" value="_xclick" />
	<input type="hidden" name="business" value="<?php echo $account; ?>" />
	<input type="hidden" name="return" value="<?php echo $return_url; ?>" />
	<input type="hidden" name="cancel_return" value="<?php echo $cancel_url; ?>" />
	<input type="hidden" name="notify_url" value="<?php echo $postback_url; ?>" />
	<input type="hidden" name="custom" value="<?php echo $custom; ?>" />

	<input type="hidden" name="currency_code" value="<?php echo $currency; ?>" />

	<input type="hidden" name="amount" value="<?php echo $amount; ?>" />
	<input type="hidden" name="tax" value="<?php echo $tax; ?>" />

	<input type="hidden" name="item_number" value="<?php echo $item_number; ?>" />
	<input type="hidden" name="item_name" value="<?php echo $item_name; ?>" />

	<button type="submit" border="0" class="btn btn-success btn-large">
		<?php echo JText::_('PLG_ZOOCART_PAYMENT_PAYPAL_PAY'); ?>
	</button>
</form>

<?php if($auto): ?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#zoocart-paypal button[type="submit"]').trigger('click');
})
</script>
<?php endif; ?>