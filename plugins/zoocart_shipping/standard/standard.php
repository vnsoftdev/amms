<?php
/**
* @package		ZOOcart
* @author		ZOOlanders http://www.zoolanders.com
* @copyright	Copyright (C) JOOlanders, SL
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Class plgShippingStandard
 *
 * Standard ZOOcart shipping plugin
 */
class plgZoocart_ShippingStandard extends JShippingDriver {

	/**
	 * Get shipping rates
	 *
	 * @param array $data
	 * @return array
	 */
	protected function getRates($data = array()) {

		// Get Zoo Application Instance:
		$this->app = App::getInstance('zoo');

		$table = $this->app->table->shippingrates;

		$rates = $table->all();

		// Transform objects to arrays:
		$rows = array();
		if(!empty($rates))
			foreach($rates as $key=>$value)
				$rows[$key] = get_object_vars($value);

		return $rows;
	}
	
}